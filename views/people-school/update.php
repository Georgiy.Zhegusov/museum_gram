<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PeopleSchool */

$this->title = 'Обновить школу: ' . $model->esr;
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->esr, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="people-school-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
