<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PeopleSchool */

$this->title = 'Добавление школы';
$this->params['breadcrumbs'][] = ['label' => 'Школы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-school-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
