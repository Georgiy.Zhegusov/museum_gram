<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContestType */
/* @var $generationTypes array */

$this->title = 'Создать категорию конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Категории конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'generationTypes' => $generationTypes
    ]) ?>

</div>
