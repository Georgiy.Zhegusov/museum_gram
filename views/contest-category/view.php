<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ContestCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['imagePath'] = $model->nestedSubstrateBack;
?>

<div class="contest-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что вы хотите удалить категорию конкурсов?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'stage',
            [
                'attribute' => 'generation_type_id',
                'value' => $model->generationType->name
            ],
            'oneForAll',
        ],
    ]) ?>

</div>
