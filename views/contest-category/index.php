<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории конкурсов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать категорию конкурса', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->name, ['/contest-category/view', 'id' => $model->id]);
                },
            ],
//            [
//                'attribute' => 'substrate',
//                'format' => 'html',
//                'value' => function($data)
//                {
//                    if(!empty($data->nestedSubstrateThumb) && file_exists(Yii::getAlias("@app/{$data->nestedSubstrateThumb}"))){
//                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->nestedSubstrateThumb);
//                    }
//
//                    return null;
//                }
//            ],
            'stage',
            [
                'attribute' => 'generation_type_id',
                'value' => function($model) {
                    return $model->generationType->name;
                }
            ],
            'oneForAll',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
