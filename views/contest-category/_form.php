<?php

use app\models\People;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestCategory */
/* @var $form yii\widgets\ActiveForm */
/* @var $generationTypes array */

$this->params['imagePath'] = $model->nestedSubstrateBack;
?>

<div class="contest-type-form">

    <?php $form = ActiveForm::begin([
		'options' => ['enctype' => 'multipart/form-data'],
		'method' => 'post']); ?>

    <?= $form->field($model, 'substrateFile')->fileInput() ?>

    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'deleteSubstrate')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'generation_type_id')->dropDownList($generationTypes) ?>

    <?= $form->field($model, 'oneForAll')->checkbox([]) ?>

    <?= $form->field($model, 'stage')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
