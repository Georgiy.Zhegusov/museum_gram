<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestType */
/* @var $generationTypes array */

$this->title = 'Обновление категории конкурсов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="contest-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'generationTypes' => $generationTypes
    ]) ?>

</div>
