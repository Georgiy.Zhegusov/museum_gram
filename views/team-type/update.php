<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeamType */
/* @var $baseForTeamList array*/


$this->title = 'Обновить тип команд: ' . $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Типы команд', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->text, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="team-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'baseForTeamList' => $baseForTeamList
    ]) ?>

</div>
