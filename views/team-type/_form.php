<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamType */
/* @var $form yii\widgets\ActiveForm */
/* @var $baseForTeamList array*/
?>

<div class="team-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'baseForTeam')->dropDownList($baseForTeamList,['prompt' => 'Без автоматического присвоения'])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
