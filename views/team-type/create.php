<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamType */
/* @var $baseForTeamList array*/

$this->title = 'Создать тип команды';
$this->params['breadcrumbs'][] = ['label' => 'Типы команд', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'baseForTeamList' => $baseForTeamList
    ]) ?>

</div>
