<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GenerationType */

$this->title = 'Обновление типа генерации: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы генерации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="generation-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
