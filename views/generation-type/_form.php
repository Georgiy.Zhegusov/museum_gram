<?php

use app\models\GenerationType;
use app\models\People;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GenerationType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="generation-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'textsOnly')->checkbox() ?>

    <?= $form->field($model, 'unique')->checkbox() ?>

    <?= $form->field($model, 'teamGram')->dropDownList(GenerationType::getTeamGrams()) ?>

    <?= $form->field($model, 'generateForArray')->checkboxButtonGroup(People::getRoles()) ?>

    <?= $form->field($model, 'baseFor')->dropDownList(GenerationType::getBaseForTypes(), ['prompt'=>'Без базы']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
