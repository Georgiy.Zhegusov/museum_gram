<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GenerationType */

$this->title = 'Добавление типа генерации';
$this->params['breadcrumbs'][] = ['label' => 'Типы генерации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generation-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
