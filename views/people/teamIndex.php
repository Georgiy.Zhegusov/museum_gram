<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="people-index">

    <h1><?= Html::encode('Участники') ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'roleName',
            'name',
            'surname',
            'patronomic',
            'schoolName',
            'schoolText',
            'classNum',
            'classText',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'action-column'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-pencil"></i>',
                            ['/people/update', 'id' => $model->id]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-eye-open"></i>',
                            ['/people/view', 'id' => $model->id]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>',
                            ['/people/delete', 'id' => $model->id],
                            [
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ]
                            ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
