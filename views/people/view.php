<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\People */

$this->title = 'Информация об участнике ' . $model->teamInfo->stage . ' этапа.';
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->teamInfo->team->number, 'url' => ['/team/view', 'id' => $model->teamInfo->team_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'roleName',
            'name',
            'surname',
            'patronomic',
            'schoolName',
            'schoolText',
            'classNum',
            'classText',
        ],
    ]) ?>

    <?php
    $dataProvider = new ActiveDataProvider([
        'query' => $model->getHonorsPeoples(),
    ]);?>

    <h1>Награды</h1>

    <?= $this->render('/honor/peopleIndex',['dataProvider'=>$dataProvider])?>

</div>
