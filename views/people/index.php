<?php

use app\models\People;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\search\TeamSearch */

$this->title = 'Участники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'roleName',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'data' => People::getRoles(),
                    'attribute' => 'role',
                    'options' => [
                        'placeholder' => 'Выбор роли',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ])
            ],
            'name',
            'surname',
            'patronomic',
            [
                'attribute' => 'schoolName',
                'filter' =>  Select2::widget([
                        'data' => ((!empty($searchModel->school_id) && !empty($searchModel->school))
                            ? [$searchModel->school_id => $searchModel->school->esr . ': ' . $searchModel->school->text]
                            : null),
                        'model' => $searchModel,
                        'attribute' => 'school_id',
                        'options' => [
                            'placeholder' => 'Поиск школы...',
                            'id'=>'isSchoolFind'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['/people-school/list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ])
            ],
            'teamNumber',
            [
                'attribute' => 'teamNumber',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->teamNumber, ['/team/view', 'id' => $model->teamInfo->team_id]);
                }
            ],
            'stage',
            'schoolText',
            'classNum',
            'classText',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
