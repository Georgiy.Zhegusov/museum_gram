<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\People */

$this->title = 'Добавить человека на ' . $model->teamInfo->stage . ' этап.';
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->teamInfo->team->number, 'url' => ['/team/view', 'id' => $model->teamInfo->team_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
