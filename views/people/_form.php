<?php

use app\models\People;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\People */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="people-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'role')->dropDownList(People::getRoles()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronomic')->textInput(['maxlength' => true]) ?>

    <label class="control-label">Выбор школы</label>

    <?=Select2::widget([
        'data' => ((!empty($model->school_id) && !empty($model->school))
            ? [$model->school_id => $model->school->esr . ': ' . $model->school->text]
            : null),
        'model' => $model,
        'attribute' => 'school_id',
        'options' => [
            'placeholder' => 'Поиск школы...',
            'id'=>'isSchoolFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/people-school/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);?>

    <br>

    <?= $form->field($model, 'schoolText')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'classNum')->textInput() ?>

    <?= $form->field($model, 'classText')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
