<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $uploadEsr string */
/* @var $downloadEsr string */

$this->title = 'Выгрузки/Загрузки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Загрузка',
        'content'=> $uploadEsr,
    ];

    $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Выгрузка',
        'content'=> $downloadEsr,
    ];?>

    <?= TabsX::widget([
        'items'=> $tabs,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);?>
</div>
