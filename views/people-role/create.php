<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PeopleRole */

$this->title = 'Create People Role';
$this->params['breadcrumbs'][] = ['label' => 'People Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
