<?php

use app\models\Contest;
use app\models\ContestCategory;
use app\models\ContestType;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\HonorGenerator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="honor-generator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contests')->widget(Select2::className(),[
        'data' => ArrayHelper::getColumn(
            Contest::find()
                ->andWhere(['id' => $model->contests])
                ->indexBy('id')
                ->asArray()
                ->all(),'name'),
        'model' => $model,
        'attribute' => 'contests',
        'options' => [
            'placeholder' => 'Поиск конкурса...',
            'id'=>'isContestFind',
            'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'contestCategories')->widget(Select2::className(),[
        'data' => ArrayHelper::getColumn(
            ContestCategory::find()
                ->andWhere(['id' => $model->contestCategories])
                ->indexBy('id')
                ->asArray()
                ->all(),'name'),
        'model' => $model,
        'attribute' => 'contestCategories',
        'options' => [
            'placeholder' => 'Поиск категории конкурса...',
            'id'=>'isContesCategorytFind',
            'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest-category/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'contestTypes')->widget(Select2::className(),[
        'data' => ArrayHelper::getColumn(
            ContestType::find()
                ->andWhere(['id' => $model->contestTypes])
                ->indexBy('id')
                ->asArray()
                ->all(),'name'),
        'model' => $model,
        'attribute' => 'contestTypes',
        'options' => [
            'placeholder' => 'Поиск типа конкурса...',
            'id'=>'isContesTypeFind',
            'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest-type/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'teams')->textarea(['maxlength' => true, 'value' => implode(',', $model->teams)]) ?>

    <?= $form->field($model, 'stagesArray')->checkboxButtonGroup([1 => 1, 2 => 2]) ?>

    <?= $form->field($model, 'with_gram')->checkbox() ?>

    <?= $form->field($model, 'wiht_info')->checkbox() ?>

    <?= $form->field($model, 'by_team')->checkbox() ?>

    <?= $form->field($model, 'by_school')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
