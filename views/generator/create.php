<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HonorGenerator */

$this->title = 'Создать выгрузку грамот';
$this->params['breadcrumbs'][] = ['label' => 'Генерация грамот', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="honor-generator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
