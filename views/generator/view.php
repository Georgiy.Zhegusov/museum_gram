<?php

use app\models\Contest;
use app\models\ContestCategory;
use app\models\ContestType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HonorGenerator */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Выгрузка грамот', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$fileText = '';
foreach ($model->generatedFiles as $index => $generatedFile) {
    $fileText .= empty($fileText) ? '' : ', ';
    $fileText .= Html::a("Скачать&nbsp;$index", Url::to(['download', 'fileId' => $generatedFile->id]));
}
?>
<div class="honor-generator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'contestTypes',
                'value' => implode('; ', ArrayHelper::getColumn(ContestType::findAll(['id' => $model->contestTypes]), 'name'))
            ],
            [
                'attribute' => 'contestCategories',
                'value' => implode('; ', ArrayHelper::getColumn(ContestCategory::findAll(['id' => $model->contestCategories]), 'name'))
            ],
            [
                'attribute' => 'contests',
                'value' => implode('; ', ArrayHelper::getColumn(Contest::findAll(['id' => $model->contests]), 'name'))
            ],
            [
                'attribute' => 'teams',
                'value' => implode('; ', $model->teams)
            ],
            [
                'attribute' => 'stagesArray',
                'value' => is_null($model->stagesArray) ? null : implode('; ', $model->stagesArray)
            ],
            'with_gram',
            'wiht_info',
            'status',
            'by_team',
            'by_school',
            [
                'format' => 'raw',
                'attribute' => 'files',
                'value' => $fileText
            ],
        ],
    ]) ?>

</div>
