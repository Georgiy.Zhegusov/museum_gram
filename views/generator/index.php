<?php

use app\models\Contest;
use app\models\ContestCategory;
use app\models\ContestType;
use app\models\HonorGenerator;
use app\models\Team;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Создать выгрузку грамот';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="honor-generator-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать выгрузку грамот', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'format' => 'raw',
                'label' => 'Прогресс',
                'value' => function(HonorGenerator $model)
                {
                    switch ($model->status){
                        case HonorGenerator::STATUS_CREATED:
                            return 'В очереди на генерацию';
                            break;
                        case HonorGenerator::STATUS_START:
                            return $this->render(
                                'progress',
                                [
                                    'model'     => $model,
                                    'updateUrl' => Url::to([
                                        '/generator/file-status',
                                        'id' => $model->id
                                    ])
                                ]
                            );
                            break;
                        case HonorGenerator::STATUS_FINISHED:
                            return 'Генерация завершена';
                            break;
                        default:
                            return 'Неизвестный статус';
                    }
                }
            ],
            [
                'attribute' => 'contestTypes',
                'value' => function($model){
                    $contestTypes = ContestType::findAll(['id' => $model->contestTypes]);
                    $contestTypes = ArrayHelper::getColumn($contestTypes, 'name');
                    return implode('; ', $contestTypes);
                }
            ],
            [
                'attribute' => 'contestCategories',
                'value' => function($model){
                    $contestCategories = ContestCategory::findAll(['id' => $model->contestCategories]);
                    $contestCategories = ArrayHelper::getColumn($contestCategories, 'name');
                    return implode('; ', $contestCategories);
                }
            ],
            [
                'attribute' => 'contests',
                'value' => function($model){
                    $contests = Contest::findAll(['id' => $model->contests]);
                    $contests = ArrayHelper::getColumn($contests, 'name');
                    return implode('; ', $contests);
                }
            ],
            [
                'attribute' => 'teams',
                'value' => function($model){
                    return implode('; ', $model->teams);
                }
            ],
            [
                'attribute' => 'stagesArray',
                'value' => function($model){
                    return is_null($model->stagesArray) ? null : implode('; ', $model->stagesArray);
                }
            ],
            'with_gram',
            'wiht_info',
            'by_team',
            'by_school',
            'status',
            [
                'attribute' => 'files',
                'format' => 'raw',
                'value' => function($model){
                    $result = '';
                    foreach ($model->generatedFiles as $index => $generatedFile) {
                        $result .= empty($result) ? '' : ', ';
                        $result .= Html::a('Страницы&nbsp;' . $generatedFile->pageStart . '-' . $generatedFile->pageFinish, Url::to(['download', 'fileId' => $generatedFile->id]));
                    }

                    return $result;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
