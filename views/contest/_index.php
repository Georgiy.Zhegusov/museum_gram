<?php

use app\models\ContestType;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\ContestSearch */
/* @var $searchModel yii\data\ActiveDataProvider */

$this->title = 'Конкурсы';
?>
<div class="contest-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить конкурс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->name, ['/contest/view', 'id' => $model->id]);
                },
            ],
            'codeName',
            'number',
            [
                'attribute' => 'contest_type_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->contestType->name, ['/contest-type/view', 'id' => $model->contest_type_id]);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'data' => ContestType::getContestTypes(),
                    'attribute' => 'contest_type_id',
                    'options' => [
                        'placeholder' => 'Выбор типа',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ])
            ],
            [
                'attribute' => 'category',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->contestType->contestCategory->name, ['/contest-category/view', 'id' => $model->contestType->contest_category_id]);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'data' => \app\models\ContestCategory::getContestCategories(),
                    'attribute' => 'category',
                    'options' => [
                        'placeholder' => 'Выбор категории',
                        'allowClear' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ])
            ],
//            [
//                'attribute' => 'substrate',
//                'format' => 'html',
//                'value' => function($data)
//                {
//                    if(!empty($data->nestedSubstrateThumb) && file_exists(Yii::getAlias("@app/{$data->nestedSubstrateThumb}"))){
//                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->nestedSubstrateThumb);
//                    }
//
//                    return null;
//                },
//            ],
            'stage',
            [
                'attribute' => 'generation_type_id',
                'value' => function($model){
                    return $model->generationType->name;
                }
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',
            'base_for_stage',
            'substrateTypeText',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
