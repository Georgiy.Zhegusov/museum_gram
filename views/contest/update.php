<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $contestTypes array */
/* @var $generationTypes array */
/* @var $substrateTypes array */
/* @var $model app\models\Contest */

$this->title = 'Обновление конкурса: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['/contest/index']];
$this->params['breadcrumbs'][] = [
    'label' => 'Категория: ' . $model->contestType->contestCategory->name,
    'url' => ['/contest-category/view', 'id' => $model->contestType->contest_category_id]];
$this->params['breadcrumbs'][] = [
    'label' => 'Тип: ' . $model->contestType->name ,
    'url' => ['/contest-type/view', 'id' => $model->contest_type_id]];
$this->params['breadcrumbs'][] = [
    'label' => 'Конкурс: ' . $model->name,
    'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="contest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contestTypes' => $contestTypes,
        'generationTypes' => $generationTypes,
        'substrateTypes' => $substrateTypes
    ]) ?>

</div>
