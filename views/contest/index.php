<?php

use app\models\ContestType;
use kartik\select2\Select2;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\ContestSearch */
/* @var $searchModel yii\data\ActiveDataProvider */
/* @var $uploadForm */
/* @var $downloadForm */

$this->title = 'Конкурсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-index">


    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Конкурсы',
        'content'=> $this->render('_index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel
            ]),
        'active'=>true,
    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Импорт',
        'content'=> $uploadForm,
    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Экспорт',
        'content'=> $downloadForm,
    ];?>

    <?= TabsX::widget([
        'items'=> $tabs,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);?>

</div>
