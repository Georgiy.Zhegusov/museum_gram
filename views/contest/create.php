<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contest */
/* @var $contestTypes array */
/* @var $generationTypes array */
/* @var $substrateTypes array */

$this->title = 'Добавление конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contestTypes' => $contestTypes,
        'generationTypes' => $generationTypes,
        'substrateTypes' => $substrateTypes
    ]) ?>

</div>
