<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contest */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['/contest/index']];
$this->params['breadcrumbs'][] = [
    'label' => 'Категория: ' . $model->contestType->contestCategory->name,
    'url' => ['/contest-category/view', 'id' => $model->contestType->contest_category_id]];
$this->params['breadcrumbs'][] = [
    'label' => 'Тип: ' . $model->contestType->name ,
    'url' => ['/contest-type/view', 'id' => $model->contest_type_id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['imagePath'] = $model->nestedSubstrateBack;
?>
<div class="contest-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что вы хотите удалить конкурс?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <p>
        <?= Html::a('Выдать награду', ['honor/create','contestId' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'codeName',
            'number',
            [
                'attribute' => 'contest_type_id',
                'value' => $model->contestType->name,
            ],
            [
                'attribute' => 'generation_type_id',
                'value' => $model->generationType->name
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',
            'base_for_stage',
            'substrateTypeText',
        ],
    ]) ?>

</div>
