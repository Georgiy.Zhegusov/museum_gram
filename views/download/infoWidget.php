<?php

use app\models\UploadFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $type*/
/* @var $dataProvider*/
/* @var $model app\models\DownloadFile */
$this->title = 'Выгрузка';
?>
<div class="download-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('index', [
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
