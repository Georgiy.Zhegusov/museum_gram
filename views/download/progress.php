<?php
/**
 * Copyright (c) 2016. Zhegusov George
 */

use common\components\modules\group\models\strategys\exploders\BySpaceExploder;
use common\components\modules\group\models\strategys\exploders\NothingExploder;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\select2\Select2Asset;
use yii\bootstrap\Progress;
use yii\helpers\Html;
use yii\jui\ProgressBar;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \app\models\DownloadFile*/
/* @var $text */
/* @var $updateUrl */
$id = 'progressBarDownload' . $model->id;
?>

<?= Progress::widget([
    'percent' => $model->percent,
    'label' => $model->percent . '%',
    'id' => $id,
    'barOptions' => ['class' => 'progress-bar-success'],
    'options' => [
        'class' => 'active progress-striped',
    ]
]);?>

<?php
$js = <<<JS
	(function(){
		var id = {$id};
		var isFirst = true;
		var object = $('#{$id} .progress-bar');
			// начать повторы с интервалом 2 сек
		var timerId = setInterval(function() {
		   $.post(
		       '{$updateUrl}'
		    )
		    .done(function(result) {
				if(result.status == 'Success'){
					object.html(result.text).css('width',result.percent+'%');
					if(result.percent==100){
					    window.location.href = window.location.href;
					    clearInterval(timerId);
					}
					if(isFirst){
				        isFirst = false;
					}
				}
				else{
					$("#message").html(result.message);
				}
		      }).fail(function() {
		        console.dir('server error')
		      });
		}, 1000);
	})();
JS;

$this->registerJs($js);
?>
