<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DownloadFile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="download-file-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'action' => Url::to()],
        'method' => 'post']); ?>

    <?= $form->field($model, 'isTemplate')->checkbox() ?>

    <?= $form->field($model, 'type')->label(false)->hiddenInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
