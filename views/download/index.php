<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Последние выгрузки';
?>
<div class="download-file-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'format' => 'raw',
                'attribute' => 'path',
                'value' => function($model)
                {
                    if($model->isFinished()){
                        return $model->isError() ? 'Ошибка.' : Html::a('Скачать', Url::to(['/download/download', 'id' => $model->id]));
                    }
                    else{
                        return 'Генерируется...';
                    }
                }
            ],
            [
                'format' => 'raw',
                'label' => 'Прогресс',
                'value' => function($model)
                {
                    if($model->isFinished()) {
                        return $model->isError() ? 'Ошибка генерации' : 'Генерация закончилась';
                    }
                    else{
                        return $this->render(
                            'progress',
                            [
                                'model'     => $model,
                                'updateUrl' => Url::to([
                                    '/download/file-status',
                                    'id' => $model->id
                                ])
                            ]
                        );
                    }
                }
            ],
            'isTemplate'
        ],
    ]); ?>
</div>

<?php
$js = <<<JS
    $('.loadCollapse').click(function () {
        if(!$(this).hasClass("alreadyLoaded")){
            
            $(this).addClass("alreadyLoaded");
            
            var depId = this.attributes['data-target']['value'];

            $.getJSON(this.attributes['data-url']['value'], function(result) {
                $(depId).html(result.data);
            });
        }
    });
JS;

$this->registerJs($js);
?>