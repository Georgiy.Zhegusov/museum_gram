<?php

use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCoefficient */
/* @var $form yii\widgets\ActiveForm */
/* @var $ageCategories array */
?>

<div class="museum-category-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'museum_id')->hiddenInput()->label(false) ?>

    <?= DatePicker::widget([
            'model' => $model,
            'attribute' => 'dateStart',
            'attribute2' => 'dateFinish',
            'options' => ['placeholder' => 'Начальная дата'],
            'options2' => ['placeholder' => 'Конечная дата'],
            'type' => DatePicker::TYPE_RANGE,
            'form' => $form,
            'separator' => 'до',
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ]);
    ?>

    <?= $form->field($model, 'value') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
