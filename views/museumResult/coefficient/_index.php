<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="museum-category-link-index">

    <h1>Коэффициенты</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'dateStart',
            'dateFinish',
            'value',

            [
                'class'      => 'yii\grid\ActionColumn',
                'template'   => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index, $there) {
                    return \yii\helpers\Url::to([
                        '/museumResult/museums/coefficient-' . $action,
                        'coefficient_id' => $model->id
                    ]);
                }
            ]
        ],
    ]); ?>
</div>
