<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $types array */
/* @var $model app\models\museumResult\MuseumCoefficient */

$this->title = 'Обновление коэффициента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->museum->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление коэффициента';
?>
<div class="museum-coefficient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
