<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCoefficient */
/* @var $ageCategories array */

?>
<div class="museum-category-link-create">

    <h3><?=$model->isNewRecord ? 'Добавить' : 'Обновить'?> коэффициент</h3>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
