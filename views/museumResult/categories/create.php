<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $museumTypes array */
/* @var $model app\models\museumResult\MuseumCategory */

$this->title = 'Создать категорию музеев';
$this->params['breadcrumbs'][] = ['label' => 'Музейные категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'museumTypes' => $museumTypes
    ]) ?>

</div>
