<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $museumTypes array */
/* @var $model app\models\museumResult\MuseumCategory */

$this->title = 'Обоновление музейной категории: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Музейные категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="museum-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'museumTypes' => $museumTypes
    ]) ?>

</div>
