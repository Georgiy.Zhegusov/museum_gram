<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="nomination-category-link-index">

    <h1>Категории</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'ageCategoryName',
            'maxValue',

            [
                'class'      => 'yii\grid\ActionColumn',
                'template'   => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index, $there) {
                    return \yii\helpers\Url::to([
                        '/museumResult/nominations/age-category-' . $action,
                        'nomination_id'   => $model->nomination_id,
                        'age_category_id' => $model->age_category_id
                    ]);
                }
            ]
        ],
    ]); ?>
</div>
