<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\NominationAgeCategoryLink */
/* @var $ageCategories array */

?>
<div class="nomination-category-link-create">

    <h3>Добавить возрастную категорию</h3>

    <?= $this->render('_form', [
        'model' => $model,
        'ageCategories' => $ageCategories
    ]) ?>

</div>
