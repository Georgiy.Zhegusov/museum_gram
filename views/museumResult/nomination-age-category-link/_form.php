<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\NominationAgeCategoryLink */
/* @var $form yii\widgets\ActiveForm */
/* @var $ageCategories array */
?>

<div class="nomination-category-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomination_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'age_category_id')->dropDownList($ageCategories) ?>

    <?= $form->field($model, 'winnerValue') ?>
    <?= $form->field($model, 'priseValue') ?>
    <?= $form->field($model, 'teamWinnerValue') ?>
    <?= $form->field($model, 'teamPriseValue') ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
