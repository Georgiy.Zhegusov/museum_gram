<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $nominationAgeCategoryLink \app\models\museumResult\NominationAgeCategoryLink */
/* @var $ageCategories array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="nomination-category-view-index">
    <?= $this->render('_index', [
        'dataProvider' => $dataProvider,
    ]) ?>

    <?= $this->render('_create', [
        'model' => $nominationAgeCategoryLink,
        'ageCategories' => $ageCategories
    ]) ?>

</div>
