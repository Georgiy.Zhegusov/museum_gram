<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\AgeCategory */

$this->title = 'Обновление возрастной категории: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Возрастные категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="age-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
