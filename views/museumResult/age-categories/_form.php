<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\AgeCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="age-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'minClass')->textInput() ?>
    <?= $form->field($model, 'maxClass')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
