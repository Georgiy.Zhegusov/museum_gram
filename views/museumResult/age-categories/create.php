<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\AgeCategory */

$this->title = 'Создать возрастную категорию';
$this->params['breadcrumbs'][] = ['label' => 'Возрастные категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="age-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
