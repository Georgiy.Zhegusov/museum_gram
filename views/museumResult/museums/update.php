<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $types array */
/* @var $model app\models\museumResult\Museum */

$this->title = 'Обновление музея: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="museum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'types' => $types
    ]) ?>

</div>
