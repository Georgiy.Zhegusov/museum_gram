<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $uploadForm string */
/* @var $coefUploadForm string */
/* @var $parkUploadForm string */
/* @var $museumCorUploadForm  string */
/* @var $museumCorDownloadForm string */
/* @var $museumInvUploadForm  string */
/* @var $museumInvDownloadForm string */
/* @var $museumPhotoUploadForm string */
/* @var $museumPhotoDownloadForm string */

$this->title                   = 'Музеи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Музем',
        'content' => $this->render('_index', [
            'dataProvider' => $dataProvider
        ]),
        'active'  => true,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт',
        'content' => $uploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт коэффициентов',
        'content' => $coefUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт парков',
        'content' => $parkUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт набивки',
        'content' => $museumCorUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт заочки',
        'content' => $museumInvUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт фото',
        'content' => $museumPhotoUploadForm,
    ]; ?>

    <?= TabsX::widget([
        'items'        => $tabs,
        'position'     => TabsX::POS_ABOVE,
        'encodeLabels' => false
    ]); ?>
</div>
