<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $types array */
/* @var $model app\models\museumResult\Museum */

$this->title = 'Создание музея';
$this->params['breadcrumbs'][] = ['label' => 'Музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'types' => $types
    ]) ?>

</div>
