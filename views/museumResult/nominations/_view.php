<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Nomination */

$this->title = $model->name;
?>
<div class="nomination-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'number',
            'typeName',
            'operatorName',
            'onlyMyAge',
            'round',
            [
                'attribute' => 'prise_contest_id',
                'value' => $model->priseContest ? $model->priseContest->name : null,
            ],
            [
                'attribute' => 'winner_contest_id',
                'value' => $model->winnerContest ? $model->winnerContest->name : null,
            ],
        ],
    ]) ?>
</div>
