<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Nomination */
/* @var $types array */
/* @var $operators array */

$this->title = 'Создание Номинации';
$this->params['breadcrumbs'][] = ['label' => 'Номинации', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nomination-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'types' => $types,
        'operators' => $operators
    ]) ?>

</div>
