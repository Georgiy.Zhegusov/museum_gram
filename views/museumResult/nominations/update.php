<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Nomination */
/* @var $types array */
/* @var $operators array */

$this->title = 'Обновление номинации: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Номинации', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="nomination-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'types' => $types,
        'operators' => $operators
    ]) ?>

</div>
