<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $uploadForm string */
/* @var $downloadResults string */
/* @var $downloadPercent string */
/* @var $uploadCriteria string */
/* @var $importMaximum string */
/* @var $downloadStatus string */

$this->title                   = 'Номинации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Номинации',
        'content' => $this->render('_index', [
            'dataProvider' => $dataProvider
        ]),
        'active'  => true,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт',
        'content' => $uploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Скачать номинации',
        'content' => $downloadResults,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Скачать проценты',
        'content' => $downloadPercent,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Скачать Результат',
        'content' => $downloadStatus,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Загрузка критериев',
        'content' => $uploadCriteria,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Выгрузка максимумов',
        'content' => $importMaximum,
    ]; ?>

    <?= TabsX::widget([
        'items'        => $tabs,
        'position'     => TabsX::POS_ABOVE,
        'encodeLabels' => false
    ]); ?>
</div>
