<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Nomination */
/* @var $ageCategoryForm string */
/* @var $museumForm string */
/* @var $museumFocus string */
/* @var $ageCategoryFocus string */

$tabs = [];

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Номинациии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Информация',
        'content'=> $this->render('_view',['model' => $model]),
        'active' => !$museumFocus && !$ageCategoryFocus
    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> ' . $model->typeName,
        'content'=> $museumForm,
        'active' => $museumFocus
    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Возрастные категории',
        'content'=> $ageCategoryForm,
        'active' => $ageCategoryFocus
    ];?>

    <?= TabsX::widget([
        'items'=> $tabs,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);?>
</div>
