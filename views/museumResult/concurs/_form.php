<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Concurs */
/* @var $form yii\widgets\ActiveForm */
/* @var $types array */
?>

<div class="museum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->dropDownList($types) ?>

    <?= $form->field($model, 'winner_contest_id')->widget(Select2::className(),[
        'data' => ((!empty($model->winner_contest_id) && !empty($model->winnerContest))
            ? [$model->winner_contest_id => $model->winnerContest->name]
            : null),
        'model' => $model,
        'attribute' => 'winner_contest_id',
        'options' => [
            'placeholder' => 'Поиск конкурса...',
            'id'=>'winnerContest'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'prise_contest_id')->widget(Select2::className(),[
        'data' => ((!empty($model->prise_contest_id) && !empty($model->priseContest))
            ? [$model->prise_contest_id => $model->priseContest->name]
            : null),
        'model' => $model,
        'attribute' => 'prise_contest_id',
        'options' => [
            'placeholder' => 'Поиск конкурса...',
            'id'=>'priseContest'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
