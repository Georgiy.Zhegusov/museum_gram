<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Concurs */
/* @var $types array */

$this->title = 'Создание конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'types' => $types
    ]) ?>

</div>
