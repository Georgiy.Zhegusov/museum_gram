<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $uploadForm string */
/* @var $uploadBalls string */

$this->title = 'Конкурсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="concurs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Конкурсы',
        'content' => $this->render('_index', [
            'dataProvider' => $dataProvider
        ]),
        'active'  => true,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт',
        'content' => $uploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт баллов',
        'content' => $uploadBalls,
    ]; ?>


    <?= TabsX::widget([
        'items'        => $tabs,
        'position'     => TabsX::POS_ABOVE,
        'encodeLabels' => false
    ]); ?>
</div>