<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\Concurs */
/* @var $categoryForm string */
/* @var $ageCategoryForm string */
/* @var $coefficientForm string */
/* @var $coefficientFocus string */

$tabs = [];

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Конкурсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Информация',
        'content'=> $this->render('_view',['model' => $model]),
        'active' => !$coefficientFocus
    ];?>

<!--    --><?php //$tabs[] = [
//        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Коэффициенты',
//        'content'=> $coefficientForm,
//        'active' => $coefficientFocus
//    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Категории',
        'content'=> $categoryForm
    ];?>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Возрастные категории',
        'content'=> $ageCategoryForm
    ];?>

    <?= TabsX::widget([
        'items'=> $tabs,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);?>
</div>
