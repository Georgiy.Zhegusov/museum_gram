<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="museum-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать конкурс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'number',
            'name',
            'typeText',
            [
                'attribute' => 'prise_contest_id',
                'value' => function($model){
                    return $model->priseContest ? $model->priseContest->name : null;
                }
            ],
            [
                'attribute' => 'winner_contest_id',
                'value' => function($model){
                    return $model->winnerContest ? $model->winnerContest->name : null;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
