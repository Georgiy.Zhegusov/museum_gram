<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCategoryLink */
/* @var $form yii\widgets\ActiveForm */
/* @var $ageCategories array */
?>

<div class="museum-category-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'museum_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'age_category_id')->dropDownList($ageCategories) ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
