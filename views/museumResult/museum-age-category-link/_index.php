<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="museum-category-link-index">

    <h1>Категории</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'ageCategoryName',

            [
                'class'      => 'yii\grid\ActionColumn',
                'template'   => '{delete}',
                'urlCreator' => function ($action, $model, $key, $index, $there) {
                    return \yii\helpers\Url::to([
                        '/museumResult/museums/age-category-' . $action,
                        'museum_id'           => $model->museum_id,
                        'age_category_id' => $model->age_category_id
                    ]);
                }
            ]
        ],
    ]); ?>
</div>
