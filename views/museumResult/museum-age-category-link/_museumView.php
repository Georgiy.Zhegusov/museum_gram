<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $museumAgeCategoryLink \app\models\museumResult\MuseumCategoryLink */
/* @var $ageCategories array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="museum-category-view-index">
    <?= $this->render('_index', [
        'dataProvider' => $dataProvider,
    ]) ?>

    <?= $this->render('_create', [
        'model' => $museumAgeCategoryLink,
        'ageCategories' => $ageCategories
    ]) ?>

</div>
