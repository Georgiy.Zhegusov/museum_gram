<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCategoryLink */
/* @var $ageCategories array */

?>
<div class="museum-category-link-create">

    <h3>Добавить возрастную категорию</h3>

    <?= $this->render('_form', [
        'model' => $model,
        'ageCategories' => $ageCategories
    ]) ?>

</div>
