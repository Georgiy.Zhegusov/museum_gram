<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $statuses array */
/* @var $model app\models\museumResult\MuseumBall */

$this->title = 'Обновление баллов за музей ' . $model->museum_id;
$this->params['breadcrumbs'][] = ['label' => 'Балллы за музеи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="museum-ball-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses
    ]) ?>

</div>
