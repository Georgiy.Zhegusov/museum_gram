<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $statuses array */
/* @var $model app\models\museumResult\MuseumBall */

$this->title = 'Добавление баллов за музей команде ' . $model->team->number;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->team->number, 'url' => ['/team/view','id' => $model->team_id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="museum-ball-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'statuses' => $statuses
    ]) ?>

</div>
