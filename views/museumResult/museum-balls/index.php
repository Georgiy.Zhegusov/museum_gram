<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баллы за музеи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="museum-ball-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить баллы за музеи', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'museum_id',
                'value' => function($model){
                    return $model->museum->name;
                }
            ],
            [
                'attribute' => 'museumType',
                'value' => function($model){
                    return $model->museum->baseTypeText;
                }
            ],
            [
                'attribute' => 'team_id',
                'value' => function($model){
                    return $model->team->number;
                }
            ],
            'visitDate',
            'correspondence_round',
            'internal_round',
            'visit_people_count',
            'photo_status',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function($data)
                {
                    if(!empty($data->image_thumb) && file_exists(Yii::getAlias("@app/{$data->image_thumb}"))){
                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->image_thumb);
                    }

                    return null;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
