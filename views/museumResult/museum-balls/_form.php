<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $statuses array */
/* @var $model app\models\museumResult\MuseumBall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="museum-ball-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'method' => 'post']); ?>

    <?= $form->field($model, 'museum_id')->widget(Select2::className(),[
        'data' => ((!empty($model->museum_id) && !empty($model->museum))
            ? [$model->museum_id => $model->museum->number . ': ' . $model->museum->name]
            : null),
        'model' => $model,
        'attribute' => 'museum_id',
        'options' => [
            'placeholder' => 'Поиск музея...',
            'id'=>'isMuseumFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 2 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/museumResult/museums/base-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])?>

    <?= $form->field($model, 'team_id')->widget(Select2::className(),[
        'data' => ((!empty($model->team_id) && !empty($model->team))
            ? [$model->team_id => $model->team->number]
            : null),
        'model' => $model,
        'attribute' => 'team_id',
        'options' => [
            'placeholder' => 'Поиск команды...',
            'id'=>'isTeamFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/team/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])?>

    <?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'visitDate',
        'options' => ['placeholder' => 'Дата посещения'],
        'type' => DatePicker::TYPE_INPUT,
        'form' => $form,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'internal_round')->textInput() ?>

    <h4>Только для музеев.</h4>
    <?= $form->field($model, 'correspondence_round')->textInput() ?>

    <?= $form->field($model, 'visit_people_count')->textInput() ?>

    <h4>Только для парков.</h4>
    <?= $form->field($model, 'photo_status')->checkbox() ?>

    <h4>Только для конкурсов.</h4>
    <?= $form->field($model, 'status')->dropDownList($statuses) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'deleteImage')->checkbox() ?>
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
