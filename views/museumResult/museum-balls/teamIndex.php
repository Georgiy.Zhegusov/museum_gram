<?php

use app\models\Contest;
use app\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\HonorSearch */
/* @var $team Team */

?>
<div class="honor-index">
    <p>
        <?= Html::a('Добавить баллы за музей', ['/museumResult/museum-balls/create', 'teamId' => $team->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'museum_id',
                'value' => function($model){
                    return $model->museum->number . ': ' . $model->museum->name;
                }
            ],
            [
                'attribute' => 'museumType',
                'value' => function($model){
                    return $model->museum->baseTypeText . '(' . $model->museum->object->typeText . ')';
                }
            ],
            [
                'attribute' => 'team_id',
                'value' => function($model){
                    return $model->team->number;
                }
            ],
            'visitDate',
            'correspondence_round',
            'internal_round',
            'coefficient',
            'sum',
            'value',
            'visit_people_count',
            'photo_status',
            'statusText',
//            [
//                'attribute' => 'image',
//                'format' => 'html',
//                'value' => function($data)
//                {
//                    if(!empty($data->image_thumb) && file_exists(Yii::getAlias("@app/{$data->image_thumb}"))){
//                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->image_thumb, ['style' => ['height' => '200px']]);
//                    }
//
//                    return null;
//                },
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'action-column'],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-pencil"></i>',
                            ['/museumResult/museum-balls/update', 'id' => $model->id]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-eye-open"></i>',
                            ['/museumResult/museum-balls/view', 'id' => $model->id]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>',
                            ['/museumResult/museum-balls/delete', 'id' => $model->id],
                            [
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ]
                            ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
