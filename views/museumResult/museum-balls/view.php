<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumBall */

$this->title = 'Баллы за музей ' . $model->museum->name . ' для команды ' . $model->team->number;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->team->number, 'url' => ['/team/view', 'id' => $model->team_id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['imagePath'] = $model->image_path;
?>
<div class="museum-ball-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'museum_id',
                'value' => $model->museum->name
            ],
            [
                'attribute' => 'museumType',
                'value' => $model->museum->baseTypeText
            ],
            [
                'attribute' => 'team_id',
                'value' => $model->team->number
            ],
            'visitDate',
            'correspondence_round',
            'internal_round',
            'visit_people_count',
            'photo_status',
        ],
    ]) ?>

</div>
