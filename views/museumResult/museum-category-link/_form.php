<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCategoryLink */
/* @var $form yii\widgets\ActiveForm */
/* @var $museumCategories array */
?>

<div class="museum-category-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'museums_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'museum_categories_id')->dropDownList($museumCategories) ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
