<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $museumCategoryLink \app\models\museumResult\MuseumCategoryLink */
/* @var $museumCategories array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="museum-category-view-index">
    <?= $this->render('_index', [
        'dataProvider' => $dataProvider,
    ]) ?>

    <?= $this->render('_create', [
        'model' => $museumCategoryLink,
        'museumCategories' => $museumCategories
    ]) ?>

</div>
