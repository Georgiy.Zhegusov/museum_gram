<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\MuseumCategoryLink */
/* @var $museumCategories array */

?>
<div class="museum-category-link-create">

    <h3>Добавить категорию</h3>

    <?= $this->render('_form', [
        'model' => $model,
        'museumCategories' => $museumCategories
    ]) ?>

</div>
