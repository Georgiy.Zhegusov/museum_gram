<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="museum-category-link-index">

    <h1>Категории</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'categoryName',

            [
                'class'      => 'yii\grid\ActionColumn',
                'template'   => '{delete}',
                'urlCreator' => function ($action, $model, $key, $index, $there) {
                    return \yii\helpers\Url::to([
                        '/museumResult/museums/category-' . $action,
                        'museums_id'           => $model->museums_id,
                        'museum_categories_id' => $model->museum_categories_id
                    ]);
                }
            ]
        ],
    ]); ?>
</div>
