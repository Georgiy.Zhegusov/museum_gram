<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $nomination \app\models\museumResult\Nomination */
/* @var $nominationMuseumLink \app\models\museumResult\NominationAgeCategoryLink */
/* @var $ageCategories array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="nomination-museum-view-index">
    <?= $this->render('_index', [
        'nomination'   => $nomination,
        'dataProvider' => $dataProvider,
    ]) ?>

    <?= $this->render('_create', [
        'nomination' => $nomination,
        'model'      => $nominationMuseumLink
    ]) ?>

</div>
