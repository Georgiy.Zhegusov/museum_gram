<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $nomination \app\models\museumResult\Nomination*/
?>
<div class="nomination-category-link-index">

    <h1><?=$nomination->typeName?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'museumText',
            'useForMax',

            [
                'class'      => 'yii\grid\ActionColumn',
                'template'   => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index, $there) {
                    return \yii\helpers\Url::to([
                        '/museumResult/nominations/museum-' . $action,
                        'nomination_id'   => $model->nomination_id,
                        'museum_id' => $model->museum_id
                    ]);
                }
            ]
        ],
    ]); ?>
</div>
