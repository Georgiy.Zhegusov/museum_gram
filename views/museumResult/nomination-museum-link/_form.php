<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\museumResult\NominationAgeCategoryLink */
/* @var $form yii\widgets\ActiveForm */
/* @var $ageCategories array */
/* @var $nomination \app\models\museumResult\Nomination */
?>

<div class="nomination-category-link-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomination_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'useForMax')->checkbox() ?>

    <?= $form->field($model, 'museum_id')->widget(Select2::className(),[
        'data' => ((!empty($model->museum_id) && !empty($model->museum))
            ? [$model->museum_id => $model->museum->number . ': ' . $model->museum->name]
            : null),
        'model' => $model,
        'attribute' => 'museum_id',
        'options' => [
            'placeholder' => 'Ищется объект...',
            'id'=>'isMuseumFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 2 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/museumResult/' . $nomination->musObjUrl . '/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])?>

    <div class="form-group">
        <?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Обновить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
