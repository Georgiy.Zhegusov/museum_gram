<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\museumResult\NominationAgeCategoryLink */
/* @var $nomination \app\models\museumResult\Nomination*/
/* @var $ageCategories array */

?>
<div class="nomination-category-link-create">

    <h3><?=$model->isNewRecord ? 'Добавить' : 'Обновить'?> объект</h3>

    <?= $this->render('_form', [
        'model' => $model,
        'nomination' => $nomination
    ]) ?>

</div>
