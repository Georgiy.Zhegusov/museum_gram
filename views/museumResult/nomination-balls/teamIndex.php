<?php

use app\models\Contest;
use app\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\HonorSearch */
/* @var $team Team */

?>
<div class="honor-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'nomination_id',
                'value' => function($model){
                    return $model->nomination->name;
                }
            ],
            [
                'attribute' => 'team_id',
                'value' => function($model){
                    return $model->team->number;
                }
            ],

            'value',
            'statusText',
        ],
    ]); ?>
</div>
