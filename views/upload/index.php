<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Последние загрузки';
?>
<div class="upload-file-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'format' => 'raw',
                'attribute' => 'fileName',
                'value' => function($model)
                {
                    return Html::a($model->fileName, Url::to(['/upload/download', 'id' => $model->id]));
                }
            ],
            [
                'format' => 'raw',
                'label' => 'Прогресс',
                'value' => function($model)
                {
                    if($model->isFinished()) {
                        return $model->isError() ? 'Ошибка обработки' : 'Обработка закончилась';
                    }
                    else{
                        return $this->render(
                            'progress',
                            [
                                'model'     => $model,
                                'updateUrl' => Url::to([
                                    '/upload/file-status',
                                    'id' => $model->id
                                ])
                            ]
                        );
                    }
                }
            ],
            [
                'format' => 'raw',
                'label' => 'Результат',
                'value' => function($model)
                {
                    return $this->render('collapse',['model' => $model]);
                }
            ]
        ],
    ]); ?>
</div>

<?php
$js = <<<JS
    $('.loadCollapse').click(function () {
        if(!$(this).hasClass("alreadyLoaded")){
            
            $(this).addClass("alreadyLoaded");
            
            var depId = this.attributes['data-target']['value'];

            $.getJSON(this.attributes['data-url']['value'], function(result) {
                $(depId).html(result.data);
            });
        }
    });
JS;

$this->registerJs($js);
?>