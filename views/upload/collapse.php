<?php
/**
 * Copyright (c) 2016. Zhegusov George
 */

use common\components\modules\group\models\strategys\exploders\BySpaceExploder;
use common\components\modules\group\models\strategys\exploders\NothingExploder;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\select2\Select2Asset;
use yii\bootstrap\Progress;
use yii\helpers\Html;
use yii\jui\ProgressBar;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \app\models\UploadFile*/
?>

Обработано <?= $model->parsedCount?> строк, из них

<a
    data-toggle="collapse"
    data-target="#new<?= $model->id?>"
    type="button"
    class="loadCollapse"
    data-url="<?= Url::to(['/upload/news', 'id' => $model->id])?>"> новых </a>: <?=$model->uploadNewsCount?>
<div id="new<?= $model->id?>" class="collapse">
    Информация загружается...
</div>,

<a
    data-toggle="collapse"
    data-target="#update<?= $model->id?>"
    type="button"
    class="loadCollapse"
    data-url="<?= Url::to(['/upload/updates', 'id' => $model->id])?>"> обновленных </a>: <?=$model->uploadUpdatesCount?>
<div id="update<?= $model->id?>" class="collapse">
    Информация загружается...
</div>,

<a
    data-toggle="collapse"
    data-target="#error<?= $model->id?>"
    type="button"
    class="loadCollapse"
    data-url="<?= Url::to(['/upload/errors', 'id' => $model->id])?>"> ошибок </a>: <?=$model->uploadErrorsCount?>
<div id="error<?= $model->id?>" class="collapse">
    Информация загружается...
</div>.