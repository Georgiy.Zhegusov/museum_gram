<?php

use app\models\UploadFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $type*/
/* @var $dataProvider*/
/* @var $model app\models\UploadFile */
?>
<div class="upload-file-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('index', [
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
