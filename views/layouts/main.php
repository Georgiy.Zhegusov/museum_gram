<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use \yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Музеи.Парки.Усадьбы.Грамоты',
        'brandUrl' => ['/site/index'],
        'options' => [
            'class' => 'fixed-height navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
	        [
		        'label' => 'Объекты',
		        'items' => [
		            ['label' => 'Музеи', 'url' => ['/museumResult/museums/index']],
		            ['label' => 'Конкурсы', 'url' => ['/museumResult/concurs/index']],
		            ['label' => 'Категории', 'url' => ['/museumResult/categories/index']],
                    ['label' => 'Возрастные категории', 'url' => ['/museumResult/age-categories/index']],

                ],
		        'visible' => Yii::$app->user->can('gramPermission')
	        ],
            [
                'label' => 'Команды',
                'items' => [
                    ['label' => 'Команды', 'url' => ['/team/index']],
                    ['label' => 'Возрастные категории', 'url' => ['/museumResult/age-categories/index']],
                    ['label' => 'Школы', 'url' => ['/people-school/index']],
                ],
                'visible' => Yii::$app->user->can('gramPermission')
            ],
            [
                'label' => 'Номинациии',
                'items' => [
                    ['label' => 'Номинации', 'url' => ['/museumResult/nominations/index']],
                ],
                'visible' => Yii::$app->user->can('gramPermission')
            ],
            [
                'label' => 'Грамоты',
                'items' => [
                    ['label' => 'Генерация грамот', 'url' => ['/generator/index']],
                    ['label' => 'Категории конкурсов', 'url' => ['/contest-category/index']],
                    ['label' => 'Типы конкурсов', 'url' => ['/contest-type/index']],
                    ['label' => 'Конкурсы', 'url' => ['/contest/index']],
                ],
                'visible' => Yii::$app->user->can('gramPermission')
            ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(Url::to(['/site/logout']), 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <?php if(isset($this->params['imagePath']) && file_exists(Yii::getAlias('@app/' . $this->params['imagePath']))):?>
        <div class="container" style="
            background: url(<?=Yii::$app->request->baseUrl . '/' . $this->params['imagePath']?>) no-repeat center;
            -webkit-background-size: contain;
            background-size: contain;
            background-origin: content-box;
            min-height: 800px;">
    <?php else:?>
            <div class="container">
    <?php endif;?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
<!--        Для сортировки по столбцу, необходимо кликнуть по его названию.-->
<!--	    Для фильтрации по столбцу необходимо ввести значение для фильтрации в строку после заголовка.-->
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Музеи.Парки.Усадьбы <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
