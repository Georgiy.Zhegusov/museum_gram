<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\search\TeamSearch */
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать команду', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'format' => 'raw',
                'attribute' => 'number',
                'value' => function($model){
                    return Html::a($model->number, ['view', 'id' => $model->id]);
                }
            ],
            'temporary_number',
            'mosolimpNumbers',
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{update} {view} {delete} {download}',
                'buttons' => [
                    'download' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-open"></i>',
                            ['/team/download', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
