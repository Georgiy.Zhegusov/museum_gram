<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\search\TeamSearch */
/* @var $teamBaseUploadForm string */
/* @var $teamBasePeopleUploadForm string */
/* @var $teamESRUploadForm string */
/* @var $teamResult string */
/* @var $teamStat string */
/* @var $teamResultValidate string */
/* @var $teamBasePeople2StageUploadForm string */
/* @var $teamHonorImport string */
/* @var $mentionImport string */

$this->title                   = 'Команды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Команды',
        'content' => $this->render('_index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel
        ]),
        'active'  => true,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт ЕСР',
        'content' => $teamESRUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт базы',
        'content' => $teamBaseUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт людей',
        'content' => $teamBasePeopleUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Импорт людей 2 этапа',
        'content' => $teamBasePeople2StageUploadForm,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Экспорт результата',
        'content' => $teamResult,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Экспорт статистики',
        'content' => $teamStat,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Валидация выгрузки',
        'content' => $teamResultValidate,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Загрузка наград',
        'content' => $teamHonorImport,
    ]; ?>

    <?php $tabs[] = [
        'label'   => '<i class="glyphicon glyphicon-info-sign"></i> Загрузка благодарностей',
        'content' => $mentionImport,
    ]; ?>

    <?= TabsX::widget([
        'items'        => $tabs,
        'position'     => TabsX::POS_ABOVE,
        'encodeLabels' => false
    ]); ?>
</div>
