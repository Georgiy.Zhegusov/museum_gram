<?php

use kartik\tabs\TabsX;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$tabs = [];
?>
<div class="team-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить информацию о команде', ['team-info/create','teamId' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> О Команде',
        'content'=> $this->render('_info',['model'=>$model]),
        'active'=>true,
    ];?>

    <?php foreach ($model->teamInfos as $teamInfo):?>
        <?php $tabs[] = [
            'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Информация о ' . $teamInfo->stage . ' этапе',
            'content'=> $this->render('/team-info/_info',['model'=>$teamInfo]),
        ];?>
    <?php endforeach;?>

    <?php
    $dataProvider = new ActiveDataProvider([
        'query' => $model->getHonors(),
    ]);

    $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Награды',
        'content'=> $this->render('/honor/teamIndex',['dataProvider'=>$dataProvider, 'team' => $model]),
    ];

    $dataProvider = new ActiveDataProvider([
        'query' => $model->getMuseumBalls(),
    ]);

    $tabs[] = [
        'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Баллы',
        'content'=> $this->render('/museumResult/museum-balls/teamIndex',['dataProvider'=>$dataProvider, 'team' => $model]),
    ];

    $dataProvider = new ActiveDataProvider([
    'query' => $model->getNominationBalls(),
    ]);

    $tabs[] = [
    'label'=>'<i class="glyphicon glyphicon-info-sign"></i> Номинации',
    'content'=> $this->render('/museumResult/nomination-balls/teamIndex',['dataProvider'=>$dataProvider, 'team' => $model]),
    ];?>

    <?= TabsX::widget([
        'items'=> $tabs,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);?>

</div>
