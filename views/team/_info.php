<?php
use app\models\museumResult\Museum;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $model \app\models\Team */
?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'number',
        'temporary_number',
        'mosolimpNumbers',
        'sum',
        'statusText',
        'classNum',
        'schoolName',
        'mentionText',
        [
            'attribute' => 'fourOne',
            'value' => $model->fourOne ? 'Ok' : 'Нет'
        ],
        [
            'label' => 'Объектов посещено',
            'value' => $model->getCount(Museum::TYPE_PARK) + $model->getCount(Museum::TYPE_ESTATE) + $model->getCount(Museum::TYPE_MUSEUM)
        ],
        [
            'label' => 'Музеев посещено',
            'value' => $model->getCount(Museum::TYPE_MUSEUM)
        ],
        [
            'label' => 'Усадеб посещено',
            'value' => $model->getCount(Museum::TYPE_ESTATE)
        ],
        [
            'label' => 'Парков посещено',
            'value' => $model->getCount(Museum::TYPE_PARK)
        ]
    ],
]) ?>
<p>
    <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</p>

<p>
    <?= Html::a('Наградить команду', ['honor/create','teamId' => $model->id], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Добавить команде баллов', ['/museumResult/museum-balls/create','teamId' => $model->id], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Добавить Лешин номер', ['team-mosolimp-id/create','teamId' => $model->id], ['class' => 'btn btn-success']) ?>
</p>

