<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamMosolimpId */

$this->title = 'Добавление Лешиного номера';
$this->params['breadcrumbs'][] = ['label' => 'Лешины номера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-mosolimp-id-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
