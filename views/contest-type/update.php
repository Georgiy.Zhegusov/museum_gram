<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestType */
/* @var $contestCategories array */
/* @var $generationTypes array */
/* @var $baseTypes array */
/* @var $baseStatuses array */

$this->title = 'Обновление типа конкурсов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => 'Категория: ' . $model->contestCategory->name,
    'url' => ['/contest-category/view', 'id' => $model->contest_category_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="contest-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contestCategories' => $contestCategories,
        'generationTypes' => $generationTypes,
        'baseTypes' => $baseTypes,
        'baseStatuses' => $baseStatuses,
    ]) ?>

</div>
