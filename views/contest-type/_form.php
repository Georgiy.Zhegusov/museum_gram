<?php

use app\models\People;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContestType */
/* @var $contestCategories array */
/* @var $generationTypes array */
/* @var $form yii\widgets\ActiveForm */
/* @var $baseTypes array */
/* @var $baseStatuses array */

$this->params['imagePath'] = $model->nestedSubstrateBack;
?>

<div class="contest-type-form">

    <?php $form = ActiveForm::begin([
		'options' => ['enctype' => 'multipart/form-data'],
		'method' => 'post']); ?>

    <?= $form->field($model, 'substrateFile')->fileInput() ?>

    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'deleteSubstrate')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'codeName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contest_category_id')->dropDownList($contestCategories) ?>

    <?= $form->field($model, 'generation_type_id')->dropDownList($generationTypes, ['prompt' => 'Наследовать от категории']) ?>

    <?= $form->field($model, 'base_for_type')->dropDownList($baseTypes, ['prompt' => 'Ни для кого не базовый']) ?>

    <?= $form->field($model, 'base_for_status')->dropDownList($baseStatuses, ['prompt' => 'Ни для кого не базовый']) ?>

    <?= $form->field($model, 'oneForAll')->checkbox([]) ?>

    <?= $form->field($model, 'textBefore1')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore1')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textBefore2')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore2')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textBefore3')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore3')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter1')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter1')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter2')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter2')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter3')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter3')->checkbox() ?>
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
