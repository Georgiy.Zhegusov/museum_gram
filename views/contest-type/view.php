<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ContestType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => 'Категория: ' . $model->contestCategory->name,
    'url' => ['/contest-category/view', 'id' => $model->contest_category_id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['imagePath'] = $model->nestedSubstrateBack;
?>

<div class="contest-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что вы хотите удалить конкурс?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'codeName',
            [
                'attribute' => 'contest_category_id',
                'value' => $model->contestCategory->name,
            ],
            'oneForAll',
            [
                'attribute' => 'generation_type_id',
                'value' => $model->generationType->name
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',
            'baseTypeText',
            'baseStatusText'
        ],
    ]) ?>

</div>
