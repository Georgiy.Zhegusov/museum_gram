<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContestType */
/* @var $contestCategories array */
/* @var $generationTypes array */
/* @var $baseTypes array */
/* @var $baseStatuses array */

$this->title = 'Создать тип конкурса';
$this->params['breadcrumbs'][] = ['label' => 'Типы конкурсов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contest-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contestCategories' => $contestCategories,
        'generationTypes' => $generationTypes,
        'baseTypes' => $baseTypes,
        'baseStatuses' => $baseStatuses,
    ]) ?>

</div>
