<?php

use app\models\People;
use app\models\TeamType;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamInfo */

$this->title = $model->name;

$dataProvider = new ActiveDataProvider([
    'query' => $model->getPeoples(),
]);
?>
<div class="team-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'team_type_id',
                'value' => empty($model->teamType) ? null : $model->teamType->text
            ],
            'name',
            'ageCategoryName',
            'participantCount'
        ],
    ]) ?>

    <?= $this->render('/people/teamIndex',[
        'dataProvider' => $dataProvider,
    ])?>

    <p>
        <?= Html::a('Обновить информация о '. $model->stage .' этапе', ['/team-info/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить информация о '. $model->stage .' этапе', ['/team-info/delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удаление информации о '. $model->stage .' этапе возможно только при отсутствии участников и наград на данном этапе. Продолжить ?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Добавить человека на '. $model->stage .' этап', ['/people/create','teamInfoId' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

</div>
