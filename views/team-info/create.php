<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamInfo */
/* @var $teamTypes array */

$this->title = 'Добавление информации о команде ' . $model->team->number;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->team->number, 'url' => ['/team/view','id' => $model->team_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teamTypes' => $teamTypes
    ]) ?>

</div>
