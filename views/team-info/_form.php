<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamInfo */
/* @var $form yii\widgets\ActiveForm */
/* @var $teamTypes array */
?>

<div class="team-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'team_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'team_type_id')->dropDownList($teamTypes) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stage')->textInput() ?>

    <?= $form->field($model, 'peopleCount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
