<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Honor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="honor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'team_id')->widget(Select2::className(),[
        'data' => ((!empty($model->team_id) && !empty($model->team))
            ? [$model->team_id => $model->team->number]
            : null),
        'model' => $model,
        'attribute' => 'team_id',
        'options' => [
            'placeholder' => 'Поиск команды...',
            'id'=>'isTeamFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/team/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ])?>

    <?= $form->field($model, 'contest_id')->widget(Select2::className(),[
        'data' => ((!empty($model->contest_id) && !empty($model->contest))
            ? [$model->contest_id => $model->contest->name]
            : null),
        'model' => $model,
        'attribute' => 'contest_id',
        'options' => [
            'placeholder' => 'Поиск конкурса...',
            'id'=>'isContestFind'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
            ],
            'ajax' => [
                'url' => Url::to(['/contest/list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'textBefore1')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore1')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textBefore2')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore2')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textBefore3')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextBefore3')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter1')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter1')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter2')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter2')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'textAfter3')->textInput(['maxlength' => true]) ?>
    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'clearTextAfter3')->checkbox() ?>
    <?php endif;?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?php if(!$model->isNewRecord):?>
        <?= $form->field($model, 'deleteImage')->checkbox() ?>
    <?php endif;?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
