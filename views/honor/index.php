<?php

use app\models\Contest;
use app\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\HonorSearch */
/* @var $searchModel yii\data\ActiveDataProvider */

$this->title = 'Награды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="honor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Наградить команду', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'team_id',
                'value' => function($model){
                    return $model->team->number;
                },
                'filter' => Select2::widget([
                    'data' => ((!empty($searchModel->team_id) && !empty(Team::findOne($searchModel->team_id)))
                        ? [$searchModel->team_id => Team::findOne($searchModel->team_id)->number]
                        : null),
                    'model' => $searchModel,
                    'attribute' => 'team_id',
                    'options' => [
                        'placeholder' => 'Поиск команды...',
                        'id'=>'isTeamFind'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                            'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/team/list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'contest_id',
                'value' => function($model){
                    return $model->contest->name;
                },
                'filter' => Select2::widget([
                    'data' => ((!empty($searchModel->contest_id) && !empty(Contest::findOne($searchModel->contest_id)))
                        ? [$searchModel->contest_id => Contest::findOne($searchModel->contest_id)->name]
                        : null),
                    'model' => $searchModel,
                    'attribute' => 'contest_id',
                    'options' => [
                        'placeholder' => 'Поиск конкурса...',
                        'id'=>'isContestFind'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Ожидание результата...'; }"),
                            'inputTooShort' => new JsExpression("function () { return 'Введите хотя бы 3 символа.';}"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/contest/list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ])
            ],
//            [
//                'attribute' => 'imageFile',
//                'format' => 'html',
//                'value' => function($data)
//                {
//                    if(!empty($data->image_thumb) && file_exists(Yii::getAlias("@app/{$data->image_thumb}"))){
//                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->image_thumb);
//                    }
//
//                    return null;
//                },
//            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
