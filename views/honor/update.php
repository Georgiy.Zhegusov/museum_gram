<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Honor */

$this->title = 'Обновление награды за конкурс ' . $model->contest->name . '.';
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->teamInfo->team->number, 'url' => ['/team/view', 'id' => $model->teamInfo->team_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="honor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
