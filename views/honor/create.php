<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Honor */

$this->title = 'Наградить команду ' . $model->team->number;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->team->number, 'url' => ['/team/view','id' => $model->team_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="honor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
