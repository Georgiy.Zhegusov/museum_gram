<?php

use app\models\Contest;
use app\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\HonorSearch */

?>
<div class="honor-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'number',
            [
                'attribute' => 'honors_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->honor->contest->name, ['/honor/view', 'id' => $model->honors_id]);
                },
            ],
            [
                'label' => 'Сгенерировано для',
                'value' => function($model){
                    return $model->honor->contest->generationType->generatedRoles;
                }
            ],
            [
                'attribute' => 'imageFile',
                'format' => 'html',
                'value' => function($data)
                {
                    if(!empty($data->image_thumb) && file_exists(Yii::getAlias("@app/{$data->image_thumb}"))){
                        return Html::img(Yii::$app->request->baseUrl . '/' . $data->image_thumb);
                    }

                    return null;
                },
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',
            'statusText',
        ],
    ]); ?>
</div>
