<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Honor */

$this->title = 'Награда за конкурс ' . $model->contest->name . '.';
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['/team/index']];
$this->params['breadcrumbs'][] = ['label' => $model->teamInfo->team->number, 'url' => ['/team/view', 'id' => $model->teamInfo->team_id]];
$this->params['breadcrumbs'][] = $this->title;

$this->params['imagePath'] = $model->image_path;
?>
<div class="honor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'team_id',
                'value' => $model->team->number,
            ],
            [
                'attribute' => 'contest_id',
                'value' => $model->contest->name,
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',
        ],
    ]) ?>

</div>
