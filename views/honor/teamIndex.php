<?php

use app\models\Contest;
use app\models\Team;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider \app\models\search\HonorSearch */
/* @var $team Team */

?>
<div class="honor-index">
    <p>
        <?= Html::a('Наградить команду', ['/honor/create', 'teamId' => $team->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'contest_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->contest->name, ['/contest/view', 'id' => $model->contest->id]);
                },
            ],
            [
                'attribute' => 'stage',
                'value' => function($model){
                    return $model->contest->stage;
                }
            ],
            [
                'attribute' => 'generation_type',
                'value' => function($model){
                    return $model->contest->generationType->name;
                }
            ],
            [
                'attribute' => 'imageFile',
                'format' => 'html',
                'value' => function($data)
                {
                    if(!empty($data->image_thumb) && file_exists(Yii::getAlias("@app/{$data->image_thumb}"))){
                        return Html::img('/' . $data->image_thumb);
                    }

                    return null;
                },
            ],
            'nestedTextBefore1',
            'nestedTextBefore2',
            'nestedTextBefore3',
            'nestedTextAfter1',
            'nestedTextAfter2',
            'nestedTextAfter3',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'action-column'],
                'template' => '{update} {view} {delete} {download}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-pencil"></i>',
                            ['/honor/update', 'id' => $model->id]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-eye-open"></i>',
                            ['/honor/view', 'id' => $model->id]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-trash"></i>',
                            ['/honor/delete', 'id' => $model->id],
                            [
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ]
                            ]);
                    },
                    'download' => function ($url, $model) {
                        return Html::a(
                            '<i class="glyphicon glyphicon-open"></i>',
                            ['/honor/download', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
