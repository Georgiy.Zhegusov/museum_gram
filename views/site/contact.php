<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<div class="site-contact">
    <?php
    $js = <<<JS
	(function(){
        var token = '905716b94f66517ff89105c5fe449e4c';
        console.dir(token);
        
        $.ajax({
            type: 'GET',
            url: 'https://ulc.apidev.pingdelivery.com:443/v1/users/info',
            crossDomain: true,
            headers: {"Authorization": token},
            dataType: 'json',
            success: function(responseData, textStatus, jqXHR) {
                console.dir(responseData);
            },
            error: function (responseData, textStatus, errorThrown) {
                alert('GET failed.');
            }
        });
        
        $.ajax({
            type: 'GET',
            url: 'http://finapi.othdev.pingdelivery.com/v1/codReport/actionedList?country=MX&company=FedEx&orderBy=-created_at&limit=20',
            crossDomain: true,
            headers: {"Authorization": token},
            dataType: 'json',
            success: function(responseData, textStatus, jqXHR) {
                console.dir(responseData);
            },
            error: function (responseData, textStatus, errorThrown) {
                alert('GET failed.');
            }
        });
        
        $.ajax({
            type: 'GET',
            url: 'http://finapi.othdev.pingdelivery.com/v1/partnerInvoiceReport/actionedList?country=MX&company=FedEx&orderBy=-created_at&limit=20',
            crossDomain: true,
            headers: {"Authorization": token},
            dataType: 'json',
            success: function(responseData, textStatus, jqXHR) {
                console.dir(responseData);
            },
            error: function (responseData, textStatus, errorThrown) {
                alert('GET failed.');
            }
        });
	})();
JS;

    $this->registerJs($js);
    ?>
</div>
