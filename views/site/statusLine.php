<?php

use app\models\Museum;
use yii\helpers\Html;
use yii\helpers\Url;
use raoul2000\widget\scrollup\Scrollup;

/* @var $this yii\web\View */
/* @var $statuses \app\models\Status[] */
/* @var $museums Museum[][]*/

$this->title = 'Статистика по статусам';
$this->params['breadcrumbs'][] = $this->title;

$counts = [];
foreach($museums as $statusId=>$museumArr){
	$counts[$statusId]  =count($museumArr);
}

$rowCount = 1;
foreach ($museums as $statusId=>$museumArr){
	$rowCount = max($rowCount, $counts[$statusId]);
}

Scrollup::widget([
	'theme' => Scrollup::THEME_IMAGE,
	'pluginOptions' => [
		'scrollText' => "ВВЕРХ", // Text for element
		'scrollName'=> 'scrollUp', // Element ID
		'topSpeed'=> 3000, // Speed back to top (ms)
		'animation' => Scrollup::ANIMATION_SLIDE, // Fade, slide, none
		'animationInSpeed' => 100, // Animation in speed (ms)
		'animationOutSpeed'=> 100, // Animation out speed (ms)
		'activeOverlay' => true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	]
]);
?>

<form id='status-form' method="post">
	<div class="site-index">
		<div class="body-content">
			<table class="table table-striped table-bordered" style="width:99.4%; margin-bottom: 2px !important">
				<thead>
				<tr>
					<?php foreach ($statuses as $status): ?>
						<?php if(isset($counts[$status->id]) && $counts[$status->id]>0):?>
							<th style="width:50px; max-width:50px;">
								<div class="panel" style="text-align: center; font-size: 12px !important">
									<?= $status->name ?>
								</div>

								<div style="text-align: center">
									Музеев: <?= $counts[$status->id]; ?>
								</div>
							</th>
						<?php endif;?>
					<?php endforeach; ?>
				</tr>
				</thead>
					<tbody>
					<?php for($i=0; $i<$rowCount; $i++): ?>
						<tr>
							<?php foreach ($statuses as $status): ?>
								<?php if(isset($counts[$status->id]) && $counts[$status->id]>0):?>
									<?php if (!isset($museums[$status->id][$i])): ?>
										<td style="width:50px; max-width:50px;">&nbsp;</td>
									<?php else: ?>
										<td style="width:50px; max-width:50px;">
											<div class="panel" style="text-align: center">
												<?=	Html::a($museums[$status->id][$i]->name,Url::to(['museum/view', 'id' => $museums[$status->id][$i]->id]),['target'=>'_blank'])?>
											</div>
										</td>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						</tr>
					<?php endfor; ?>
					</tbody>
				</table>
				</div>
			</div>
</form>