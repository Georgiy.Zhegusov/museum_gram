# noinspection SqlNoDataSourceInspectionForFile

CREATE DATABASE IF NOT EXISTS museum_doc DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON museum_doc.* TO museum_doc@localhost IDENTIFIED BY '50iB2CvlHj';