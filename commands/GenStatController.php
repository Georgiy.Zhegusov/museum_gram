<?php
namespace app\commands;

use app\generator\HtmlGenerator;
use app\models\HonorGenerator;
use app\models\museumResult\NominationBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;


class GenStatController extends Controller{

    /**
     * generate history of changes for teams
     */
    public function actionGenBad()
    {

        $res = [];

        $path = Yii::getAlias('@uploadBase/partic.xls');

        $path = Yii::getAlias('@app/' . $path);

        $file = fopen($path, 'r');

        fgetcsv($file, null, '	', '"', '"');

        echo "Start parse file \n";

        $countParsed = 0;
        $count = 0;

        while(is_array($dataArray = fgetcsv($file, null, '	', '"', '"'))){
            $countParsed++;

            if($countParsed % 200 == 0)
            {
                echo $countParsed . "\n";
            }

            $newCommandData = $this->parseArray($dataArray);

            $num = $newCommandData['number'];

            if($num===1152751){
                $count++;
            }

            if(empty($num)){
                continue;
            }

            if(!isset($res[$num])){
                $res[$num] = [
                    'peoples' => [],
                    'types' => [],
                    'changes' => 0,
                    'number' => $num
                ];
            }

            $res[$num]['changes']++;
            $this->addType($newCommandData['type'], $res[$num]['types']);
            $this->addPeople($newCommandData['peoples'], $res[$num]['peoples']);
        }

        fclose($file);

        echo $count . "\n";

        echo 'Start generate file, all count: ' . count($res) . "\n";

        $this->generateStatFile($res);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function parseArray($data)
    {
        $nameCoordinates = [
            [5, 6, 7],
            [34, 35, 36],
            [39, 40, 41],
            [44, 45, 46],
            [49, 50, 51],
            [54, 55, 56],
            [59, 60, 61],
            [64, 65, 66],
            [69, 70, 71],
            [74, 75, 76],
        ];

        $commandType = 30;

        $commandNumber = 87;

        $commandData = [];

        $commandData['type'] = ArrayHelper::getValue($data, $commandType);
        $commandData['number'] = intval(ArrayHelper::getValue($data, $commandNumber));

        $peoples = [];

        foreach ($nameCoordinates as $nameCoordinate) {
            $surname = trim($this->mb_ucfirst(ArrayHelper::getValue($data, $nameCoordinate[0], '')));
            $name = trim($this->mb_ucfirst(ArrayHelper::getValue($data, $nameCoordinate[1], '')));
            $patronymic = trim($this->mb_ucfirst(ArrayHelper::getValue($data, $nameCoordinate[2], '')));

            $fio = [];
            if(!empty($surname)){
                $fio[] = $surname;
            }

            if(!empty($name)){
                $fio[] = $name;
            }

            if(!empty($patronymic)){
                $fio[] = $patronymic;
            }

            if(!empty($fio)) {
                $peoples[implode(' ', $fio)] = 1;
            }
        }

        $commandData['peoples'] = $peoples;

        return $commandData;
    }

    /**
     * @param $newType
     * @param $oldTypes
     */
    private function addType($newType, &$oldTypes)
    {
        if(end($oldTypes) !== $newType){
            $oldTypes[] = $newType;
        }
    }

    /**
     * @param $newPeoples
     * @param $oldPeoples
     */
    private function addPeople($newPeoples, &$oldPeoples)
    {
        foreach ($newPeoples as $newPeople => $one) {
            if(!isset($oldPeoples[$newPeople])){
                $oldPeoples[$newPeople] = 0;
            }

            $oldPeoples[$newPeople]++;
        }
    }

    private function generateStatFile($data)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $titles = [
            'id',
            'Кол-во смен данных',
            'Общее кол-во участников команды',
            'Eсть во всех редакциях/Общее кол-во участников команды',
            'Общий список участников команды',
            'Общий список участников команды[В скольки редакциях присутствует]',
            'Люди присутствующие во всех редакциях',
            'История смены типов команды(0: Индивид, 1: Семья, 2: Друзья )',
            'Кол-во смен типа команды'
        ];


        $objPHPExcel->getActiveSheet()->fromArray($titles, '', 'A1');

        $rowCount = 2;

        foreach ($data as $teamData) {

            if($rowCount % 200 == 0)
            {
                echo $rowCount . "\n";
            }

            $objPHPExcel->getActiveSheet()->fromArray($this->getTeamInfo($teamData), NULL, 'A' . $rowCount);

            $rowCount++;
        }

        $newFileName = Yii::getAlias('@uploadBase/particGroupResult.xls');

        $newFileName = Yii::getAlias('@app/' . $newFileName);

        echo "Start save file \n";

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($newFileName);
        echo "Finish save file \n";
    }

    private function getTeamInfo($teamData){
        $teamInAllChanges = array_filter(
            $teamData['peoples'],
            function($value) use($teamData){
                return $value == $teamData['changes'];
            }
        );

        $teamPeoplesWithCount = [];
        foreach ($teamData['peoples'] as $people=>$count) {
            $teamPeoplesWithCount[] = $people . '[' . $count . ']';
        }
        $teamPeoplesWithCount = implode(', ', $teamPeoplesWithCount);

        return [
            $teamData['number'],
            $teamData['changes'],
            count($teamData['peoples']),
            count($teamInAllChanges) . '/' . count($teamData['peoples']),
            implode(', ', array_keys($teamData['peoples'])),
            $teamPeoplesWithCount,
            implode(', ', array_keys($teamInAllChanges)),
            implode('=>', $teamData['types']),
            count($teamData['types'])
        ];
    }

    private function mb_ucfirst($text)
    {
        $first = mb_substr($text, 0, 1);
        $other = mb_substr($text, 1);

        return mb_strtoupper($first) . mb_strtolower($other);
    }

    public function actionGenerateGramm($contests = '', $contestTypes = '', $stage = null, $withGram=true, $withInfo = false, $teams = '')
    {
        $stage = $stage == 'null' ? null : intval($stage);

        if(!empty($contests) && $contests != 'null'){
            $contests = explode(',', $contests);

            foreach ($contests as &$contest) {
                $contest = intval($contest);
            }
            unset($contest);
        }
        else{
            $contests = null;
        }

        if(!empty($contestTypes) && $contestTypes != 'null'){
            $contestTypes = explode(',', $contestTypes);

            foreach ($contestTypes as &$contestType) {
                $contestType = intval($contestType);
            }
            unset($contestType);
        }
        else{
            $contestTypes = null;
        }

        if(!empty($teams) && $teams != 'null'){
            $teams = explode(',', $teams);

            foreach ($teams as &$team) {
                $team = intval($team);
            }
            unset($team);
        }
        else{
            $teams = null;
        }

        switch ($withGram){
            case 'true':
                $withGram = true;
                break;
            case 'false':
                $withGram = false;
                break;
            default:
                $withGram = boolval($withGram);
        }

        switch ($withInfo){
            case 'true':
                $withInfo = HtmlGenerator::WITH_INFO;
                break;
            case 'false':
                $withInfo = HtmlGenerator::WITHOUT_INFO;
                break;
            default:
                $withInfo = (bool)$withInfo;
        }

        // Для каждой школы выбрать все команды, капитаны которых на каком либо этапе были в этому школе, сортировка по класса.

        ini_set("memory_limit","16336M");

        $offset = 0;
        $limit = 300;

        $localPath = Yii::getAlias('@texPdf/grams_' . time());
        $path = Yii::getAlias('@app/' . $localPath);

        $generator = new HtmlGenerator(1000, 1500, $path);

        while($peopleSchools = PeopleSchool::find()->orderBy(['number' => SORT_ASC])->offset($offset)->limit($limit)->all()){
            foreach ($peopleSchools as $peopleSchool) {
                ini_set('max_execution_time', 5000);

                $generator->addSchool($peopleSchool, $withInfo, $stage, $withGram, $contests, $contestTypes, $teams);
            }
            $offset += $limit;
        }


        ini_set('max_execution_time', 5000);
        $generator->addSchool(null, $withInfo, $stage, $withGram, $contests, $contestTypes, $teams);

//        $localPath = Yii::getAlias('@texPdf/grams_' . time() . '.pdf');
//
//        $path = Yii::getAlias('@app/' . $localPath);
//
//        file_put_contents($path, $generator->render());

        $generator->render();

        echo $localPath;
    }

    public function actionGenerateGenerator($honorGeneratorId)
    {
        $honorGenerator = HonorGenerator::findOne(['id' => $honorGeneratorId]);

        if(empty($honorGenerator)){
            echo 'Cant find generator';
            exit;
        }

        $honorGenerator->setStarted();
        $honorGenerator->save();

        $withInfo = $honorGenerator->withInfo;
        $stage = null;
        $withGram = $honorGenerator->with_gram;
        $contests = $honorGenerator->contests;
        $contestTypes = $honorGenerator->contestTypes;
        $contestCategories = $honorGenerator->contestCategories;
        $teams = $honorGenerator->teams;

        // Для каждой школы выбрать все команды, капитаны которых на каком либо этапе были в этому школе, сортировка по класса.

        ini_set("memory_limit","16336M");

        $offset = 0;
        $limit = 300;

        $localPath = Yii::getAlias('@texPdf/grams_' . time());

        $generator = new HtmlGenerator(1000, 1500, $localPath, $honorGenerator->by_school, $honorGenerator->by_team);

        $teamCount = Team::find()->count();
        $schoolCount = PeopleSchool::find()->count();

        $honorGenerator->all = $teamCount + $schoolCount;
        $honorGenerator->proceed = 0;
        $honorGenerator->save();

        $count = 0;

        while($peopleSchools = PeopleSchool::find()->orderBy(['number' => SORT_ASC])->offset($offset)->limit($limit)->all()){
            foreach ($peopleSchools as $peopleSchool) {
                $count++;

                if($count >= 20){
                    $honorGenerator->proceed += 20;
                    if(!$honorGenerator->save()){
                        var_dump($honorGenerator->errors);
                        exit;
                    }
                    $count = 0;
                }

                ini_set('max_execution_time', 5000);

                $generator->addSchool($peopleSchool, $withInfo, $stage, $withGram, $contests, $contestTypes, $teams, [$honorGenerator, 'addFile'], [$honorGenerator, 'add']);
            }
            $offset += $limit;
        }

        ini_set('max_execution_time', 5000);
        $generator->addSchool(null, $withInfo, $stage, $withGram, $contests, $contestTypes, $teams, [$honorGenerator, 'addFile'], [$honorGenerator, 'add']);

//        $localPath = Yii::getAlias('@texPdf/grams_' . time() . '.pdf');
//
//        $path = Yii::getAlias('@app/' . $localPath);
//
//        file_put_contents($path, $generator->render());

        $generator->render([$honorGenerator, 'addFile']);

        $honorGenerator->setFInished();
        $honorGenerator->save();

        echo $localPath;
    }
}
