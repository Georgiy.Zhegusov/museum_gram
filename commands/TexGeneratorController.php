<?php
namespace app\commands;

use app\generator\TexGenerator;
use app\models\DownloadFile;
use app\models\HonorsPeople;
use app\models\UploadFile;
use app\parsers\BaseParser;
use app\parsers\ContestRawObject;
use yii\base\Exception;
use yii\console\Controller;


class TexGeneratorController extends Controller
{
    public function actionTexGenerateDemon($workerCount = 20)
    {
        if($this->isDemonStarted()){
            echo 'Demon already started';
            exit;
        }

        $lastUsedHonorsPeoples = [];

        /** @var HonorsPeople $nextHonorPeople */
        while ($nextHonorPeople = HonorsPeople::getForGenerate())
        {
            if(isset($lastUsedHonorsPeoples[$nextHonorPeople->id])){
                continue;
            }

            //Ждать пока освободится лимит воркером
            while ($this->getWorkerCount() >= $workerCount){
                sleep(1);
            }

            $this->startGenerate($nextHonorPeople->id);

            foreach ($lastUsedHonorsPeoples as $id=>$counter) {
                if($counter == 0){
                    unset($lastUsedHonorsPeoples[$id]);
                }
                else{
                    $lastUsedHonorsPeoples[$id] = $counter - 1;
                }
            }

            $linkedHonorsPeoples = $nextHonorPeople->linkedHonorsPeople;
            if(empty($linkedHonorPeoples)){
                $linkedHonorsPeoples = [];
            }
            $linkedHonorsPeoples[] = $nextHonorPeople;

            foreach ($linkedHonorsPeoples as $linkedHonorsPeople) {
                $lastUsedHonorsPeoples[$linkedHonorsPeople->id] = $workerCount;
            }
        }
    }

    /**
     * @param $honorsPeopleId
     */
    private function startGenerate($honorsPeopleId)
    {
        exec("/usr/local/bin/php " . \Yii::getAlias("@app/yii tex-generator/tex-generate-worker $honorsPeopleId") . " > /dev/null &");
    }

    /**
     * @return int
     */
    private function getWorkerCount()
    {
        exec('ps faux | grep tex-generator/tex-generate-worker', $out);

        return count($out)-2;
    }

    /**
     * @return bool
     */
    private function isDemonStarted()
    {
        exec('ps faux | grep tex-generator/tex-generate-demon', $out);

        return count($out) > 3;
    }

    /**
     * method for generate one pdf for one honorsPeople
     *
     * @param $honorsPeopleId
     */
    public function actionTexGenerateWorker($honorsPeopleId)
    {
        $honorsPeople = HonorsPeople::findOne($honorsPeopleId);

        if(empty($honorsPeople)){
            echo "Cant find honorsPeople with this id \n";
            return;
        }

        /** @var HonorsPeople[] $linkedHonorsPeoples */
        $linkedHonorsPeoples = $honorsPeople->linkedHonorsPeople;
        if(empty($linkedHonorsPeoples)){
            $linkedHonorsPeoples = [];
        }

        $linkedHonorsPeoples[] = $honorsPeople;

        try {
            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->setGenerationStart();
                $linkedHonorPeople->save();
            }

            //генерация TeX
            $generator = new TexGenerator();

            if(!is_null($honorsPeople->path) && file_exists(\Yii::getAlias('@app/' . $honorsPeople->path))) {
                unlink(\Yii::getAlias('@app/' . $honorsPeople->path));
            }

            $path = $generator->generate($honorsPeople);

            $pageCount = TexGenerator::getPageCount(\Yii::getAlias('@app/' . $path));

            if($pageCount != 1){
                throw new Exception('HonorsPeople pdf must contain only one page');
            }

            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->path = $path;
                $linkedHonorPeople->setGenerated();
                $linkedHonorPeople->save();
            }
        }
        catch (\Exception $e) {
            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->setGenerationError();
                $linkedHonorPeople->save();
            }
            var_dump($e->getMessage());

            echo 'Error' . "\n";
        }
    }

    /**
     * method for generate one pdf for one honorsPeople
     */
    public function actionTexGenerateFile()
    {
        $honorsPeople = HonorsPeople::find()
            ->andWhere(['id' => 2])
            ->one();

        $pdf = new \mPDF();
    }
}