<?php
namespace app\commands;

use app\generator\TexGenerator;
use app\models\Contest;
use app\models\DownloadFile;
use app\models\HonorsPeople;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCoefficient;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamType;
use app\models\UploadFile;
use app\parsers\BaseParser;
use app\parsers\ContestRawObject;
use yii\base\Exception;
use yii\console\Controller;


class TestController extends Controller
{
    public function actionTexGenerateDemon($field, $value, $sleep=0)
    {
        $db = \Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            /** @var Contest $contest */
            $contest = Contest::findByNumber(15);
            $contest->$field = $value;
            sleep($sleep);
            $contest->save();
            $transaction->commit();
            echo "All ok \n";
        } catch (Exception $e) {
            $transaction->rollBack();
            echo "not good \n";
        }
    }

    public function actionGenNominations()
    {
        $nomination = Nomination::findOne(['id' => 4]);
       $nomination->generateForAll();
        echo 123 . "\n";
    }

    public function actionSetCommandTypes()
    {
        $offset = 0;
        $limit = 300;

        /**@var $teams TeamInfo[] */
        $teams = TeamInfo::find()->limit($limit)->offset($offset)->all();

        while (!empty($teams)) {
            echo $offset . "\n";
            foreach ($teams as $team) {
                set_time_limit(30);
                try {
                    $team->recalculateTeamType();
                    $team->save();
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->number . "\n";
                }
            }
            $offset += $limit;
            $teams = TeamInfo::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionSetTeamSchools()
    {
        $offset = 0;
        $limit = 300;

        /**@var $teams Team[] */
        $teams = Team::find()->limit($limit)->offset($offset)->all();

        while (!empty($teams)) {
            echo $offset . "\n";
            foreach ($teams as $team) {
                set_time_limit(30);
                try {
                    $school_id = null;
                    $classNum = null;

                    $teamInfo = $team->getTeamInfo(1);
                    if(!empty($teamInfo)) {
                        $captain = $teamInfo->captain;
                        if (!empty($captain)) {
                            $school_id = $captain->school_id;
                            $classNum  = $captain->classNum;
                        }

                        if (empty($school_id) || empty($classNum)) {
                            foreach ($teamInfo->tommies as $tommy) {
                                $school_id = empty($school_id) ? $tommy->school_id : $school_id;
                                $classNum  = empty($classNum) ? $tommy->classNum : $classNum;
                            }
                        }
                    }

                    if(empty($school_id) || empty($classNum)){
                        $teamInfo = $team->getTeamInfo(2);
                        if(!empty($teamInfo)) {
                            $captain = $teamInfo->captain;
                            if (!empty($captain)) {
                                $school_id = empty($school_id) ? $captain->school_id : $school_id;
                                $classNum  = empty($classNum) ? $captain->classNum : $classNum;
                            }

                            if (empty($school_id) || empty($classNum)) {
                                foreach ($teamInfo->tommies as $tommy) {
                                    $school_id = empty($school_id) ? $tommy->school_id : $school_id;
                                    $classNum  = empty($classNum) ? $tommy->classNum : $classNum;
                                }
                            }
                        }
                    }

                    $team->school_id = $school_id;
                    $team->classNum = $classNum;
                    $team->save();
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->number . ' in ' . $e->getLine() . "\n";
                }
            }
            $offset += $limit;
            $teams = Team::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionSetTeamAgeCategories()
    {
        $offset = 0;
        $limit = 300;

        /**@var $teams Team[] */
        $teams = Team::find()->limit($limit)->offset($offset)->all();

        while (!empty($teams)) {
            echo $offset . "\n";
            foreach ($teams as $team) {
                set_time_limit(30);
                try {
                    $teamInfo = $team->getTeamInfo(1);

                    if(empty($teamInfo)){
                        continue;
                    }

                    $captain = $teamInfo->captain;

                    $ageCategory = AgeCategory::findByClass($captain->classNum);

                    if(empty($ageCategory)){
                        $teamInfo->age_category_id = null;
                    }
                    else{
                        $teamInfo->age_category_id = $ageCategory->id;
                    }

                    $teamInfo->save();
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->number . ' in ' . $e->getLine() . "\n";
                }
            }
            $offset += $limit;
            $teams = Team::find()->limit($limit)->offset($offset)->all();
        }
    }



    public function actionSetMaxValues()
    {
        $offset = 0;
        $limit = 300;

        /**@var $ageCategoryLinks NominationAgeCategoryLink[] */
        $ageCategoryLinks = NominationAgeCategoryLink::find()->limit($limit)->offset($offset)->all();

        $count = 0;
        while (!empty($ageCategoryLinks)) {
            foreach ($ageCategoryLinks as $team) {
                set_time_limit(300);
                echo $count++ . "\n";
                try {
                    $team->recalculate();
                    $team->save();
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->number . "\n";
                }
            }
            $offset += $limit;
            $ageCategoryLinks = NominationAgeCategoryLink::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionSetNominationStatuses()
    {
        $offset = 0;
        $limit = 300;

        /**@var $ageCategoryLinks NominationAgeCategoryLink[] */
        $ageCategoryLinks = NominationAgeCategoryLink::find()->limit($limit)->offset($offset)->all();

        $count = 0;
        while (!empty($ageCategoryLinks)) {
            foreach ($ageCategoryLinks as $team) {
                set_time_limit(300);
                echo $count++ . "\n";
                try {
                    $team->calculateStatuses();
                    $team->save();
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->id . "\n";
                }
            }
            $offset += $limit;
            $ageCategoryLinks = NominationAgeCategoryLink::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionRecalculateBalls()
    {
        $nominationId = 1;

        $nomination = Nomination::findOne(['number' => $nominationId]);

        if(!empty($nomination)){
            $nomination->recalculate();
        }
        $offset = 0;
        $limit = 300;

        /**@var $teams MuseumBall[] */
        $teams = MuseumBall::find()->limit($limit)->offset($offset)->all();
        while (!empty($teams)) {
            echo $offset . "\n";
            foreach ($teams as $team) {
                set_time_limit(30);
                try {
                    $lastValue = $team->value;
                    $team->recalculate();

                    if($lastValue !== $team->value){
                        $team->save();
                    }
                } catch (\Exception $e) {
                    echo $e->getMessage() . ' for ' . $team->team->number . "\n";
                }
            }

            $offset += $limit;
            $teams = MuseumBall::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionCreateContests()
    {
        /** @var Concurs[] $concurses */
        $concurses = Concurs::find()->all();

        foreach ($concurses as $concurs) {
            $concurs->updateContest();
            $concurs->save();
        }

        /** @var Nomination[] $nominations */
        $nominations = Nomination::find()->all();

        foreach ($nominations as $nomination) {
            $nomination->updateContest();
            $nomination->save();
        }
    }

    public function actionCreateMuseumBallHonor()
    {
        ini_set("memory_limit","8168M");

        $offset = 0;
        $limit = 300;

        /**@var $teams NominationBall[] */
        $nominationBalls = MuseumBall::find()->limit($limit)->offset($offset)->orderBy(['id' => SORT_ASC])->all();
        while (!empty($nominationBalls)) {
            echo $offset . "\n";
            /** @var MuseumBall $nominationBall */
            foreach ($nominationBalls as $nominationBall) {
                ini_set('max_execution_time', 9999);
                $nominationBall->generateHonors();
            }

            $offset += $limit;
            $nominationBalls = MuseumBall::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionCreateNominationBallHonor()
    {
        ini_set("memory_limit","8168M");

        $offset = 0;
        $limit = 300;

        /**@var $teams NominationBall[] */
        $nominationBalls = NominationBall::find()->limit($limit)->offset($offset)->all();
        while (!empty($nominationBalls)) {
            echo $offset . "\n";
            /** @var NominationBall $nominationBall */
            foreach ($nominationBalls as $nominationBall) {
                ini_set('max_execution_time', 9999);
                $nominationBall->generateHonors();
            }

            $offset += $limit;
            $nominationBalls = NominationBall::find()->limit($limit)->offset($offset)->all();
        }
    }

    public function actionDeleteEmptyNames()
    {
        $teamInfos = TeamInfo::find()
            ->andWhere(['name' => ['Команда этапа 1', 'Пустое название']])
            ->all();
        foreach ($teamInfos as $item) {
            $item->name = null;
            $item->save();
        }
    }
}