<?php
namespace app\commands;

use app\models\DownloadFile;
use app\models\UploadFile;
use app\parsers\BaseParser;
use app\parsers\CoeffRawObject;
use app\parsers\ConcursBallRawObject;
use app\parsers\ConcursRawObject;
use app\parsers\ContestRawObject;
use app\parsers\HonorRawObject;
use app\parsers\MentionRawObject;
use app\parsers\MuseumCorrRawObject;
use app\parsers\MuseumInvRawObject;
use app\parsers\MuseumPhotoRawObject;
use app\parsers\MuseumRawObject;
use app\parsers\NominationBallPercentRawObject;
use app\parsers\NominationBallRawObject;
use app\parsers\NominationBallStatusRawObject;
use app\parsers\NominationCriteriaObject;
use app\parsers\NominationMaxRawObject;
use app\parsers\NominationRawObject;
use app\parsers\ParkRawObject;
use app\parsers\SchoolRawObject;
use app\parsers\TeamBaseRawObject;
use app\parsers\TeamEsrRawObject;
use app\parsers\TeamPeopleBase2StageRawObject;
use app\parsers\TeamPeopleBaseRawObject;
use app\parsers\TeamResultRawObject;
use app\parsers\TeamStatRawObject;
use yii\base\Exception;
use yii\console\Controller;


class UploadController extends Controller
{
    public function actionParseFile($id)
    {
        $file = UploadFile::findOne($id);

        if (empty($file)) {
            throw new Exception('Cant find needed file');
        }

        try {
            switch ($file->type) {
                case UploadFile::TYPE_CONTEST:
                    $rowObjectClassName = ContestRawObject::className();
                    break;
                case UploadFile::TYPE_MOSOLIMP:
                    $rowObjectClassName = TeamEsrRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM:
                    $rowObjectClassName = MuseumRawObject::className();
                    break;
                case UploadFile::TYPE_COEFFICIENT:
                    $rowObjectClassName = CoeffRawObject::className();
                    break;
                case UploadFile::TYPE_PARK:
                    $rowObjectClassName = ParkRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_CORR:
                    $rowObjectClassName = MuseumCorrRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_INV:
                    $rowObjectClassName = MuseumInvRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_PHOTO:
                    $rowObjectClassName = MuseumPhotoRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE:
                    $rowObjectClassName = TeamBaseRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS:
                    $rowObjectClassName = NominationRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_BALLS:
                    $rowObjectClassName = NominationBallRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_RESULT:
                    $rowObjectClassName = TeamResultRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_CRITERIA:
                    $rowObjectClassName = NominationCriteriaObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_PERCENT:
                    $rowObjectClassName = NominationBallPercentRawObject::className();
                    break;
                case UploadFile::TYPE_CONCURS:
                    $rowObjectClassName = ConcursRawObject::className();
                    break;
                case UploadFile::TYPE_CONCURS_BALLS:
                    $rowObjectClassName = ConcursBallRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_STAT:
                    $rowObjectClassName = TeamStatRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_RESULT:
                    $rowObjectClassName = NominationBallStatusRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE_PEOPLE:
                    $rowObjectClassName = TeamPeopleBaseRawObject::className();
                    break;
                case UploadFile::TYPE_SCHOOL:
                    $rowObjectClassName = SchoolRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE_PEOPLE_2STAGE:
                    $rowObjectClassName = TeamPeopleBase2StageRawObject::className();
                    break;
                case UploadFile::TYPE_HONOR:
                    $rowObjectClassName = HonorRawObject::className();
                    break;
                case UploadFile::TYPE_MENTION:
                    $rowObjectClassName = MentionRawObject::className();
                    break;
                default:
                    throw new Exception('Wrong type');
            }

            $parser = new BaseParser($rowObjectClassName);

            $file->clearResults();

            $file->setStarted();
            $file->save();

            $parser->parseUpload($file);

            $file->setFinished();
            $file->save();
        } catch (\Exception $e) {
            $file->setError($e->getMessage());
            $file->save();

            $file->addResultError($e->getMessage(), null);
            $file->saveResults();
        }
    }

    public function actionGenerateFile($id)
    {
        $file = DownloadFile::findOne($id);

        if (empty($file)) {
            throw new Exception('Cant find needed file');
        }

        try {
            switch ($file->type) {
                case UploadFile::TYPE_CONTEST:
                    $rowObjectClassName = ContestRawObject::className();
                    break;
                case UploadFile::TYPE_MOSOLIMP:
                    $rowObjectClassName = TeamEsrRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM:
                    $rowObjectClassName = MuseumRawObject::className();
                    break;
                case UploadFile::TYPE_COEFFICIENT:
                    $rowObjectClassName = CoeffRawObject::className();
                    break;
                case UploadFile::TYPE_PARK:
                    $rowObjectClassName = ParkRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_CORR:
                    $rowObjectClassName = MuseumCorrRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_INV:
                    $rowObjectClassName = MuseumInvRawObject::className();
                    break;
                case UploadFile::TYPE_MUSEUM_PHOTO:
                    $rowObjectClassName = MuseumPhotoRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE:
                    $rowObjectClassName = TeamBaseRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS:
                    $rowObjectClassName = NominationRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_BALLS:
                    $rowObjectClassName = NominationBallRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_RESULT:
                    $rowObjectClassName = TeamResultRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_CRITERIA:
                    $rowObjectClassName = NominationCriteriaObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_PERCENT:
                    $rowObjectClassName = NominationBallPercentRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_MAXIMUM:
                    $rowObjectClassName = NominationMaxRawObject::className();
                    break;
                case UploadFile::TYPE_CONCURS:
                    $rowObjectClassName = ConcursRawObject::className();
                    break;
                case UploadFile::TYPE_CONCURS_BALLS:
                    $rowObjectClassName = ConcursBallRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_STAT:
                    $rowObjectClassName = TeamStatRawObject::className();
                    break;
                case UploadFile::TYPE_NOMINATIONS_RESULT:
                    $rowObjectClassName = NominationBallStatusRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE_PEOPLE:
                    $rowObjectClassName = TeamPeopleBaseRawObject::className();
                    break;
                case UploadFile::TYPE_SCHOOL:
                    $rowObjectClassName = SchoolRawObject::className();
                    break;
                case UploadFile::TYPE_TEAM_BASE_PEOPLE_2STAGE:
                    $rowObjectClassName = TeamPeopleBase2StageRawObject::className();
                    break;
                case UploadFile::TYPE_HONOR:
                    $rowObjectClassName = HonorRawObject::className();
                    break;
                case UploadFile::TYPE_MENTION:
                    $rowObjectClassName = MentionRawObject::className();
                    break;
                default:
                    throw new Exception('Wrong type');
            }

            $parser = new BaseParser($rowObjectClassName);

            $file->setStarted();
            $file->save();

            if ($file->isTemplate) {
                $parser->generateTemplate($file);
            } else {
                $parser->generateDownload($file);
            }

            $file->setFinished();
            $file->save();
        } catch (\Exception $e) {
            var_dump($e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
            exit;
            $file->setError();
            $file->save();
        }
    }
}