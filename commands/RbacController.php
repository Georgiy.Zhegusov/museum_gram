<?php
namespace app\commands;

use yii;
use yii\console\Controller;


class RbacController extends Controller
{
	public function actionInit()
	{
		$auth =  Yii::$app->authManager;
		// clear all
		$auth->removeAll();

	////Gram block
		// add "gramPermission" permission
		$gramPermission = $auth->createPermission('gramPermission');
        $gramPermission->description = 'can do some with gram';
		$auth->add($gramPermission);

	// create roles
		$root  = $auth->createRole('root');
		$admin  = $auth->createRole('admin');
		$gramRole  = $auth->createRole('gram');

	//register roles in a system
		$auth->add($root);
		$auth->add($admin);
		$auth->add($gramRole);

	//Add rules for guest

	//Add guest rules to expert
		$auth->addChild($root,$admin);
		$auth->addChild($admin,$gramRole);
		$auth->addChild($gramRole,$gramPermission);
	}
}