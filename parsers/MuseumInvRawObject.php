<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamId
 * @property string $museumId
 * @property string $isReady
 * @property string $value
 */
class MuseumInvRawObject extends RawObject
{
    protected static $columns = [
        'teamId'   => 'mosOlimpId',
        'museumId' => 'Музей (id)',
        'isReady'  => 'Оценка',
        'value'    => 'dateVisit',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        if (!$this->isReady) {
            return [];
        }

        $museumNumber = (integer)$this->museumId;

        /** @var Museum $museum */
        $museum = Museum::findByNumber($museumNumber);

        if (empty($museum)) {
            throw new Exception('Cant find museum ' . $museumNumber);
        }

        if (is_null($this->teamId)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamId = (integer)$this->teamId;
        $team   = Team::findByMosolimp($teamId);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->teamId);
        }

        $newBall = false;

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $museum->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $museum->id,
                'team_id'   => $team->id
            ]);
        }

        $this->value = floatval(is_null($this->value) ? 0 : $this->value);

        if (
            !$museumBall->isNewRecord
            && $museumBall->correspondence_round == $this->value
        ) {
            return [];
        }

        $museumBall->correspondence_round = $this->value;

        try {
            $this->trySave($museumBall);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumBall->id,
                'message' => 'Добавлен баллы команде ' . $this->teamId . ' для музея ' . $this->museumId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumBall->id,
                'message' => 'Обновлены баллы команде ' . $this->teamId . ' для музея ' . $this->museumId
            ];
        }

        return $result;
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
        $teamIds = $museumBall->team->mosolimpIds;

        if (!empty($teamIds)) {
            $teamId = array_pop($teamIds);
            $teamId = $teamId->number;
        } else {
            $teamId = null;
        }
        $this->teamId   = $teamId;
        $this->museumId = $museumBall->museum_id;
        $this->isReady  = 1;
        $this->value    = $museumBall->correspondence_round;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->andWhere(['>','correspondence_round','0'])
            ->count();
    }
}