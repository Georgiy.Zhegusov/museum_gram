<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string code
 * @property string anket
 * @property string teamEsr
 * @property string surname
 * @property string name
 * @property string patronomic
 * @property string class
 * @property string pastClass
 * @property string way
 * @property string convoy
 * @property string pastTeams
 * @property string tommies
 * @property string peopleCount
 * @property string friends
 * @property string phone
 * @property string schoolEsr
 */
class TeamPeopleBase2StageRawObject extends RawObject
{
    protected static $columns = [
        'code'        => 'Код (не менять)',
        'anket'       => 'код анкеты',
        'teamEsr'     => 'Код участника или команды',
        'surname'     => 'Фамилия',
        'name'        => 'Имя',
        'patronomic'  => 'Отчество',
        'class'       => 'Класс',
        'pastClass'   => 'Класс зачёта на отборочном этапе',
        'way'         => 'Маршрут какой категории будет проходить команда (Описание маршрутов)*',
        'convoy'      => 'ФИО совершеннолетнего сопровождающего на призовой игре*',
        'pastTeams'   => 'Список команды на призовой игре.',
        'tommies'     => 'Перечислите через запятую фамилии и имена всех школьников, кроме капитана, которые будут принимать участие в призовой игре.',
        'peopleCount' => 'Количество участников команды (школьников) включая капитана на призовой игре*',
        'friends'     => 'Список кодов команд или участников, с которыми сформирована объединённая команда на призовую игру (если есть)',
        'phone'       => 'Контактный телефон для связи во время призовой игры (актуальный!)',
        'cancel'      => 'Отказаться от участия',
        'schoolEsr'   => 'Логин школы в системе СтатГрад*'
    ];

    const NEEDED_STAGE = 2;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        $newTeam = false;
        if (empty($team)) {
            $newTeam = true;

            $team = new Team([
                'number' => $teamEsr
            ]);
        }

        $this->trySave($team);

        $teamInfo = $team->getTeamInfo(static::NEEDED_STAGE);

        if (empty($teamInfo)) {
            $teamInfo = new TeamInfo([
                'team_id' => intval($team->id),
                'name'    => '',
                'stage'   => static::NEEDED_STAGE
            ]);
            $this->trySave($teamInfo);
        }

        $lastSchoolId = $this->schoolEsr;

        if($lastSchoolId != 'нет') {

            if (substr($this->schoolEsr, 0, 3) == 'sch') {
                $this->schoolEsr = substr($this->schoolEsr, 3);
            }

            $schoolId = $this->schoolEsr;

            if (!empty($schoolId) && ctype_digit($this->schoolEsr) && mb_strlen($lastSchoolId) - mb_strlen($this->schoolEsr) == 3) {
                $this->schoolEsr = intval($this->schoolEsr);
                $school         = PeopleSchool::findByNumber($this->schoolEsr);
                if (empty($school)) {
                    $school = new PeopleSchool([
                        'number' => intval($this->schoolEsr),
                        'text'   => null
                    ]);

                    $this->trySave($school);
                }
            }
        }

        $captain = $teamInfo->captain;

        $classNum = null;
        $classText = null;

        if (empty($captain)) {
            $captain = new People([
                'team_info_id' => $teamInfo->id,
                'role'         => People::ROLE_CAPTAIN
            ]);
        }

        if (!ctype_digit($this->class) && !is_int($this->class) && !is_float($this->class)) {
            $classText = $this->class;
            $classNum  = 1;
        } else {
            $classNum = intval($this->class);
        }

        $captain->name       = $this->name;
        $captain->surname    = $this->surname;
        $captain->patronomic = $this->patronomic;
        $captain->classNum   = $classNum;
        $captain->classText  = $classText;
        $captain->school_id  = isset($school) ? $school->id : null;
        $captain->schoolText = null;

        $this->trySave($captain);

        $tommies = $this->tommies;

        $newTommies = explode(',', $tommies);

        $existedTommies = $teamInfo->tommies;

        foreach ($newTommies as $tommy) {
            $tommy = explode(' ' ,trim($tommy));

            $surname = trim(ArrayHelper::getValue($tommy, 0, ''));
            $name = trim(ArrayHelper::getValue($tommy, 1, ''));

            $this->addTommy(
                $existedTommies,
                $teamInfo,
                $name,
                $surname,
                $classNum,
                $classText,
                isset($school) ? $school->id : null);

        }

        foreach ($existedTommies as $tommy) {
            $tommy->delete();
        }

        $result = [];

        if ($newTeam) {
            $result[] = [
                'isNew'   => true,
                'id'      => $team->id,
                'message' => 'Создана команда ' . $this->teamEsr
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $team->id,
                'message' => 'Обновлена команда ' . $this->teamEsr
            ];
        }

        return $result;
    }

    /**
     * @param $tommies
     * @param $teamInfo
     * @param $name1
     * @param $surname1
     * @param $classNum
     * @param $classText
     * @param $capSchoolId
     */
    private function addTommy(&$tommies, $teamInfo, $name1, $surname1, $classNum, $classText, $capSchoolId)
    {
        if (empty($name1) && empty($surname1)) {
            return;
        }

        $findedTommy = false;
        foreach ($tommies as $id => $tommy) {
            if ($tommy->name == $name1
                && $tommy->surname == $surname1
            ) {
                $findedTommy = $tommy;
                unset($tommies[$id]);
                break;
            }
        }

        if (empty($findedTommy)) {
            $findedTommy = new People([
                'team_info_id' => $teamInfo->id,
                'role'         => People::ROLE_TOMMY,
                'name'         => $name1,
                'surname'      => $surname1,
                'patronomic'   => null
            ]);
        }

        $findedTommy->classNum   = $classNum;
        $findedTommy->classText  = $classText;
        $findedTommy->school_id  = $capSchoolId;
        $findedTommy->schoolText = null;

        $this->trySave($findedTommy);
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Team::find()->count();
    }
}