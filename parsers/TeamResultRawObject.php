<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use app\models\TeamType;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamId
 * @property string $teamESR
 * @property string $museumId
 * @property string $correspondence_round
 * @property string $internal_round
 * @property string $coefficient
 * @property string $sum
 * @property string $date_4_1
 * @property string $status
 * @property string $museumCount
 * @property string $parkCount
 * @property string $estateCount
 * @property string $type
 * @property string $school
 * @property string $classNum
 * @property string $photo_valid
 */
class TeamResultRawObject extends RawObject
{
    protected static $columns = [
        'teamId'               => 'Лешин номер',
        'teamESR'              => 'ЕСР номер',
        'museumId'             => 'Музей номер',
        'correspondence_round' => 'Заочка',
        'internal_round'       => 'Очка',
        'coefficient'          => 'Коэффициент',
        'sum'                  => 'Сумма',
        'date_4_1'             => 'Дата/4_1',
        'status'               => 'Статус',
        'museumCount'          => 'Кол-во музеев',
        'parkCount'            => 'Кол-во парков',
        'estateCount'          => 'Кол-во усадеб',
        'type'                 => 'тип команды',
        'school'               => 'школа',
        'classNum'             => 'класс',
        'photo_valid'          => 'Валидация фото'
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        if($this->museumId == 0){
            //here command stat, just for check
            if (is_null($this->teamESR)) {
                throw new Exception('Cant find team with empty number');
            }

            $teamEsr = (integer)$this->teamESR;
            $team    = Team::findByNumber($teamEsr);

            //here command stat, just for check
            if (is_null($team)) {
                throw new Exception('Cant find team ' . $this->teamESR);
            }

            $errors = [];

            if(intval($team->sum * 100) != intval($this->sum * 100)){
                $errors[] = 'Разная сумма ' . $team->sum . '!=' . $this->sum;
            }

            if($team->fourOne != $this->date_4_1){
                $errors[] = 'Разное 4_1';
            }

            if($team->getCount(Museum::TYPE_MUSEUM) != $this->museumCount){
                $errors[] = 'Разное количество музеев';
            }

            if($team->getCount(Museum::TYPE_ESTATE) != $this->estateCount){
                $errors[] = 'Разное количество усадеб';
            }

            if($team->getCount(Museum::TYPE_PARK) != $this->parkCount){
                $errors[] = 'Разное количество парков';
            }

            if(!empty($errors)){
                throw new \Exception('Errors for ' . $teamEsr . ' ' . implode(';', $errors));
            }

            return [];
        }

        //here save museumBalls

        $museumNumber = (integer)$this->museumId;

        /** @var Museum $museum */
        $museum = Museum::findByNumber($museumNumber);

        if (empty($museum)) {
            throw new Exception('Cant find museum ' . $museumNumber);
        }

        if (is_null($this->teamESR)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamESR;
        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->teamEsr);
        }

        if($this->date_4_1 != '0000-01-01'){
            $date = new \DateTime($this->date_4_1);
        }
        else{
            $date = new \DateTime();
        }

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $museum->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        $newBall = false;

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $museum->id,
                'team_id'   => $team->id
            ]);
        }

        $internalRound = floatval(is_null($this->internal_round) ? 0 : $this->internal_round);

        if($internalRound < 0){
            $internalRound = null;
        }

        $correspondenceRound = floatval(is_null($this->correspondence_round) ? 0 : $this->correspondence_round);

        if($correspondenceRound < 0){
            $correspondenceRound = null;
        }

        $value = floatval(is_null($this->sum) ? 0 : $this->sum);

        if($value < 0){
            $value = null;
        }

        if (
            !$museumBall->isNewRecord
            && $museumBall->internal_round === $internalRound
            && $museumBall->correspondence_round === $correspondenceRound
            && $museumBall->photo_status == $this->photo_valid
            && $museumBall->visitDate === $date->format('Y-m-d')
        ) {
            unset($team);
            unset($museum);
            unset($museumBall);
            return [];
        }

        $museumBall->internal_round = $internalRound;
        $museumBall->correspondence_round = $correspondenceRound;
        $museumBall->visitDate            = $date->format('Y-m-d');
        $museumBall->photo_status         = (bool)($this->photo_valid);

        if($museumBall->coefficient == 0 && $this->sum > 0){
            $museumBall->visit_people_count = $team->getTeamInfo(MuseumBall::neededStage)->participantCount;
        }

        $this->trySave($museumBall);

        $this->coefficient = floatval($this->coefficient);

        if(intval($museumBall->coefficient * 100) != intval(floatval($this->coefficient) * 100)){

            $coefficient = $museumBall->coefficient;

            unset($team);
            unset($museum);
            unset($museumBall);

            throw new \Exception('Wrong coeffficient ' . $coefficient . '|' . $this->coefficient . ' for ' . $this->museumId . ' and team ' . $teamEsr);
        }

        $this->sum = floatval($this->sum);

        if(intval($museumBall->value * 100) != intval($value * 100)){

            $musValue = $museumBall->value;
            unset($team);
            unset($museum);
            unset($museumBall);

            throw new \Exception('Wrong value ' . $musValue . '|' . $value . ' for ' . $this->museumId . ' and team ' . $teamEsr);
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $id,
                'message' => 'Добавлен баллы команде ' . $this->teamESR . ' для музея ' . $this->museumId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $id,
                'message' => 'Обновлены баллы команде ' . $this->teamESR . ' для музея ' . $this->museumId
            ];
        }

        unset($museumBall);

        return $result;
    }

    /**
     * @param Team $team
     *
     * @return array
     */
    protected function load($team)
    {
        $mosolimpIds = $team->mosolimpIds;

        if (empty($mosolimpIds)) {
            $mosolimpId = null;
        } else {
            $mosolimpId = array_pop($mosolimpIds)->number;
        }

        $result = [];

        foreach ($team->museumBalls as $museumBall) {
            $local = new self([
                'teamId'               => $mosolimpId,
                'teamESR'              => $team->number,
                'museumId'             => $museumBall->museum->number,
                'correspondence_round' => $museumBall->correspondence_round,
                'internal_round'       => $museumBall->sum < 0 ? -1 : $museumBall->internal_round,
                'coefficient'          => $museumBall->coefficient,
                'sum'                  => $museumBall->sum,
                'date_4_1'             => $museumBall->visitDate,
                'photo_valid'          => $museumBall->photo_status ? 1 : 0
            ]);

            $result[] = $local;
        }

        unset($museumBall);

        $this->teamId      = $mosolimpId;
        $this->teamESR     = $team->number;
        $this->museumId    = 0;
        $this->sum         = $team->fourOne ? $team->sum : -1;
        $this->date_4_1    = $team->fourOne ? 1 : 0;

        switch ($team->status){
            case Team::STATUS_WINNER:
                $this->status = 2;
                break;
            case Team::STATUS_PRISE:
                $this->status = 1;
                break;
            default:
                $this->status = 0;
                break;
        }
        $this->museumCount = $team->getCount(Museum::TYPE_MUSEUM);
        $this->parkCount   = $team->getCount(Museum::TYPE_PARK);
        $this->estateCount = $team->getCount(Museum::TYPE_ESTATE);
        $teamInfo          = $team->getTeamInfo(1);

        $this->type        = 0;

        $captain = null;
        $teamType = null;
        if (!empty($teamInfo)) {
            $captain = $teamInfo->captain;

            $teamType = $teamInfo->teamType;
        }

        if (!empty($captain)) {
            $this->school   = $captain->school->esr;
            $this->classNum = $captain->classNum;
        }

        if(!empty($teamType)){
            switch ($teamType->baseForTeam){
                case TeamType::BASE_FOR_SOLO:
                    $this->type = 1;
                    break;
                case TeamType::BASE_FOR_TEAM:
                    $this->type = 2;
                    break;
                default:
                    $this->type = 0;
                    break;
            }
        }

        return $result;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Team::find()
            ->count();
    }
}