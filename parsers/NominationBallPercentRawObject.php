<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\museumResult\NominationMuseumLink;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $team_id
 * @property string $age_category
 * @property string $team_type
 */
class NominationBallPercentRawObject extends RawObject
{
    protected static $columns = [];

    private $_links = [];

    const NEEDED_STAGE = 1;

    public static function initObj($header=null)
    {
        $nominations = Nomination::find()
            ->all();

        static::$columns = [
            'team_id' => 'Команда',
            'age_category' => 'Возрастная категория',
            'team_type' => 'Тип зачета'
        ];

        foreach ($nominations as $nomination) {
            static::$columns[$nomination->id] = $nomination->name;
        }

        parent::initObj();
    }

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        return [];
    }

    /**
     * @param Team $team
     */
    protected function load($team)
    {
        $this->team_id = $team->number;

        /** @var TeamInfo $teamInfo */
        $teamInfo = $team->getTeamInfo(1);

        if(!empty($teamInfo) && !empty($teamInfo->ageCategory)){
            $this->age_category = $teamInfo->ageCategory->name;
        }

        if(!empty($teamInfo) && !empty($teamInfo->teamType)){
            $this->team_type = $teamInfo->teamType->baseForTeam;
        }

        foreach ($team->nominationBalls as $nominationBall) {
            $id = $nominationBall->nomination_id;
            $value = $nominationBall->value;

            $ageCategoryId = null;

            if(!empty($teamInfo)) {
                $ageCategoryId = $teamInfo->age_category_id;
            }

            /** @var NominationAgeCategoryLink $nominationAgeLink */
            $nominationAgeLink = $this->getNominationAgeLink($nominationBall->nomination_id, $ageCategoryId);

            if($nominationAgeLink && $nominationAgeLink->maxValue > 0){
                $this->$id = ($value / $nominationAgeLink->maxValue) * 100;
            }
            else{
                $this->$id = 0;
            }
        }
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Team::find()->count();
    }

    /**
     * @param $nominationId
     * @param $ageCategoryId
     *
     * @return NominationAgeCategoryLink
     */
    private function getNominationAgeLink($nominationId, $ageCategoryId)
    {
        if(!isset($this->_links[$nominationId])){
            $this->_links[$nominationId] = [];
        }

        if(!isset($this->_links[$nominationId][$ageCategoryId])){
            $this->_links[$nominationId][$ageCategoryId] = NominationAgeCategoryLink::findBy($nominationId, $ageCategoryId);
        }

        return $this->_links[$nominationId][$ageCategoryId];
    }
}