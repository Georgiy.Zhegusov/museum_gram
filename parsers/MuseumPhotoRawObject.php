<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamId
 * @property string $teamEsr
 * @property string $status
 * @property string $museumId
 */
class MuseumPhotoRawObject extends RawObject
{
    protected static $columns = [
        'teamId'   => 'mosOlimpId',
        'teamEsr'  => 'regid',
        'status'   => 'status',
        'museumId' => 'museumId'
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $museumNumber = (integer)$this->museumId;

        /** @var Museum $museum */
        $museum = Museum::findByNumber($museumNumber);

        if (empty($museum)) {
            throw new Exception('Cant find museum ' . $museumNumber);
        }

        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->teamEsr);
        }

        $newBall = false;

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $museum->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $museum->id,
                'team_id'   => $team->id
            ]);
        }

        if ($this->status == 1) {
            $this->status = true;
        } else {
            $this->status = false;
        }

        if (
            !$museumBall->isNewRecord
            && $museumBall->photo_status == $this->status
        ) {
            return [];
        }

        $museumBall->photo_status = $this->status;

        try {
            $this->trySave($museumBall);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumBall->id,
                'message' => 'Добавлена инфо о фото ' . $this->teamEsr . ' для музея ' . $this->museumId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumBall->id,
                'message' => 'Обновлены инфо о фото ' . $this->teamEsr . ' для музея ' . $this->museumId
            ];
        }

        return $result;
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
        $this->teamEsr  = $museumBall->team->number;
        $this->museumId = $museumBall->museum_id;
        $this->status   = $museumBall->photo_status;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->andWhere(['>', 'internal_round', '0'])
            ->count();
    }
}