<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\museumResult\NominationMuseumLink;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use app\models\TeamType;
use Symfony\Component\CssSelector\Tests\Parser\ReaderTest;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $codeName
 * @property string $ageName
 */
class NominationCriteriaObject extends RawObject
{
    /** @var Nomination[] */
    static $nominations = [];
    static $types = [];
    static $_ageCategories = [];

    public static function initObj($header=null)
    {
        $nominations = Nomination::find()
            ->all();

        static::$columns = [
            'codeName'              => 'Тип критерия',
            'ageName'                => 'Возрастная номинация',
        ];

        if($header) {
            $header = array_slice($header, 2);

            foreach ($header as $col) {
                $nomination = Nomination::findByString($col, true);
                if($nomination){
                    static::$columns['nomination_' . $nomination->id] = $nomination->name;
                    static::$nominations[$nomination->id] = $nomination;
                }
            }
        }

        static::$types = [
            'Победитель индивидуальный' => [
                'teamType' => TeamType::BASE_FOR_SOLO,
                'status'   => NominationBall::STATUS_WINNER
            ],
            'Призер индивидуальный' => [
                'teamType' => TeamType::BASE_FOR_SOLO,
                'status'   => NominationBall::STATUS_PRISE
            ],
            'Победитель командный' => [
                'teamType' => TeamType::BASE_FOR_TEAM,
                'status'   => NominationBall::STATUS_WINNER
            ],
            'Призер командный' => [
                'teamType' => TeamType::BASE_FOR_TEAM,
                'status'   => NominationBall::STATUS_PRISE
            ]
        ];

        parent::initObj();
    }

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Nomination::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $info = ArrayHelper::getValue(static::$types, $this->codeName, null);

        if(empty($info) || empty($info['teamType'])) {
            return [];
        }

        /** @var TeamType $teamType */
        $teamType = $info['teamType'];

        $nominationStatus = $info['status'];

        list($classMin,$classMax) = explode('-', $this->ageName);

        $ageCategory = $this->findAgeCategory($classMin, $classMax);

        if(empty($ageCategory)){
            throw new \Exception('Не могу найти возрастную категорию ' . $this->ageName);
        }

        foreach (static::$nominations as $nominationId => $nomination) {
            /** @var NominationAgeCategoryLink $nominationAgeLink */
            $nominationAgeLink = NominationAgeCategoryLink::findBy($nomination->id, $ageCategory->id);

            if(empty($nominationAgeLink)){
                continue;
            }

            $column = 'nomination_' . $nomination->id;

            $value = $this->$column;

            $value = str_replace(',', '.', $value);

            $value = floatval($value);

            $nominationAgeLink->setCriteria($teamType, $nominationStatus, $value);
            $nominationAgeLink->save();
        }

        $result = [];
        $result[] = [
            'isNew'   => false,
            'id'      => 1,
            'message' => 'Было обновлен критерий'
        ];

        return $result;
    }

    /**
     * @param Nomination $nomination
     */
    protected function load($nomination)
    {
        $this->number     = $nomination->number;
        $this->name     = $nomination->name;
        $this->onlyYour     = $nomination->onlyMyAge;

        $museumForMax = [];
        $museumLinks = $nomination->nominationsMuseums;
        foreach ($museumLinks as $museumLink) {
            if($museumLink->useForMax){
                $museumForMax[] = $museumLink->museum->number;
            }
        }
        
        $museums = $nomination->museums;
        $this->museums = implode(';', ArrayHelper::getColumn($museums, function ($museum) {
            return $museum->number;
        }));



        foreach ($nomination->ageCategories as $ageCategory) {
            $property = "cat_{$ageCategory->minClass}_{$ageCategory->maxClass}";
            try {
                $this->$property = 1;
            } catch (\Exception $e) {
                //ignore this
            }
        }
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Nomination::find()->count();
    }

    private static function findAgeCategory($classMin, $classMax)
    {
        if (!isset(static::$_ageCategories["{$classMin}_{$classMax}"])) {
            static::$_ageCategories["{$classMin}_{$classMax}"] = AgeCategory::find()
                ->andWhere(['minClass' => $classMin])
                ->andWhere(['maxClass' => $classMax])
                ->one();
        }

        return static::$_ageCategories["{$classMin}_{$classMax}"];
    }
}