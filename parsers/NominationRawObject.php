<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\museumResult\NominationMuseumLink;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $number
 * @property string $name
 * @property string $museums
 * @property string $museumsMax
 * @property string $onlyYour
 * @property string $ageCategory
 * @property string $minCount
 * @property string $operator
 * @property string $correspondenceRound
 * @property string $type
 */
class NominationRawObject extends RawObject
{
    protected static $columns = array(
        'number'              => '№ номинации',
        'name'                => 'номинации',
        'museums'             => 'список музеев',
        'museumsMax'          => 'музеи, которые учитываем',
        'onlyYour'            => 'только свои или все (все -1)',
        'ageCategory'         => 'возрастная категория',
        'minCount'            => 'Категория музея',
        'operator'            => 'Музей пока не принимает участников олимпиады',
        'correspondenceRound' => 'заочный тур',
        'type'                => 'Отображать музей на сайте',
    );

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Nomination::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $nominationNumber = (integer)$this->number;

        if (empty($nominationNumber)) {
            throw new Exception('Cant create nomination with empty id');
        }

        $museumTypeId = null;

        //Создаем номинацию
        $nomination    = Nomination::findByNumber($nominationNumber);
        $newNomination = false;
        if (empty($nomination)) {
            $newNomination = true;

            $nomination = new Nomination();

            $nomination->number = $nominationNumber;
        }

        $museums = $this->museums;
        if (!empty($museums)) {
            $museumNumbers = explode(',', $museums);
        } else {
            $museumNumbers = [];
        }
        $museumNumbers     = array_flip($museumNumbers);
        $museumLinkIds = $museumNumbers;

        $museumsMax = $this->museumsMax;
        if (!empty($museumsMax)) {
            $museumsMaxNumbers = explode(',', $museumsMax);
        } else {
            $museumsMaxNumbers = [];
        }
        $museumsMaxNumbers = array_flip($museumsMaxNumbers);

        $mustBeAgeCategories = [];
        if (!empty($this->ageCategory)) {
            $ageCategoryNames = explode(',', $this->ageCategory);

            foreach ($ageCategoryNames as $ageCategoryName) {
                list($minClass, $maxClass) = explode('-', $ageCategoryName);

                $ageCat = static::findAgeCategory($minClass, $maxClass);

                if (empty($ageCat)) {
                    throw new \Exception("Cant find age category $minClass-$maxClass");
                }

                $mustBeAgeCategories[$ageCat->id] = $ageCat;
            }
        } else {
            $mustBeAgeCategories = AgeCategory::find()->indexBy('id')->all();
        }
        $ageCategoryLinkIds = $mustBeAgeCategories;

        $needGenerate = true;

        //Для музеев проверяем, есть ли среди списка новые.
        if (!$newNomination) {
            $nominationMuseums = $nomination->museums;

            foreach ($nominationMuseums as $nominationMuseum) {
               if(isset($museumNumbers[$nominationMuseum->number])){
                   unset($museumLinkIds[$nominationMuseum->number]);
               }
            }

            //Если есть что сохранить, то значит сносим все к херам и пересоздаем (так быстрее), иначе просто обновляем статус и забиваем
            if(!empty($museumLinkIds)){
                $needGenerate = true;
            }
            else{
                $nominationMuseumLinks = $nomination->nominationsMuseums;

                foreach ($nominationMuseumLinks as $museumCategoryLink) {
                    $museumCategoryLink->useForMax = isset($museumsMaxNumbers[$museumCategoryLink->museum_id]);
                    $museumCategoryLink->save();
                }
            }

            /** @var NominationAgeCategoryLink[] $ageCategoriesLink */
            $ageCategoriesLink = $nomination->nominationsAgeCategories;

            foreach ($ageCategoriesLink as $ageCategoryLink) {
                if (isset($ageCategoryLinkIds[$ageCategoryLink->age_category_id])) {
                    unset($ageCategoryLinkIds[$ageCategoryLink->age_category_id]);
                }
            }

            //Если есть что сохранить, то значит сносим все к херам и пересоздаем (так быстрее), иначе просто обновляем статус и забиваем
            if(!empty($ageCategoryLinkIds) || $needGenerate){
                $needGenerate = true;
            }

            if($needGenerate){
                NominationAgeCategoryLink::deleteAll(['nomination_id' => $nomination->id]);
                NominationMuseumLink::deleteAll(['nomination_id' => $nomination->id]);
                NominationBall::deleteAll(['nomination_id' => $nomination->id]);
            }
        }

        $nomination->name      = $this->name;
        $nomination->onlyMyAge = intval(!$this->onlyYour);
        $nomination->minCount  = $this->minCount;
        $nomination->operator  = $this->operator ? Nomination::OPERATOR_COUNT : Nomination::OPERATOR_SUM;
        $nomination->type      = $this->type ? Nomination::TYPE_CONCURS : Nomination::TYPE_MUSEUM;
        $nomination->round     = (boolean)$this->correspondenceRound;

        $this->trySave($nomination);

        //Теперь если есть $needGenerate, то batchInsert, recalculate, generateAll, иначе ничего не делаем
        $updateCount = 0;

        if($needGenerate) {
            if($nomination->type == Nomination::TYPE_MUSEUM) {
                $justMuseums = Museum::find()
                    ->andWhere(['number' => array_keys($museumNumbers)])
                    ->indexBy('number')
                    ->all();
            }
            else{
                $justMuseums = Concurs::find()
                    ->andWhere(['number' => array_keys($museumNumbers)])
                    ->indexBy('number')
                    ->all();
            }

            $museumLinks = [];

            foreach ($museumNumbers as $museumNum => $true) {
                if (!isset($justMuseums[$museumNum])) {
                    throw new \Exception("Cant find museum $museumNum");
                } else {
                    $museum       = $justMuseums[$museumNum];
                    $museumLinks[] = new NominationMuseumLink([
                        'nomination_id' => $nomination->id,
                        'museum_id'     => $museum->id,
                        'useForMax'     => isset($museumsMaxNumbers[$museumNum])
                    ]);

                    $updateCount++;
                }
            }

            echo 'add ' . count($museumLinks) . ' museums';

            //add Museums
            if (count($museumLinks) > 0) {
                $inserted = Yii::$app->db->createCommand()->batchInsert(
                    NominationMuseumLink::tableName(),
                    $museumLinks[0]->attributes(),
                    $museumLinks)
                    ->execute();
            }

            $ageCategoryLinks = [];

            foreach ($mustBeAgeCategories as $mustBeAgeCategory => $ageCategory) {
                $ageCategoryLinks[] = new NominationAgeCategoryLink([
                    'nomination_id' => $nomination->id,
                    'age_category_id' => $ageCategory->id
                ]);
                $updateCount++;
            }

            //add Museums
            if (count($ageCategoryLinks) > 0) {
                $inserted = Yii::$app->db->createCommand()->batchInsert(
                    NominationAgeCategoryLink::tableName(),
                    $ageCategoryLinks[0]->attributes(),
                    $ageCategoryLinks)
                    ->execute();
            }

            $nomination->recalculate();
            $nomination->generateForAll();
        }

        $result = [];

        echo memory_get_usage() . "\n";

        if ($newNomination) {
            $result[] = [
                'isNew'   => true,
                'id'      => $nomination->id,
                'message' => 'Номинация ' . $nomination->name
            ];
        } else {
            if ($updateCount > 0) {
                $result[] = [
                    'isNew'   => false,
                    'id'      => $nomination->id,
                    'message' => 'Было обновлено ' . $updateCount . ' всяких записей для номинации ' . $nomination->name
                ];
            }
        }

        return $result;
    }

    /**
     * @param Nomination $nomination
     */
    protected function load($nomination)
    {
        $this->number     = $nomination->number;
        $this->name     = $nomination->name;
        $this->onlyYour     = $nomination->onlyMyAge;

        $museumForMax = [];
        $museumLinks = $nomination->nominationsMuseums;
        foreach ($museumLinks as $museumLink) {
            if($museumLink->useForMax){
                $museumForMax[] = $museumLink->museum->number;
            }
        }
        
        $museums = $nomination->museums;
        $this->museums = implode(';', ArrayHelper::getColumn($museums, function ($museum) {
            return $museum->number;
        }));



        foreach ($nomination->ageCategories as $ageCategory) {
            $property = "cat_{$ageCategory->minClass}_{$ageCategory->maxClass}";
            try {
                $this->$property = 1;
            } catch (\Exception $e) {
                //ignore this
            }
        }
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Nomination::find()->count();
    }

    private static function findAgeCategory($classMin, $classMax)
    {
        if (!isset(static::$_ageCategories["{$classMin}_{$classMax}"])) {
            static::$_ageCategories["{$classMin}_{$classMax}"] = AgeCategory::find()
                ->andWhere(['minClass' => $classMin])
                ->andWhere(['maxClass' => $classMax])
                ->one();
        }

        return static::$_ageCategories["{$classMin}_{$classMax}"];
    }
}