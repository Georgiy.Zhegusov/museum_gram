<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamId
 * @property string $teamEsr
 * @property string $parkId
 * @property string $value
 * @property string $parkReady
 */
class ParkRawObject extends RawObject
{
    const REVERSE = true;

    private $_addedParks = [];

    protected static $columns = [
        'teamId'    => 'MosOlimp team id',
        'teamEsr'   => 'ESR team id',
        'parkId'    => 'id парка',
        'value'     => 'Сумма баллов',
        'parkReady' => 'Фото подтверждено',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => Museum::TYPE_PARK])
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $museumNumber = (integer)$this->parkId;

        /** @var Museum $park */
        $park = Museum::findByNumber($museumNumber);

        if (empty($park)) {
            throw new Exception('Cant find park ' . $museumNumber);
        }

        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->teamEsr);
        }

        $date = new \DateTime();
        $date->setTimestamp(0);

        $newBall = false;

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $park->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $park->id,
                'team_id'   => $team->id
            ]);
        }

        $this->value = floatval(is_null($this->value) ? 0 : $this->value);

        if (
            !$museumBall->isNewRecord
            && $museumBall->internal_round >= $this->value
            && $museumBall->photo_status == $this->parkReady
        ) {
            return [];
        }

        $museumBall->internal_round = $this->value;
        $museumBall->photo_status   = $this->parkReady;
        $museumBall->visitDate      = $date->format('Y-m-d');

        try {
            $this->trySave($museumBall);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumBall->id,
                'message' => 'Добавлен баллы команде ' . $this->teamEsr . ' для парка ' . $this->parkId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumBall->id,
                'message' => 'Обновлены баллы команде ' . $this->teamEsr . ' для парка ' . $this->parkId
            ];
        }

        return $result;
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
        $this->teamEsr   = $museumBall->team->number;
        $this->parkId    = $museumBall->museum_id;
        $this->parkReady = $museumBall->photo_status;
        $this->value     = $museumBall->value;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => Museum::TYPE_PARK])
            ->count();
    }
}