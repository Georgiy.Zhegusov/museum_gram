<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamEsr
 * @property string museumId
 * @property string $dateVisit
 * @property string $value
 * @property string $peopleCount
 */
class MuseumCorrRawObject extends RawObject
{
    const REVERSE = true;

    protected static $columns = [
        'teamEsr'     => 'regid',
        'people'      => 'Участник',
        'museumId'    => 'Музей (id)',
        'value'       => 'Оценка',
        'dateVisit'   => 'dateVisit',
        'dateType'    => 'Дата вбивки',
        'addedBy'     => 'Добавил',
        'peopleCount' => 'Количество участников',
        'check'       => 'Сверка'
    ];

    const NEEDED_STAGE = 1;

    private $_addedBalls = [];

    private $_deletedBalls = [];

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $museumNumber = (integer)$this->museumId;

        /** @var Museum $museum */
        $museum = Museum::findByNumber($museumNumber);

        if (empty($museum)) {
            throw new Exception('Cant find museum ' . $museumNumber);
        }

        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->teamEsr);
        }

        $key = $team->id . '_' . $museum->id;

        if (isset($this->_addedBalls[$key])) {
            return [];
        }

        if (isset($this->_deletedBalls[$key])) {
            unset($this->_deletedBalls[$key]);

            return [];
        }

        if ($this->value == '-1') {
            $this->_deletedBalls[$key] = true;
        }

        $this->_addedBalls[$key] = true;

        $date = new \DateTime($this->dateVisit);

        $newBall = false;

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $museum->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $museum->id,
                'team_id'   => $team->id
            ]);
        } else {
            //Если приходит ноль и до этого что то было, то надо удалить все данные посещения.
            if ($this->value == 0) {
                $museumBall->delete();

                return [];
            }
        }

        $this->value = floatval(is_null($this->value) ? 0 : $this->value);

        if (
            !$museumBall->isNewRecord
            && $museumBall->internal_round == $this->value
            && $museumBall->visit_people_count == $this->peopleCount
        ) {
            return [];
        }

        $museumBall->internal_round = $this->value;
        $museumBall->visit_people_count   = $this->peopleCount;
        $museumBall->visitDate            = $date->format('Y-m-d');

        try {
            $this->trySave($museumBall);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumBall->id,
                'message' => 'Добавлен баллы команде ' . $this->teamEsr . ' для музея ' . $this->museumId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumBall->id,
                'message' => 'Обновлены баллы команде ' . $this->teamEsr . ' для музея ' . $this->museumId
            ];
        }

        unset($museumBall);

        return $result;
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
        $this->teamEsr     = $museumBall->team->number;
        $this->museumId    = $museumBall->museum_id;
        $this->peopleCount = $museumBall->visit_people_count;
        $this->dateVisit   = $museumBall->visitDate;
        $this->value       = $museumBall->internal_round;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => [Museum::TYPE_MUSEUM, Museum::TYPE_ESTATE]])
            ->andWhere(['>','internal_round','0'])
            ->count();
    }
}