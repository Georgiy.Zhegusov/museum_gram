<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\museumResult\NominationMuseumLink;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $name
 */
class NominationMaxRawObject extends RawObject
{
    protected static $columns = [];

    const NEEDED_STAGE = 1;

    public static function initObj($header=null)
    {
        $ageCategories = AgeCategory::find()
            ->orderBy(['minClass' => SORT_ASC])
            ->all();

        static::$columns = [
            'name' => 'Номинация',
        ];

        foreach ($ageCategories as $ageCategory) {
            static::$columns['age_' . $ageCategory->id] = $ageCategory->name;
        }

        parent::initObj();
    }

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Nomination::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        return [];
    }

    /**
     * @param Nomination $nomination
     */
    protected function load($nomination)
    {
        $this->name = $nomination->name;

        $ageCategoryLinks = $nomination->nominationsAgeCategories;

        foreach ($ageCategoryLinks as $ageCategoryLink) {
            $id = 'age_' . $ageCategoryLink->age_category_id;
            $this->$id = $ageCategoryLink->maxValue;
        }
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Nomination::find()->count();
    }
}