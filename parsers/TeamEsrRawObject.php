<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $teamId
 * @property string $teamESR
 * @property string $peopleCount
 */
class TeamEsrRawObject extends RawObject
{
    protected static $columns = [
        'teamId'      => 'Лешин ID команды',
        'teamESR'     => 'ESR номер команды (основной)',
        'peopleCount' => 'Количество людей в команде 1 этапа',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return TeamMosolimpId::find()->offset($offset)->limit($limit)->all();
    }

    public function save()
    {
        $teamESR        = (integer)$this->teamESR;
        $mosOlimpNumber = (integer)$this->teamId;

        if (empty($teamESR)) {
            throw new Exception('Cant create team with empty id');
        }

        if ($teamESR < 1000000 || $teamESR > 9999999) {
            return [];
        }

        if (empty($mosOlimpNumber)) {
            throw new Exception('Cant add empty mosOlimpNumber to team');
        }

        $team = Team::findByNumber($teamESR);

        $mosOlympiadId = null;

        $newTeam = false;

        if (empty($team)) {
            $newTeam = true;

            $team = new Team();

            $team->number = $teamESR;

            $this->trySave($team);
        } else {
            $createdIds = $team->mosolimpIds;

            /** @var TeamMosolimpId $createdId */
            foreach ($createdIds as $createdId) {
                if ($createdId->number == $mosOlimpNumber) {
                    $mosOlympiadId = $createdId;
                    break;
                }
            }
        }

        $teamInfo = null;
        if (!is_null($this->peopleCount)) {
            $teamInfo = $team->getTeamInfo(static::NEEDED_STAGE);

            $newTeamInfo = false;

            if (empty($teamInfo)) {
                $newTeamInfo = true;

                $teamInfo = new TeamInfo([
                    'team_id' => $team->id,
                    'name'    => 'Команда этапа ' . static::NEEDED_STAGE,
                    'stage'   => static::NEEDED_STAGE,
                ]);
            }

            $teamInfo->peopleCount = $this->peopleCount;

            $this->trySave($teamInfo);
        }

        $newMosOlympiadId = false;

        if (empty($mosOlympiadId)) {
            $mosOlympiadId = new TeamMosolimpId([
                'team_id' => $team->id,
                'number'  => $mosOlimpNumber
            ]);

            $this->trySave($mosOlympiadId);
        }

        $result = [];

        if ($newTeam) {
            $result[] = [
                'isNew'   => true,
                'id'      => null,
                'message' => 'Команда ' . $team->number
            ];
        }

        if (isset($newTeamInfo)) {
            $result[] = [
                'isNew'   => $newTeamInfo,
                'id'      => null,
                'message' => 'Установлено кол-во людей для ' . static::NEEDED_STAGE . ' этапа команды ' . $team->number
            ];
        }

        $result[] = [
            'isNew'   => $newMosOlympiadId,
            'id'      => null,
            'message' => 'Добавлен номер МосОлим ' . $mosOlympiadId->number . ' для команды ' . $team->number
        ];

        return $result;
    }

    /**
     * @param TeamMosolimpId $mosOlympiadObject
     */
    protected function load($mosOlympiadObject)
    {
        $this->teamId  = $mosOlympiadObject->number;
        $this->teamESR = $mosOlympiadObject->team->number;

        /** @var TeamInfo $teamInfo */
        $teamInfo = $mosOlympiadObject->team->getTeamInfo(static::NEEDED_STAGE);

        $this->peopleCount = empty($teamInfo) ? null : $teamInfo->peopleCount;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return TeamMosolimpId::find()->count();
    }
}