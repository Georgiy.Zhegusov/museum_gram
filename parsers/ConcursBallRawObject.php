<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
//use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
//use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $id
 * @property string $esr
 * @property string $surname
 * @property string $name
 * @property string $school
 * @property string $class
 * @property string $team
 * @property string $text
 * @property string $concursId
 * @property string $image
 * @property string $status
 */
class ConcursBallRawObject extends RawObject
{
    protected static $columns = array(
        'id' => 'id',
        'esr' => 'еср',
        'surname' => 'фамилия',
        'name' => 'имя',
        'school' => 'школа',
        'class' => 'класс',
        'team' => 'команда',
        'text' => 'на сайт',
        'concursId' => 'id конкурса',
        'image' => 'файл',
        'status' => 'статус'
    );

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Concurs::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $concursNumber = (integer)$this->concursId;

        /** @var Concurs $concurs */
        $concurs = Concurs::findByNumber($concursNumber);

        if (empty($concurs)) {
            throw new Exception('Cant find concurs ' . $concursNumber);
        }

        if (is_null($this->esr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->esr;
        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new Exception('Cant find team ' . $this->esr);
        }

        $newBall = false;

        $museumBall = MuseumBall::find()
            ->andWhere(['museum_id' => $concurs->id])
            ->andWhere(['team_id' => $team->id])
            ->one();

        if (empty($museumBall)) {
            $newBall = true;

            $museumBall = new MuseumBall([
                'museum_id' => $concurs->id,
                'team_id'   => $team->id
            ]);
        }

        switch ($this->status){
            case 'победитель':
            case 'Победитель':
                $museumBall->status = MuseumBall::STATUS_WINNER;
                break;
            case 'призер':
            case 'призёр':
            case 'Призер':
            case 'Призёр':
                $museumBall->status = MuseumBall::STATUS_PRISE;
                break;
        }

        $date = new \DateTime();
        $date->setTimestamp(0);

        $museumBall->internal_round = 5;
        $museumBall->visitDate      = $date->format('Y-m-d');

        try {
            $this->trySave($museumBall);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        if(!is_null($this->image)){
            if($this->image == ''){
                $museumBall->image_path = null;
                $museumBall->image_thumb = null;
            }
            else{
                $path = $this->image;

                $path = ltrim($path, '/');
                $path = \Yii::getAlias('@downloadsFile/'  . $path);
                $path = rtrim($path, '/');

                $basePath = \Yii::getAlias('@downloadsFile');

                if(!file_exists($path)){
                    throw new Exception('Не смог найти файл ' . $path);
                }

                $extension     = pathinfo($path, PATHINFO_EXTENSION);
                $imageFile = Yii::getAlias("@honorImage/museumBall_{$museumBall->id}." . $extension);
                $fullPath = Yii::getAlias("@app/$imageFile");
                rename($path, $fullPath);

//                $image=new \Imagick($path);
//                $image->setImageFormat('png');
//                $image->writeImage($fullPath);
//                chmod($fullPath, 0777);
//
//                $thumbnail = Yii::getAlias("@honorImage/museumBall_{$museumBall->museum_id}_t." . $extension);
//                $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//                Image::frame($fullPath)
//                    ->thumbnail(new Box(200, 200))
//                    ->save($fullPathThumbnail, ['quality' => 80]);
//                chmod($fullPathThumbnail, 0777);

                $museumBall->image_path = $imageFile;
                $museumBall->image_thumb = $imageFile;

                $this->trySave($museumBall);

                //unlink($path);

                while ($path != $basePath){
                    $path = pathinfo($path, PATHINFO_DIRNAME);

                    $files = scandir($path);

                    if(count($files)>2){
                        break;
                    }

                    rmdir($path);
                }
            }
        }

        $result = [];

        if ($newBall) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumBall->id,
                'message' => 'Добавлен баллы команде ' . $this->esr . ' для конкурсы ' . $this->concursId
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumBall->id,
                'message' => 'Обновлены баллы команде ' . $this->esr . ' для конкурса ' . $this->concursId
            ];
        }

        unset($museumBall);

        return $result;
    }

    /**
     * @param Museum $museum
     */
    protected function load($museum)
    {
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Concurs::find()->count();
    }
}