<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\MuseumCoefficient;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $value
 * @property string $number
 * @property string $dateFrom
 * @property string $dateTo
 */
class CoeffRawObject extends RawObject
{
    protected static $columns = array(
        'value'    => 'Коэффициент',
        'number'   => 'Номер музея',
        'dateFrom' => 'Дата начала',
        'dateTo'   => 'Дата окончания',
    );

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return MuseumCoefficient::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        $museumNumber = (integer)$this->number;

        $museum = Museum::findByNumber($museumNumber);

        if (empty($museum)) {
            throw new Exception('Cant find museum ' . $museumNumber);
        }

        if(is_null($this->dateFrom)){
            $this->dateFrom = new \DateTime();
            $this->dateFrom->setTimestamp(0);
        }
        else{
            $this->dateFrom = new \DateTime($this->dateFrom);
        }
        $this->dateFrom = $this->dateFrom->format('Y-m-d');


        if(is_null($this->dateTo)){
            $this->dateTo = new \DateTime();
            $this->dateTo->setDate(9999, 12, 01);
        }
        else{
            $this->dateTo = new \DateTime($this->dateTo);
        }
        $this->dateTo = $this->dateTo->format('Y-m-d');

        $newCoef = false;

        $museumCoef = MuseumCoefficient::find()
            ->andWhere(['dateStart' => $this->dateFrom])
            ->andWhere(['dateFinish' => $this->dateTo])
            ->andWhere(['museum_id' => $museum->id])
            ->one();

        if(empty($museumCoef)){
            $newCoef = true;

            $museumCoef = new MuseumCoefficient([
                'dateStart' => $this->dateFrom,
                'dateFinish' => $this->dateTo,
                'museum_id' => $museum->id
            ]);
        }

        if(!$museumCoef->isNewRecord && $museumCoef->value == $this->value){
            return [];
        }

        if($this->value > $museumCoef->value) {
            $museumCoef->value = $this->value;
        }

        $this->trySave($museumCoef);

        $result = [];

        if ($newCoef) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museumCoef->id,
                'message' => 'Коэффициент был создан'
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $museumCoef->id,
                'message' => 'Было обновлено'
            ];
        }

        return $result;
    }

    /**
     * @param MuseumCoefficient $museumCoef
     */
    protected function load($museumCoef)
    {
        $this->number = $museumCoef->museum->number;
        $this->dateFrom = $museumCoef->dateStart;
        $this->dateTo = $museumCoef->dateFinish;
        $this->value = $museumCoef->value;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumCoefficient::find()->count();
    }
}