<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\Honor;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $time
 * @property string $textBefore2
 * @property string $teamEsr
 * @property string $textBefore3
 * @property string $textAfter1
 * @property string $some1
 * @property string $some2
 */
class MentionRawObject extends RawObject
{
    private static $_ageCategories = [];
    protected static $columns = array(
        'time'   => 'Отметка времени',
        'textBefore2'    => 'ФИО в дательном падеже (Иванову Ивану Ивановичу)',
        'teamEsr'    => 'Код участника',
        'textBefore3'    => 'Должность в дательном падеже (классному руководителю 4 "Г" класса)',
        'textAfter1' => 'Название учебного учреждения (ГБОУ Школа №)',
        'some1'  => 'Деятельность учителя в рамках олимпиады',
        'some2'  => 'Что именно делал учитель? (если в предыдущем пункте выбрали 2 вариант)'
    );

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return null;
    }

    public function save()
    {
        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        if(empty($team)){
            throw new Exception('Cant find team with number ' . $teamEsr);
        }

        /** @var ContestType $baseContestType */
        $baseContestType = ContestType::find()->andWhere(['base_for_type' => ContestType::BASE_FOR_MENTION])->one();

        if(empty($baseContestType)){
            throw new Exception('Cant find contestType for mentions' . $teamEsr);
        }

        /** @var Contest $contest */
        $contest = $baseContestType->getContests()->one();

        if(empty($contest)){
            throw new Exception('Cant find contest for mentions' . $teamEsr);
        }

        $this->textBefore2 = (string)$this->textBefore2;
        $this->textBefore3 = (string)$this->textBefore3;
        $this->textAfter1 = (string)$this->textAfter1;

        $createdHonor = $team->getHonors()
            ->andWhere(['textBefore2' => $this->textBefore2])
            ->andWhere(['textBefore3' => $this->textBefore3])
            ->andWhere(['textAfter1' => $this->textAfter1])
            ->one();

        $newHonor = false;

        if(empty($createdHonor)){
            $newHonor = true;
            $createdHonor = new Honor([
                'team_id' => $team->id,
                'contest_id' => $contest->id,
                'textBefore2' => $this->textBefore2,
                'textBefore3' => $this->textBefore3,
                'textAfter1' => $this->textAfter1
            ]);
        }

        $createdHonor->textAfter2 = $team->mentionText;

        $this->trySave($createdHonor);

        if ($newHonor) {
            $result[] = [
                'isNew'   => true,
                'id'      => $createdHonor->id,
                'message' => 'Создана награда ' . $createdHonor->id
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $createdHonor->id,
                'message' => 'Обновлена награда ' . $createdHonor->id
            ];
        }

        return $result;
    }

    /**
     * @param Museum $museum
     *
     * @return array
     */
    protected function load($museum)
    {
       return [];
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return 0;
    }
}