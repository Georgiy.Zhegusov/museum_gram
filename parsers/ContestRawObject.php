<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use Yii;
use yii\base\Exception;
//use Imagine\Image\Box;
//use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $number
 * @property string $name
 * @property string $codeName
 * @property string $contestTypeId
 * @property string $contestTypeCodeName
 * @property string $generationType
 * @property string $textBefore1
 * @property string $textBefore2
 * @property string $textBefore3
 * @property string $textAfter1
 * @property string $textAfter2
 * @property string $textAfter3
 * @property string $substrate
 */
class ContestRawObject extends RawObject
{
    protected static $columns = [
        'number' => 'ID существующего конкурса',
        'name' => 'Название конкурса',
        'codeName' => 'Кодовое название',
        'contestTypeId' => 'ID типа конкурса',
        'contestTypeCodeName' =>' Кодовое название типа конкурса',
        'generationType' => 'Тип генерации',
        'textBefore1' => 'Текст до 1',
        'textBefore2' => 'Текст до 2',
        'textBefore3' => 'Текст до 3',
        'textAfter1' => 'Текст после 1',
        'textAfter2' => 'Текст после 2',
        'textAfter3' => 'Текст после 3',
        'substrate' => 'Путь до обложки'
    ];

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Contest::find()->offset($offset)->limit($limit)->all();
    }

    public function save(){

        $contestTypeId = $this->contestTypeId;
        $contestTypeCodeName = $this->contestTypeCodeName;
        if(empty($contestTypeId) && empty($contestTypeCodeName)){
            throw new Exception('Обязательно указывать id или codeName типа конкурса');
        }

        $potentialTypes = ContestType::findByIdCodename((int)$this->contestTypeId, (string)$this->contestTypeCodeName);

        if(empty($potentialTypes)){
            throw new Exception('Не получаетс найти тип конкурса для ' . $this->contestTypeId . '/' . $this->contestTypeCodeName);
        }

        if(count($potentialTypes)>1){
            throw new Exception('Найдено несколько типов конкурсов для ' . $this->contestTypeId . '/' . $this->contestTypeCodeName);
        }

        $contestType = array_pop($potentialTypes);

        $number = $this->number;
        if(empty($number)){
            throw new Exception('ID конкурса должен быть указан');
        }

        $contest = Contest::findByNumber((integer)$this->number);

        $isNew = true;

        if(empty($contest)){
            $contest = new Contest();

            $contest->number = (integer)$this->number;
            $isNew = false;
        }

        $contest->contest_type_id = $contestType->id;

        if(!empty($this->generationType)) {
            $generationType = GenerationType::findByName($this->generationType);

            if (empty($generationType)) {
                throw new Exception('Не найдено тип генерации грамот:' . $this->generationType);
            }

            $contest->generation_type_id = $generationType->id;
        }
        else{
            $contest->generation_type_id = null;
        }

        $contest->textBefore1 = is_null($this->textBefore1) ? null : (string)$this->textBefore1;
        $contest->textBefore2 = is_null($this->textBefore2) ? null : (string)$this->textBefore2;
        $contest->textBefore3 = is_null($this->textBefore3) ? null : (string)$this->textBefore3;
        $contest->textAfter1 = is_null($this->textAfter1) ? null : (string)$this->textAfter1;
        $contest->textAfter2 = is_null($this->textAfter2) ? null : (string)$this->textAfter2;
        $contest->textAfter3 = is_null($this->textAfter3) ? null : (string)$this->textAfter3;

        if(!$contest->save()){
            $strings = [];

            foreach ($contest->errors as $field => $fieldErrors) {
                $strings[] = "$field: " . implode(", ", $fieldErrors);
            }

            throw new Exception(implode('; ', $strings));
        }

        if(!is_null($this->substrate)){
            if($this->substrate == ''){
                $contest->substrate = null;
                $contest->substrate_back = null;
                $contest->substrate_thumb = null;
            }
            else{
                $path = $this->substrate;

                $path = ltrim($path, '/');
                $path = \Yii::getAlias('@downloadsFile/'  . $path);
                $path = rtrim($path, '/');

                $basePath = \Yii::getAlias('@downloadsFile');

                if(!file_exists($path)){
                    throw new Exception('Не смог найти файл ' . $path);
                }

                $extension     = pathinfo($path, PATHINFO_EXTENSION);
                $substrateFile = Yii::getAlias("@substrateBase/contest_{$contest->id}." . $extension);
                $fullPath = Yii::getAlias("@app/$substrateFile");
                move_uploaded_file($path, $fullPath);

//                $image=new \Imagick($path);
//                $image->setImageFormat('png');
//                $image->writeImage($fullPath);
//                chmod($fullPath, 0777);
//
//                $substrateBackGroundFile = Yii::getAlias("@substrateBase/contest_{$contest->id}_b.png");
//                $fullPathBackGround = Yii::getAlias("@app/$substrateBackGroundFile");
//
//                $image=new \Imagick($path);
//                $image->setImageOpacity(0.5);
//                $image->writeImage($fullPathBackGround);
//                chmod($fullPathBackGround, 0777);
//
//                $thumbnail = Yii::getAlias("@substrateBase/contest_{$contest->id}_t." . $extension);
//                $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//                Image::frame($fullPath)
//                    ->thumbnail(new Box(200, 200))
//                    ->save($fullPathThumbnail, ['quality' => 80]);
//                chmod($fullPathThumbnail, 0777);

                $contest->substrate_back = $substrateFile;
                $contest->substrate_thumb = $substrateFile;
                $contest->substrate = $substrateFile;

                if(!$contest->save()){
                    $strings = [];

                    foreach ($contest->errors as $field => $fieldErrors) {
                        $strings[] = "$field: " . implode(", ", $fieldErrors);
                    }

                    throw new Exception(implode('; ', $strings));
                }

                unlink($path);

                while ($path != $basePath){
                    $path = pathinfo($path, PATHINFO_DIRNAME);

                    $files = scandir($path);

                    if(count($files)>2){
                        break;
                    }

                    rmdir($path);
                }
            }
        }

        return [[
            'isNew' => $isNew,
            'id' => $contest->id,
            'message' => 'Конкурс ' . $contest->name
        ]];
    }

    /**
     * @param Contest $contestObject
     */
    protected function load($contestObject)
    {
        $this->number = $contestObject->number;
        $this->name = $contestObject->name;
        $this->codeName = $contestObject->codeName;
        $this->contestTypeId = $contestObject->contest_type_id;
        $this->contestTypeCodeName = $contestObject->contestType->codeName;

        $this->generationType = $contestObject->generationType->name;

        $this->textBefore1 = $contestObject->textBefore1;
        $this->textBefore2 = $contestObject->textBefore2;
        $this->textBefore3 = $contestObject->textBefore3;
        $this->textAfter1 = $contestObject->textAfter1;
        $this->textAfter2 = $contestObject->textAfter2;
        $this->textAfter3 = $contestObject->textAfter3;

        $this->substrate = null;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Contest::find()->count();
    }
}