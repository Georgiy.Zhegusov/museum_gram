<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;


use yii\base\Object;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "contest_categories".
 *
 * @property array $columns
 * @property array $columnTitles
 * @property Object $object
 */
abstract class RawObject extends Object
{
    const REVERSE = false;

    private $attributes = [];

    protected static $columns = [];

    public static function initObj($header = null)
    {

    }

    public static function getColumns()
    {
        return array_keys(static::$columns);
    }

    public static function getColumnTitles()
    {
        return array_values(static::$columns);
    }

    public function __get($name)
    {
        if(isset(static::$columns[$name])){
            return ArrayHelper::getValue($this->attributes, $name, null);
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        if(isset(static::$columns[$name])){
            $this->attributes[$name] = $value;
            return;
        }

        parent::__set($name, $value);
    }

    public function __construct(array $config)
    {
        $newConfig = [];

        foreach (static::getColumns() as $id=>$columnAttribute) {
            $newConfig[$columnAttribute] = ArrayHelper::getValue($config, $id, null);

            if (empty($newConfig[$columnAttribute])) {
                $newConfig[$columnAttribute] = null;
            }

            if ($newConfig[$columnAttribute] == "=''" || $newConfig[$columnAttribute] == '=""') {
                $newConfig[$columnAttribute] = '';
            }
        }

        foreach ($config as $key => $item) {
            if(!is_int($key)){
                $newConfig[$key] = $item;
            }
        }

        foreach ($newConfig as $column => $item) {
            $this->$column = $item;
        }

        parent::__construct($newConfig);
    }

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset){
        return [];
    }

    /**
     * @return
     */
    abstract public function save();


    /**
     * @return integer
     */
    public static function count(){
        return 0;
    }

    /**
     * @param $object
     *
     * @return
     */
    abstract protected function load($object);

    /**
     * @param $object
     *
     * @return array[]
     */
    public function toRow($object)
    {
        try {
            $additionalRows = $this->load($object);

            $result = [];

            if (empty($additionalRows)) {
                $additionalRows = [];
            }

            $additionalRows[] = $this;


            foreach ($additionalRows as $additionalRow) {
                $local = [];
                foreach (static::getColumns() as $column) {
                    $local[$column] = $this->preFormatValue(ArrayHelper::getValue($additionalRow, $column, null));
                }

                $result[] = $local;
            }

            return $result;
        }
        catch (\Exception $e){
            //var_dump($e->getMessage(), $e->getFile(), $e->getLine());
            return [];
        }
    }

    private function preFormatValue($value)
    {

        if(is_null($value)){
            return '';
        }

        if($value === ''){
            return '=""';
        }

        return $value;
    }

    protected function trySave(ActiveRecord $object)
    {
        if(!$object->save()){
            $strings = [];

            foreach ($object->errors as $field => $fieldErrors) {
                $strings[] = "$field: " . implode(", ", $fieldErrors);
            }

            throw new \Exception(implode('; ', $strings));
        }
    }
}