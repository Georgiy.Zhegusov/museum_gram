<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string surname
 * @property string name
 * @property string patronomic
 * @property string schoolId
 * @property string schoolText
 * @property string classNum
 * @property string classText
 * @property string teamName
 * @property string surname1
 * @property string name1
 * @property string patronomic1
 * @property string school1
 * @property string class1
 * @property string surname2
 * @property string name2
 * @property string patronomic2
 * @property string school2
 * @property string class2
 * @property string surname3
 * @property string name3
 * @property string patronomic3
 * @property string school3
 * @property string class3
 * @property string surname4
 * @property string name4
 * @property string patronomic4
 * @property string school4
 * @property string class4
 * @property string surname5
 * @property string name5
 * @property string patronomic5
 * @property string school5
 * @property string class5
 * @property string surname6
 * @property string name6
 * @property string patronomic6
 * @property string school6
 * @property string class6
 * @property string surname7
 * @property string name7
 * @property string patronomic7
 * @property string school7
 * @property string class7
 * @property string surname8
 * @property string name8
 * @property string patronomic8
 * @property string school8
 * @property string class8
 * @property string surname9
 * @property string name9
 * @property string patronomic9
 * @property string school9
 * @property string class9
 * @property string teamSize
 * @property string teamEsr
 */
class TeamPeopleBaseRawObject extends RawObject
{
    protected static $columns = [
        'surname'    => 'Фамилия*',
        'name'       => 'Имя*',
        'patronomic' => 'Отчество*',
        'schoolId'   => 'Логин школы в системе СтатГрад*',
        'schoolText' => 'Название школы',
        'classNum'   => 'Класс обучения*',
        'classText'  => 'Класс (если не по системе 1-11)',
        'teamName'   => 'Название команды (для командного зачета)',
        'surname1'   => 'Фамилия участника №1',
        'name1'      => 'Имя участника №1',
        'patronomic1'=> 'Отчество участника №1',
        'class1'     => 'Класс участника №1',
        'school1'    => 'Логин школы участника №1',
        'surname2'   => 'Фамилия участника №2',
        'name2'      => 'Имя участника №2',
        'patronomic2'=> 'Отчество участника №2',
        'class2'     => 'Класс участника №2',
        'school2'    => 'Логин школы участника №2',
        'surname3'   => 'Фамилия участника №3',
        'name3'      => 'Имя участника №3',
        'patronomic3'=> 'Отчество участника №3',
        'class3'     => 'Класс участника №3',
        'school3'    => 'Логин школы участника №3',
        'surname4'   => 'Фамилия участника №4',
        'name4'      => 'Имя участника №4',
        'patronomic4'=> 'Отчество участника №4',
        'class4'     => 'Класс участника №4',
        'school4'    => 'Логин школы участника №4',
        'surname5'   => 'Фамилия участника №5',
        'name5'      => 'Имя участника №5',
        'patronomic5'=> 'Отчество участника №5',
        'class5'     => 'Класс участника №5',
        'school5'    => 'Логин школы участника №5',
        'surname6'   => 'Фамилия участника №6',
        'name6'      => 'Имя участника №6',
        'patronomic6'=> 'Отчество участника №6',
        'class6'     => 'Класс участника №6',
        'school6'    => 'Логин школы участника №6',
        'surname7'   => 'Фамилия участника №7',
        'name7'      => 'Имя участника №7',
        'patronomic7'=> 'Отчество участника №7',
        'class7'     => 'Класс участника №7',
        'school7'    => 'Логин школы участника №7',
        'surname8'   => 'Фамилия участника №8',
        'name8'      => 'Имя участника №8',
        'patronomic8'=> 'Отчество участника №8',
        'class8'     => 'Класс участника №8',
        'school8'    => 'Логин школы участника №8',
        'surname9'   => 'Фамилия участника №9',
        'name9'      => 'Имя участника №9',
        'patronomic9'=> 'Отчество участника №9',
        'class9'     => 'Класс участника №9',
        'school9'    => 'Логин школы участника №9',
        'teamSize'   => 'Размер команды',
        'teamEsr'    => 'SUBMISSION-INDEX',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        $newTeam = false;
        if(empty($team)){
            $newTeam = true;

            $team = new Team([
                'number' => $teamEsr
            ]);
        }

        $this->trySave($team);

        $teamInfo = $team->getTeamInfo(static::NEEDED_STAGE);

        if(empty($teamInfo)){
            $teamInfo = new TeamInfo([
                'team_id' => intval($team->id),
                'name' => $this->teamName,
                'stage' => static::NEEDED_STAGE
            ]);
            $this->trySave($teamInfo);
        }

        $lastSchoolId = $this->schoolId;

        if(substr($this->schoolId, 0, 3) == 'sch'){
            $this->schoolId = substr($this->schoolId, 3);
        }

        $schoolId = $this->schoolId;

        if (!empty($schoolId) && ctype_digit($this->schoolId) && mb_strlen($lastSchoolId)-mb_strlen($this->schoolId) == 3) {
            $this->schoolId = intval($this->schoolId);
            $school = PeopleSchool::findByNumber($this->schoolId);
            if (empty($school)) {
                $school = new PeopleSchool([
                    'number' => intval($this->schoolId),
                    'text'   => (string)$this->schoolText
                ]);

                $this->trySave($school);
            }
        }

        $captain = $teamInfo->captain;

        if(empty($captain)){
            $captain = new People([
                'team_info_id' => $teamInfo->id,
                'role' => People::ROLE_CAPTAIN
            ]);
        }

        if(!ctype_digit($this->classNum) && !is_int($this->classNum) && !is_float($this->classNum)){
            $this->classText = $this->classNum;
            $this->classNum = 1;
        }
        else{
            $this->classNum = intval($this->classNum);
        }

        $captain->name = $this->name;
        $captain->surname = $this->surname;
        $captain->patronomic = $this->patronomic;
        $captain->classNum = $this->classNum;
        $captain->classText = $this->classText;
        $captain->school_id = isset($school) ? $school->id : null;
        $captain->schoolText = $this->schoolText;

        $this->trySave($captain);

        $tommies = $teamInfo->tommies;

        $this->addTommy($tommies, $teamInfo, $this->name1, $this->surname1, $this->patronomic1, $this->school1, $this->class1, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name2, $this->surname2, $this->patronomic2, $this->school2, $this->class2, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name3, $this->surname3, $this->patronomic3, $this->school3, $this->class3, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name4, $this->surname4, $this->patronomic4, $this->school4, $this->class4, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name5, $this->surname5, $this->patronomic5, $this->school5, $this->class5, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name6, $this->surname6, $this->patronomic6, $this->school6, $this->class6, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name7, $this->surname7, $this->patronomic7, $this->school7, $this->class7, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name8, $this->surname8, $this->patronomic8, $this->school8, $this->class8, isset($school) ? $school->id : null);
        $this->addTommy($tommies, $teamInfo, $this->name9, $this->surname9, $this->patronomic9, $this->school9, $this->class9, isset($school) ? $school->id : null);

        foreach ($tommies as $tommy) {
            $tommy->delete();
        }

        $result = [];

        if ($newTeam) {
            $result[] = [
                'isNew'   => true,
                'id'      => $team->id,
                'message' => 'Создана команда ' . $this->teamEsr
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $team->id,
                'message' => 'Обновлена команда ' . $this->teamEsr
            ];
        }

        return $result;
    }

    /**
     * @param $tommies
     * @param $teamInfo
     * @param $name1
     * @param $surname1
     * @param $patronomic1
     * @param $school1
     * @param $class1
     */
    private function addTommy(&$tommies, $teamInfo, $name1, $surname1, $patronomic1, $school1, $class1, $capSchoolId)
    {
        if (empty($name1) && empty($surname1) && empty($patronomic1)) {
            return;
        }

        $findedTommy = false;
        foreach ($tommies as $id => $tommy) {
            if ($tommy->name == $name1
                && $tommy->surname == $surname1
                && $tommy->patronomic == $patronomic1
            ) {
                $findedTommy = $tommy;
                unset($tommies[$id]);
                break;
            }
        }

        if (empty($findedTommy)) {
            $findedTommy = new People([
                'team_info_id' => $teamInfo->id,
                'role'         => People::ROLE_TOMMY,
                'name'         => $name1,
                'surname'      => $surname1,
                'patronomic'   => $patronomic1
            ]);
        }

        $lastSchoolId = $school1;

        if (substr($school1, 0, 3) == 'sch') {
            $school1 = substr($school1, 3);
        }

        $schoolText = null;
        $schoolId = null;

        if (!empty($school1) && ctype_digit($school1) && mb_strlen($lastSchoolId)-mb_strlen($school1) == 3) {
            $school1 = intval($school1);
            $school = PeopleSchool::findByNumber($school1);

            if (empty($school)) {
                $school = new PeopleSchool([
                    'number' => $school1,
                    'text'   => $school1
                ]);

                $this->trySave($school);
            }

            $schoolId = $school->id;
        }
        else{
            $schoolText = (string)$school1;
        }

        $classNum  = null;
        $classText = null;

        if(!empty($class1)) {
            if (!ctype_digit($class1) && !is_int($class1) && !is_float($class1)) {
                $classNum  = 1;
                $classText = $class1;
            } else {
                $classNum = intval($class1);
            }
        }


        if($classNum > 11){
            $classNum = null;
            $classText = $classNum;
        }

        if (empty($classNum) && empty($classText)) {
            $classNum  = $this->classNum;
            $classText = $this->classText;
        }

        if(empty($schoolText) && empty($schoolId)){
            $schoolText = $this->schoolText;
            $schoolId = $capSchoolId;
        }

        $findedTommy->classNum   = $classNum;
        $findedTommy->classText  = $classText;
        $findedTommy->school_id  = $schoolId;
        $findedTommy->schoolText = $schoolText;

        $this->trySave($findedTommy);
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => Museum::TYPE_PARK])
            ->count();
    }
}