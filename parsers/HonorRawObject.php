<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\Honor;
use app\models\Team;
use Yii;
use yii\base\Exception;

//use Imagine\Image\Box;
//use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $contestNumber
 * @property string $teamEsr
 * @property string $image
 * @property string $textBefore1
 * @property string $textBefore2
 * @property string $textBefore3
 * @property string $textAfter1
 * @property string $textAfter2
 * @property string $textAfter3
 */
class HonorRawObject extends RawObject
{
    protected static $columns = [
        'teamEsr'       => 'ESR команды',
        'contestNumber' => 'Номер конкурса',
        'image'         => 'Картинка',
        'textBefore1'   => 'Текст до 1',
        'textBefore2'   => 'Текст до 2',
        'textBefore3'   => 'Текст до 3',
        'textAfter1'    => 'Текст после 1',
        'textAfter2'    => 'Текст после 2',
        'textAfter3'    => 'Текст после 3',
    ];

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Honor::find()->offset($offset)->limit($limit)->all();
    }

    public function save()
    {
        $number = $this->contestNumber;
        if (empty($number)) {
            throw new Exception('Номер конкурса должен быть указан');
        }

        /** @var Contest $contest */
        $contest = Contest::findByNumber((integer)$this->contestNumber);

        if (empty($contest)) {
           throw new \Exception('Не найден конкурс номер ' . $this->contestNumber);
        }

        $teamEsr = intval($this->teamEsr);

        if (empty($teamEsr)) {
            throw new \Exception('Номер команды не может быть пустым');
        }

        $team    = Team::findByNumber($teamEsr);

        if (empty($team)) {
            throw new \Exception('Не найдена команда номер ' . $this->teamEsr);
        }

        $teamInfo = $team->getTeamInfo($contest->stage);

        if(empty($teamInfo)){
            throw new \Exception('Команда номер ' . $this->teamEsr . ' не имеет информации о этапе ' . $contest->stage);
        }

        $honor = Honor::findHonor($team->id, $contest->id);

        $newHonor = false;

        if(empty($honor)){
            $newHonor = true;

            $honor = new Honor([
                'team_id' => $team->id,
                'contest_id' => $contest->id
            ]);
        }

        $honor->textBefore1 = is_null($this->textBefore1) ? null : (string)$this->textBefore1;
        $honor->textBefore2 = is_null($this->textBefore2) ? null : (string)$this->textBefore2;
        $honor->textBefore3 = is_null($this->textBefore3) ? null : (string)$this->textBefore3;
        $honor->textAfter1  = is_null($this->textAfter1) ? null : (string)$this->textAfter1;
        $honor->textAfter2  = is_null($this->textAfter2) ? null : (string)$this->textAfter2;
        $honor->textAfter3  = is_null($this->textAfter3) ? null : (string)$this->textAfter3;

        $this->trySave($honor);

        if (!is_null($this->image)) {
            if ($this->image == '') {
                $honor->image_path       = null;
                $honor->image_thumb  = null;
            } else {
                $path = $this->image;

                $path = ltrim($path, '/');
                $path = \Yii::getAlias('@downloadsFile/' . $path);
                $path = rtrim($path, '/');

                $basePath = \Yii::getAlias('@downloadsFile');

                if (!file_exists($path)) {
                    throw new Exception('Не смог найти файл ' . $path);
                }

                $extension     = pathinfo($path, PATHINFO_EXTENSION);
                $imageFile = Yii::getAlias("@substrateBase/honor_{$honor->id}." . $extension);
                $fullPath      = Yii::getAlias("@app/$imageFile");
                rename($path, $fullPath);

//                $image=new \Imagick($path);
//                $image->setImageFormat('png');
//                $image->writeImage($fullPath);
//                chmod($fullPath, 0777);
//
//                $substrateBackGroundFile = Yii::getAlias("@substrateBase/contest_{$contest->id}_b.png");
//                $fullPathBackGround = Yii::getAlias("@app/$substrateBackGroundFile");
//
//                $image=new \Imagick($path);
//                $image->setImageOpacity(0.5);
//                $image->writeImage($fullPathBackGround);
//                chmod($fullPathBackGround, 0777);
//
//                $thumbnail = Yii::getAlias("@substrateBase/contest_{$contest->id}_t." . $extension);
//                $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//                Image::frame($fullPath)
//                    ->thumbnail(new Box(200, 200))
//                    ->save($fullPathThumbnail, ['quality' => 80]);
//                chmod($fullPathThumbnail, 0777);

                $honor->image_path  = $imageFile;
                $honor->image_thumb = $imageFile;

                $this->trySave($honor);
                //unlink($path);

                while ($path != $basePath) {
                    $path = pathinfo($path, PATHINFO_DIRNAME);

                    $files = scandir($path);

                    if (count($files) > 2) {
                        break;
                    }

                    rmdir($path);
                }
            }
        }

        return [
            [
                'isNew'   => $newHonor,
                'id'      => $honor->id,
                'message' => 'награда ' . $honor->id
            ]
        ];
    }

    /**
     * @param Contest $contestObject
     */
    protected function load($contestObject)
    {
        $this->number              = $contestObject->number;
        $this->name                = $contestObject->name;
        $this->codeName            = $contestObject->codeName;
        $this->contestTypeId       = $contestObject->contest_type_id;
        $this->contestTypeCodeName = $contestObject->contestType->codeName;

        $this->generationType = $contestObject->generationType->name;

        $this->textBefore1 = $contestObject->textBefore1;
        $this->textBefore2 = $contestObject->textBefore2;
        $this->textBefore3 = $contestObject->textBefore3;
        $this->textAfter1  = $contestObject->textAfter1;
        $this->textAfter2  = $contestObject->textAfter2;
        $this->textAfter3  = $contestObject->textAfter3;

        $this->substrate = null;
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Honor::find()->count();
    }
}