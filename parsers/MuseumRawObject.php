<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $number
 * @property string $categories
 * @property string $shortName
 * @property string $cat_1_2
 * @property string $cat_3_4
 * @property string $cat_5_7
 * @property string $cat_8_9
 * @property string $cat_10_11
 */
class MuseumRawObject extends RawObject
{
    private static $_ageCategories = [];
    protected static $columns = array(
        'number'          => 'ID',
        'coordLng'        => 'Координаты музея-долгота',
        'museCoop'        => 'Музейные объединения',
        'forInv'          => 'Доступен для инвалидов',
        'forScholar'      => 'Ориентирован на дошкольников',
        'tasks'           => 'Задания для ОВЗ',
        'categories'      => 'Категория музея',
        'active'          => 'Музей пока не принимает участников олимпиады',
        'link'            => 'Ссылка на скачивание приглашения',
        'show'            => 'Отображать музей на сайте',
        'name'            => 'Название музея',
        'shortName'       => 'Краткое название музея',
        'address'         => 'Адрес музея',
        'workTime'        => 'Время работы',
        'phone'           => 'Телефон музея',
        'url'             => 'Ссылка на сайт музея',
        'cat_1_2'         => 'Музей ориентирован на 1–2 класс',
        'cat_3_4'         => 'Музей ориентирован на 3-4 класс',
        'cat_1_4'         => 'Музей ориентирован на 1-4 класс',
        'cat_5_7'         => 'Музей ориентирован на 5-7 класс',
        'cat_8_9'         => 'Музей ориентирован на 8-9 класс',
        'cat_10_11'       => 'Музей ориентирован на 10-11 класс',
        'price'           => 'Цена билета',
        'discount'        => 'Дает ли приглашение скидку',
        'discountComment' => 'Пояснение про скидку',
        'specific'        => 'Особенности посещения',
        'preCall'         => 'Предварительная запись',
        'about'           => 'Описание музея',
        'photos'          => 'Фотография музея',
        'coordLat'        => 'Координаты музея-широта',
        'tag'             => 'тэг',
        'updatedDate'     => 'Дата редактироваия',
    );

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Museum::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {

        $museumNumber = (integer)$this->number;

        if (empty($museumNumber)) {
            throw new Exception('Cant create museum with empty id');
        }

        $categories = $this->categories;
        if(!empty($categories)) {
            $categories = explode(';', $categories);
        }
        else{
            $categories = [];
        }

        //hardcode 18 category, its inactive museums
        if (in_array(18, $categories)) {
            return [];
        }

        /** @var MuseumCategory[] $museumCategories */
        $museumCategories = MuseumCategory::find()
            ->andWhere(['number' => $categories])
            ->indexBy('number')
            ->all();

        $categories = array_flip($categories);

        $museumTypeId = null;

        //Проверяем, что нету двух категория с разными типами
        foreach ($museumCategories as $museumCategory) {
            if (!is_null($museumCategory->type_id)) {
                if (empty($museumTypeId)) {
                    $museumTypeId = $museumCategory->type_id;
                } else {
                    throw new \Exception('Нельзя присвоить музею сразу два типа');
                }
            }
        }

        //Создаем музей
        $museum    = Museum::findByNumber($museumNumber);
        $newMuseum = false;
        if (empty($museum)) {
            $newMuseum = true;

            $museum = new Museum();

            $museum->number = $museumNumber;
        }

        $museum->name    = $this->shortName;
        $museum->type_id = is_null($museumTypeId) ? Museum::TYPE_MUSEUM : $museumTypeId;
        $this->trySave($museum);

        $updateCount = 0;

        //Для новых музеев проверяем, какие категории уже добавлены
        if (!$newMuseum) {
            $museumCategoryLinks = $museum->categories;

            foreach ($museumCategoryLinks as $museumCategoryLink) {
                unset($categories[$museumCategoryLink->number]);
                unset($museumCategories[$museumCategoryLink->number]);
            }
        }

        foreach ($categories as $categoryNum => $true) {
            if (!isset($museumCategories[$categoryNum])) {
                throw new \Exception("Cant find category $categoryNum");
            } else {
                $museum->link('categories', $museumCategories[$categoryNum]);
                $updateCount++;
            }
        }

        /** @var MuseumAgeCategoryLink[] $ageCategoriesLink */
        $ageCategoriesLink = $museum->getAgeCategoriesLinks()->all();

        $mustBeAgeCategories = [];

        $this->mayBeMustBeAgeCat($mustBeAgeCategories, 1, 2, $this->cat_1_2);
        $this->mayBeMustBeAgeCat($mustBeAgeCategories, 3, 4, $this->cat_3_4);
        $this->mayBeMustBeAgeCat($mustBeAgeCategories, 5, 7, $this->cat_5_7);
        $this->mayBeMustBeAgeCat($mustBeAgeCategories, 8, 9, $this->cat_8_9);
        $this->mayBeMustBeAgeCat($mustBeAgeCategories, 10, 11, $this->cat_10_11);

        foreach ($ageCategoriesLink as $ageCategoryLink) {
            if (isset($mustBeAgeCategories[$ageCategoryLink->age_category_id])) {
                unset($mustBeAgeCategories[$ageCategoryLink->age_category_id]);
            } else {
                $ageCategoryLink->delete();
                $updateCount++;
            }
        }

        foreach ($mustBeAgeCategories as $mustBeAgeCategory => $ageCategory) {
            $museum->link('ageCategories', $ageCategory);
            $updateCount++;
        }

        $result = [];

        if ($newMuseum) {
            $result[] = [
                'isNew'   => true,
                'id'      => $museum->id,
                'message' => 'Музей ' . $museum->name
            ];
        } else {
            if ($updateCount > 0) {
                $result[] = [
                    'isNew'   => false,
                    'id'      => $museum->id,
                    'message' => 'Было обновлено ' . $updateCount . ' всяких записей для музей ' . $museum->name
                ];
            }
        }

        return $result;
    }

    /**
     * @param Museum $museum
     */
    protected function load($museum)
    {
        $categories = $museum->categories;

        $this->number     = $museum->number;
        $this->categories = implode(';', ArrayHelper::getColumn($categories, function ($category) {
            return $category->number;
        }));
        $this->shortName  = $museum->name;
        $this->cat_1_2    = 0;
        $this->cat_3_4    = 0;
        $this->cat_5_7    = 0;
        $this->cat_8_9    = 0;
        $this->cat_10_11  = 0;

        foreach ($museum->ageCategories as $ageCategory) {
            $property        = "cat_{$ageCategory->minClass}_{$ageCategory->maxClass}";
            try {
                $this->$property = 1;
            }
            catch (\Exception $e){
                //ignore this
            }
        }
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Museum::find()->count();
    }

    private static function findAgeCategory($classMin, $classMax)
    {
        if (!isset(static::$_ageCategories["{$classMin}_{$classMax}"])) {
            static::$_ageCategories["{$classMin}_{$classMax}"] = AgeCategory::find()
                ->andWhere(['minClass' => $classMin])
                ->andWhere(['maxClass' => $classMax])
                ->one();
        }

        return static::$_ageCategories["{$classMin}_{$classMax}"];
    }

    private function mayBeMustBeAgeCat(&$mustBeAgeCat, $minClass, $maxClass, $mustBe)
    {
        if ($mustBe) {
            $ageCat = static::findAgeCategory($minClass, $maxClass);

            if (empty($ageCat)) {
                throw new \Exception("Cant find age category $minClass-$maxClass");
            }

            $mustBeAgeCat[$ageCat->id] = $ageCat;
        }
    }
}