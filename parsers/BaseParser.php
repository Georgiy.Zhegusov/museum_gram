<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 28.02.17
 * Time: 10:49
 */

namespace app\parsers;


use app\models\DownloadFile;
use app\models\UploadFile;
use Exception;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Writer_Excel2007;
use Yii;

class BaseParser
{
    /** @var  UploadFile */
    private $_upload;

    /** @var  array */
    private $_results = [];

    private $_rawObjectClassName;

    public function __construct($rawObjectClassName)
    {
        if(!class_exists($rawObjectClassName)){
            throw new Exception('Не могу найти класс ' . $rawObjectClassName);
        }

        $rawObjectClassName::initObj();

        $this->_rawObjectClassName = $rawObjectClassName;
    }

    public function parseUpload(UploadFile $uploadFile)
    {
        ini_set("memory_limit","4096M");

        $this->_upload = $uploadFile;

        $this->parseFile($this->_upload->getFullPath());

        //save results to base as batch insert
        $this->_upload->saveResults();
    }

    /**
     * @param DownloadFile $downloadFile
     *
     * @return bool|string
     */
    public function generateTemplate(DownloadFile $downloadFile)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $rawObjectClassName = $this->_rawObjectClassName;

        $titles = $rawObjectClassName::getColumnTitles();
        $objPHPExcel->getActiveSheet()->fromArray($titles, NULL, 'A1');;

        $fileName = time() . '_' . rand(10,10000);

        $newFileName = Yii::getAlias("@templateBase/$fileName.xlsx");
        $downloadFile->stringCount = 1;
        $downloadFile->generatedCount = 1;
        $downloadFile->path = $newFileName;
        $downloadFile->save();

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        $fullPath = Yii::getAlias("@app/$newFileName");

        $objWriter->save($fullPath);

        return $newFileName;
    }

    public function generateDownload(DownloadFile $downloadFile)
    {

        ini_set('max_execution_time', 5000);
        ini_set("memory_limit","4096M");

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $rawObjectClassName = $this->_rawObjectClassName;

        $limit = 100;
        $offset = 0;

        $rowCount = 1;

        $titles = $rawObjectClassName::getColumnTitles();
        $objPHPExcel->getActiveSheet()->fromArray($titles, NULL, 'A' . $rowCount);;
        $rowCount++;

        $fileName = time() . '_' . rand(10,10000);
        $newFileName = Yii::getAlias("@downloadBase/$fileName.xlsx");

        $downloadFile->stringCount = $rawObjectClassName::count();
        $downloadFile->generatedCount = 0;
        $downloadFile->path = $newFileName;
        $downloadFile->save();

        $stringAddedCount = 0;

        $parsedTeam = 0;

        while($objects = $rawObjectClassName::find($limit, $offset)){
            foreach ($objects as $object) {
                /** @var RawObject $rawObject */
                $rawObject = new $rawObjectClassName([]);
                $row = $rawObject->toRow($object);

                foreach ($row as $item) {
                    $objPHPExcel->getActiveSheet()->fromArray($item, NULL, 'A' . $rowCount);
                    $rowCount++;
                }

                $stringAddedCount++;
                $parsedTeam++;

                if($stringAddedCount == 20){
                    $downloadFile->generatedCount = $parsedTeam;
                    $downloadFile->save();
                    $stringAddedCount = 0;
                }
            }

            unset($object);
            $offset += $limit;
        }

        if($stringAddedCount > 0){
            $downloadFile->generatedCount = $parsedTeam;
            $downloadFile->save();
        }

        $fullPath = Yii::getAlias("@app/$newFileName");

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fullPath);
    }

    private function parseFile($path)
    {
        try {
            $sheet = $this->readFile($path);

            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            $rowData = $sheet->rangeToArray('A1'.':' . $highestColumn . $highestRow,
                NULL,
                FALSE,
                FALSE);

            //delete header
            $header = array_shift($rowData);

            $this->_upload->stringCount = count($rowData);
            $this->_upload->parsedCount = 0;
            $this->_upload->save();

            $errors = [];

            $stringAddedCount = 0;
            $rowNum = 0;

            $rawObjectClassName = $this->_rawObjectClassName;

            $rawObjectClassName::initObj($header);

            if($rawObjectClassName::REVERSE){
                $currentElem = end($rowData);
            }
            else{
                $currentElem = reset($rowData);
            }

            while ($currentElem) {
                ini_set('max_execution_time', 9999);
                $rowNum++;
                try {
                    $parseResults = $this->parseRow($currentElem);

                    foreach ($parseResults as $parseResult) {
                        if ($parseResult['isNew']) {
                            $this->_upload->addResultNew($parseResult['message'], $rowNum + 2, $parseResult['id']);
                        } else {
                            $this->_upload->addResultUpdate($parseResult['message'], $rowNum + 2, $parseResult['id']);
                        }
                    }
                }
                catch(Exception $e){
                    if(!isset($errors[$e->getMessage() . $e->getLine()])){
                        $errors[$e->getMessage() . $e->getLine()] = [];
                    }

                    $errors[$e->getMessage() . '_' . $e->getLine()][] = $rowNum;
                }

                $stringAddedCount++;

                if($stringAddedCount == 20){
                    $this->_upload->parsedCount = $rowNum + 1;
                    $this->_upload->save();
                    $stringAddedCount = 0;
                }

                if($rawObjectClassName::REVERSE){
                    $currentElem = prev($rowData);
                }
                else{
                    $currentElem = next($rowData);
                }
            }

            if($stringAddedCount > 0){
                $this->_upload->parsedCount = $rowNum + 1;
                $this->_upload->save();
            }

            foreach ($errors as $error => $strings) {
                $this->_upload->addResultError($error, implode(', ', $strings));
            }
        }
        catch (\Exception $e){
            $this->_upload->addResultError($e->getMessage() . $e->getFile() . $e->getLine(), '');
            return;
        }
    }

    /**
     * parse row
     *
     * @param $rowData
     */
    protected function parseRow($rowData){
        $rawObjectClassName = $this->_rawObjectClassName;

        /** @var RawObject $rawObject */
        $rawObject = new $rawObjectClassName($rowData);

        return $rawObject->save();
    }

    /**
     * parse row
     *
     * @param $rowData
     */
    protected function saveRow($rowData){
        $rawObjectClassName = $this->_rawObjectClassName;

        /** @var RawObject $rawObject */
        $rawObject = new $rawObjectClassName($rowData);

        $rawObject->save();
    }

    /**
     * @param string $path
     *
     * @return \PHPExcel_Worksheet
     * @throws \yii\base\Exception
     */
    private function readFile($path)
    {
        $inputFileType = PHPExcel_IOFactory::identify($path);

        if($inputFileType == 'CSV'){

            $delimiters = [';', ','];

            $activeDelimiter = null;

            foreach ($delimiters as $delimiter) {
                $objReader = PHPExcel_IOFactory::createReader($inputFileType)
                    ->setDelimiter($delimiter);
                $objPHPExcel = $objReader->load($path);
                $sheet = $objPHPExcel->getSheet(0);

                $highestColumn = $sheet->getHighestColumn();
                if($highestColumn >= 'C'){
                    $activeDelimiter = $delimiter;
                    break;
                }
            }

            if(empty($activeDelimiter)){
                throw new \yii\base\Exception('Поддерживается только разделитель ; и ,');
            }
        }
        else{

            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $sheet = $objPHPExcel->getSheet(0);
        }

        return $sheet;
    }
}