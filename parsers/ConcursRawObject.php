<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\MuseumCategory;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $number
 * @property string $type
 * @property string $name
 * @property string $ages
 * @property string $contestType
 */
class ConcursRawObject extends RawObject
{
    protected static $columns = array(
        'number'      => 'ID',
        'type'        => 'Личный тип зачета конкурса (если галочка стоит - то тип зачета личный, если не стоит - то Личный и командный (для команд)',
        'name'        => 'Название конкурса',
        'some'        => 'Адрес страницы конкурса',
        'ages'        => 'Возрастная категория',
        'contestType' => 'Задания для ОВЗ',
    );

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Concurs::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {

        $concursNumber = (integer)$this->number;

        if (empty($concursNumber)) {
            throw new Exception('Cant create concurs with empty id');
        }

        //Создаем музей
        $concurs    = Concurs::findByNumber($concursNumber);
        $newConcurs = false;
        if (empty($concurs)) {
            $newConcurs = true;

            $concurs = new Concurs();

            $concurs->number = $concursNumber;
        }

        $concurs->name = $this->name;
        switch ($this->type) {
            case 1:
                $concurs->type_id = Concurs::TYPE_INDIVIDUAL;
                break;
            default:
                $concurs->type_id = Concurs::TYPE_TEAM;
        }

        $this->trySave($concurs);

        $updateCount = 0;

        /** @var MuseumAgeCategoryLink[] $ageCategoriesLink */
        $ageCategoriesLink = $concurs->getAgeCategoriesLinks()->all();

        list($classMin, $classMax) = explode('-', $this->ages);

        $mustBeAgeCategories = static::findAgeCategory(intval(trim($classMin)), intval(trim($classMax)));

        foreach ($ageCategoriesLink as $ageCategoryLink) {
            if (isset($mustBeAgeCategories[$ageCategoryLink->age_category_id])) {
                unset($mustBeAgeCategories[$ageCategoryLink->age_category_id]);
            } else {
                $ageCategoryLink->delete();
                $updateCount++;
            }
        }

        foreach ($mustBeAgeCategories as $mustBeAgeCategory => $ageCategory) {
            $concurs->link('ageCategories', $ageCategory);
            $updateCount++;
        }

        $result = [];

        if ($newConcurs) {
            $result[] = [
                'isNew'   => true,
                'id'      => $concurs->id,
                'message' => 'Музей ' . $concurs->name
            ];
        } else {
            if ($updateCount > 0) {
                $result[] = [
                    'isNew'   => false,
                    'id'      => $concurs->id,
                    'message' => 'Было обновлено ' . $updateCount . ' всяких записей для конкурса ' . $concurs->name
                ];
            }
        }

        return $result;
    }

    /**
     * @param Museum $museum
     */
    protected function load($museum)
    {
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Concurs::find()->count();
    }

    private static function findAgeCategory($classMin, $classMax)
    {
        return AgeCategory::find()
            ->andWhere(['>=', 'minClass', $classMin])
            ->andWhere(['<=', 'maxClass', $classMax])
            ->all();
    }
}