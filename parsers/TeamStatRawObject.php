<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\BaseMusObj;
use app\models\museumResult\Concurs;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\NominationBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use app\models\TeamType;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $classNum
 * @property string $peopleCount
 * @property string $peopleCountFinal
 * @property string $concursSolo
 * @property string $concursTeam
 * @property string $nominationWinners
 * @property string $nominationPrises
 * @property string $final
 * @property string $esr
 * @property string $gramCount
 */
class TeamStatRawObject extends RawObject
{
    protected static $columns = [
        'classNum'          => 'Класс',
        'esr'               => 'ESR',
        'peopleCount'       => 'Количество на основном этапе',
        'peopleCountFinal'  => 'Количество на финальном этапе',
        'concursSolo'       => 'Наград в конкурсах индивидуально',
        'concursTeam'       => 'наград в конкурсах командно',
        'nominationWinners' => 'Количество победителей номинаций',
        'nominationPrises'  => 'Количество призёров номинаций',
        'final'             => 'Результат финала',
        'gramCount'         => 'Количество грамот'
    ];

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->orderBy(['id' => SORT_ASC])
            ->all();
    }

    public function save()
    {
        return [];
    }

    /**
     * @param Team $team
     *
     * @return array
     * @throws \Exception
     */
    protected function load($team)
    {
        $resultCount = 0;

        $class = null;

        $firstStage = $team->getTeamInfo(1);

        $this->concursSolo = $team->getMuseumBalls()
            ->andWhere(['status' => [MuseumBall::STATUS_WINNER, MuseumBall::STATUS_PRISE]])
            ->joinWith('museum', false)
            ->andWhere([BaseMusObj::tableName() . '.type' => BaseMusObj::BASE_TYPE_CONCURS])
            ->andWhere([BaseMusObj::tableName() . '.type_id' => Concurs::TYPE_INDIVIDUAL])
            ->count();

        $resultCount += $this->concursSolo;

        $this->concursTeam = $team->getMuseumBalls()
            ->andWhere(['status' => [MuseumBall::STATUS_WINNER, MuseumBall::STATUS_PRISE]])
            ->joinWith('museum', false)
            ->andWhere([BaseMusObj::tableName() . '.type' => BaseMusObj::BASE_TYPE_CONCURS])
            ->andWhere([BaseMusObj::tableName() . '.type_id' => Concurs::TYPE_TEAM])
            ->count();

        $resultCount += $this->concursTeam;

        $this->nominationPrises = $team->getNominationBalls()
            ->andWhere(['status' => NominationBall::STATUS_PRISE])
            ->count();

        $this->nominationWinners = $team->getNominationBalls()
            ->andWhere(['status' => NominationBall::STATUS_WINNER])
            ->count();

        $this->esr = $team->number;

        $resultCount += $this->nominationPrises;
        $resultCount += $this->nominationWinners;

        if ($firstStage) {
            /** @var People $captain */
            $captain = $firstStage->captain;

            if ($captain) {
                $class = $captain->classNum;

                if (empty($class)) {
                    $class = $captain->classText;
                }
            }

            $this->peopleCount = $firstStage->participantCount;
        }

        $finalStage = $team->getTeamInfo(2);

        if ($finalStage) {
            if (empty($class)) {
                /** @var People $captain */
                $captain = $firstStage->captain;

                if ($captain) {
                    $class = $captain->classNum;

                    if (empty($class)) {
                        $class = $captain->classText;
                    }
                }
            }

            $this->peopleCountFinal = $finalStage->participantCount;
        }

        $this->classNum = $class;

        $honors = $team->honors;

        $used = [];

        $gramCount = 0;

        foreach ($honors as $honor) {

            foreach ($honor->honorsPeoples as $honorsPeople) {
                if(!isset($used[$honorsPeople->id])){
                    $gramCount ++;
                }

                foreach ($honorsPeople->linkedHonorsPeople as $linkedHonorsPeople) {
                    $used[$linkedHonorsPeople->id] = true;
                }

                $used[$honorsPeople->id] = true;
            }
        }

        $this->gramCount = $gramCount;

        $resultCount+= $gramCount;

        if ($resultCount == 0) {
            throw new \Exception('Ты никому не нужен, тупой неудачник. =(');
        }

        return [];
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return Team::find()
            ->count();
    }
}