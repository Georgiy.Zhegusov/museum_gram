<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\People;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $schoolId
 * @property string $classNum
 * @property string $tempEsr
 * @property string $teamEsr
 */
class TeamBaseRawObject extends RawObject
{
    protected static $columns = [
        'tempEsr'    => 'Номер карточки',
        'schoolId'   => 'Логин школы в системе СтатГрад*',
        'classNum'    => 'Класс обучения*',
        'type'     => 'Выберите тип зачёта',
        'teamEsr' => 'SUBMISSION-INDEX',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return Team::find()
            ->offset($offset)
            ->limit($limit)
            ->all();
    }

    public function save()
    {
        if (is_null($this->teamEsr)) {
            throw new Exception('Cant find team with empty number');
        }

        $teamEsr = (integer)$this->teamEsr;
        $team    = Team::findByNumber($teamEsr);

        $newTeam = false;
        if(empty($team)){
            $newTeam = true;

            $team = new Team([
                'number' => $teamEsr
            ]);
        }

        $team->temporary_number = $this->tempEsr;

        $this->trySave($team);

        $teamInfo = $team->getTeamInfo(static::NEEDED_STAGE);

        if(empty($teamInfo)){
            $teamInfo = new TeamInfo([
                'team_id' => $team->id,
                'name' => 'Пустое название',
                'stage' => static::NEEDED_STAGE
            ]);
            $this->trySave($teamInfo);
        }

        if(substr($this->schoolId, 0, 3) == 'sch'){
            $this->schoolId = substr($this->schoolId, 3);
        }

        $school = PeopleSchool::findByNumber($this->schoolId);

        if(empty($school)){
            $school = new PeopleSchool([
                'number' => $this->schoolId,
                'text' => $this->schoolId
            ]);

            $this->trySave($school);
        }

        $captain = $teamInfo->captain;

        if(empty($captain)){
            $captain = new People([
                'team_info_id' => $teamInfo->id,
                'role' => People::ROLE_CAPTAIN
            ]);
        }

        $captain->classNum = $this->classNum;
        $captain->school_id = $school->id;

        $this->trySave($captain);

        $result = [];

        if ($newTeam) {
            $result[] = [
                'isNew'   => true,
                'id'      => $team->id,
                'message' => 'Создана команда ' . $this->teamEsr
            ];
        } else {
            $result[] = [
                'isNew'   => false,
                'id'      => $team->id,
                'message' => 'Обновлена команда ' . $this->teamEsr
            ];
        }

        return $result;
    }

    /**
     * @param MuseumBall $museumBall
     */
    protected function load($museumBall)
    {
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return MuseumBall::find()
            ->joinWith('museum')
            ->andWhere([Museum::tableName() . '.type_id' => Museum::TYPE_PARK])
            ->count();
    }
}