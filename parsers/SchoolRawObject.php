<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 01.03.17
 * Time: 19:33
 */

namespace app\parsers;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\PeopleSchool;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamMosolimpId;
use Yii;
use yii\base\Exception;
use Imagine\Image\Box;
use yii\imagine\Image;

/**
 * This is the model class for table "contest_categories".
 *
 * @property string $schoolESR
 * @property string $schoolName
 * @property string $honorCount
 */
class SchoolRawObject extends RawObject
{
    protected static $columns = [
        'schoolESR'      => 'ESR школы',
        'schoolName'     => 'Красивое название',
        'honorCount'     => 'Количество грамот',
    ];

    const NEEDED_STAGE = 1;

    /**
     * @param $limit
     * @param $offset
     *
     * @return array
     */
    public static function find($limit, $offset)
    {
        return PeopleSchool::find()->offset($offset)->limit($limit)->all();
    }

    public function save()
    {
        $schoolEsr = $this->schoolESR;

        if(substr($schoolEsr, 0, 3) == 'sch'){
            $schoolEsr = substr($schoolEsr, 3);
        }

        $schoolEsr = intval($schoolEsr);

        $school = PeopleSchool::find()
            ->andWhere(['number' => $schoolEsr])
            ->one();

        if(empty($school)){
            $newSchool = true;

            $school = new PeopleSchool([
                'number' => $schoolEsr,
            ]);
        }
        else{
            $newSchool = false;
        }

        $school->text = $this->schoolName;
        $this->trySave($school);

        $result = [];

        $result[] = [
            'isNew'   => $newSchool,
            'id'      => $school->id,
            'message' => 'Обработана школа ' . $school->id
        ];

        return $result;
    }

    /**
     * @param PeopleSchool $school
     *
     * @return array
     */
    protected function load($school)
    {
        try {
            $this->schoolESR  = $school->esr;
            $this->schoolName = $school->text;
            $this->honorCount = $school->getHonorPeoples()->count();

        }
        catch (\Exception $e){
            var_dump($e->getMessage());
            exit;
        }
        return [];
    }

    /**
     * @return integer
     */
    public static function count()
    {
        return PeopleSchool::find()->count();
    }
}