<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uploads".
 *
 * @property integer $id
 * @property string $path
 * @property integer $status
 * @property boolean $isTemplate
 * @property string $type
 * @property integer $stringCount
 * @property integer $generatedCount
 */
class DownloadFile extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_FINISHED = 2;
    const STATUS_ERROR = 3;

    const TYPE_CONTEST = 'cntst';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'downloads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['isTemplate', 'safe'],
            [['status', 'stringCount', 'generatedCount'], 'integer'],
            [['path'], 'string', 'max' => 128],
            [['type'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Путь',
            'status' => 'Статус',
            'type' => 'Тип',
            'stringCount' => 'Кол-во строк',
            'isTemplate' => 'Только шаблон',
            'generatedCount' => 'Обработано стро',
        ];
    }

    public function setNew()
    {
        $this->status = self::STATUS_NEW;
    }

    public function setStarted()
    {
        $this->status = self::STATUS_IN_PROCESS;
    }

    public function setFinished()
    {
        $this->status = self::STATUS_FINISHED;
    }

    public function setError()
    {
        $this->status = self::STATUS_ERROR;
    }

    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->status == self::STATUS_FINISHED || $this->status == self::STATUS_ERROR;
    }

    public function isError()
    {
        return $this->status == self::STATUS_ERROR;
    }

    public function getPercent()
    {
        if($this->isFinished()){
            return 100;
        }

        if($this->isNew() || $this->generatedCount==0){
            return 0;
        }

        return intval(100*$this->generatedCount/$this->stringCount);
    }
}
