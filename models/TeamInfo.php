<?php

namespace app\models;

use app\models\museumResult\AgeCategory;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\NominationBall;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "team_infos".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $team_type_id
 * @property string $name
 * @property integer $stage
 * @property integer $age_category_id
 * @property integer $peopleCount
 *
 * @property integer  $participantCount
 * @property string   $ageCategoryName
 * @property People[] $peoples
 * @property People[] $tommies
 * @property People   $captain
 * @property People   $convoy
 * @property string   $schoolText
 * @property Team $team
 * @property TeamType $teamType
 * @property AgeCategory $ageCategory
 */
class TeamInfo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_infos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'team_type_id', 'stage', 'peopleCount'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['team_id', 'stage'], 'required'],
            [['team_id', 'stage'], 'unique', 'targetAttribute' => ['team_id', 'stage']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['team_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeamType::className(), 'targetAttribute' => ['team_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Команда',
            'team_type_id' => 'Тип команды',
            'name' => 'Название',
            'stage' => 'Этап',
            'peopleCount' => 'Кол-во людей',
            'participantCount' => 'Кол-во людей',
            'ageCategoryName' => 'Возрастная категория',
        ];
    }

    private $_participantCount;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeoples()
    {
        return $this->hasMany(People::className(), ['team_info_id' => 'id'])->orderBy('role');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTommies()
    {
        return $this->hasMany(People::className(), ['team_info_id' => 'id'])->andWhere(['role' => People::ROLE_TOMMY]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaptain()
    {
        return $this->hasOne(People::className(), ['team_info_id' => 'id'])->andWhere(['role' => People::ROLE_CAPTAIN]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvoy()
    {
        return $this->hasOne(People::className(), ['team_info_id' => 'id'])->andWhere(['role' => People::ROLE_CONVOY]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategory()
    {
        return $this->hasOne(AgeCategory::className(), ['id' => 'age_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamType()
    {
        return $this->hasOne(TeamType::className(), ['id' => 'team_type_id']);
    }

    public function getSchoolText()
    {
        return empty($this->captain) ? '' : $this->captain->schoolGramText;
    }

    public function getAgeCategoryName()
    {
        $ageCategory = $this->ageCategory;

        if(empty($ageCategory)){
            return null;
        }

        return $ageCategory->name;
    }

    public function getParticipantCount()
    {
        if(empty($this->_participantCount)) {
            if (empty($this->peopleCount)) {
                $this->_participantCount = $this
                    ->getPeoples()
                    ->andWhere(['role' => [People::ROLE_CAPTAIN, People::ROLE_TOMMY]])
                    ->count();
            }
            else{
                $this->_participantCount  =$this->peopleCount;
            }
        }

        return $this->_participantCount;
    }

    public function recalculateTeamType()
    {
        $newTeamType = TeamType::getTeamTypeForCount($this->participantCount);

        if(!is_null($newTeamType)){
            $this->team_type_id = $newTeamType->id;
        }
    }

    public function beforeSave($insert)
    {
        if($this->isAttributeChanged('peopleCount')){
            $calcValue = $this
                ->getPeoples()
                ->andWhere(['role' => [People::ROLE_CAPTAIN, People::ROLE_TOMMY]])
                ->count();

            if(is_null($this->getOldAttribute('peopleCount'))){
                //тогда до этого использовался $calcValue
                $isChangedCount = ($calcValue != $this->participantCount);
            }
            else{
                $isChangedCount = ($this->getOldAttribute('peopleCount') != $this->participantCount);
            }

            if($isChangedCount){
                $this->recalculateTeamType();
            }
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(key_exists('peopleCount', $changedAttributes)){
            $calcValue = $this
                ->getPeoples()
                ->andWhere(['role' => [People::ROLE_CAPTAIN, People::ROLE_TOMMY]])
                ->count();

            if(is_null($changedAttributes['peopleCount'])){
                //тогда до этого использовался $calcValue
                $isChangedCount = ($calcValue != $this->participantCount);
            }
            else{
                $isChangedCount = ($changedAttributes['peopleCount'] != $this->participantCount);
            }

            if($isChangedCount){
                MuseumBall::recalculateByTeam($this->team_id, $this->stage);
            }
        }

        // recalculate museumBalls;
        if(key_exists('age_category_id', $changedAttributes)) {
            //drop all nominations with old age_category only
            NominationBall::dropByAgeCategoryTeam($this->team_id, $changedAttributes['age_category_id'], $this->age_category_id);

            //create all nominations with new age_category_only
            NominationBall::createByAgeCategoryTeam($this->team, $this->age_category_id, $changedAttributes['age_category_id']);
        }

        parent::afterSave($insert, $changedAttributes);
    }

}

//Nano Kleber
