<?php

namespace app\models;

use app\common\behaviors\NestedValuesBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "honors_peoples".
 *
 * @property integer $id
 * @property integer $honors_id
 * @property integer $peoples_id
 * @property string  $path
 * @property integer $status
 *
 * @property string $number
 * @property Honor $honor
 * @property People $people
 *
 * @property string $nestedTextBefore1
 * @property string $nestedTextBefore2
 * @property string $nestedTextBefore3
 * @property string $nestedTextAfter1
 * @property string $nestedTextAfter2
 * @property string $nestedTextAfter3
 *
 * @property string $statusText
 * @property string $baseText
 * @property array  $teamPeoplesText
 * @property string $captainText
 * @property string $convoyText
 * @property $this[] linkedHonorsPeople
 */
class HonorsPeople extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_INVALID = 1;
    const STATUS_FOR_GENERATE = 2;
    const STATUS_IN_GENERATE = 3;
    const STATUS_GENERATED = 4;
    const STATUS_ERROR_GENERATED = 5;

    public static function getStatuses()
    {
        return [
            static::STATUS_NEW => 'Новая',
            static::STATUS_INVALID => 'Ошибка',
            static::STATUS_FOR_GENERATE => 'Для генерациии',
            static::STATUS_IN_GENERATE => 'Генерируется',
            static::STATUS_GENERATED => 'Сгенерировано',
            static::STATUS_ERROR_GENERATED => 'Ошибка генерациии'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'honors_peoples';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['honors_id'], 'required'],
            [['path'], 'string', 'max' => 256],
            [['honors_id', 'peoples_id', 'status'], 'integer'],
            [['honors_id'], 'exist', 'skipOnError' => true, 'targetClass' => Honor::className(), 'targetAttribute' => ['honors_id' => 'id']],
            [['peoples_id'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['peoples_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            'nestedValues' => [
                'class' => NestedValuesBehavior::className(),
                'nestedSuffix' => null,
                'nestProperty' => 'honor',
                'nestedFields' => [
                    'textAfter1',
                    'textAfter2',
                    'textAfter3',
                    'textBefore1',
                    'textBefore2',
                    'textBefore3'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Награда',
            'number' => 'Номер грамоты',

            'honors_id' => 'Награда',
            'peoples_id' => 'Участник',

            'nestedTextBefore1' => 'Текст до 1',
            'nestedTextBefore2' => 'Текст до 2',
            'nestedTextBefore3' => 'Текст до 3',
            'nestedTextAfter1' => 'Текст после 1',
            'nestedTextAfter2' => 'Текст после 2',
            'nestedTextAfter3' => 'Текст после 3',
        ];
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->honors_id . '_' . (100000000 + $this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonor()
    {
        return $this->hasOne(Honor::className(), ['id' => 'honors_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasOne(People::className(), ['id' => 'peoples_id']);
    }

    public function getStatusText()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status, $this->status);
    }

    public function beforeSave($insert)
    {
        $status = $this->status;

        if(empty($status)){
            $this->status = self::STATUS_NEW;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return array|null|ActiveRecord
     */
    public static function getForGenerate()
    {
        return self::find()
            ->andWhere(['status' => self::STATUS_NEW])
            ->one();
    }

    /**
     * @param null $peopleId
     * @param      $contestIds
     *
     * @return $this[]
     *
     */
    public static function findForPeopleHonor($peopleId = null, $contestIds, $team_id)
    {
        if(empty($contestIds)){
            return [];
        }

        return self::find()
            ->joinWith('honor', false)
            ->andWhere([Honor::tableName() . '.contest_id' => $contestIds])
            ->andWhere([Honor::tableName() . '.team_id' => $team_id])
            ->andWhere(['peoples_id' => $peopleId])
            ->all();
    }

    public static function findByContests($peopleId, $contestIds)
    {
        if(empty($peopleId) && empty($honorId)){
            return [];
        }

        return self::find()
            ->andFilterWhere(['peoples_id' => $peopleId])
            ->andFilterWhere(['honors_id' => $honorId])
            ->all();
    }

    /**
     * @param $honorId
     *
     * @return ActiveRecord|null
     */
    public static function findTeamGram($honorId)
    {
        if(empty($honorId)){
            return null;
        }

        return self::find()
            ->andWhere(['peoples_id' => null])
            ->andWhere(['honors_id' => $honorId])
            ->one();
    }

    public function isForGenerate()
    {
        return $this->status == self::STATUS_FOR_GENERATE;
    }

    public function setGenerationStart()
    {
        $this->status = self::STATUS_IN_GENERATE;
    }

    public function setGenerated()
    {
        $this->status = self::STATUS_GENERATED;
    }

    public function setGenerationError()
    {
        $this->status = self::STATUS_ERROR_GENERATED;
    }

    /**
     * @return string
     */
    public function getCaptainText(){
        $generatedRoles = $this->honor->contest->generationType->generatedRoles;
        if(isset($generatedRoles[People::ROLE_CAPTAIN])
            && !empty($generatedRoles[People::ROLE_CAPTAIN])
            && !empty($this->honor->teamInfo->captain)){
            return $this->honor->teamInfo->captain->text;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getConvoyText(){
        $generatedRoles = $this->honor->contest->generationType->generatedRoles;
        if(isset($generatedRoles[People::ROLE_CONVOY])
            && !empty($generatedRoles[People::ROLE_CONVOY])
            && !empty($this->honor->teamInfo->convoy)){
            return $this->honor->teamInfo->convoy->text;
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getTeamPeoplesText()
    {
        $contest = $this->honor->contest;

        $contestCategory = $contest->contestType->contestCategory;

        if($contestCategory->oneForAll){
            $generationType = $contestCategory->generationType;
        }
        else{
            $contestType = $contest->contestType;
            if($contestType->oneForAll) {
                $generationType = $contestType->generationType;
            }
            else{
                $generationType = $contest->generationType;
            }
        }

        $generatedRoles = $generationType->generatedRoles;
        if(isset($generatedRoles[People::ROLE_TOMMY])
            && !empty($generatedRoles[People::ROLE_TOMMY])
            && !empty($this->honor->teamInfo->tommies)){
            return ArrayHelper::getColumn(
                $this->honor->teamInfo->tommies,
                function(People $people){
                    return $people->text;
                }
            );
        }

        return null;
    }

    /**
     * @return $this[]|null
     */
    public function getLinkedHonorsPeople()
    {
        $contests = null;

        $contestCategory = $this->honor->contest->contestType->contestCategory;

        if($contestCategory->oneForAll){
            $contests = $contestCategory->contests;
        }
        else{
            $contestType = $this->honor->contest->contestType;
            if($contestType->oneForAll) {
                $contests = $contestType->contests;
            }
        }

        if(empty($contests)){
            return [$this];
        }

        return self::findForPeopleHonor($this->peoples_id,
            ArrayHelper::getColumn(
                $contests,
                function($value)
                {
                    return $value->id;
                }
            ),
            $this->honor->team_id
        );
    }
}
