<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "generation_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $baseForText
 * @property boolean $teamGram
 * @property integer $generateFor
 * @property integer $baseFor
 * @property array $generateForArray
 * @property array $generatedRoles
 * @property boolean $textsOnly
 * @property boolean $unique
 */
class GenerationType extends ActiveRecord
{
    const BASE_FOR_CONCURS_TEAM = 1;
    const BASE_FOR_CONCURS_INDIVIDUAL = 2;

    const NO_TEAM_GRAM = 0;
    const TEAM_GRAM_FOR_TEAM = 1;
    const TEAM_GRAM_FOR_ALL = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'generation_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['textsOnly', 'unique'], 'boolean'],
            [['generateFor', 'baseFor', 'teamGram'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'generateFor' => 'Для кого генерировать',
            'teamGram'   => 'Командная грамота',
            'teamGramText' => 'Командная грамота',
            'generateForArray' => 'Для кого генерировать',
            'generatedRoles' => 'Для кого генерировать',
            'baseFor' => 'Базовый для',
            'baseForText' => 'Базовый для',
            'textsOnly' => 'Отображать только тексты',
            'unique' => 'Награда уникальная для команды',
        ];
    }

    public static function getBaseForTypes()
    {
        return [
            static::BASE_FOR_CONCURS_TEAM => 'Командные конкурсы',
            static::BASE_FOR_CONCURS_INDIVIDUAL => 'Индивидуальные конкурсы'
        ];
    }

    public static function getTeamGrams()
    {
        return [
            static::NO_TEAM_GRAM => 'Без командных грамот',
            static::TEAM_GRAM_FOR_TEAM => 'С командными грамотами для команд',
            static::TEAM_GRAM_FOR_ALL => 'С командными грамотами для всех'
        ];
    }

    /**
     * @param $name
     *
     * @return ActiveRecord
     */
    public static function findByName($name)
    {
        return self::find()->andWhere(['name' => $name])->one();
    }

    /**
     * @return array
     */
    public function getGeneratedRoles()
    {
        if(!is_null($this->generateForArray)){
            $roles = People::getRoles();
            $generatedRoles = $this->generateForArray;

            $result = [];

            foreach ($generatedRoles as $generatedRole) {
                $result[$generatedRole] = $roles[$generatedRole];
            }

            return implode(', ', $result);
        }

        return null;
    }

    /**
     * @return array
     */
    public function setGeneratedRoles($value)
    {
        if(is_null($value)){
            $this->generateFor = null;
        }
        else{
            $this->generateForArray = $value;
        }

        return null;
    }

    /**
     * @return array
     */
    public function getGenerateForArray()
    {
        $generateFor = $this->generateFor;

        if(empty($generateFor)){
            return null;
        }

        $result = [];

        $id = 1;

        while ($generateFor>0){
            if($generateFor & 0x1){
                $result[] = $id;
            }

            $id++;
            $generateFor = $generateFor >> 1;
        }

        return $result;
    }

    public function setGenerateForArray($value)
    {
        if(!is_array($value)){
            return;
        }

        $generateFor = 0;

        foreach ($value as $position) {
            $generateFor = $generateFor | (0x1 << (intval($position)-1));
        }

        $this->generateFor = $generateFor;
    }

    public static function getGenerationTypes()
    {
        $types = self::find()->asArray()->select(['name','id'])->indexBy('id')->orderBy(['name' => SORT_DESC])->all();
        return ArrayHelper::getColumn($types, 'name');
    }

    public function getBaseForText()
    {
        return ArrayHelper::getValue(static::getBaseForTypes(), $this->baseFor, $this->baseFor);
    }

    public function getTeamGramText()
    {
        return ArrayHelper::getValue(static::getTeamGrams(), $this->teamGram, $this->teamGram);
    }

    public function recalculateContests()
    {
        /** @var Contest[] $contests */
        $contests = Contest::find()
            ->orWhere(['generation_type_id' => null])
            ->orWhere(['generation_type_id' => $this->id])
            ->all();

        foreach ($contests as $contest) {
            if($contest->nestedGeneration_type_id == $this->id) {
                $contest->recalculateHonors();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if((isset($changedAttributes['generateFor']) && $this->generateFor != $changedAttributes['generateFor'])
            || (isset($changedAttributes['teamGram']) && $this->teamGram != $changedAttributes['teamGram']))
        {
            $this->recalculateContests();
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
