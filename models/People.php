<?php

namespace app\models;

use app\models\museumResult\AgeCategory;
use app\models\museumResult\MuseumBall;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "peoples".
 *
 * @property integer        $id
 * @property integer        $role
 * @property string         $name
 * @property string         $surname
 * @property string         $patronomic
 * @property integer        $school_id
 * @property string         $schoolText
 * @property string         $schoolGramText
 * @property string         $schoolName
 * @property integer        $classNum
 * @property string         $classText
 * @property integer        $team_info_id
 * @property integer        $teamNumber
 * @property string         $text
 *
 * @property bool           $isShowRoleAlways
 * @property string         $roleName
 * @property HonorsPeople[] $honorsPeoples
 * @property Honor[]        $honors
 * @property PeopleSchool   $school
 * @property TeamInfo       $teamInfo
 */
class People extends ActiveRecord
{
    const ROLE_CAPTAIN = 1;

    const ROLE_TOMMY = 2;

    const ROLE_CONVOY = 3;

    private static $_roleNames = [
        self::ROLE_CAPTAIN => 'Капитан',
        self::ROLE_TOMMY   => 'Участник',
        self::ROLE_CONVOY  => 'Сопровождающий'
    ];

    private static $_gramRoleNameAlways = [
        self::ROLE_CAPTAIN => false,
        self::ROLE_TOMMY   => false,
        self::ROLE_CONVOY  => true
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'peoples';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'school_id', 'team_info_id', 'role'], 'integer'],
            [['classNum'], 'integer', 'min' => 1, 'max' => 11],
            [['name', 'surname', 'patronomic'], 'string', 'max' => 64],
            [['schoolText'], 'string', 'max' => 128],
            [['classText'], 'string', 'max' => 16],
            [
                ['team_info_id', 'role'],
                'unique',
                'targetAttribute' => ['team_info_id', 'role'],
                'when'            => function ($model) {
                    return $model->role != self::ROLE_TOMMY;
                }
            ],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => PeopleRole::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => PeopleSchool::className(), 'targetAttribute' => ['school_id' => 'id']],
            [['team_info_id'], 'exist', 'skipOnError' => true, 'targetClass' => TeamInfo::className(), 'targetAttribute' => ['team_info_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'role'         => 'Роль участника',
            'roleName'     => 'Роль участника',
            'role_id'      => 'Роль участника',
            'name'         => 'Имя',
            'surname'      => 'Фамилия',
            'patronomic'   => 'Отчество',
            'stage'        => 'Этап',
            'teamNumber'   => 'Номер команды',
            'school_id'    => 'Школа',
            'schoolName'   => 'Школа',
            'schoolText'   => 'Название школы',
            'classNum'     => 'Номер класса',
            'classText'    => 'Текст класса',
            'team_info_id' => 'Инфо о команде',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonorsPeoples()
    {
        return $this->hasMany(HonorsPeople::className(), ['peoples_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonors()
    {
        return $this->hasMany(Honor::className(), ['id' => 'honors_id'])->viaTable('honors_peoples', ['peoples_id' => 'id']);
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getRole()
//    {
//        return $this->hasOne(PeopleRole::className(), ['id' => 'role_id']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(PeopleSchool::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamInfo()
    {
        return $this->hasOne(TeamInfo::className(), ['id' => 'team_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id'])
            ->via('teamInfo');
    }

    public static function getRoles()
    {
        return self::$_roleNames;
    }

    public function getRoleName()
    {
        $roles = self::getRoles();

        return ArrayHelper::getValue($roles, $this->role, 'Неизвестная роль под номером ' . $this->role);
    }

    public function getSchoolName()
    {
        if (empty($this->school_id) || empty($this->school)) {
            return null;
        }

        return $this->school->esr . ': ' . $this->school->text;
    }

    public function getSchoolGramText()
    {
        if (empty($this->school_id) || empty($this->school)) {
            return $this->schoolText;
        }

        return $this->school->text;
    }

    public function getStage()
    {
        return $this->teamInfo->stage;
    }

    public function getTeamNumber()
    {
        return $this->team->number;
    }

    public function getText()
    {
        return $this->surname . ' ' . $this->name;
    }

    public function getIsShowRoleAlways()
    {
        return ArrayHelper::getValue(self::$_gramRoleNameAlways, $this->role, false);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($this->role == self::ROLE_CAPTAIN && ((isset($changedAttributes['classNum']) && $changedAttributes['classNum']!=$this->classNum) || $insert)) {
            $ageCategory = AgeCategory::findByClass($this->classNum);

            $teamInfo = $this->teamInfo;
            if(empty($ageCategory)){
                $teamInfo->age_category_id = null;
            }
            else{
                $teamInfo->age_category_id = $ageCategory->id;
            }

            $teamInfo->save();
        }

        if($insert
            || (isset($changedAttributes['classNum']) && $changedAttributes['classNum']!=$this->classNum)
            || (isset($changedAttributes['school_id']) && $changedAttributes['school_id']!=$this->school_id))
        {
            $team = $this->teamInfo->team;

            $lastScoolId = $team->school_id;
            if(empty($lastScoolId)){
                $team->school_id = $this->school_id;
            }
            elseif($this->role == self::ROLE_CAPTAIN){
                $team->school_id = $this->school_id;
            }

            $lastClassNum = $team->classNum;
            if(empty($lastClassNum)){
                $team->classNum = $this->classNum;
            }
            elseif($this->role == self::ROLE_CAPTAIN){
                $team->classNum = $this->classNum;
            }
        }

        if($insert){
            // recalculate museumBalls;
            $this->teamInfo->recalculateTeamType();
            $this->teamInfo->save();

            MuseumBall::recalculateByTeam($this->teamInfo->team_id, $this->teamInfo->stage);
            Honor::recalculateByTeam($this->teamInfo->team_id, $this->teamInfo->stage);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        // recalculate museumBalls;
        $this->teamInfo->recalculateTeamType();
        $this->teamInfo->save();

        parent::afterDelete();
    }

    public function beforeDelete()
    {
        HonorsPeople::deleteAll(['peoples_id' => $this->id]);

        return parent::beforeDelete();
    }
}
