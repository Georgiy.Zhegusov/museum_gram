<?php

namespace app\models\search;

use app\models\Team;
use app\models\TeamMosolimpId;
use yii\data\ActiveDataProvider;


/**
 * Class TeamSearch
 *
 * @property string $mosolimpNumbers
 *
 * @package app\models\search
 */
class TeamSearch extends Team
{
    public $mosolimpNumbers;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'temporary_number', 'mosolimpNumbers'], 'integer'],
        ];
    }

    public function search($params)
    {
        $query = Team::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if(!empty($this->mosolimpNumbers)){
            $query->joinWith('mosolimpIds');
            $query->andWhere(['like', TeamMosolimpId::tableName() . '.number', $this->mosolimpNumbers]);
        }

        $query->andFilterWhere(['like','number', $this->number]);

        $query->andFilterWhere(['like','temporary_number', $this->temporary_number]);

        return $dataProvider;
    }
}
