<?php

namespace app\models\search;

use app\models\Honor;
use app\models\People;
use app\models\Team;
use app\models\TeamInfo;
use Yii;
use yii\data\ActiveDataProvider;


class HonorSearch extends People
{
    public $nestedTextAfter1;
    public $nestedTextAfter2;
    public $nestedTextAfter3;
    public $nestedTextBefore1;
    public $nestedTextBefore2;
    public $nestedTextBefore3;
    public $team_id;
    public $contest_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nestedTextAfter1', 'nestedTextAfter2', 'nestedTextAfter3', 'nestedTextBefore1', 'nestedTextBefore2', 'nestedTextBefore3'], 'string'],
            [['team_id', 'contest_id'], 'integer']
        ];
    }

    public function search($params)
    {
        $query = Honor::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'textAfter1', $this->nestedTextAfter1]);
        $query->andFilterWhere(['like', 'textAfter2', $this->nestedTextAfter2]);
        $query->andFilterWhere(['like', 'textAfter3', $this->nestedTextAfter3]);
        $query->andFilterWhere(['like', 'textBefore1', $this->nestedTextBefore1]);
        $query->andFilterWhere(['like', 'textBefore2', $this->nestedTextBefore2]);
        $query->andFilterWhere(['like', 'textBefore3', $this->nestedTextBefore3]);

        $query->andFilterWhere(['team_id' => $this->team_id]);
        $query->andFilterWhere(['contest_id' => $this->contest_id]);

        return $dataProvider;
    }
}
