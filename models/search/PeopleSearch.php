<?php

namespace app\models\search;

use app\models\People;
use app\models\Team;
use app\models\TeamInfo;
use Yii;
use yii\data\ActiveDataProvider;


class PeopleSearch extends People
{
    public $stage;
    public $teamNumber;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'patronomic','schoolText','classText', 'teamNumber'], 'string'],
            [['classNum', 'role', 'school_id', 'stage'], 'integer']
        ];
    }

    public function search($params)
    {
        $query = People::find();

        $query->joinWith('team', false);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'surname', $this->surname]);
        $query->andFilterWhere(['like', 'patronomic', $this->patronomic]);

        $query->andFilterWhere(['school_id' => $this->school_id]);
        $query->andFilterWhere(['role' => $this->role]);
        $query->andFilterWhere(['classNum' => $this->classNum]);
        $query->andFilterWhere(['like', 'schoolText', $this->schoolText]);
        $query->andFilterWhere(['like', 'classText', $this->schoolText]);

        $query->andFilterWhere(['like', Team::tableName() . '.number', $this->teamNumber]);

        $query->andFilterWhere([TeamInfo::tableName() . '.stage' => $this->stage]);

        return $dataProvider;
    }
}
