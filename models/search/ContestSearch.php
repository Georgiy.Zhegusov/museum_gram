<?php

namespace app\models\search;

use app\models\Contest;
use app\models\ContestType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**

 */
class ContestSearch extends Contest
{
    public $stage;
    public $category;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contest_type_id'], 'integer'],
            [['textBefore1', 'textBefore2', 'textBefore3', 'textAfter1', 'textAfter2', 'textAfter3'], 'string', 'max' => 32],
            [['name', 'codeName'], 'string', 'max' => 32],
            ['stage', 'integer'],
            ['category', 'integer']
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contest::find();

        // add conditions that should always apply here

        $query->joinWith('contestType',false);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['attributes' => [
                'id',
                'name',
                'codeName',
                'contest_type_id',
                'category' => [
                    'asc' => [
                        ContestType::tableName() . '.contest_category_id' => SORT_ASC
                    ],
                    'desc' => [
                        ContestType::tableName() . '.contest_category_id' => SORT_DESC
                    ]
                ],
                'stage' => [
                    'asc' => [
                        ContestType::tableName() . '.stage' => SORT_ASC
                    ],
                    'desc' => [
                        ContestType::tableName() . '.stage' => SORT_DESC
                    ]
                ]]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if(!empty($this->name)) {
            $query->andWhere(['like', static::tableName() . '.name', $this->name]);
        }

        if(!empty($this->codeName)) {
            $query->andWhere(['like', static::tableName() . '.name', $this->name]);
        }

        $query->andFilterWhere(['contest_type_id' => $this->contest_type_id]);

        $query->andFilterWhere(['like', 'textBefore1', $this->textBefore1]);
        $query->andFilterWhere(['like', 'textBefore2', $this->textBefore2]);
        $query->andFilterWhere(['like', 'textBefore3', $this->textBefore3]);
        $query->andFilterWhere(['like', 'textAfter1', $this->textAfter1]);
        $query->andFilterWhere(['like', 'textAfter2', $this->textAfter2]);
        $query->andFilterWhere(['like', 'textAfter3', $this->textAfter3]);

        if(!empty($this->stage) || !empty($this->category))
        {
            $query->joinWith('contestType',false);

            $query->andFilterWhere([ContestType::tableName() . '.stage' => $this->stage]);
            $query->andFilterWhere([ContestType::tableName() . '.contest_category_id' => $this->category]);
        }

        return $dataProvider;
    }
}
