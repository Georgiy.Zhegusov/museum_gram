<?php

namespace app\models;

use app\common\behaviors\NestedValuesBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "honors".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $contest_id
 * @property string $textBefore
 * @property string $textAfter
 * @property integer $number
 * @property string $image_path
 * @property string $image_thumb
 *
 * @property Contest $contest
 * @property Team $team
 * @property TeamInfo $teamInfo
 * @property HonorsPeople[] $honorsPeoples
 * @property People[] $peoples
 * @property Honor[] $linkedHonors
 *
 * @property string $generatedRoles
 * @property string $text
 *
 * @property string $textBefore1
 * @property string $textBefore2
 * @property string $textBefore3
 * @property string $textAfter1
 * @property string $textAfter2
 * @property string $textAfter3
 *
 * @property string   $nestedSubstrate
 * @property string   $nestedSubstrateThumb
 * @property string   $nestedSubstrateBack
 * @property string $nestedTextBefore1
 * @property string $nestedTextBefore2
 * @property string $nestedTextBefore3
 * @property string $nestedTextAfter1
 * @property string $nestedTextAfter2
 * @property string $nestedTextAfter3
 */
class Honor extends ActiveRecord
{
    public $imageFile;
    public $deleteImage;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'honors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imageFile', 'deleteImage', 'clearGeneratedRoles',
                'clearTextAfter1','clearTextAfter2','clearTextAfter3',
                'clearTextBefore1','clearTextBefore2','clearTextBefore3'], 'safe'],
            [['image_path', 'image_thumb'], 'string', 'max' => 256],
            [['team_id', 'contest_id', 'number'], 'integer'],
            [['textBefore1', 'textBefore2', 'textBefore3', 'textAfter1', 'textAfter2', 'textAfter3'], 'string'],
            [['team_id', 'contest_id'], 'required'],
            [['team_id', 'contest_id'], 'unique', 'targetAttribute' => ['team_id', 'contest_id'],'when' => function($some) {
                return $this->contest->generationType->unique;
            }],
            [['contest_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contest::className(), 'targetAttribute' => ['contest_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            ['contest_id', function($attribute, $params, $validator){
                $contest = $this->contest;

                $teamInfo = $this->team->getTeamInfo($contest->stage);

                if(empty($teamInfo)){
                    $this->addError($attribute, 'Для команды ' . $this->team->number . ' не указана информация для ' . $contest->stage . ' этапа.');
                }

                return true;
            }],
        ];
    }

    public function behaviors()
    {
        return [
            'nestedValues' => [
                'class' => NestedValuesBehavior::className(),
                'nestedSuffix' => null,
                'nestProperty' => 'contest',
                'nestedFields' => [
                    'textAfter1',
                    'textAfter2',
                    'textAfter3',
                    'textBefore1',
                    'textBefore2',
                    'textBefore3'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Команда',
            'contest_id' => 'Конкурс',
            'number' => 'Номер награды',

            'stage' => 'Этап',
            'generation_type' => 'Тип генерации',

            'textBefore1' => 'Текст до 1',
            'textBefore2' => 'Текст до 2',
            'textBefore3' => 'Текст до 3',
            'textAfter1' => 'Текст после 1',
            'textAfter2' => 'Текст после 2',
            'textAfter3' => 'Текст после 3',

            'imageFile' => 'Картинка на грамоту',
            'deleteImage' => 'Удалить картинку с грамоты',

            'nestedTextBefore1' => 'Текст до 1',
            'nestedTextBefore2' => 'Текст до 2',
            'nestedTextBefore3' => 'Текст до 3',
            'nestedTextAfter1' => 'Текст после 1',
            'nestedTextAfter2' => 'Текст после 2',
            'nestedTextAfter3' => 'Текст после 3',
            'clearTextBefore1' => 'Очистить текст до 1',
            'clearTextBefore2' => 'Очистить текст до 2',
            'clearTextBefore3' => 'Очистить текст до 3',
            'clearTextAfter1' => 'Очистить текст после 1',
            'clearTextAfter2' => 'Очистить текст после 2',
            'clearTextAfter3' => 'Очистить текст после 3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'contest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return null|ActiveRecord
     */
    public function getTeamInfo()
    {
        if(empty($this->team_id) || empty($this->team) || empty($this->contest_id) || empty($this->contest)){
            return null;
        }

        return $this->team->getTeamInfo($this->contest->stage);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonorsPeoples()
    {
        return $this->hasMany(HonorsPeople::className(), ['honors_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeoples()
    {
        return $this->hasMany(People::className(), ['id' => 'peoples_id'])->viaTable(HonorsPeople::tableName(), ['honors_id' => 'id'])->indexBy('id');
    }

    public static function findHonor($teamId, $contestId)
    {
        return self::find()
            ->andWhere(['team_id' => $teamId])
            ->andWhere(['contest_id' => $contestId])
            ->one();
    }

    public static function findHonors($teamId, $contestId)
    {
        return self::find()
            ->andWhere(['team_id' => $teamId])
            ->andWhere(['contest_id' => $contestId])
            ->all();
    }

    public function getGeneratedRoles()
    {
        return $this->contest->nestedGeneratedRoles;
    }

    public static function recalculateByTeam($team_id, $stage)
    {
        /** @var Honor[] $honors */
        $honors = static::find()
            ->andWhere(['team_id' => $team_id])
            ->all();

        foreach ($honors as $honor) {
            $honor->recalculate();
        }
    }

    public function afterSave($insert, $changeAttributes)
    {
        $this->recalculate();

        return parent::afterSave($insert, $changeAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * @return $this[]
     */
    public function getLinkedHonors()
    {
        $contests = null;

        $contestCategory = $this->contest->contestType->contestCategory;

        if($contestCategory->oneForAll){
            $contests = $contestCategory->contests;
        }
        else{
            $contestType = $this->contest->contestType;
            if($contestType->oneForAll) {
                $contests = $contestType->contests;
            }
        }

        if(empty($contests)){
            return [$this];
        }

        return static::findHonors($this->team_id,
            ArrayHelper::getColumn(
                $contests,
                function($value)
                {
                    return $value->id;
                }
            ));
    }

    public function recalculate()
    {
        $generatedRoles = $this->contest->generationType->generateForArray;

        $generatedFor = 0;

        if(!empty($generatedRoles)) {
            $peoples = $this->teamInfo->peoples;

            $linkedPeoples = $this->peoples;

            foreach ($peoples as $people) {
                if (in_array($people->role, $generatedRoles)) {
                    if (!isset($linkedPeoples[$people->id])) {
                        $people->link('honors', $this);
                    }

                    $generatedFor++;
                } else {
                    $people->unlink('honors', $this, true);
                }
            }
        }

        $teamHonorsPeople = HonorsPeople::findTeamGram($this->id);

        if($this->contest->generationType->teamGram == GenerationType::TEAM_GRAM_FOR_ALL
            || ($this->contest->generationType->teamGram == GenerationType::TEAM_GRAM_FOR_TEAM && $generatedFor>1)){
            if(empty($teamHonorsPeople)){
                $teamHonorsPeople = new HonorsPeople();
                $teamHonorsPeople->honors_id = $this->id;
                $teamHonorsPeople->save();
            }
        }
        else{
            if(!empty($teamHonorsPeople)){
                $teamHonorsPeople->delete();
            }
        }
    }

    public function beforeDelete()
    {
        $honorsPeople = $this->honorsPeoples;

        foreach ($honorsPeople as $honorPeople) {
            $honorPeople->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
