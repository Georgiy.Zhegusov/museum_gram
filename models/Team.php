<?php

namespace app\models;

use app\models\museumResult\AgeCategory;
use app\models\museumResult\BaseMusObj;
use app\models\museumResult\Museum;
use app\models\museumResult\MuseumBall;
use app\models\museumResult\NominationBall;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "teams".
 *
 * @property integer          $id
 * @property integer          $number
 * @property integer          $temporary_number
 * @property integer          $school_id
 * @property integer          $classNum
 *
 * @property string           $mentionText
 * @property array            $achives
 * @property integer          $schoolName
 * @property PeopleSchool     $school
 * @property integer          $statusText
 * @property integer          $status
 * @property integer          $fourOne
 * @property integer          $sum
 * @property Honor[]          $honors
 * @property TeamInfo[]       $teamInfos
 * @property TeamMosolimpId[] $mosolimpIds
 * @property MuseumBall[]     $museumBalls
 * @property NominationBall[]     $nominationBalls
 */
class Team extends ActiveRecord
{
    private $_status;
    private $_counts;
    private $_fourOne;
    private $_sum;

    const STATUS_BAAD = null;
    const STATUS_BAD = 0;
    const STATUS_WINNER = 1;
    const STATUS_PRISE = 2;

    public static function getStatuses()
    {
        return [
            static::STATUS_BAAD => 'Ничего',
            static::STATUS_BAD => 'Ничего',
            static::STATUS_PRISE => 'Призер',
            static::STATUS_WINNER => 'Победитель'
        ];
    }

    const WITH_MUSEUMS = true;
    const WITHOUT_MUSEUMS = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'temporary_number', 'school_id', 'classNum'], 'integer'],
            [['number'], 'required'],
            [['number', 'temporary_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'number'           => 'Номер команды',
            'temporary_number' => 'Временный номер команды',
            'mosolimpNumbers'  => 'Лешины номера',
            'fourOne'          => '4 музея + 1 парк',
            'sum'              => 'Сумма',
            'statusText'       => 'Статус',
            'classNum'         => 'Класс',
            'schoolText'       => 'Школа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonors()
    {
        return $this->hasMany(Honor::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamInfos()
    {
        return $this->hasMany(TeamInfo::className(), ['team_id' => 'id'])->orderBy('stage');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(PeopleSchool::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMosolimpIds()
    {
        return $this->hasMany(TeamMosolimpId::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseumBalls()
    {
        return $this->hasMany(MuseumBall::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominationBalls()
    {
        return $this->hasMany(NominationBall::className(), ['team_id' => 'id']);
    }

    /**
     * @param $stage
     *
     * @return TeamInfo|null
     */
    public function getTeamInfo($stage)
    {
        $teamInfo = $this->getTeamInfos()->andWhere(['stage' => $stage])->one();

        if (empty($teamInfo)) {
            return null;
        }

        return $teamInfo;
    }

    public function getStatusText()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status, $this->status);
    }

    public function getSchoolName()
    {
        if (empty($this->school_id) || empty($this->school)) {
            return null;
        }

        return $this->school->esr . ': ' . $this->school->text;
    }

    /**
     * Get all teams, which has not zero balls
     *
     * @param      $museumIds
     * @param null $limit
     * @param int  $offset
     * @param bool $with
     *
     * @return ActiveQuery
     */
    public static function findByMuseums($museumIds, $limit=null, $offset=0, $with = self::WITH_MUSEUMS)
    {
        $query = static::find()
            ->joinWith('museumBalls')
            ->andWhere(['>', MuseumBall::tableName() . '.value', 0]);

        if($with){
            $query->andWhere([MuseumBall::tableName() . '.museum_id' => $museumIds]);
        }
        else{
            $query->andWhere(['not in', MuseumBall::tableName() . '.museum_id', $museumIds]);
        }

        $query->limit($limit)->offset($offset);

        return $query->all();
    }

    /**
     * @param $number
     *
     * @return Team
     */
    public static function findByNumber($number)
    {
        /** @var Team $result */
        $result = self::find()
            ->andWhere(['number' => $number])
            ->one();

        if (empty($result)) {
            $result = self::find()
                ->andWhere(['temporary_number' => $number])
                ->one();
        }

        return $result;
    }

    /**
     * @param $number
     *
     * @return static
     */
    public static function findByMosolimp($number)
    {
        return static::find()
            ->joinWith('mosolimpIds')
            ->andWhere([TeamMosolimpId::tableName() . '.number' => $number])
            ->one();
    }

    /**
     * Get all teams, which has not zero balls
     *
     * @param             $museumIds
     * @param AgeCategory $ageCategory
     * @param bool        $with
     *
     * @param null        $limit
     * @param int         $offset
     *
     * @return ActiveQuery
     */
    public static function findByMuseumsAgeCategories($museumIds, AgeCategory $ageCategory, $stage, $limit=null, $offset=0,$with = self::WITH_MUSEUMS)
    {
        $query = static::find()
            ->joinWith('museumBalls')
            ->andWhere(['or',
                ['>', MuseumBall::tableName() . '.correspondence_round', 0],
                ['>', MuseumBall::tableName() . '.internal_round', 0]
            ])
            ->joinWith('teamInfos')
            ->andWhere([TeamInfo::tableName() . '.stage' => $stage])
            ->andWhere([TeamInfo::tableName() . '.age_category_id' => $ageCategory->id]);

        if($with){
            $query->andWhere([MuseumBall::tableName() . '.museum_id' => $museumIds]);
        }
        else{
            $query->andWhere(['not in', MuseumBall::tableName() . '.museum_id', $museumIds]);
        }

        $query->limit($limit)->offset($offset);

        return $query->all();
    }

    public static function findByString($string)
    {
        $result = self::find()
            ->andWhere(['like', 'number', $string])
            ->all();

        if (empty($result)) {
            $result = self::find()
                ->andWhere(['like', 'temporary_number', $string])
                ->all();
        }

        return $result;
    }

    public static function findForGram($school_id)
    {
        static::find()
            ->joinWith('teamInfo', false);
    }

    public function getMosolimpNumbers()
    {
        $mosolimpIds = $this->mosolimpIds;

        if (empty($mosolimpIds)) {
            return null;
        }

        $numbers = [];

        foreach ($mosolimpIds as $mosolimpId) {
            $numbers[] = $mosolimpId->number;
        }

        return implode(', ', $numbers);
    }

    public function getSum()
    {
        if (!isset($this->_sum)) {
            if(!$this->fourOne){
                $this->_sum = -1;
            }
            else {
                /* Рассчет кол-ва музее учавствующего в рассчете
                 * m, p, u - количества музеев, парков усадеб у участника
                 * если m+p+u<=15, то
                 * p<=m+u
                 * m+u - берем все
                 * если m+p+u>15, то
                 * если p<=7, то p - все берем, а m+u=15-p
                 * если p>7, то
                 * если m+u>=8, то p=7, m+u=8
                 * если m+u<8, то m+u - все, p=m+u
                 */
                $museumCount = $this->getCount(Museum::TYPE_MUSEUM);
                $parkCount   = $this->getCount(Museum::TYPE_PARK);
                $estateCount = $this->getCount(Museum::TYPE_ESTATE);

                if ($museumCount + $estateCount + $parkCount <= 15) {
                    $parkCountForSum   = min($museumCount + $estateCount, $parkCount);
                    $museumCountForSum = $museumCount + $estateCount;
                } else {
                    if ($parkCount <= 7) {
                        $parkCountForSum   = $parkCount;
                        $museumCountForSum = 15 - $parkCount;
                    } else {
                        if ($museumCount + $estateCount >= 8) {
                            $parkCountForSum   = 7;
                            $museumCountForSum = 8;
                        } else {
                            $parkCountForSum   = $museumCount + $estateCount;
                            $museumCountForSum = $museumCount + $estateCount;
                        }
                    }
                }

                /* Мы берем нужное количество музеев и парков,
                 * берем нужное количество МАКСИМАЛЬНЫХ баллов из каждой группы,
                 * а потм СРАВНИВАЕМ минимальный результат, засчитанный по паркам
                 * и минимальный результат, засчитанным по музеям и усадьбам.
                 * Если результат по парку больше, то всё ок, эти баллы и засчитываем.
                 * Если результат минимальный по парку меньше, чем минимальный по музею,
                 * то НЕ засчитываем этот результат по парку, а засчитываем следующий
                 * по музеям и усадьбам. Далее снова сравниваем. Сравниваем до того момента,
                 * пока не кончатся засчитывыемые парки или пока минимальный по парку
                 * не станет больше или равен минимальному засчитываемому по музеям или усадьбам.
                 */
                $museumSums = [];
                $parkSums   = [];

                foreach ($this->museumBalls as $museumBall) {
                    if ($museumBall->museum->type == BaseMusObj::BASE_TYPE_CONCURS) {
                        continue;
                    }

                    $activeMuseumSum = $museumBall->getSum();
                    $activeMuseumSum = $activeMuseumSum < 0 ? 0 : $activeMuseumSum;

                    if ($museumBall->museum->type_id == Museum::TYPE_PARK) {
                        $parkSums[] = $activeMuseumSum;
                    } else {
                        $museumSums[] = $activeMuseumSum;
                    }
                }

                rsort($museumSums);
                rsort($parkSums);

                $sum = 0;
                // Сначала добавим рассчетное кол-во музеев
                for ($museumIndex = 0; $museumIndex < $museumCountForSum; $museumIndex++) {
                    $sum += $museumSums[$museumIndex];
                }

                // Теперь выполняем вторую часть алгоритма
                //Сравнение наименьшего парка с наименьшим( из уже просуммированных ) музеев, пока существует следующий музей.
                while (isset($museumSums[$museumIndex])
                    && $museumSums[$museumIndex] > 0
                    && $parkCountForSum > 0
                    && $museumSums[$museumIndex] > $parkSums[$parkCountForSum - 1]) {
                    $sum += $museumSums[$museumIndex];
                    $museumIndex++;
                    $parkCountForSum--;
                }

                //  добавим рассчетное кол-во музеев
                for ($parkIndex = 0; $parkIndex < $parkCountForSum; $parkIndex++) {
                    $sum += $parkSums[$parkIndex];
                }

                $this->_sum = $sum;
            }
        }

        return $this->_sum;
    }

    public function getFourOne()
    {
        if (!isset($this->_fourOne)) {
            if ($this->getCount(Museum::TYPE_PARK) + $this->getCount(Museum::TYPE_ESTATE) >= 1
                && $this->getCount(Museum::TYPE_MUSEUM) >= 4
            ) {
                $this->_fourOne = true;
            } else {
                $this->_fourOne = false;
            }
        }

        return $this->_fourOne;
    }

    public function getCount($type)
    {
        return ArrayHelper::getValue($this->getCounts(), $type, 0);
    }

    private function getCounts()
    {
        if (!isset($this->_counts)) {
            $counts = [];

            foreach ($this->museumBalls as $museumBall) {
                if ($museumBall->museum->type == BaseMusObj::BASE_TYPE_CONCURS) {
                    continue;
                }

                $value = $museumBall->value;
                if ((empty($value) && $museumBall->museum->type_id != Museum::TYPE_PARK)
                  || (!$museumBall->photo_status && $museumBall->museum->type_id == Museum::TYPE_PARK)){
                    continue;
                }

                if (!isset($counts[$museumBall->museum->type_id])) {
                    $counts[$museumBall->museum->type_id] = 0;
                }

                $counts[$museumBall->museum->type_id]++;
            }

            $this->_counts = $counts;
        }

        return $this->_counts;
    }

    public function getStatus()
    {
        if(!isset($this->_status)){
            if(!$this->fourOne) {
                return $this->_status = static::STATUS_BAD;
            }

            if ($this->sum >= 400 && array_sum($this->getCounts()) >= 7) {
                return $this->_status = static::STATUS_WINNER;
            }
            if ($this->sum >= 300) {
                return $this->_status = static::STATUS_PRISE;
            }

            return $this->_status = static::STATUS_BAD;

        }

        return $this->_status;
    }

    public function getMentionText()
    {
        $achivs = $this->achives;

        if( isset( $achivs['2stage'] ) ){
            if( isset( $achivs['2stage']['win'] ) ){
                return 'за привлечение школьников к участию в олимпиаде, за активную работу с командой и успехи на первом и финальном этапах олимпиады';
            }
            return 'за привлечение школьников к участию в олимпиаде, активную работу с командой, а также сопровождение и помощь ей во время первого этапа олимпиады, что позволило команде принять участие в финальном этапе олимпиады';
        }
        if( isset( $achivs['1stage'] ) ){
            if( $achivs['1stage']['visits']['other']==0 && $achivs['1stage']['visits']['parks']>0 ){
                return 'за привлечение школьников к участию в олимпиаде, помощь команде при поиске ответов на задания по паркам Москвы.';
            }
        }
        if( isset( $achivs['competition'] ) ){
            return 'за привлечение школьников к участию в олимпиаде, консультации и активную работу с командой.';
        }
        if( isset( $achivs['1stage'] ) ){
            return 'за привлечение школьников к участию в олимпиаде, работу с ними в ходе олимпиады, а также сопровождение команд учащихся в музеях и помощь при поиске ответов на вопросы в них.';
        }
        return 'за привлечение школьников к участию в олимпиаде, помощь при регистрации команды';
    }

    public function getAchives(){
        $honors = $this->honors;
        $result = [];
        foreach( $honors as $honor ){
            if($honor->contest->contestType->base_for_type == ContestType::BASE_FOR_CONCURS){
                if( !isset( $result['competition']) ){
                    $result['competition']=0;
                }
                $result['competition']++;
            }
            elseif ($honor->contest->base_for_stage == 2){
                if( !isset( $result['2stage']) ){
                    $result['2stage']=[];
                }
                if( !isset( $result['2stage']['win']) ){
                    $result['2stage']['win']=0;
                }
                $result['2stage']['win']++;
            }
        }

        $stage2 = $this->getTeamInfo(2);
        if(!empty($stage2)){
            if( !isset( $result['2stage']) ){
                $result['2stage']=[];
            }
            if( !isset( $result['2stage']['was']) ){
                $result['2stage']['was']=0;
            }
            $result['2stage']['was']++;
        }

        $result['1stage']=[];
        $result['1stage']['visits']['parks'] = $this->getCount(Museum::TYPE_PARK);
        $result['1stage']['visits']['other'] = array_sum($this->getCounts()) - $this->getCount(Museum::TYPE_PARK);

        $status = $this->status;

        if( !empty($status) ){
            $result['1stage']['win'] = 1;
        }
        else{
            $result['1stage']['was'] = 1;
        }
        return $result;
    }
}
