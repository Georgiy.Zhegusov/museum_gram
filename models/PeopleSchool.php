<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "people_schools".
 *
 * @property integer $id
 * @property string $text
 * @property integer $number
 * @property string $esr
 *
 * @property People[] $peoples
 */
class PeopleSchool extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people_schools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string', 'max' => 128],
            [['number'], 'integer'],
            [['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'number' => 'Номер ЕСР',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeoples()
    {
        return $this->hasMany(People::className(), ['school_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonorPeoples()
    {
        return $this->hasMany(HonorsPeople::className(), ['peoples_id' => 'id'])->viaTable('peoples', ['school_id' => 'id']);
    }

    public static function findByString($string)
    {
        return self::find()
            ->orWhere(['like', 'number', "%$string%", false])
            ->orWhere(['like', 'text', "%$string%", false])
            ->all();
    }

    public static function findByNumber($number)
    {
        return self::find()
            ->andWhere(['number' => $number])
            ->one();
    }

    public function getEsr()
    {
        return 'sch' . str_pad($this->number,6,'0',STR_PAD_LEFT);
    }
}
