<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "honor_generators".
 *
 * @property integer $id
 * @property string $JContestTypes
 * @property string $JContestCategories
 * @property string $JContests
 * @property string $JTeams
 * @property integer $stages
 * @property integer $with_gram
 * @property integer $wiht_info
 * @property integer $by_team
 * @property integer $by_school
 * @property integer $status
 * @property integer $all
 * @property integer $proceed
 *
 * @property integer $percent
 * @property array $contests
 * @property array $contestTypes
 * @property array $contestCategories
 * @property integer $with_info
 *
 * @property GeneratedFile[] $generatedFiles
 */
class HonorGenerator extends \yii\db\ActiveRecord
{
    private $_stages;
    private $_teams;
    private $_contests;
    private $_contestTypes;
    private $_contestCategories;

    const STATUS_CREATED = 0;
    const STATUS_START = 1;
    const STATUS_FINISHED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'honor_generators';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teams', 'contests', 'contestTypes', 'contestCategories', 'stagesArray'], 'safe'],
            [['stages', 'with_gram', 'wiht_info', 'by_team', 'by_school', 'status', 'all', 'proceed'], 'integer'],
            [['JContestTypes', 'JContestCategories', 'JContests', 'JTeams'], 'string', 'max' => 1024],
            ['JTeams', 'validateTeams' , 'skipOnEmpty' => false, 'skipOnError' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'JContestTypes' => 'Jcontest Types',
            'JContestCategories' => 'Jcontest Categories',
            'JContests' => 'Jcontests',
            'JTeams' => 'Jteams',
            'stages' => 'Stages',
            'with_gram' => 'With Gram',
            'wiht_info' => 'Wiht Info',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGeneratedFiles()
    {
        return $this->hasMany(GeneratedFile::className(), ['honor_generator_id' => 'id']);
    }

    public function setCreated()
    {
        foreach ($this->generatedFiles as $file) {
            $file->delete();
        }
        $this->status = self::STATUS_CREATED;
    }

    public function setStarted()
    {
        foreach ($this->generatedFiles as $file) {
            $file->delete();
        }
        $this->status = self::STATUS_START;
    }

    public function setFInished()
    {
        $this->status = self::STATUS_FINISHED;
    }

    public function getContests()
    {
        if(!isset($this->_contests)){
            $contests = json_decode($this->JContests);
            if(empty($contests)){
                $this->_contests = [];
            }
            else{
                $this->_contests = array_map('intval', $contests);
            }
        }

        return $this->_contests;
    }

    public function setContests($value){
        $this->JContests = json_encode($value);
    }

    public function getContestTypes()
    {
        if(!isset($this->_contestTypes)){
            $contestTypes = json_decode($this->JContestTypes);
            if(empty($contestTypes)){
                $this->_contestTypes = [];
            }
            else{
                $this->_contestTypes = array_map('intval', $contestTypes);
            }
        }

        return $this->_contestTypes;
    }

    public function setContestTypes($value){
        $this->JContestTypes = json_encode($value);
    }

    public function getContestCategories()
    {
        if(!isset($this->_contestCategories)){
            $contestCategories = json_decode($this->JContestCategories);
            if(empty($contestCategories)){
                $this->_contestCategories = [];
            }
            else{
                $this->_contestCategories = array_map('intval', $contestCategories);
            }
        }

        return $this->_contestCategories;
    }

    public function setContestCategories($value){
        $this->JContestCategories = json_encode($value);
    }

    public function getTeams()
    {
        if(!isset($this->_teams)){
           $teams = json_decode($this->JTeams);
            if(empty($teams)){
                $this->_teams = [];
            }
            else{
                $this->_teams = $teams;
            }
        }

        return $this->_teams;
    }

    public function setTeams($value){
        $teams = explode(',', $value);

        $result = [];

        foreach ($teams as $team) {
            if(!empty($team)) {
                $result[] = intval(trim($team));
            }
        }

        $result = array_unique($result);

        $this->JTeams = json_encode($result);
    }

    /**
     * @return array
     */
    public function getStagesArray()
    {
        $stages = $this->stages;

        if(empty($stages)){
            return null;
        }

        $result = [];

        $id = 1;

        while ($stages>0){
            if($stages & 0x1){
                $result[] = $id;
            }

            $id++;
            $stages = $stages >> 1;
        }

        return $result;
    }

    public function setStagesArray($value)
    {
        if(!is_array($value)){
            return;
        }

        $stages = 0;

        foreach ($value as $position) {
            $stages = $stages | (0x1 << (intval($position)-1));
        }

        $this->stages = $stages;
    }

    public function getWithInfo()
    {
        return $this->wiht_info;
    }

    public function setWithInfo($value)
    {
        $this->wiht_info = $value;
    }

    public function validateTeams($attribute, $params)
    {
        $numbers = $this->teams;

        $existedTeams = Team::find()
            ->andWhere(['number' => $numbers])
            ->indexBy('number')
            ->asArray()
            ->select(['number', 'id'])
            ->all();

        $unfindedNumbers = [];

        foreach ($numbers as $number) {
            if(!isset($existedTeams[$number])){
                $unfindedNumbers[] = $number;
            }
        }

        if(!empty($unfindedNumbers)){
            $this->addError('teams', "Не найдена команды: " . implode(', ', $unfindedNumbers));
        }
    }

    public function addFile($fileName, $pageStart, $pageFinish)
    {
        $file = new GeneratedFile([
            'honor_generator_id' => $this->id,
            'path' => $fileName,
            'pageStart' => empty($pageStart) ? 0 : $pageStart,
            'pageFinish' => empty($pageFinish) ? 0 : $pageFinish
        ]);

        $file->save();
    }

    public function beforeSave($insert)
    {
        if(is_null($this->status)){
            $this->status = self::STATUS_CREATED;
        }

        return parent::beforeSave($insert);
    }

    public function getPercent()
    {
        if($this->status == self::STATUS_FINISHED){
            return 100;
        }

        $all = $this->all;

        if(empty($all)){
            return 0;
        }

        $percent = $this->proceed / $this->all;

        $percent *= 100;

        $percent = ceil($percent);

        return $percent >= 100 ? 99 : $percent;
    }

    public function add($count)
    {
        $this->proceed += $count;

        $this->save();
    }
}
