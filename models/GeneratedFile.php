<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generated_files".
 *
 * @property integer $id
 * @property string $path
 * @property integer $honor_generator_id
 *
 * @property HonorGenerator $honorGenerator
 */
class GeneratedFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'generated_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['honor_generator_id'], 'integer'],
            [['path'], 'string', 'max' => 255],
            [['honor_generator_id'], 'exist', 'skipOnError' => true, 'targetClass' => HonorGenerator::className(), 'targetAttribute' => ['honor_generator_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'honor_generator_id' => 'Honor Generator ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonorGenerator()
    {
        return $this->hasOne(HonorGenerator::className(), ['id' => 'honor_generator_id']);
    }

    public function beforeDelete()
    {
        $filePath = Yii::getAlias('@app/' . $this->path);

        if(file_exists($filePath)){
            unlink($filePath);
        }
        return parent::beforeDelete();
    }
}
