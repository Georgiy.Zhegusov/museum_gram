<?php

namespace app\models;

use app\common\behaviors\NestedValuesBehavior;
use Yii;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contests".
 *
 * @property integer        $id
 * @property integer        $contest_type_id
 * @property integer        $generation_type_id
 * @property string         $substrate
 * @property string         $substrate_thumb
 * @property string         $substrate_back
 * @property string         $textBefore1
 * @property string         $textBefore2
 * @property string         $textBefore3
 * @property string         $textAfter1
 * @property string         $textAfter2
 * @property string         $textAfter3
 * @property string         $codeName
 * @property string         $name
 * @property integer        $number
 * @property string         $substrateFile
 * @property bool           $deleteSubstrate
 * @property integer        $stage
 * @property integer        $base_for_stage
 *
 * @property string         $nestedSubstrate
 * @property string         $nestedSubstrateThumb
 * @property string         $nestedSubstrateBack
 * @property string         $nestedTextBefore1
 * @property string         $nestedTextBefore2
 * @property string         $nestedTextBefore3
 * @property string         $nestedTextAfter1
 * @property string         $nestedTextAfter2
 * @property string         $nestedTextAfter3
 * @property string         $nestedGeneration_type_id
 * @property integer        $substrate_type
 * @property string         $substrateTypeText
 *
 * @property ContestType    $contestType
 * @property GenerationType $generationType
 * @property Honor[]        $honors
 */
class Contest extends ActiveRecord
{
    const TYPE_IMAGE = 1;
    const TYPE_PLANE = 2;
    const TYPE_CENTER_IMAGE = 3;

    public $substrateFile;
    public $deleteSubstrate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'clearTextAfter1',
                    'clearTextAfter2',
                    'clearTextAfter3',
                    'clearTextBefore1',
                    'clearTextBefore2',
                    'clearTextBefore3'
                ],
                'safe'
            ],
            [['substrateFile', 'deleteSubstrate'], 'safe'],
            [['name', 'contest_type_id'], 'required'],
            [['name', 'codeName', 'number'], 'unique'],
            [['contest_type_id','base_for_stage', 'substrate_type', 'generation_type_id'], 'integer'],
            [['substrate', 'substrate_thumb', 'substrate_back',], 'string', 'max' => 255],
            [['textBefore1', 'textBefore2', 'textBefore3', 'textAfter1', 'textAfter2', 'textAfter3'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['contest_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContestType::className(), 'targetAttribute' => ['contest_type_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            'nestedValues' => [
                'class'        => NestedValuesBehavior::className(),
                'nestedSuffix' => null,
                'nestProperty' => 'contestType',
                'nestedFields' => [
                    'substrateBack'  => [
                        'property' => 'substrate_back',
                    ],
                    'substrateThumb' => [
                        'property' => 'substrate_thumb',
                    ],
                    'generation_type_id',
                    'substrate',
                    'textAfter1',
                    'textAfter2',
                    'textAfter3',
                    'textBefore1',
                    'textBefore2',
                    'textBefore3'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'contest_type_id' => 'Тип конкурса',
            'substrate'       => 'Обложка',
            'deleteSubstrate' => 'Сбросить обложку',
            'substrateFile'   => 'Обложка',
            'stage'           => 'Этап',
            'category'        => 'Категория конкурса',
            'textBefore1'     => 'Текст до 1',
            'textBefore2'     => 'Текст до 2',
            'textBefore3'     => 'Текст до 3',
            'textAfter1'      => 'Текст после 1',
            'textAfter2'      => 'Текст после 2',
            'textAfter3'      => 'Текст после 3',
            'name'            => 'Название',
            'codeName'        => 'Кодовое название',
            'number'          => 'Номер конкурса',
            'base_for_stage'  => 'Грамоты для этапа',

            'substrate_type'  => 'Тип подложки',
            'substrateTypeText'  => 'Тип подложки',
            'generation_type_id' => 'Тип генерации',

            'nestedTextBefore1' => 'Текст до 1',
            'nestedTextBefore2' => 'Текст до 2',
            'nestedTextBefore3' => 'Текст до 3',
            'nestedTextAfter1'  => 'Текст после 1',
            'nestedTextAfter2'  => 'Текст после 2',
            'nestedTextAfter3'  => 'Текст после 3',
            'clearTextBefore1'  => 'Очистить текст до 1',
            'clearTextBefore2'  => 'Очистить текст до 2',
            'clearTextBefore3'  => 'Очистить текст до 3',
            'clearTextAfter1'   => 'Очистить текст после 1',
            'clearTextAfter2'   => 'Очистить текст после 2',
            'clearTextAfter3'   => 'Очистить текст после 3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestType()
    {
        return $this->hasOne(ContestType::className(), ['id' => 'contest_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHonors()
    {
        return $this->hasMany(Honor::className(), ['contest_id' => 'id']);
    }

    public function getStage()
    {
        return $this->contestType->contestCategory->stage;
    }

    public function getGenerationType()
    {
        return GenerationType::findOne($this->nestedGeneration_type_id);
    }

    public static function findByString($string)
    {
        return self::find()
            ->andWhere(['like', 'name', $string])
            ->all();
    }

    /**
     * @return array
     */
    public static function findByNumber($number)
    {
        return self::find()
            ->andFilterWhere(['number' => $number])
            ->one();
    }

    public static function findBaseForStage($stage)
    {
        static::find()
            ->andWhere(['base_for_stage' => $stage])
            ->one();
    }

    public static function getSubstrateTypeTexts()
    {
        return [
            static::TYPE_IMAGE => 'Тип с потенциальной картинкой',
            static::TYPE_PLANE => 'Тип во всю страницу',
            static::TYPE_CENTER_IMAGE => 'Картинка по центру'
        ];
    }

    public function getSubstrateTypeText()
    {
        return ArrayHelper::getValue(static::getSubstrateTypeTexts(), $this->substrate_type, null);
    }

    public function beforeSave($insert)
    {
        $substrateType = $this->substrate_type;
        if($insert && empty($substrateType)){
            $this->substrate_type = Contest::TYPE_PLANE;
        }

        return parent::beforeSave($insert);
    }

    public function recalculateHonors()
    {
        foreach ($this->honors as $honor) {
            $honor->recalculate();
        }
    }
}
