<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uploads".
 *
 * @property integer        $id
 * @property string         $fileName
 * @property string         $path
 * @property integer        $status
 * @property string         $type
 * @property integer        $stringCount
 * @property integer        $uploadedCount
 * @property integer        $parsedCount
 * @property string         $comment
 *
 * @property UploadResult[] $uploadErrors
 * @property integer        $uploadErrorsCount
 * @property UploadResult[] $uploadNews
 * @property integer        $uploadNewsCount
 * @property UploadResult[] $uploadUpdates
 * @property integer        $uploadUpdatesCount
 */
class UploadFile extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_FINISHED = 2;
    const STATUS_ERROR = 3;

    const TYPE_HONOR = 'honor';
    const TYPE_CONTEST = 'cntst';
    const TYPE_MOSOLIMP = 'idesr';
    const TYPE_MUSEUM = 'musem';
    const TYPE_COEFFICIENT = 'coeff';
    const TYPE_PARK = 'park';
    const TYPE_SCHOOL = 'schol';
    const TYPE_MUSEUM_CORR = 'musco';
    const TYPE_MUSEUM_INV = 'musin';
    const TYPE_MUSEUM_PHOTO = 'musph';
    const TYPE_TEAM_BASE = 'teamb';
    const TYPE_TEAM_BASE_PEOPLE = 'teamp';
    const TYPE_TEAM_BASE_PEOPLE_2STAGE = 'tmp2s';
    const TYPE_NOMINATIONS = 'nomin';
    const TYPE_NOMINATIONS_BALLS = 'nombl';
    const TYPE_NOMINATIONS_PERCENT = 'nompr';
    const TYPE_TEAM_RESULT = 'teamr';
    const TYPE_TEAM_STAT = 'teams';
    const TYPE_NOMINATIONS_CRITERIA = 'nomcr';
    const TYPE_NOMINATIONS_MAXIMUM = 'nommx';
    const TYPE_NOMINATIONS_RESULT = 'nomrt';
    const TYPE_CONCURS = 'concr';
    const TYPE_CONCURS_BALLS = 'cncbl';
    const TYPE_MENTION = 'mentn';

    public $uploadedFile;

    private $_loadedResults = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['uploadedFile', 'safe'],
            [['status', 'stringCount', 'uploadedCount', 'parsedCount'], 'integer'],
            [['fileName'], 'string', 'max' => 64],
            [['path', 'comment'], 'string', 'max' => 128],
            [['type'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'uploadedFile'  => 'Файл',
            'fileName'      => 'Файл',
            'path'          => 'Путь',
            'status'        => 'Статус',
            'type'          => 'Тип',
            'stringCount'   => 'Кол-во строк',
            'uploadedCount' => 'Сохранено строк',
            'parsedCount'   => 'Обработано стро',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadErrors()
    {
        return $this->hasMany(UploadResult::className(), ['upload_id' => 'id'])->andWhere(['type' => UploadResult::TYPE_ERROR]);
    }

    public function getUploadErrorsCount()
    {
        return $this->getUploadErrors()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadNews()
    {
        return $this->hasMany(UploadResult::className(), ['upload_id' => 'id'])->andWhere(['type' => UploadResult::TYPE_NEW]);
    }

    public function getUploadNewsCount()
    {
        return $this->getUploadNews()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadUpdates()
    {
        return $this->hasMany(UploadResult::className(), ['upload_id' => 'id'])->andWhere(['type' => UploadResult::TYPE_UPDATE]);
    }

    public function getUploadUpdatesCount()
    {
        return $this->getUploadUpdates()->count();
    }

    public function setNew()
    {
        $this->status = self::STATUS_NEW;
    }

    public function setStarted()
    {
        $this->status = self::STATUS_IN_PROCESS;
    }

    public function setFinished()
    {
        $this->status = self::STATUS_FINISHED;
    }

    public function setError($comment = null)
    {
        $this->status = self::STATUS_ERROR;
        $this->comment = $comment;
    }

    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    public function isFinished()
    {
        return $this->status == self::STATUS_FINISHED || $this->status == self::STATUS_ERROR;
    }

    public function isError()
    {
        return $this->status == self::STATUS_ERROR;
    }

    public function getPercent()
    {
        if ($this->isFinished()) {
            return 100;
        }

        if ($this->isNew() || $this->parsedCount == 0) {
            return 0;
        }

        $value = intval(100 * $this->parsedCount / $this->stringCount);

        if($value >= 100 && !$this->isFinished()){
            return 99;
        }

        return $value;
    }

    /**
     * @return int
     */
    public function clearResults()
    {
        return UploadResult::deleteAll(['upload_id' => $this->id]);
    }

    public function addResultNew($message, $strings, $objectId = null)
    {
        $result = new UploadResult();

        $result->type      = UploadResult::TYPE_NEW;
        $result->message   = $message;
        $result->strings   = $strings;
        $result->objectId  = $objectId;
        $result->upload_id = $this->id;

        $this->_loadedResults[] = $result;

        return $this;
    }

    public function addResultUpdate($message, $strings, $objectId = null)
    {
        $result = new UploadResult();

        $result->type      = UploadResult::TYPE_UPDATE;
        $result->message   = $message;
        $result->strings   = $strings;
        $result->objectId  = $objectId;
        $result->upload_id = $this->id;

        $this->_loadedResults[] = $result;

        return $this;
    }

    public function addResultError($message, $strings, $objectId = null)
    {
        $result = new UploadResult();

        $result->type      = UploadResult::TYPE_ERROR;
        $result->message   = $message;
        $result->strings   = $strings;
        $result->objectId  = $objectId;
        $result->upload_id = $this->id;

        $this->_loadedResults[] = $result;

        return $this;
    }

    public function saveResults()
    {
        if (empty($this->_loadedResults)) {
            return true;
        }

        $someResult = current($this->_loadedResults);

        $rows = [];

        $count = 0;

        foreach ($this->_loadedResults as $key => $loadedResult) {
            $count++;
            unset($this->_loadedResults[$key]);

            $rows[] = $loadedResult->attributes;

            if (empty($this->_loadedResults) || $count == 200) {
                Yii::$app->db->createCommand()->batchInsert(
                    UploadResult::tableName(),
                    $someResult->attributes(),
                    $rows)
                    ->execute();

                $count = 0;
                $rows  = [];
            }
        }
    }

    public function getFullPath()
    {
        return Yii::getAlias('@app/' . $this->path);
    }
}
