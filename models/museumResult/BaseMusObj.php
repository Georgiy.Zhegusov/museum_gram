<?php

namespace app\models\museumResult;

use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "museums".
 *
 * @property integer $id
 * @property string $name
 * @property integer $number
 * @property integer $type
 * @property integer $winner_contest_id
 * @property integer $prise_contest_id
 *
 * @property float   $maxValue
 * @property float   $maxInternal
 * @property float   $maxCorrespondence
 * @property string  $baseTypeText
 * @property string  $baseTypeUrl
 * @property AgeCategory[] $ageCategories
 * @property MuseumCategory[] $categories
 * @property MuseumCoefficient[] $coefficients
 * @property MuseumBall[] $museumBalls
 * @property Contest                     $winnerContest
 * @property Contest                     $priseContest
 */
class BaseMusObj extends ActiveRecord
{
    private $_ageCategories = [];

    const BASE_TYPE_MUSEUMS = 0;
    const BASE_TYPE_CONCURS = 1;

    private static $_baseTypeNames = [
        self::BASE_TYPE_MUSEUMS => 'Музей',
        self::BASE_TYPE_CONCURS => 'Конкурс',
    ];

    private static $_baseTypeUrls = [
        self::BASE_TYPE_MUSEUMS => 'museums',
        self::BASE_TYPE_CONCURS => 'concurs',
    ];

    protected static $baseType = null;

    private $_coefficient = [];

    private $_object;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 128],
            [['number'], 'unique', 'targetAttribute' => ['number', 'type']],
            [['name'], 'required'],
            [['winner_contest_id', 'prise_contest_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'number' => 'Номер',
            'type' => 'Тип'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefficients()
    {
        return $this->hasMany(MuseumCoefficient::className(), ['museum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseumBalls()
    {
        return $this->hasMany(MuseumBall::className(), ['museum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(MuseumCategory::className(), ['id' => 'museum_categories_id'])->viaTable(MuseumCategoryLink::tableName(), ['museums_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesLinks()
    {
        return $this->hasMany(MuseumCategoryLink::className(), ['museums_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategoriesLinks()
    {
        return $this->hasMany(MuseumAgeCategoryLink::className(), ['museum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategories()
    {
        return $this->hasMany(AgeCategory::className(), ['id' => 'age_category_id'])->viaTable(MuseumAgeCategoryLink::tableName(), ['museum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWinnerContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'winner_contest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriseContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'prise_contest_id']);
    }

    public function getTypeText()
    {
        return Museum::getTypeName($this->type_id);
    }

    /**
     * @param AgeCategory $ageCategory
     *
     * @return bool
     * @internal param $classNum
     *
     */
    public function checkCategory(AgeCategory $ageCategory)
    {
        if(!isset($this->_ageCategories[$ageCategory->id])) {
            $this->_ageCategories[$ageCategory->id] = ($this->getAgeCategories()
                ->andWhere(['id' => $ageCategory->id])
                ->count() > 0);
        }

        return $this->_ageCategories[$ageCategory->id];
    }

    public function getBaseTypeText()
    {
        return static::getBaseTypeName($this->type);
    }

    public static function getBaseTypes()
    {
        return self::$_baseTypeNames;
    }

    public static function getBaseTypeName($typeId)
    {
        return ArrayHelper::getValue(self::$_baseTypeNames, $typeId, $typeId);
    }

    public static function getBaseTypeUrl($typeId)
    {
        return ArrayHelper::getValue(self::$_baseTypeUrls, $typeId, $typeId);
    }

    public static function findByString($string)
    {
        $result = static::find()
            ->andWhere(['or',['like', 'name', $string], ['like', 'number', $string]])
            ->all();

        return $result;
    }

    public static function findByNumber($number)
    {
        $result = static::find()
            ->andWhere(['number' => $number])
            ->one();

        return $result;
    }


    public function beforeSave($insert)
    {
        $this->type = static::$baseType;

        if($insert || $this->name != $this->getOldAttribute('name')){
            $this->updateContest();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return ActiveQuery
     */
    public static function find(){
        return parent::find()
            ->andFilterWhere(['type' => static::$baseType]);
    }

    public function getCoefficient($visitDate){
        if(!isset($this->_coefficient[$visitDate])){
            $this->_coefficient[$visitDate] = MuseumCoefficient::getValue($visitDate, $this->id);
        }

        return $this->_coefficient[$visitDate];
    }

    public function getMaxInternal()
    {
        $object = $this->getObject();

        return $object ? $object->getMaxInternal() : 0;
    }

    public function getMaxCorrespondence()
    {
        $object = $this->getObject();

        return $object ? $object->getMaxCorrespondence() : 0;
    }

    public function getMaxValue()
    {
        return $this->getMaxCorrespondence() + $this->getMaxInternal();
    }

    public function getObject()
    {
        if(!$this->_object){
            if($this->type == static::BASE_TYPE_CONCURS){
                $this->_object = Concurs::findOne(['id' => $this->id]);
            }elseif($this->type == static::BASE_TYPE_MUSEUMS){
                $this->_object = Museum::findOne(['id' => $this->id]);
            }
        }

        return $this->_object;
    }

    public function updateContest()
    {
        if($this->priseContest){
            $this->priseContest->textAfter2 = $this->name;
            $this->priseContest->save();
        }else{
            $baseType = ContestType::findBaseFor(ContestType::BASE_FOR_CONCURS, ContestType::BASE_FOR_PRISE);

            if($baseType){
                $newContest = new Contest();
                $newContest->codeName = 'Призёр: ' . $this->name;
                $newContest->name = 'Призёр: ' . $this->name;
                $newContest->contest_type_id = $baseType->id;
                $newContest->textAfter2 = $this->name;
                $newContest->substrate_type = Contest::TYPE_IMAGE;

                if($this->type_id == Concurs::TYPE_INDIVIDUAL) {
                    $baseGenerationType = GenerationType::find()
                        ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_INDIVIDUAL])
                        ->one();
                }
                else{
                    $baseGenerationType = GenerationType::find()
                        ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_TEAM])
                        ->one();
                }

                if(!empty($baseGenerationType)){
                    $newContest->generation_type_id = $baseGenerationType->id;
                }

                $newContest->save();

                $this->prise_contest_id = $newContest->id;
            }
        }

        if($this->winnerContest){
            $this->winnerContest->textAfter2 = $this->name;
            $this->winnerContest->save();
        }else{
            $baseType = ContestType::findBaseFor(ContestType::BASE_FOR_CONCURS, ContestType::BASE_FOR_WINNER);

            if($baseType){
                $newContest = new Contest();
                $newContest->codeName = 'Победитель: ' . $this->name;
                $newContest->name = 'Победитель: ' . $this->name;
                $newContest->contest_type_id = $baseType->id;
                $newContest->textAfter2 = $this->name;
                $newContest->substrate_type = Contest::TYPE_IMAGE;

                if($this->type_id == Concurs::TYPE_INDIVIDUAL) {
                    $baseGenerationType = GenerationType::find()
                        ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_INDIVIDUAL])
                        ->one();
                }
                else{
                    $baseGenerationType = GenerationType::find()
                        ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_TEAM])
                        ->one();
                }

                if(!empty($baseGenerationType)){
                    $newContest->generation_type_id = $baseGenerationType->id;
                }

                $newContest->save();

                $this->winner_contest_id = $newContest->id;
            }
        }
    }
}
