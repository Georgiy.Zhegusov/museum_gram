<?php

namespace app\models\museumResult;

use Yii;

/**
 * This is the model class for table "nominations_museums".
 *
 * @property integer $nomination_id
 * @property integer $museum_id
 * @property integer $useForMax
 *
 * @property string $museumText
 * @property BaseMusObj $museum
 * @property Nomination $nomination
 */
class NominationMuseumLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nominations_museums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomination_id', 'museum_id'], 'required'],
            [['nomination_id', 'museum_id', 'useForMax'], 'integer'],
            [['museum_id'], 'unique', 'targetAttribute' => ['nomination_id', 'museum_id']],
            [['museum_id'], 'exist', 'skipOnError' => true, 'targetClass' => BaseMusObj::className(), 'targetAttribute' => ['museum_id' => 'id']],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomination::className(), 'targetAttribute' => ['nomination_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomination_id' => 'Номинация',
            'museum_id' => 'Объект',
            'useForMax' => 'Использовать в рассчетах максимального',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseum()
    {
        return $this->hasOne(BaseMusObj::className(), ['id' => 'museum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nomination::className(), ['id' => 'nomination_id']);
    }

    public function getMuseumText()
    {
        return $this->museum->number . ': ' . $this->museum->name;
    }

    /**
     * @param $nomination_id
     * @param $museum_id
     *
     * @return array|null|\yii\db\ActiveRecord
     *
     */
    public static function findBy($nomination_id, $museum_id)
    {
        return static::find()
            ->andWhere(['nomination_id' => $nomination_id])
            ->andWhere(['museum_id' => $museum_id])
            ->one();
    }

    public function getId()
    {
        return null;
    }

    public function afterDelete()
    {
        $this->nomination->recalculate();
        $this->nomination->generateForAll();

        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($changedAttributes['museum_id'])
            || isset($changedAttributes['nomination_id'])
            || $insert)
        {
            $this->nomination->recalculate();
            $this->nomination->generateForAll();
        }
        else{
            if(isset($changedAttributes['useForMax'])){
                $this->nomination->recalculate();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
