<?php

namespace app\models\museumResult;

use app\common\components\ActiveRecord\ActiveRecord;
use app\models\Contest;
use app\models\ContestType;
use app\models\GenerationType;
use app\models\Team;
use app\models\TeamInfo;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nominations".
 *
 * @property integer                     $id
 * @property integer                     $number
 * @property string                      $name
 * @property integer                     $operator
 * @property integer                     $onlyMyAge
 * @property integer                     $minCount
 * @property integer                     $type
 * @property bool                        $round
 * @property integer                     $winner_contest_id
 * @property integer                     $prise_contest_id
 *
 *
 * @property float                       $maxVal
 * @property string                      $musObjName
 * @property string                      $musObjUrl
 * @property integer                     $musObjType
 * @property string                      $operatorName
 * @property string                      $typeName
 * @property NominationAgeCategoryLink[] $nominationsAgeCategories
 * @property AgeCategory[]               $ageCategories
 * @property NominationMuseumLink[]      $nominationsMuseums
 * @property BaseMusObj[]                $museums
 * @property Contest                     $winnerContest
 * @property Contest                     $priseContest
 */
class Nomination extends \yii\db\ActiveRecord
{
    const neededStage = 1;

    const OPERATOR_COUNT = 0;
    const OPERATOR_SUM = 1;

    private static $_operatorNames = [
        self::OPERATOR_COUNT => 'Количество',
        self::OPERATOR_SUM   => 'Сумму'
    ];

    const TYPE_MUSEUM = 0;
    const TYPE_CONCURS = 1;

    private static $_typeNames = [
        self::TYPE_MUSEUM  => 'Музеи',
        self::TYPE_CONCURS => 'Конкурсы'
    ];

    private static $_musObjTypes = [
        self::TYPE_MUSEUM  => BaseMusObj::BASE_TYPE_MUSEUMS,
        self::TYPE_CONCURS => BaseMusObj::BASE_TYPE_CONCURS
    ];

    private $_maxValue;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nominations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'operator', 'onlyMyAge', 'minCount', 'type', 'prise_contest_id','winner_contest_id'], 'integer'],
            ['round', 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['type', 'operator'], 'required'],
            [
                'type',
                function ($attributes) {
                    if ($this->hasAnotherMusTypes()) {
                        $this->addError('type', 'Нельзя сменить тип номинации с привязанными объектам');

                        return false;
                    }

                    return true;
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'number'       => 'Номер',
            'name'         => 'Имя',
            'operator'     => 'Что считать:',
            'operatorName' => 'Что считать:',
            'onlyMyAge'    => 'Только моя возрастная категория',
            'minCount'     => 'Минимальное количество',
            'type'         => 'По чему считать:',
            'typeName'     => 'По чему считать:',
            'round'        => 'Только заочка'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominationsAgeCategories()
    {
        return $this->hasMany(NominationAgeCategoryLink::className(), ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategories()
    {
        return $this->hasMany(AgeCategory::className(), ['id' => 'age_category_id'])->viaTable('nominations_age_category', ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominationsMuseums()
    {
        return $this->hasMany(NominationMuseumLink::className(), ['nomination_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWinnerContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'winner_contest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriseContest()
    {
        return $this->hasOne(Contest::className(), ['id' => 'prise_contest_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseums()
    {
        return $this->hasMany(BaseMusObj::className(), ['id' => 'museum_id'])->viaTable('nominations_museums', ['nomination_id' => 'id']);
    }

    /**
     * @param $ageCategory
     *
     * @return self[]
     */
    public static function findWithAgeCategory($ageCategory)
    {
        return Nomination::find()
            ->joinWith('nominationsAgeCategories')
            ->andWhere([NominationAgeCategoryLink::tableName() . '.age_category_id' => $ageCategory])
            ->all();
    }

    /**
     * @param BaseMusObj|Museum $museum
     *
     * @return Nomination[]
     */
    public static function findNominationForRecalculate(BaseMusObj $museum)
    {
        return static::find()
            ->andWhere(['onlyMyAge' => true])
            ->joinWith('nominationsMuseums')
            ->andWhere([NominationMuseumLink::tableName() . '.museum_id' => $museum->id])
            ->distinct()
            ->all();
    }

    public static function recalculateNominations(Museum $museum, AgeCategory $ageCategory = null)
    {
        $nominations = static::findNominationForRecalculate($museum);
        foreach ($nominations as $nomination) {
            $nomination->recalculate($ageCategory);
        }
    }

    public function generateForAll()
    {
        $limit  = 500;
        $offset = 0;

        $museumIds = ArrayHelper::getColumn(
            $this->getNominationsMuseums()
                ->select('museum_id')
                ->asArray()
                ->all(),
            'museum_id');

        $museumIds = array_map('intval', $museumIds);

        $badTeams = [];

        $usedTeams = [];

        /** @var Team[] $teams */
        while ($teams = Team::findByMuseums($museumIds, $limit, $offset)) {
            echo "offset $offset \n";
            foreach ($teams as $team) {
                $value = $this->getValue($team);

                $usedTeams[] = $team->id;

                if (empty($value)) {
                    $badTeams[] = $team->id;
                } else {
                    $createdNomination = NominationBall::find()
                        ->andWhere(['nomination_id' => $this->id])
                        ->andWhere(['team_id' => $team->id])
                        ->one();
                    if (empty($createdNomination)) {
                        $createdNomination = new NominationBall([
                            'team_id'       => $team->id,
                            'nomination_id' => $this->id
                        ]);
                    }

                    $createdNomination->value = $value;
                    $createdNomination->save();
                }
            }

            NominationBall::deleteAll([
                'team_id'       => $badTeams,
                'nomination_id' => $this->id
            ]);

            $offset += $limit;
        }

        NominationBall::deleteAll([
            'and',
            ['not in', 'team_id', $usedTeams],
            ['nomination_id' => $this->id]
        ]);
    }

    /**
     * full re generate nomination balls for this team+museum
     *
     * @param Team                   $team
     * @param BaseMusObj|Museum|null $museum
     */
    public static function generateForTeam(Team $team, BaseMusObj $museum = null)
    {
        $stage = static::neededStage;

        /** @var TeamInfo $teamInfo */
        $teamInfo = $team->getTeamInfo($stage);


        //только те номинации, у которых есть этот музей и эта возрастная категория
        $query = Nomination::find()
            ->joinWith('nominationsAgeCategories')
            ->andWhere([NominationAgeCategoryLink::tableName() . '.age_category_id' => $teamInfo->age_category_id]);

        if (!empty($museum)) {
            $query->joinWith('nominationsMuseums')
                ->andWhere([NominationMuseumLink::tableName() . '.museum_id' => $museum->id]);
        }

        /** @var Nomination[] $nominations */
        $nominations = $query->all();

        $badNominations = [];

        foreach ($nominations as $nomination) {
            $value = $nomination->getValue($team);
            if (!empty($value)) {
                $createdNomination = NominationBall::find()
                    ->andWhere(['nomination_id' => $nomination->id])
                    ->andWhere(['team_id' => $team->id])
                    ->one();
                if (empty($createdNomination)) {
                    $createdNomination = new NominationBall([
                        'team_id'       => $team->id,
                        'nomination_id' => $nomination->id
                    ]);
                }

                $createdNomination->value = $value;
                $createdNomination->save();
            } else {
                $badNominations[] = $nomination->id;
            }
        }

        NominationBall::deleteAll([
            'team_id'       => $team->id,
            'nomination_id' => $badNominations
        ]);
    }

    /**
     * full re generate nomination balls for this team+museum
     *
     * @param AgeCategory $ageCategory
     */
    public function generateForCategory(AgeCategory $ageCategory)
    {
        $stage = static::neededStage;

        $limit  = 500;
        $offset = 0;

        $museumIds = ArrayHelper::getColumn(
            $this->getNominationsMuseums()
                ->select('museum_id')
                ->asArray()
                ->all(),
            'museum_id');

        $badTeams = [];

        /** @var Team[] $teams */
        while ($teams = Team::findByMuseumsAgeCategories($museumIds, $ageCategory, $stage, $limit, $offset)) {
            foreach ($teams as $team) {
                $value = $this->getValue($team);

                if (empty($value)) {
                    $badTeams[] = $team->id;
                } else {
                    $createdNomination = NominationBall::find()
                        ->andWhere(['nomination_id' => $this->id])
                        ->andWhere(['team_id' => $team->id])
                        ->one();
                    if (empty($createdNomination)) {
                        $createdNomination = new NominationBall([
                            'team_id'       => $team->id,
                            'nomination_id' => $this->id
                        ]);
                    }

                    $createdNomination->value = $value;
                    $createdNomination->save();
                }
            }

            NominationBall::deleteAll([
                'team_id'       => $badTeams,
                'nomination_id' => $this->id
            ]);

            $offset += $limit;
        }
    }

    /**
     * @return bool
     */
    public function hasAnotherMusTypes()
    {
        return self::getMuseums()
            ->andWhere(['!=', 'type', $this->musObjType])
            ->count() > 0;
    }

    /**
     * @param $string
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findByString($string, $strict = false)
    {
        $result = self::find();

        if($strict){
            $result->andWhere(['name' => $string]);
        }
        else{
            $result->andWhere(['or', ['like', 'name', $string], ['like', 'number', $string]]);
        }

        $result = $result->one();

        return $result;
    }

    public static function findByNumber($number)
    {
        $result = static::find()
            ->andWhere(['number' => $number])
            ->one();

        return $result;
    }

    public static function getTypes()
    {
        return self::$_typeNames;
    }

    public static function getOperators()
    {
        return self::$_operatorNames;
    }

    public function getTypeName()
    {
        return self::$_typeNames[$this->type];
    }

    public function getMusObjType()
    {
        return self::$_musObjTypes[$this->type];
    }

    public function getMusObjName()
    {
        return BaseMusObj::getBaseTypeName($this->musObjType);
    }

    public function getMusObjUrl()
    {
        return BaseMusObj::getBaseTypeUrl($this->musObjType);
    }

    public function getOperatorName()
    {
        return self::$_operatorNames[$this->operator];
    }

    public function getValue(Team $team)
    {
        $count = 0;
        $sum   = 0;

        $museums = $this->museums;

        $ids = ArrayHelper::getColumn($museums,
            function ($element) {
                return $element->id;
            });

        $museumBalls = $team->museumBalls;

        foreach ($museumBalls as $museumBall) {
            $value = $this->round
                ? $museumBall->correspondence_round
                : $museumBall->sum;

            //Проверить соответствие музея
            if (!in_array($museumBall->museum_id, $ids)
                || $museumBall->museum->type !== $this->musObjType
                || $value <= 0
            ) {
                continue;
            }

            //Проверить возрастную категорию(если надо),
            if ($this->onlyMyAge) {
                $stage = static::neededStage;

                /** @var TeamInfo $teamInfo */
                $teamInfo = $team->getTeamInfo($stage);

                if (empty($teamInfo) || is_null($teamInfo->ageCategory)
                    || !$museumBall->museum->checkCategory($teamInfo->ageCategory)
                ) {
                    continue;
                }
            }

            //Здесь музей имеет нужную возрастную категорию и нужный тип, можно прибавлять.
            $count++;
            $sum += $value;
        }

        if ($count < $this->minCount) {
            return 0;
        }

        switch ($this->operator) {
            case self::OPERATOR_SUM:
                return $sum;
            case self::OPERATOR_COUNT:
                return $count;
            default:
                return 0;
        }
    }

    public function getMaxVal(AgeCategory $ageCategory)
    {
        if (!isset($this->_maxValue[$ageCategory->id])) {
            $maxValue = 0;

            foreach ($this->nominationsMuseums as $museumLink) {
                //Проверить соответствие музея
                if (!$museumLink->useForMax
                    || $museumLink->museum->type !== $this->musObjType
                ) {
                    continue;
                }

                //Проверить возрастную категорию(если надо),
                if ($this->onlyMyAge && !$museumLink->museum->checkCategory($ageCategory)) {
                    continue;
                }

                switch ($this->operator) {
                    case self::OPERATOR_SUM:
                        if($this->round){
                            $maxValue += $museumLink->museum->maxCorrespondence;
                        }
                        else {
                            $maxValue += $museumLink->museum->maxValue;
                        }
                        break;
                    case self::OPERATOR_COUNT:
                        $maxValue++;
                        break;
                }
            }

            $this->_maxValue[$ageCategory->id] = $maxValue;
        }

        return $this->_maxValue[$ageCategory->id];
    }

    /**
     * @param $ageCategoryId
     *
     * @return NominationAgeCategoryLink
     */
    public function getCategoryLink($ageCategoryId)
    {
        /** @var NominationAgeCategoryLink $nominationLink */
        $nominationLink = $this->getNominationsAgeCategories()
            ->andWhere(['age_category_id' => $ageCategoryId])
            ->one();

        return $nominationLink;
    }

    public function getStatus($ageCategoryId, $teamTypeId, $value)
    {
        $nominationLink = $this->getCategoryLink($ageCategoryId);

        if(empty($nominationLink)){
            return NominationBall::STATUS_BAD;
        }

        return $nominationLink->getStatus($teamTypeId, $value);
    }

    public function getPercent($value, $ageCategoryId)
    {
        $nominationLink = $this->getCategoryLink($ageCategoryId);

        if(empty($nominationLink)){
            return null;
        }

        return 100 * $value/$nominationLink->maxValue;
    }

    public function hasAgeCategory($ageCategory)
    {
        foreach ($this->nominationsAgeCategories as $nominationsAgeCategoryLink) {
            if ($nominationsAgeCategoryLink->age_category_id == $ageCategory) {
                return true;
            }
        }

        return false;
    }

    public function updateContest()
    {
        if($this->priseContest){
            $this->priseContest->textBefore2 = $this->name;
            $this->priseContest->save();
        }else{
            $baseType = ContestType::findBaseFor(ContestType::BASE_FOR_NOMINATION, ContestType::BASE_FOR_PRISE);

            if($baseType){
                $newContest = new Contest();
                $newContest->codeName = 'Призёр: ' . $this->name;
                $newContest->name = 'Призёр: ' . $this->name;
                $newContest->contest_type_id = $baseType->id;
                $newContest->textBefore2 = $this->name;
                $newContest->substrate_type = Contest::TYPE_PLANE;

                $baseGenerationType = GenerationType::find()
                    ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_TEAM])
                    ->one();

                if(!empty($baseGenerationType)){
                    $newContest->generation_type_id = $baseGenerationType->id;
                }

                $newContest->save();

                $this->prise_contest_id = $newContest->id;
            }
        }

        if($this->winnerContest){
            $this->winnerContest->textBefore2 = $this->name;
            $this->winnerContest->save();
        }else{
            $baseType = ContestType::findBaseFor(ContestType::BASE_FOR_NOMINATION, ContestType::BASE_FOR_WINNER);

            if($baseType){
                $newContest = new Contest();
                $newContest->codeName = 'Победитель: ' . $this->name;
                $newContest->name = 'Победитель: ' . $this->name;
                $newContest->contest_type_id = $baseType->id;
                $newContest->textBefore2 = $this->name;
                $newContest->substrate_type = Contest::TYPE_PLANE;

                $baseGenerationType = GenerationType::find()
                    ->andWhere(['baseFor' => GenerationType::BASE_FOR_CONCURS_TEAM])
                    ->one();

                if(!empty($baseGenerationType)){
                    $newContest->generation_type_id = $baseGenerationType->id;
                }

                $newContest->save();

                $this->winner_contest_id = $newContest->id;
            }
        }
    }

    public function recalculate(AgeCategory $ageCategory = null)
    {
        foreach ($this->nominationsAgeCategories as $ageCategoryLink) {
            if (!is_null($ageCategory) && $ageCategoryLink->age_category_id != $ageCategory->id) {
                continue;
            }

            $ageCategoryLink->recalculate();
            $ageCategoryLink->save();
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (
            (isset($changedAttributes['operator']) && $changedAttributes['operator'] != $this->operator)
            || (isset($changedAttributes['onlyMyAge']) && $changedAttributes['onlyMyAge'] != $this->onlyMyAge)
            || (isset($changedAttributes['type']) && $changedAttributes['type'] != $this->type)
        ) {
            $this->recalculate();
            $this->generateForAll();
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        if($insert || $this->name != $this->getOldAttribute('name')){
            $this->updateContest();
        }

        return parent::beforeSave($insert);
    }
}

/**
 * NominationAgeCategoriesLink:
 *+ delete: Удалить все баллы командам с этой категорией
 *+ update: Удалить все баллы командам с прошлой категорией и сгенерить все с новой категорией
 *+ create: сгенерить все с новой категорией
 *
 * NominationMuseumLink:
 *+ delete: Пересчитать существующие значения на вопрос удаления.
 *+ update: Полный пересчет номинации
 *+ create: Полный пересчет номинации
 *
 * Nomination:
 *+ delete: Удалить все баллы
 *+ update: Оператор - пересчитать существующие значения на вопрос удаления.
 *          Кол-во - полный пересчет номинации
 *          Только мой возраст - полный пересчет номинации
 *+ create: полный пересчет номинации
 *
 * TeamAgeCategory:
 *+ update: полный пересчет номинаций для команды
 *
 * TeamPeopleCount:
 *+  update: полный пересчет номинаций для команды
 *
 * MuseumBallValue:
 *+ delete: Пересчет всех номинаций с этим музеем для команды
 *+ update: Пересчет всех номинаций с этим музеем для команды
 *+ create: Пересчет всех номинаций с этим музеем для команды
 *
 * MuseumAgeCategories:
 *+ delete: Пересчет всех номинаций с этим музеем и onlyMyAge
 *+ update: Пересчет всех номинаций с этим музеем и onlyMyAge
 *+ create: Пересчет всех номинаций с этим музеем и onlyMyAge
 *
 * MuseumCoefficient:
 *+  delete: Пересчет всех номинаций с этим музеем для всех команд, посетивших этот музей в тот период, где этот коэффициент является максимальным.
 *+  update: Пересчет всех номинаций с этим музеем для всех команды, посетивших этот музей в тот период, где этот коэффициент является максимальным.
 *+  create: Пересчет всех номинаций с этим музеем для всех команды, посетивших этот музей в тот период, где этот коэффициент является максимальным.
 *
 *
 *
 * 1. Удаление все баллов номинации + возрастной категории команды.
 * 2. Рассчет баллов по номинации + возрастная категория
 * 3. Перерасчет существующих значений на вопрос удаления по номинации
 * 4. Полный перерасчет номинации
 * 5. Удалить все баллы по номинации
 * 6. Полный пересчет номинаций для команды
 * 7. Пересчет всех номинаций с этим музеем для команды
 * 8. Пересчет всех номинаций с этим музеем и onlyMyAge
 * 9. Пересчет всех номинаций с этим музеем для всех команд, посетивших этот музей в тот период, где этот коэффициент является максимальным.
 *
 *
 *
 *
 * 1. Пересчет номинаци(и/й) по некоторым условиям выборки команд.
 * 2. Пересчет номинаци(и/й) в рамках команды
 * 3. Перерасчет существующих значений.
 * 4. Удаление баллов номинации
 *
 *
 *
 * 1. Пересчет номинации для команды +
 * 2. Поиск минимальных требований к команде - наличие нужного количества нужных баллов, пока что забиваем на onlyMyAge, все, что не вошло можно удалить, остальное, можно проверять.
 * 3.
 */