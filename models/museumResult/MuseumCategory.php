<?php

namespace app\models\museumResult;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "museum_categories".
 *
 * @property integer $id
 * @property integer $number
 * @property string $name
 * @property integer $type_id
 * @property string $typeName
 */
class MuseumCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museum_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'type_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'name' => 'Название',
            'type_id' => 'Привязка к типу',
            'typeName' => 'Привязка к типу'
        ];
    }

    public function getTypeName()
    {
        return Museum::getTypeName($this->type_id);
    }

    public static function findAllCategories()
    {
        $categories = static::find()->select(['id', 'name'])->indexBy('id')->all();

        return ArrayHelper::getColumn($categories, function ($category){
            return $category->name;
        });
    }
}
