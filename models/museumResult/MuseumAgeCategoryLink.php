<?php

namespace app\models\museumResult;

use Yii;

/**
 * This is the model class for table "museums_age_categories".
 *
 * @property integer $museum_id
 * @property integer $age_category_id
 *
 * @property AgeCategory $ageCategory
 * @property Museum $museum
 */
class MuseumAgeCategoryLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museums_age_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['museum_id', 'age_category_id'], 'required'],
            [['museum_id', 'age_category_id'], 'integer'],
            [['age_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgeCategory::className(), 'targetAttribute' => ['age_category_id' => 'id']],
            [['museum_id'], 'exist', 'skipOnError' => true, 'targetClass' => Museum::className(), 'targetAttribute' => ['museum_id' => 'id']],
            ['age_category_id', 'unique', 'targetAttribute' => ['age_category_id', 'museum_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'museum_id' => 'Музей',
            'age_category_id' => 'Возрастная категория',
            'ageCategoryName' => 'Возрастная категория'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategory()
    {
        return $this->hasOne(AgeCategory::className(), ['id' => 'age_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseum()
    {
        return $this->hasOne(Museum::className(), ['id' => 'museum_id']);
    }

    /**
     * @param $museum_id
     * @param $age_category_id
     *
     * @return array|null|\yii\db\ActiveRecord
     *
     */
    public static function findBy($museum_id, $age_category_id)
    {
        return static::find()
            ->andWhere(['museum_id' => $museum_id])
            ->andWhere(['age_category_id' => $age_category_id])
            ->one();
    }

    /**
     * @return mixed
     */
    public function getAgeCategoryName()
    {
        return $this->ageCategory->name;
    }

    public function getId()
    {
        return null;
    }

    public function afterDelete()
    {
        if(isset($changedAttributes['age_category_id']))
        {
            $nominations = Nomination::findNominationForRecalculate($this->museum);
            foreach ($nominations as $nomination) {
                $nomination->recalculate($this->ageCategory);
                $nomination->generateForCategory($this->ageCategory);

                if(isset($changedAttributes['age_category_id'])){
                    $nomination->generateForCategory($changedAttributes['age_category_id']);
                }

            }
        }
        parent::afterDelete();
    }

    public function beforeSave($insert)
    {
        if(!$insert && $this->isAttributeChanged('museum_id')){
            return false;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($changedAttributes['age_category_id'])
            || $insert)
        {
            $nominations = Nomination::findNominationForRecalculate($this->museum);
            foreach ($nominations as $nomination) {
                $nomination->recalculate($this->ageCategory);
                $nomination->generateForCategory($this->ageCategory);

                if(isset($changedAttributes['age_category_id'])){
                    $nomination->generateForCategory($changedAttributes['age_category_id']);
                }

            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
