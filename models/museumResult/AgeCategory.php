<?php

namespace app\models\museumResult;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "age_categories".
 *
 * @property integer $id
 * @property integer $maxClass
 * @property integer $minClass
 * @property string  $name
 */
class AgeCategory extends \yii\db\ActiveRecord
{
    private static $_byClass = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'age_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['maxClass', 'minClass'], 'integer', 'min' => 1, 'max' => 11],
            [['minClass', 'maxClass'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maxClass' => 'Максимальный класс',
            'minClass' => 'Минимальный класс',
        ];
    }

    public function getName()
    {
        return $this->minClass . '-' . $this->maxClass;
    }

    public static function findAllCategories()
    {
        $categories = static::find()->indexBy('id')->all();

        return ArrayHelper::getColumn($categories, function ($category){
            return $category->name;
        });
    }

    public static function findByClass($classNum)
    {
        if(!isset(self::$_byClass[$classNum])) {
            self::$_byClass[$classNum] = static::find()
                ->andWhere(['<=', 'minClass', $classNum])
                ->andWhere(['>=', 'maxClass', $classNum])
                ->one();
        }

        return self::$_byClass[$classNum];
    }
}
