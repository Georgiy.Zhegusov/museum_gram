<?php

namespace app\models\museumResult;

use app\models\Honor;
use app\models\Team;
use app\models\TeamInfo;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "museum_balls".
 *
 * @property integer $id
 * @property integer $museum_id
 * @property integer $team_id
 * @property string  $visitDate
 * @property integer $internal_round
 * @property integer $correspondence_round
 * @property bool    $photo_status
 * @property integer $visit_people_count
 * @property float   $value
 * @property float   $status
 * @property string   $image_thumb
 * @property string   $image_path
 *
 * @property boolean   $deleteImage
 * @property float   $sum
 * @property float   $coefficient
 * @property Museum  $museum
 * @property Team    $team
 */
class MuseumBall extends ActiveRecord
{
    public $imageFile;
    public $deleteImage;

    const STATUS_BAAD = null;
    const STATUS_BAD = 0;
    const STATUS_WINNER = 1;
    const STATUS_PRISE = 2;

    public static function getStatuses()
    {
        return [
            static::STATUS_BAAD => 'Ничего',
            static::STATUS_BAD => 'Ничего',
            static::STATUS_PRISE => 'Призер',
            static::STATUS_WINNER => 'Победитель'
        ];
    }

    const neededStage = 1;

    private $_coefficient;
    private $_sum;

    private $_neededCount = [
        10 => 4,
        9 => 4,
        8 => 3,
        7 => 3,
        6 => 2,
        5 => 2,
        4 => 2,
        3 => 2,
        2 => 2,
        1 => 1
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museum_balls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['museum_id', 'team_id'], 'required'],
            [['internal_round', 'correspondence_round', 'museum_id', 'team_id', 'visit_people_count', 'status'], 'integer'],
            ['photo_status', 'boolean'],
            [['visitDate'], 'safe'],
            [
                'visitDate',
                'required',
                'when'       => function ($model) {
                    return !empty($model->internal_round);
                },
                'whenClient' => "function (attribute, value) {
                return false;
            }"
            ],
            [['imageFile', 'deleteImage'], 'safe'],
            [['image_thumb', 'image_path',], 'string', 'max' => 255],
            [['museum_id', 'team_id'], 'unique', 'targetAttribute' => ['museum_id', 'team_id']],
            [['museum_id'], 'exist', 'skipOnError' => true, 'targetClass' => BaseMusObj::className(), 'targetAttribute' => ['museum_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'museum_id'            => 'Название объекта',
            'museumType'           => 'Тип объекта',
            'team_id'              => 'Команда',
            'visitDate'            => 'Дата посещения',
            'internal_round'       => 'Баллы посещения',
            'correspondence_round' => 'Баллы заочки',
            'visit_people_count'   => 'Людей посетило',
            'photo_status'         => 'Фото подтверждено',
            'value'                => 'Сумма',
            'ImageFile'            => 'Картинка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseum()
    {
        return $this->hasOne(BaseMusObj::className(), ['id' => 'museum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @param      $museum_id
     * @param null $dateFrom
     * @param null $dateTo
     *
     * @return MuseumBall[]
     **/
    public static function findByDate($museum_id, $dateFrom = null, $dateTo = null)
    {
        return static::find()
            ->andWhere(['museum_id' => $museum_id])
            ->andFilterWhere(['>=', 'visitDate', $dateFrom])
            ->andFilterWhere(['<=', 'visitDate', $dateTo])
            ->all();
    }

    public static function recalculateByDate($museum_id, $dateFrom = null, $dateTo = null)
    {
        $museumBalls = static::findByDate($museum_id, $dateFrom, $dateTo);

        foreach ($museumBalls as $museumBall) {
            $museumBall->recalculate();
            $museumBall->save();
        }

        return true;
    }

    public function getStatusText()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status, $this->status);
    }

    public function getCoefficient()
    {
        if (is_null($this->_coefficient)) {
            $this->_coefficient = $this->generateCoefficient();
        }

        return $this->_coefficient;
    }

    public function getSum()
    {
        if (is_null($this->_sum)) {
            $this->_sum = $this->generateSum();
        }

        return $this->_sum;
    }

    private function generateSum(){
        if (is_null($this->internal_round)) {
            return null;
        }

        if ($this->museum->type == BaseMusObj::BASE_TYPE_CONCURS) {
            return $this->internal_round;
        }

        /** @var Museum $museum */
        $museum = $this->museum;

        if ($museum->type_id == Museum::TYPE_PARK && !$this->photo_status) {
            return -1;
        }

        return $this->internal_round * $this->coefficient + $this->correspondence_round;
    }

    private function generateCoefficient()
    {
        /** @var TeamInfo $teamInfo */
        $teamInfo = $this->team->getTeamInfo(static::neededStage);
        $teamPeopleCount = empty($teamInfo) ? 0 : $teamInfo->participantCount;

        $neededCount = ArrayHelper::getValue($this->_neededCount, $teamPeopleCount, $teamPeopleCount/2);

        if($this->visit_people_count > 0 && $this->visitDate > '2016-12-07' && $this->visit_people_count < $neededCount){
            return 0.0;
        }

        return $this->museum->getCoefficient($this->visitDate);
    }

    private function calcStatus()
    {
        if($this->museum->type == BaseMusObj::BASE_TYPE_MUSEUMS){

        }
    }

    public function generateHonors()
    {
        $winnerContest = $this->museum->winnerContest;
        $priseContest = $this->museum->priseContest;

        if($winnerContest) {
            $winnerHonor = Honor::findHonor($this->team_id, $winnerContest->id);

            if ($this->status == static::STATUS_WINNER) {
                if (empty($winnerHonor)) {
                    $winnerHonor             = new Honor();
                    $winnerHonor->contest_id = $winnerContest->id;
                    $winnerHonor->team_id = $this->team_id;
                }

                $winnerHonor->image_path = $this->image_path;
                $winnerHonor->image_thumb = $this->image_thumb;

                if(!$winnerHonor->save()){
                    var_dump($winnerHonor->errors);
                }
            } else {
                if(!empty($winnerHonor)) {
                    $winnerHonor->delete();
                }
            }
        }

        if($priseContest) {
            $priseHonor = Honor::findHonor($this->team_id, $priseContest->id);

            if ($this->status == static::STATUS_PRISE) {
                if (empty($priseHonor)) {
                    $priseHonor             = new Honor();
                    $priseHonor->contest_id = $priseContest->id;
                    $priseHonor->team_id = $this->team_id;
                }
                $priseHonor->image_path = $this->image_path;
                $priseHonor->image_thumb = $this->image_thumb;

                if(!$priseHonor->save()){
                    var_dump($priseHonor->errors);
                }
            } else {
                if(!empty($priseHonor)) {
                    $priseHonor->delete();
                }
            }
        }
    }

    public function recalculate()
    {
        $this->value = $this->generateSum();
    }

    public function beforeSave($insert)
    {
        if($this->isAttributeChanged('internal_round')
            || $this->isAttributeChanged('correspondence_round')
            || $this->isAttributeChanged('visitDate')
            || $this->isAttributeChanged('visit_people_count'))
        {
            $this->recalculate();
        }

        $this->calcStatus();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($changedAttributes['value']) && $changedAttributes['value']!=$this->value){
            //Если изменились баллы, то надо обновить все номинации, в которых учавствует данный музей, для данной команды.
            Nomination::generateForTeam($this->team, $this->museum);
        }

        if(isset($changedAttributes['status']) && $changedAttributes['status']!=$this->status){
            //Если изменились баллы, то надо обновить все номинации, в которых учавствует данный музей, для данной команды.
            $this->generateHonors();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        Nomination::generateForTeam($this->team, $this->museum);

        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public static function recalculateByTeam($team_id, $stage)
    {
        /** @var MuseumBall[] $museumBalls */
        $museumBalls = static::find()
            ->andWhere(['team_id' => $team_id])
            ->all();

        foreach ($museumBalls as $museumBall) {
            $museumBall->recalculate();
            $museumBall->save();
        }
    }
}
