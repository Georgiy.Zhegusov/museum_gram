<?php

namespace app\models\museumResult;

use app\models\Honor;
use app\models\Team;
use app\models\TeamInfo;
use app\models\TeamType;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nomination_balls".
 *
 * @property integer $id
 * @property integer $nomination_id
 * @property double $value
 * @property integer $team_id
 * @property integer $status
 *
 * @property string $statusText
 * @property TeamInfo $teamInfo
 * @property Nomination $nomination
 * @property Team $team
 */
class NominationBall extends \yii\db\ActiveRecord
{
    const STATUS_BAAD = null;
    const STATUS_BAD = 0;
    const STATUS_WINNER = 1;
    const STATUS_PRISE = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nomination_balls';
    }

    public static function getStatuses()
    {
        return [
            static::STATUS_BAAD => 'Ничего',
            static::STATUS_BAD => 'Ничего',
            static::STATUS_PRISE => 'Призер',
            static::STATUS_WINNER => 'Победитель'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomination_id', 'team_id', 'status'], 'integer'],
            [['value'], 'number'],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomination::className(), 'targetAttribute' => ['nomination_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomination_id' => 'Nomination ID',
            'value' => 'Value',
            'team_id' => 'Team ID',
            'status' => 'Status'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nomination::className(), ['id' => 'nomination_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamInfo()
    {
        return $this->hasOne(TeamInfo::className(), ['team_id' => 'team_id'])
            ->andWhere(['stage' => Nomination::neededStage]);
    }

    public function getStatusText()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status, $this->status);
    }

    public function recalculate()
    {
        $this->value = $this->nomination->getValue($this->team);
    }

    public function calcStatus()
    {
        $this->status = $this->nomination->getStatus($this->teamInfo->age_category_id, $this->teamInfo->team_type_id, $this->value);
    }

    public function generateHonors()
    {
        $winnerContest = $this->nomination->winnerContest;
        $priseContest = $this->nomination->priseContest;

        if($winnerContest) {
            $winnerHonor = Honor::findHonor($this->team_id, $winnerContest->id);

            if ($this->status == static::STATUS_WINNER) {
                if (empty($winnerHonor)) {
                    $winnerHonor             = new Honor();
                    $winnerHonor->contest_id = $winnerContest->id;
                    $winnerHonor->team_id = $this->team_id;
                    if(!$winnerHonor->save()){
                        var_dump($winnerHonor->errors);
                    }
                }
            } else {
                if(!empty($winnerHonor)) {
                    $winnerHonor->delete();
                }
            }
        }

        if($priseContest) {
            $priseHonor = Honor::findHonor($this->team_id, $priseContest->id);

            if ($this->status == static::STATUS_PRISE) {
                if (empty($priseHonor)) {
                    $priseHonor             = new Honor();
                    $priseHonor->contest_id = $priseContest->id;
                    $priseHonor->team_id = $this->team_id;
                    if(!$priseHonor->save()){
                        var_dump($priseHonor->errors);
                    }
                }
            } else {
                if(!empty($priseHonor)) {
                    $priseHonor->delete();
                }
            }
        }
    }

    public function beforeSave($insert)
    {
        if($this->isAttributeChanged('nomination_id') || $this->isAttributeChanged('team_id')){
            $this->recalculate();
            $this->calcStatus();
        }

        if($this->isAttributeChanged('value')){
            $this->calcStatus();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($changedAttributes['status']) && $changedAttributes['status']!=$this->status){
            //Если изменились баллы, то надо обновить все номинации, в которых учавствует данный музей, для данной команды.
            $this->generateHonors();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public static function dropByAgeNomination(Nomination $nomination, $ageCategory)
    {
        $offset = 0;
        $limit = 500;
        while( $teamIds = TeamInfo::find()
            ->andWhere(['stage' => $nomination::neededStage])
            ->andWhere(['age_category_id' => $ageCategory])
            ->select('team_id')
            ->limit($limit)
            ->offset($offset)
            ->asArray()
            ->all()) {

            NominationBall::deleteAll(['team_id' => array_map('intval', $teamIds), 'nomination_id' => $nomination->id]);

            $offset += $limit;
        }
    }

    public static function dropByAgeCategoryTeam($team_id, $ageCategory, $withoutAgeCategory)
    {
        $nominations = Nomination::findWithAgeCategory($ageCategory);

        $nominationsIds = [];

        foreach ($nominations as $nomination) {
            if(!$nomination->hasAgeCategory($withoutAgeCategory)){
                $nominationsIds[] = $nomination->id;
            }
        }

        static::deleteAll(['nomination_id' => $nominationsIds, 'team_id' => $team_id]);
    }

    public static function createByAgeCategoryTeam(Team $team, $ageCategory, $withoutAgeCategory)
    {
        $nominations = Nomination::findWithAgeCategory($ageCategory);

        $badNominations = [];

        foreach ($nominations as $nomination) {
            $value = $nomination->getValue($team);
            if(!empty($value) || !$nomination->hasAgeCategory($withoutAgeCategory)){
                $createdNomination = NominationBall::find()
                    ->andWhere(['nomination_id' => $nomination->id])
                    ->andWhere(['team_id' => $team->id])
                    ->one();
                if(empty($createdNomination)){
                    $createdNomination = new static([
                        'team_id' => $team->id,
                        'nomination_id' => $nomination->id
                    ]);
                }

                $createdNomination->value = $value;
                $createdNomination->save();
            }
            else{
                $badNominations[] = $nomination->id;
            }
        }

        static::deleteAll([
            'team_id' => $team->id,
            'nomination_id' => $badNominations
        ]);
    }

    public static function setStatus($nominationId, $teamType, $status, $ageCategoryId, $from, $to)
    {
        $teamType = TeamType::find()
            ->andWhere(['baseForTeam' => $teamType])
            ->one();

        if (empty($teamType)) {
            return;
        }

        $from = empty($from) ? null : floatval($from);
        $to = empty($to) ? null : floatval($to);

        $limit  = 300;
        $offset = 0;

        $query = NominationBall::find()
            ->joinWith('teamInfo')
            ->andWhere([TeamInfo::tableName() . '.team_type_id' => $teamType->id])
            ->andFilterWhere(['>', 'value', 0])
            ->andFilterWhere(['<', 'value', $to])
            ->andFilterWhere(['>=', 'value', $from])
            ->andFilterWhere(['nomination_id' => $nominationId])
            ->andWhere([TeamInfo::tableName() . '.age_category_id' => $ageCategoryId]);

        echo "$nominationId $ageCategoryId " . $teamType->baseForTeam . " $status $from $to " . $query->count() . "\n";

        $nominations = $query->limit($limit)->offset($offset)->all();

        while (!empty($nominations)) {
            /** @var NominationBall $nomination */
            foreach ($nominations as $nomination) {
                if ($nomination->status != $status) {
                    $nomination->status = $status;
                    $nomination->save();
                }
            }

            $offset += $limit;
            $nominations = $query->limit($limit)->offset($offset)->all();
        }
    }
}
