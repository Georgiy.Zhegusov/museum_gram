<?php

namespace app\models\museumResult;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "museums".
 *
 * @property integer $type_id
 * @property string $typeText
 */
class Concurs extends BaseMusObj
{
    protected static $baseType = self::BASE_TYPE_CONCURS;

    const neededStage = 1;

    const TYPE_INDIVIDUAL = 3;
    const TYPE_TEAM = 4;

    static private $_typeNames = [
        self::TYPE_INDIVIDUAL => 'Индивидуальный',
        self::TYPE_TEAM => 'Командный',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([
            [['type_id'], 'integer']
        ],
            parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge([
            'type_id' => 'Тип',
            'typeText' => 'Тип',
        ],
            parent::attributeLabels());
    }

    public function getTypeText()
    {
        return static::getTypeName($this->type_id);
    }

    public static function getTypes()
    {
        return self::$_typeNames;
    }

    public static function getTypeName($typeId)
    {
        return ArrayHelper::getValue(self::$_typeNames, $typeId, $typeId);
    }

    public function getMaxInternal()
    {
        return 50;
    }

    public function getMaxCorrespondence()
    {
        return 0;
    }
}
