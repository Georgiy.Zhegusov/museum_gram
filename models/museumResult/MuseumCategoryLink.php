<?php

namespace app\models\museumResult;

use Yii;

/**
 * This is the model class for table "museums_museum_categories".
 *
 * @property integer $museums_id
 * @property integer $museum_categories_id
 *
 * @property MuseumCategory $museumCategories
 * @property Museum $museums
 */
class MuseumCategoryLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'museums_museum_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['museums_id', 'museum_categories_id'], 'required'],
            [['museums_id', 'museum_categories_id'], 'integer'],
            [['museum_categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MuseumCategory::className(), 'targetAttribute' => ['museum_categories_id' => 'id']],
            [['museums_id'], 'exist', 'skipOnError' => true, 'targetClass' => Museum::className(), 'targetAttribute' => ['museums_id' => 'id']],
            ['museum_categories_id', 'unique', 'targetAttribute' => ['museum_categories_id', 'museums_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'museums_id' => 'Музей',
            'museum_categories_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseumCategories()
    {
        return $this->hasOne(MuseumCategory::className(), ['id' => 'museum_categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuseums()
    {
        return $this->hasOne(Museum::className(), ['id' => 'museums_id']);
    }

    public static function findBy($museums_id, $museum_categories_id)
    {
        return static::find()
            ->andWhere(['museums_id' => $museums_id])
            ->andWhere(['museum_categories_id' => $museum_categories_id])
            ->one();
    }

    public function getId()
    {
        return null;
    }

    public function getCategoryName()
    {
        return $this->museumCategories->name;
    }
}
