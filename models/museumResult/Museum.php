<?php

namespace app\models\museumResult;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "museums".
 *
 * @property integer $type_id
 * @property string $typeText
 */
class Museum extends BaseMusObj
{
    const neededStage = 1;

    protected static $baseType = self::BASE_TYPE_MUSEUMS;

    const TYPE_MUSEUM = 0;
    const TYPE_PARK = 1;
    const TYPE_ESTATE = 2;

    static private $_typeNames = [
        self::TYPE_MUSEUM => 'Музей',
        self::TYPE_PARK => 'Парк',
        self::TYPE_ESTATE => 'Усадьба',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([
                [['type_id'], 'integer']
            ],
            parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge([
                'type_id' => 'Тип',
                'typeText' => 'Тип',
            ],
            parent::attributeLabels());
    }

    public function getTypeText()
    {
        return static::getTypeName($this->type_id);
    }

    public static function getTypes()
    {
        return self::$_typeNames;
    }

    public static function getTypeName($typeId)
    {
        return ArrayHelper::getValue(self::$_typeNames, $typeId, $typeId);
    }

    public function getMaxCorrespondence()
    {
        if($this->type_id != self::TYPE_PARK){
            return 5;
        }

        return 0;
    }


    public function getMaxInternal()
    {
        return 50;
    }
}
