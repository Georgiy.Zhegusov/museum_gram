<?php

namespace app\models\museumResult;

use app\models\TeamInfo;
use app\models\TeamType;
use Yii;

/**
 * This is the model class for table "nominations_age_category".
 *
 * @property integer     $nomination_id
 * @property integer     $age_category_id
 * @property float       $maxValue
 * @property float       $winnerValue
 * @property float       $priseValue
 * @property float       $teamWinnerValue
 * @property float       $teamPriseValue
 *
 * @property AgeCategory $ageCategory
 * @property Nomination  $nomination
 */
class NominationAgeCategoryLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nominations_age_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomination_id', 'age_category_id'], 'required'],
            [['nomination_id', 'age_category_id'], 'integer'],
            [['winnerValue', 'priseValue', 'teamWinnerValue', 'teamPriseValue'], 'number'],
            [['age_category_id'], 'unique', 'targetAttribute' => ['age_category_id', 'nomination_id']],
            [['age_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgeCategory::className(), 'targetAttribute' => ['age_category_id' => 'id']],
            [['nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nomination::className(), 'targetAttribute' => ['nomination_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomination_id'   => 'Номинация',
            'age_category_id' => 'Возрастная категория',
            'ageCategoryName' => 'Возрастная категория',
            'maxValue'        => 'Максимальное значение',
            'winnerValue'     => 'Предел победителя одиночки',
            'priseValue'      => 'Предел призёра одиночки',
            'teamWinnerValue' => 'Предел победителя команды',
            'teamPriseValue'  => 'Предел призёра команды'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeCategory()
    {
        return $this->hasOne(AgeCategory::className(), ['id' => 'age_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomination()
    {
        return $this->hasOne(Nomination::className(), ['id' => 'nomination_id']);
    }

    /**
     * @param $nomination_id
     * @param $age_category_id
     *
     * @return array|null|\yii\db\ActiveRecord
     *
     */
    public static function findBy($nomination_id, $age_category_id)
    {
        return static::find()
            ->andWhere(['nomination_id' => $nomination_id])
            ->andWhere(['age_category_id' => $age_category_id])
            ->one();
    }

    /**
     * @return mixed
     */
    public function getAgeCategoryName()
    {
        return $this->ageCategory->name;
    }

    public function getId()
    {
        return null;
    }

    public function recalculate()
    {
        $this->maxValue = $this->nomination->getMaxVal($this->ageCategory);
    }

    public function getStatus($teamType, $value)
    {
        $winnerCriteria = $this->getWinnerCriteria($teamType);
        $priseCriteria = $this->getPriseCriteria($teamType);

        if($value >= $winnerCriteria && !empty($winnerCriteria)){
            return NominationBall::STATUS_WINNER;
        }

        if($value >= $priseCriteria && !empty($priseCriteria)){
            return NominationBall::STATUS_PRISE;
        }

        return NominationBall::STATUS_BAD;
    }

    public function getWinnerCriteria($teamType)
    {
        switch ($teamType){
            case TeamType::BASE_FOR_SOLO:
                return $this->winnerValue;
                break;
            case TeamType::BASE_FOR_TEAM:
                return $this->teamWinnerValue;
                break;
            default:
                return null;
        }
    }

    public function getPriseCriteria($teamType)
    {
        switch ($teamType){
            case TeamType::BASE_FOR_SOLO:
                return $this->priseValue;
                break;
            case TeamType::BASE_FOR_TEAM:
                return $this->teamPriseValue;
                break;
            default:
                return null;
        }
    }

    public function setCriteria($teamType, $status, $value)
    {
        switch ($teamType){
            case TeamType::BASE_FOR_TEAM:
                if($status == NominationBall::STATUS_WINNER){
                    $this->teamWinnerValue = $value;
                    $this->teamPriseValue = $this->teamPriseValue > $this->teamWinnerValue
                        ? $this->teamWinnerValue
                        : $this->teamPriseValue;
                }
                elseif($status == NominationBall::STATUS_PRISE){
                    $this->teamPriseValue = $value;
                    $this->teamWinnerValue = $this->teamWinnerValue < $this->teamPriseValue
                        ? $this->teamPriseValue
                        : $this->teamWinnerValue;
                }
                break;
            case TeamType::BASE_FOR_SOLO:
                if($status == NominationBall::STATUS_WINNER){
                    $this->winnerValue = $value;
                    $this->priseValue = $this->priseValue > $this->winnerValue
                        ? $this->winnerValue
                        : $this->priseValue;
                }
                elseif($status == NominationBall::STATUS_PRISE){
                    $this->priseValue = $value;
                    $this->winnerValue = $this->winnerValue < $this->priseValue
                        ? $this->priseValue
                        : $this->winnerValue;
                }

                var_dump($this->winnerValue);
                var_dump($this->priseValue);
                break;
            default:
                return null;
        }
    }

    public function beforeSave($insert)
    {
        if ($this->isAttributeChanged('nomination_id') || $this->isAttributeChanged('age_category_id')) {
            $this->recalculate();
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        NominationBall::dropByAgeNomination($this->nomination, $this->age_category_id);

        return parent::beforeDelete();
    }

    public function calculateStatuses()
    {
        if($this->teamWinnerValue != $this->teamPriseValue) {
            NominationBall::setStatus(
                $this->nomination_id,
                TeamType::BASE_FOR_TEAM,
                NominationBall::STATUS_PRISE,
                $this->age_category_id,
                ($this->teamPriseValue / 100) * $this->maxValue,
                ($this->teamWinnerValue / 100) * $this->maxValue
            );
        }

        NominationBall::setStatus(
            $this->nomination_id,
            TeamType::BASE_FOR_TEAM,
            NominationBall::STATUS_WINNER,
            $this->age_category_id,
            ($this->teamWinnerValue /100) * $this->maxValue,
            null
        );

        if($this->winnerValue != $this->priseValue) {
            NominationBall::setStatus(
                $this->nomination_id,
                TeamType::BASE_FOR_SOLO,
                NominationBall::STATUS_PRISE,
                $this->age_category_id,
                ($this->priseValue / 100) * $this->maxValue,
                ($this->winnerValue / 100) * $this->maxValue
            );
        }

        NominationBall::setStatus(
            $this->nomination_id,
            TeamType::BASE_FOR_SOLO,
            NominationBall::STATUS_WINNER,
            $this->age_category_id,
            ($this->winnerValue /100) * $this->maxValue,
            null
        );
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->nomination->generateForCategory($this->ageCategory);
        } elseif (isset($changedAttributes['nomination_id']) || isset($changedAttributes['age_category_id'])) {
            $oldNomination = Nomination::findOne(['id' => $this->getOldAttribute('nomination_id')]);
            NominationBall::dropByAgeNomination($oldNomination, $this->getOldAttribute('age_category_id'));

            $this->nomination->generateForCategory($this->ageCategory);
        }

        if($insert || (isset($changedAttributes['teamPriseValue']) && $changedAttributes['teamPriseValue']!=$this->teamPriseValue)){
            if($this->teamPriseValue < $changedAttributes['teamPriseValue']){
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_TEAM,
                    NominationBall::STATUS_PRISE,
                    $this->age_category_id,
                    ($this->teamPriseValue /100) * $this->maxValue,
                    (min($this->teamWinnerValue, $changedAttributes['teamPriseValue']) /100) * $this->maxValue
                );
            }
            else{
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_TEAM,
                    NominationBall::STATUS_BAD,
                    $this->age_category_id,
                    ($changedAttributes['teamPriseValue'] /100) * $this->maxValue,
                    ($this->teamPriseValue /100) * $this->maxValue
                );
            }
        }

        if($insert || (isset($changedAttributes['teamWinnerValue']) && $changedAttributes['teamWinnerValue']!=$this->teamWinnerValue)){
            if($this->teamWinnerValue < $changedAttributes['teamWinnerValue']){
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_TEAM,
                    NominationBall::STATUS_WINNER,
                    $this->age_category_id,
                    ($this->teamWinnerValue /100) * $this->maxValue,
                    ($changedAttributes['teamWinnerValue'] /100) * $this->maxValue
                );
            }
            else{
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_TEAM,
                    NominationBall::STATUS_PRISE,
                    $this->age_category_id,
                    (max($changedAttributes['teamWinnerValue'], $this->teamPriseValue ) /100) * $this->maxValue,
                    ($this->teamWinnerValue /100) * $this->maxValue
                );
            }
        }

        if($insert || (isset($changedAttributes['priseValue']) && $changedAttributes['priseValue']!=$this->priseValue)){
            if($this->priseValue < $changedAttributes['priseValue']){
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_SOLO,
                    NominationBall::STATUS_PRISE,
                    $this->age_category_id,
                    ($this->priseValue /100) * $this->maxValue,
                    (min($this->winnerValue, $changedAttributes['priseValue']) /100) * $this->maxValue
                );
            }
            else{
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_SOLO,
                    NominationBall::STATUS_BAD,
                    $this->age_category_id,
                    ($changedAttributes['priseValue'] /100) * $this->maxValue,
                    ($this->priseValue /100) * $this->maxValue
                );
            }
        }

        if($insert || (isset($changedAttributes['winnerValue']) && $changedAttributes['winnerValue']!=$this->winnerValue)){
            if($this->winnerValue < $changedAttributes['winnerValue']){
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_SOLO,
                    NominationBall::STATUS_WINNER,
                    $this->age_category_id,
                    ($this->winnerValue /100) * $this->maxValue,
                    ($changedAttributes['winnerValue'] /100) * $this->maxValue
                );
            }
            else{
                NominationBall::setStatus(
                    $this->nomination_id,
                    TeamType::BASE_FOR_SOLO,
                    NominationBall::STATUS_PRISE,
                    $this->age_category_id,
                    (max($changedAttributes['winnerValue'], $this->priseValue ) /100) * $this->maxValue,
                    ($this->winnerValue /100) * $this->maxValue
                );
            }
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
