<?php

namespace app\models;

use app\common\behaviors\NestedValuesBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contest_types".
 *
 * @property integer $id
 * @property string $substrate
 * @property string $substrate_thumb
 * @property string $substrate_back
 * @property bool $oneForAll
 * @property integer $contest_category_id
 * @property string $textBefore1
 * @property string $textBefore2
 * @property string $textBefore3
 * @property string $textAfter1
 * @property string $textAfter2
 * @property string $textAfter3
 * @property string $name
 * @property string $codeName
 * @property string $substrateFile
 * @property bool   $deleteSubstrate
 * @property string   $nestedSubstrate
 * @property string   $nestedSubstrateThumb
 * @property string   $nestedSubstrateBack
 * @property string   $nestedGeneration_type_id
 * @property integer  $base_for_status
 * @property integer  $base_for_type
 *
 * @property ContestCategory   $contestCategory
 * @property GenerationType    $generationType
 *
 * @property Contest[] $contests
 */
class ContestType extends ActiveRecord
{
    const BASE_FOR_NOMINATION = 1;
    const BASE_FOR_CONCURS = 2;
    const BASE_FOR_STAGE = 3;
    const BASE_FOR_MENTION = 4;

    const BASE_FOR_WINNER = 1;
    const BASE_FOR_PRISE = 2;

    public $substrateFile;
    public $deleteSubstrate;

    private static $baseForTypes = [
        self::BASE_FOR_CONCURS    => 'конкурс',
        self::BASE_FOR_NOMINATION => 'номинации',
        self::BASE_FOR_STAGE => 'этап',
        self::BASE_FOR_MENTION => 'благодарности'
    ];

    private static $baseForStatuses = [
        self::BASE_FOR_PRISE => 'призер',
        self::BASE_FOR_WINNER => 'победитель'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contest_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[  'clearTextAfter1','clearTextAfter2','clearTextAfter3',
                'clearTextBefore1','clearTextBefore2','clearTextBefore3'], 'safe'],
            [['name', 'codeName', 'contest_category_id'], 'required'],
            [['oneForAll'], 'boolean'],
            [['name','codeName'], 'unique'],
            [['substrateFile', 'deleteSubstrate'], 'safe'],
            [['contest_category_id', 'base_for_status', 'base_for_type'], 'integer'],
            [['substrate', 'substrate_thumb', 'substrate_back'], 'string', 'max' => 255],
            [['textBefore1', 'textBefore2', 'textBefore3', 'textAfter1', 'textAfter2', 'textAfter3'], 'string'],
            [['name'], 'string', 'max' => 32],
            [['contest_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContestCategory::className(), 'targetAttribute' => ['contest_category_id' => 'id']],
        ];
    }


    public function behaviors()
    {
        return [
            'nestedValues' => [
                'class' => NestedValuesBehavior::className(),
                'nestedSuffix' => null,
                'nestProperty' => null,
                'nestedFields' => [
                    'substrateBack' => [
                        'property' => 'substrate_back',
                        'nest' => 'contestCategory'
                    ],
                    'substrateThumb' => [
                        'property' => 'substrate_thumb',
                        'nest' => 'contestCategory'
                    ],
                    'generation_type_id' => [
                        'nest' => 'contestCategory'
                    ],
                    'substrate' => [
                        'nest' => 'contestCategory'
                    ],
                    'textAfter1',
                    'textAfter2',
                    'textAfter3',
                    'textBefore1',
                    'textBefore2',
                    'textBefore3'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'substrate' => 'Обложка',
            'substrateFile' => 'Обложка',
            'textBefore1' => 'Текст до 1',
            'textBefore2' => 'Текст до 2',
            'textBefore3' => 'Текст до 3',
            'textAfter1' => 'Текст после 1',
            'textAfter2' => 'Текст после 2',
            'textAfter3' => 'Текст после 3',
            'name' => 'Название',
            'codeName' => 'Кодовое названиа',
            'deleteSubstrate' => 'Сбросить обложку',
            'contest_category_id' => 'Категория конкурса',
            'oneForAll' => 'Одна грамота на весь тип',

            'generation_type_id' => 'Тип генерации',

            'nestedTextBefore1' => 'Текст до 1',
            'nestedTextBefore2' => 'Текст до 2',
            'nestedTextBefore3' => 'Текст до 3',
            'nestedTextAfter1' => 'Текст после 1',
            'nestedTextAfter2' => 'Текст после 2',
            'nestedTextAfter3' => 'Текст после 3',
            'clearTextBefore1' => 'Очистить текст до 1',
            'clearTextBefore2' => 'Очистить текст до 2',
            'clearTextBefore3' => 'Очистить текст до 3',
            'clearTextAfter1' => 'Очистить текст после 1',
            'clearTextAfter2' => 'Очистить текст после 2',
            'clearTextAfter3' => 'Очистить текст после 3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContests()
    {
        return $this->hasMany(Contest::className(), ['contest_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestCategory()
    {
        return $this->hasOne(ContestCategory::className(), ['id' => 'contest_category_id']);
    }

    public function getGenerationType()
    {
        return GenerationType::findOne($this->nestedGeneration_type_id);
    }

    public static function getContestTypes()
    {
        $types = self::find()->asArray()->select(['name','id'])->indexBy('id')->orderBy(['name' => SORT_DESC])->all();
        return ArrayHelper::getColumn($types, 'name');
    }

    /**
     * @param null $id
     * @param null $codeName
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findByIdCodename($id=null, $codeName=null)
    {
        return self::find()
            ->andFilterWhere(['id' => $id])
            ->andFilterWhere(['codeName' => $codeName])
            ->all();
    }


    public static function findBaseFor($type, $status)
    {
        return self::find()
            ->andWhere(['base_for_type' => $type])
            ->andWhere(['base_for_status' => $status])
            ->one();
    }

    public static function findByString($string)
    {
        return self::find()
            ->andWhere(['like', 'name', $string])
            ->all();
    }

    public static function getBaseTypes()
    {
        return static::$baseForTypes;
    }

    public function getBaseTypeText()
    {
        return ArrayHelper::getValue(static::getBaseTypes(), $this->base_for_type, null);
    }

    public static function getBaseStatuses()
    {
        return static::$baseForStatuses;
    }

    public function getBaseStatusText()
    {
        return ArrayHelper::getValue(static::getBaseStatuses(), $this->base_for_status, null);
    }
}
