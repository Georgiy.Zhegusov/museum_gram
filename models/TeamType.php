<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "team_types".
 *
 * @property integer $id
 * @property string $text
 * @property integer $baseForTeam
 *
 * @property string $baseForTeamText
 * @property TeamInfo[] $teamInfos
 */
class TeamType extends ActiveRecord
{
    const BASE_FOR_SOLO = 1;
    const BASE_FOR_TEAM = 2;

    public static function getBases()
    {
        return [
            static::BASE_FOR_SOLO => 'Одиночный участник',
            static::BASE_FOR_TEAM => 'Командный участник'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string', 'max' => 64],
            [['baseForTeam'], 'integer'],
            ['baseForTeam', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'baseForTeamText' => 'Тип команды по умолчанию для',
            'baseForTeam' => 'Тип команды по умолчанию для'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamInfos()
    {
        return $this->hasMany(TeamInfo::className(), ['team_type_id' => 'id']);
    }

    public static function getTeamTypes()
    {
        $types = self::find()->asArray()->select(['text','id'])->indexBy('id')->orderBy(['text' => SORT_DESC])->all();
        return ArrayHelper::getColumn($types, 'text');
    }

    public function getBaseForTeamText()
    {
        return ArrayHelper::getValue(static::getBases(), $this->baseForTeam, null);
    }

    public static function getTeamTypeForCount($peopleCount){
        if(empty($peopleCount)){
            return null;
        }

        $type = $peopleCount > 1
            ? static::BASE_FOR_TEAM
            : static::BASE_FOR_SOLO;

        return static::find()
            ->andWhere(['baseForTeam' => $type])
            ->one();
    }
}
