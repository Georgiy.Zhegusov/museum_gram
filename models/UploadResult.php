<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "upload_results".
 *
 * @property integer $id
 * @property integer $type
 * @property string $message
 * @property integer $upload_id
 * @property integer $objectId
 * @property string $strings
 *
 * @property UploadFile $upload
 */
class UploadResult extends \yii\db\ActiveRecord
{

    const TYPE_ERROR  = 0;
    const TYPE_NEW    = 1;
    const TYPE_UPDATE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upload_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'upload_id', 'objectId'], 'integer'],
            [['message', 'strings'], 'string'],
            [['upload_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadFile::className(), 'targetAttribute' => ['upload_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'message' => 'Message',
            'upload_id' => 'Upload ID',
            'objectId' => 'Object ID',
            'strings' => 'Strings',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpload()
    {
        return $this->hasOne(UploadFile::className(), ['id' => 'upload_id']);
    }
}
