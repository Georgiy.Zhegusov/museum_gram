<?php

namespace app\models;

use app\common\behaviors\NestedValuesBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contest_categories".
 *
 * @property integer $id
 * @property string $name
 * @property integer $stage
 * @property bool $oneForAll
 *
 * @property string $substrateFile
 * @property bool   $deleteSubstrate
 *
 * @property string $substrate
 * @property string $substrate_back
 * @property string $substrate_thumb
 *
 * @property string   $nestedSubstrate
 * @property string   $nestedSubstrateThumb
 * @property string   $nestedSubstrateBack
 *
 * @property GenerationType $generationType
 * @property ContestType[] $contestTypes
 * @property Contest[] $contests
 */
class ContestCategory extends \yii\db\ActiveRecord
{
    public $substrateFile;
    public $deleteSubstrate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contest_categories';
    }

    public function behaviors()
    {
        return [
            'nestedValues' => [
                'class' => NestedValuesBehavior::className(),
                'nestedSuffix' => null,
                'nestProperty' => null,
                'nestedFields' => [
                    'substrateBack' => [
                        'property' => 'substrate_back',
                    ],
                    'substrateThumb' => [
                        'property' => 'substrate_thumb',
                    ],
                    'generation_type_id',
                    'substrate'
                ]
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stage'], 'integer'],
            [['oneForAll'], 'boolean'],
            [['name'], 'string', 'max' => 32],
            [['substrate', 'substrate_back', 'substrate_thumb'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name', 'stage', 'generation_type_id'], 'required'],
            [['substrateFile', 'deleteSubstrate', 'generateForArray'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'stage' => 'Этап',
            'substrate' => 'Обложка',
            'substrateFile' => 'Обложка',
            'deleteSubstrate' => 'Сбросить обложку',
            'oneForAll' => 'Одна грамота на всю категорию',
            'generation_type_id' => 'Тип генерации',
        ];
    }

    public static function findByString($string)
    {
        return self::find()
            ->andWhere(['like', 'name', $string])
            ->all();
    }

    public static function getContestCategories()
    {
        $types = self::find()->asArray()->select(['name','id'])->indexBy('id')->orderBy(['name' => SORT_DESC])->all();
        return ArrayHelper::getColumn($types, 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContestTypes()
    {
        return $this->hasMany(ContestType::className(), ['contest_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContests()
    {
        return $this->hasMany(Contest::className(), ['contest_type_id' => 'id'])->viaTable(ContestType::tableName(), ['contest_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenerationType()
    {
        return $this->hasOne(GenerationType::className(), ['id' => 'generation_type_id']);
    }
}
