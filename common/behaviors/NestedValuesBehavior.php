<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 17.02.17
 * Time: 2:21
 */

namespace app\common\behaviors;


use yii\base\Behavior;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class NestedValuesBehavior extends Behavior
{
    public $nestedSuffix = '(*)';
    public $nestProperty;
    public $nestedPrefix = 'nested';
    public $nestedFields = [];

    private $_clearedNestedFields = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'clearFields'
        ];
    }

    public function clearFields()
    {
        foreach ($this->_clearedNestedFields as $clearedNestedField) {
            try{
                $this->owner->$clearedNestedField = null;
            }
            catch (\Exception $e){
            }
        }
    }

    public function __get($name)
    {
        try {
            $field = parent::__get($name);
        }
        catch (UnknownPropertyException $e) {
            $potentialProperty = substr($name, strlen($this->nestedPrefix));

            //if its nestedSomeProperty field
            if($this->nestedPrefix . $potentialProperty == $name) {
                //convert nestedSomeProperty to someProperty
                $potentialProperty = lcfirst($potentialProperty);

                $nestedProperties = null;

                if(!isset($this->nestedFields[$potentialProperty])){
                    $position = array_search($potentialProperty, $this->nestedFields);

                    if($position!==false){
                        $nestedProperties = [];
                    }
                }
                else{
                    $nestedProperties = $this->nestedFields[$potentialProperty];
                }

                if (!is_null($nestedProperties)) {
                    $potentialProperty = ArrayHelper::getValue($nestedProperties, 'property', $potentialProperty);

                    try {
                        $potentialValue = ArrayHelper::getValue($this->owner, $potentialProperty, null);
                    }
                    catch (\Exception $e){
                        $potentialValue = null;
                    }

                    if(!is_null($potentialValue)){
                        return $potentialValue;
                    }

                    $nestProperty = ArrayHelper::getValue($nestedProperties, 'nest', $this->nestProperty);
                    $suffix = ArrayHelper::getValue($nestedProperties, 'suffix', $this->nestedSuffix);

                    if(is_null($nestProperty) || is_null($this->owner->$nestProperty)){
                        return null;
                    }

                    $nestedValue = $this->owner->$nestProperty->$name;

                    if(is_null($nestedValue)){
                        return $nestedValue;
                    }

                    return $nestedValue . $suffix;
                }
            }

            $potentialProperty = substr($name, strlen('clear'));

            //if its nestedSomeProperty field
            if('clear' . $potentialProperty == $name) {
                //convert nestedSomeProperty to someProperty
                $potentialProperty = lcfirst($potentialProperty);

                $nestedProperties = null;

                if(!isset($this->nestedFields[$potentialProperty])){
                    $position = array_search($potentialProperty, $this->nestedFields);

                    if($position!==false){
                        $nestedProperties = [];
                    }
                }
                else{
                    $nestedProperties = $this->nestedFields[$potentialProperty];
                }

                if (!is_null($nestedProperties)) {
                    $potentialProperty = ArrayHelper::getValue($nestedProperties, 'property', $potentialProperty);

                    try {
                        $potentialValue = ArrayHelper::getValue($this->owner, $potentialProperty, null);
                    }
                    catch (\Exception $e){
                        $potentialValue = null;
                    }

                    return is_null($potentialValue);
                }
            }

            throw $e;
        }

        return $field;
    }

    public function __set($name, $value)
    {
        try {
            parent::__set($name, $value);
        }
        catch (UnknownPropertyException $e){
            $potentialProperty = substr($name, strlen('clear'));

            //if its nestedSomeProperty field
            if('clear' . $potentialProperty == $name) {
                $potentialProperty = lcfirst($potentialProperty);

                $nestedProperties = null;

                if(!isset($this->nestedFields[$potentialProperty])){
                    $position = array_search($potentialProperty, $this->nestedFields);

                    if($position!==false){
                        $nestedProperties = [];
                    }
                }
                else{
                    $nestedProperties = $this->nestedFields[$potentialProperty];
                }

                if (!is_null($nestedProperties)) {
                    if($value != true){
                        return;
                    }

                    $potentialProperty = ArrayHelper::getValue($nestedProperties, 'property', $potentialProperty);

                    $this->_clearedNestedFields[] = $potentialProperty;
                    return;
                }
            }

            throw $e;
        }
    }

    public function canGetProperty($name, $checkVars=true)
    {
        $potentialProperty = substr($name, strlen('clear'));

        //if its nestedSomeProperty field
        if('clear' . $potentialProperty == $name) {
            $potentialProperty = lcfirst($potentialProperty);

            $nestedProperties = null;

            if (!isset($this->nestedFields[$potentialProperty])) {
                $position = array_search($potentialProperty, $this->nestedFields);

                if ($position !== false) {
                    $nestedProperties = [];
                }
            } else {
                $nestedProperties = $this->nestedFields[$potentialProperty];
            }

            if (!is_null($nestedProperties)) {
                return true;
            }
        }

        $potentialProperty = substr($name, strlen($this->nestedPrefix));

        //if its nestedSomeProperty field
        if($this->nestedPrefix . $potentialProperty == $name) {
            $potentialProperty = lcfirst($potentialProperty);

            $nestedProperties = null;

            if (!isset($this->nestedFields[$potentialProperty])) {
                $position = array_search($potentialProperty, $this->nestedFields);

                if ($position !== false) {
                    $nestedProperties = [];
                }
            } else {
                $nestedProperties = $this->nestedFields[$potentialProperty];
            }

            if (!is_null($nestedProperties)) {
                return true;
            }
        }

        return parent::canGetProperty($name, $checkVars);
    }

    public function canSetProperty($name, $checkVars=true)
    {
        $potentialProperty = substr($name, strlen('clear'));

        //if its nestedSomeProperty field
        if('clear' . $potentialProperty == $name) {
            $potentialProperty = lcfirst($potentialProperty);

            $nestedProperties = null;

            if (!isset($this->nestedFields[$potentialProperty])) {
                $position = array_search($potentialProperty, $this->nestedFields);

                if ($position !== false) {
                    $nestedProperties = [];
                }
            } else {
                $nestedProperties = $this->nestedFields[$potentialProperty];
            }

            if (!is_null($nestedProperties)) {
                return true;
            }
        }

        $potentialProperty = substr($name, strlen($this->nestedPrefix));

        //if its nestedSomeProperty field
        if($this->nestedPrefix . $potentialProperty == $name) {
            $potentialProperty = lcfirst($potentialProperty);

            $nestedProperties = null;

            if (!isset($this->nestedFields[$potentialProperty])) {
                $position = array_search($potentialProperty, $this->nestedFields);

                if ($position !== false) {
                    $nestedProperties = [];
                }
            } else {
                $nestedProperties = $this->nestedFields[$potentialProperty];
            }

            if (!is_null($nestedProperties)) {
                return true;
            }
        }

        return parent::canGetProperty($name, $checkVars);
    }
}