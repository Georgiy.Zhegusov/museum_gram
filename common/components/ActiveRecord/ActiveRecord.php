<?php
namespace app\common\components\ActiveRecord;

use yii;

class ActiveRecord extends \yii\db\ActiveRecord
{
	/*private function beforeSave(){

	}*/

	private function log( $text, $user_id, $objects ){
		Yii::$app->getModule('logger')->addLog($text, $user_id, $objects);
	}

}