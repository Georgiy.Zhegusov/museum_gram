<?php
namespace app\common\components\DbManager;

use yii;

class DbManager extends yii\rbac\DbManager
{
	public function init()
	{
		parent::init();
	}

	public function getAssignments($userId)
	{
		$assignment = new yii\rbac\Assignment;
		if(!Yii::$app->user->isGuest && !is_null($this->getRole(Yii::$app->user->identity->group->role))){
			$assignment->userId = $userId;
			$assignment->roleName = Yii::$app->user->identity->group->role;
			return [$assignment->roleName => $assignment];
		}
		$assignment->userId = $userId;
		$assignment->roleName = 'guest';
		return [$assignment->roleName => $assignment];
	}
}