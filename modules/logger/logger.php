<?php

namespace app\modules\logger;
use yii\db\ActiveRecord;
use yii\base\Event;
use app\modules\logger\models\Log;
use app\modules\logger\models\logObjInfo;
use app\modules\logger\controllers\LoggerController;
use yii\helpers\StringHelper;
use yii;
use yii\base\View;
use yii\base\Exception;

/**
 * Logger module definition class
 */
class logger extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\logger\controllers';

	public $userClass;

	public $ignoredAttributes;

	/**
     * @inheritdoc
     */
    public function init( )
    {
        parent::init();

	    Event::on(
		    ActiveRecord::className(),
		    ActiveRecord::EVENT_AFTER_INSERT,
		    function($event){
			    $this->logInsert($event->sender);
		    }
	    );

	    Event::on(
		    ActiveRecord::className(),
		    ActiveRecord::EVENT_AFTER_UPDATE,
		    function($event){
			    $this->logUpdate($event->changedAttributes,$event->sender);
		    }
	    );

	    if($this->ignoredAttributes == null){
		    $this->ignoredAttributes = [];
	    }
    }

	/**
	 * @param $changedAttributes array
	 * @param $object ActiveRecord
	 * @return bool
	 */
	public function logUpdate($changedAttributes, $object){
		if( $changedAttributes==[] ||$object instanceof Log || $object instanceof logObjInfo){
			return true;
		}

		$objClassName = $this->getObjClassName($object);

		$objMembers = [$object];

		$attributes = [];

		$objectName = $this->getRelationName($object);

		foreach ($changedAttributes as $name=>$oldValue){

			if( in_array($name,$this->ignoredAttributes) ){
				continue;
			}
			$attributeLabel = $object->getAttributeLabel($name);

			$relationName = str_replace('Id','',$name);

			$value = $object->$name;
			if( isset($object->$relationName) && is_subclass_of($object->$relationName, ActiveRecord::className())){

				$updatedRelationObject = $object->$relationName;
				$value = $this->getRelationName($updatedRelationObject);

				if(is_null($oldValue)){
					$oldValue = null;
				}
				else{
					$oldRelationObject = $updatedRelationObject::findOne(['id'=>$oldValue]);
					$objMembers[] = $oldRelationObject;
					$oldValue = $this->getRelationName($oldRelationObject);
				}
				$objMembers[] = $object->$relationName;
			}

			$attributes[$attributeLabel] = [];
			$attributes[$attributeLabel]['old'] = $oldValue;
			$attributes[$attributeLabel]['new'] = $value;
		}

		$text = "Была изменена запись $objClassName $objectName: \n";
		foreach ($attributes as $name=>$values){
			$text.="$name: с {$values['old']} на  {$values['new']} \n";
		}

		$this->addLog(
			$text,
			$objMembers);
	}

	/**
	 * @param ActiveRecord $object
	 * @return bool
	 */
	public function logInsert($object){

		if($object instanceof Log || $object instanceof logObjInfo){
			return true;
		}

		$objectAttributes = $object->attributes;

		$objClassName = $this->getObjClassName($object);

		$objMembers = [$object];

		$attributes = [];

		foreach ($objectAttributes as $name=>$value){

			$attributeLabel = $object->getAttributeLabel($name);

			$relationName = str_replace('Id','',$name);

			if( isset($object->$relationName) && is_subclass_of($object->$relationName, ActiveRecord::className())){
				$value = $this->getRelationName($object->$relationName);
				$objMembers[] = $object->$relationName;
			}

			$attributes[$attributeLabel] = $value;
		}

		$text = "Была добавлена запись $objClassName: \n";
		foreach ($attributes as $name=>$value){
			$text.="$name: $value \n";
		}

		$this->addLog(
			$text,
			$objMembers);
	}

	public function addLog($text, $objects=[]){
		$log = new Log();
		$log->text = $text;
		$log->userId = \Yii::$app->user->id;
		$log->ip = Yii::$app->getRequest()->getUserIP();
		$log->save();
		$addedObjects = [];
		foreach ($objects as $object){

			$logObject = logObjInfo::findOne(['objectClass'=>$object->className(), 'objectId'=>$object->id]);

			if(is_null($logObject)) {
				$logObject = new logObjInfo;
				$logObject->objectClass = $object->className();
				$logObject->objectId = $object->id;
				try {
					$logObject->save();
				}
				catch(Eception $e){
				}
			}

			if(!in_array($logObject,$addedObjects)) {
				$log->link('logObjInfo', $logObject);
				$addedObjects[] = $logObject;
			}
		}
		$log->save();
	}

	public function getRelationName(yii\base\Object $relationObject){
		if(isset($relationObject->fullName)) {
			return $relationObject->id.'('.$relationObject->fullName.')';
		}
		elseif(isset($relationObject->name)) {
			return $relationObject->id.'('.$relationObject->name.')';
		}
		else{
			return $relationObject->id;
		}
	}

	public function getObjClassName($obj){
		return isset($obj->classLabel)?
			$obj->classLabel:
			StringHelper::basename($obj::className());
	}

	public function getLog($object){
		$model = Log::findByObject($object);
		return $model;
	}

	public function getUserLog($user){
		$model = Log::findByUser($user);
		return $model;
	}

	public function getLastLog($object){
		$model = Log::getLastLog($object);
		return $model;
	}
}
