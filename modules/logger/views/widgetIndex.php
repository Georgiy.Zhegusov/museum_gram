<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\logger\models\Log;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $logs yii\data\ActiveDataProvider */
/* @var $userClass string|null User class name */
?>
<div class="log-index">
	<?= GridView::widget([
		'dataProvider' => $logs,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'label' => 'Пользователь',
				'value' => function($log, $key, $index, $grid) use ($userClass){
					if(is_null($userClass)){
						return $log->userId;
					}
					else{
						return call_user_func(array($userClass, 'findOne'),['id'=>$log->userId])->fullName;
					}
				}
			],
			'ip',
			'text:ntext',
			[
				'attribute' => 'created_at',
				'format' => ['date', 'php:Y-m-d H:i:s']
			],
		],
	]); ?>
</div>
