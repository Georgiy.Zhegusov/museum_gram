<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\logger\models\Log;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'userId',
            'ip',
            'text:ntext',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d']
            ],
	        [
		        'attribute' => 'objects',
		        'value' => function( Log $log, $key, $index, $grid){
			        return implode(', ', ArrayHelper::getColumn($log->logObjInfo, 'objectClass')).':'.implode(', ', ArrayHelper::getColumn($log->logObjInfo, 'objectId'));
		        },
	        ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
