<?php

namespace app\modules\logger\models;

use app\common\components\ActiveRecord\ActiveRecord;
use yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "loggerLogs".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $ip
 * @property string $text
 * @property integer $created_at
 *
 * @property logObjInfo[] loggerObjInfo
 */
class Log extends \yii\db\ActiveRecord
{

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loggerLogs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'created_at'], 'integer'],
            [['text'], 'string'],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'ip' => 'Ip',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogObjInfo()
    {
        return $this
	        ->hasMany(logObjInfo::className(), ['id' => 'loggerObjectsId'])
	        ->viaTable('loggerLogs_loggerObjects', ['loggerLogsId' => 'id']);
    }

	public static function getLastLog($object){
		$query = self::find()
			->joinWith('logObjInfo')
			->where([logObjInfo::tableName().'.objectId'=>$object->id])
			->andWhere([logObjInfo::tableName().'.objectClass'=>$object->className()])
			->orderBy(['created_at'=>SORT_DESC])
			->limit(1)->all();
		if(is_null($query)){
			return null;
		}
		return array_pop($query);
	}

	public static function findFor($conditions=null){

		if(is_null($conditions)){
			$conditions = [];
		}

		$query = self::find();

		if(isset($conditions['object']) && !is_null($conditions['object'])){
			$object = $conditions['object'];
			$query->joinWith('logObjInfo')
				->where(['and',
					[logObjInfo::tableName().'.objectId'=>$object->id],
					[logObjInfo::tableName().'.objectClass'=>$object->className()]]);
		}

		if(isset($conditions['userId'])) {

			$userId = $conditions['userId'];

			if(isset($conditions['byUserOnly']) && $conditions['byUserOnly']==true){
				$query->orWhere(['userId' => $userId]);
			}
			else{

				$query->andWhere(['userId' => $userId]);
			}
		}

		$query->orderBy(['created_at'=>SORT_DESC]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		return $dataProvider;
	}

}
