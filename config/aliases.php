<?php

return [
    '@uploadBase' => 'uploads',
    '@substrateBase' => '@uploadBase/substrates',
    '@parsingBase' => '@uploadBase/parsing',
    '@downloadBase' => '@uploadBase/downloads',
    '@templateBase' => '@uploadBase/templates',
    '@honorImage' => '@uploadBase/honorImage',
    '@texPdf' => '@uploadBase/gramIngots',

    '@texImg' => '@app/uploads/tex/img',

    '@tex' => '@app/uploads/tex',
    '@downloadsFile' => '@app/downloadsFile',
];
