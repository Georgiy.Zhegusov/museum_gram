<?php

$params = require(__DIR__ . '/params.php');
$aliases = require(__DIR__ . '/aliases.php');

$devOrNothing = (YII_ENV_DEV?'dev':'');
$slashDevOrNothing = (YII_ENV_DEV?'/'.$devOrNothing:'');

$config = [
	'homeUrl' => $slashDevOrNothing . '/museum_gram/site/index',
    'id' => 'basic',
    'defaultRoute' => 'museum/index',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','logger'],
    'components' => [
        'authManager' => [
            'class' => 'app\common\components\DbManager\DbManager',
        ],
        'request' => [
            'baseUrl' => $slashDevOrNothing . '/museum_gram',
            'cookieValidationKey' => 'p7bJs6ndBJ55udTFJznOhneGVYm9w6q1',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
	        'identityCookie' => [
	        	'name' => 'museum_gram' . $devOrNothing,
		        'path' => 'museum_gram/' . $slashDevOrNothing,
		        'httpOnly' => true
	        ]
        ],
	    'session' => [
		    'name' => 'museum_gram_SESSION' . $devOrNothing,
		    'savePath' => __DIR__ . '/../runtime/session/',
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'assetManager' => [
						'basePath' => '@app/web/assets/',
						'baseUrl' => $slashDevOrNothing . '/museum_gram/assets/',
				],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'dbUser' => require(__DIR__ . '/dbUser' . $devOrNothing . '.php'),
        'db' => require(__DIR__ . '/db' . $devOrNothing . '.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
	    'i18n' => [
		    'translations' => [
			    'app' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@app/messages',
			    ],
			    'yii2mod.*' => [
				    'class' => 'yii\i18n\PhpMessageSource',
				    'basePath' => '@app/messages',
			    ],
		    ]
	    ],
    ],
	'modules' => [
		'logger' => [
			'class' => 'app\modules\logger\logger',
			'userClass' => 'app\models\User',
			'ignoredAttributes' => [
				'created_at',
				'updated_at'
			]
		],
		'comment' => [
			'class' => 'yii2mod\comments\Module',
			'controllerMap' => [
				'comments' => 'yii2mod\comments\controllers\ManageController'
			],
		],
		'gridview' =>  [
			'class' => '\kartik\grid\Module'
		]
	],
    'params' => $params,
    'aliases' => $aliases
];

if (YII_ENV_DEV) {

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
