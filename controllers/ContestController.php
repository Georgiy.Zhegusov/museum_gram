<?php

namespace app\controllers;

use app\models\ContestType;
use app\models\GenerationType;
use app\models\search\ContestSearch;
use app\models\UploadFile;
//use Imagine\Image\Box;
use Yii;
use app\models\Contest;
//use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ContestController implements the CRUD actions for Contest model.
 */
class ContestController extends Controller
{

    use TraitExportImportable;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Lists all Contest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'uploadForm' => $this->trySaveFile(UploadFile::TYPE_CONTEST),
            'downloadForm' => $this->tryStartGenerate(UploadFile::TYPE_CONTEST)
        ]);
    }

    /**
     * Displays a single Contest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contest();

        $contestTypes = ContestType::getContestTypes();
        $generationTypes = GenerationType::getGenerationTypes();
        $substrateTypes = Contest::getSubstrateTypeTexts();

        if(!$model->load(Yii::$app->request->post())){
            return $this->render('create', [
                'model' => $model,
                'contestTypes' => $contestTypes,
                'generationTypes' => $generationTypes,
                'substrateTypes' => $substrateTypes
            ]);
        }

        $uploadedSubstrate = UploadedFile::getInstance($model, 'substrateFile');

        //needed id for generate path for substrate
        if(!$model->save()){
            return $this->render('create', [
                'model' => $model,
                'contestTypes' => $contestTypes,
                'generationTypes' => $generationTypes,
                'substrateTypes' => $substrateTypes
            ]);
        }

        if(!empty($uploadedSubstrate)){
            $extension     = pathinfo($uploadedSubstrate->name, PATHINFO_EXTENSION);
            $substrateFile = Yii::getAlias("@substrateBase/contest_{$model->id}." . $extension);
            $fullPath = Yii::getAlias("@app/$substrateFile");
            move_uploaded_file($uploadedSubstrate->tempName, $fullPath);

//            $image=new \Imagick($uploadedSubstrate->tempName);
//            $image->setImageFormat('png');
//            $image->writeImage($fullPath);
//            chmod($fullPath, 0777);
//
//            $substrateBackGroundFile = Yii::getAlias("@substrateBase/contest_{$model->id}_b.png");
//            $fullPathBackGround = Yii::getAlias("@app/$substrateBackGroundFile");
//
//            $image=new \Imagick($uploadedSubstrate->tempName);
//            $image->setImageOpacity(0.5);
//            $image->writeImage($fullPathBackGround);
//            chmod($fullPathBackGround, 0777);
//
//            $thumbnail = Yii::getAlias("@substrateBase/contest_{$model->id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            Image::frame($fullPath)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);
//            chmod($fullPathThumbnail, 0777);

            $model->substrate_back = $substrateFile;
            $model->substrate_thumb = $substrateFile;
            $model->substrate = $substrateFile;

            if(!$model->save()){
                return $this->render('create', [
                    'model' => $model,
                    'contestTypes' => $contestTypes,
                    'generationTypes' => $generationTypes,
                    'substrateTypes' => $substrateTypes
                ]);
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Updates an existing Contest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $contestTypes = ContestType::getContestTypes();
        $generationTypes = GenerationType::getGenerationTypes();
        $substrateTypes = Contest::getSubstrateTypeTexts();

        if(empty($model)){
            return $this->render('create', [
                'model' => $model,
                'contestTypes' => $contestTypes,
                'generationTypes' => $generationTypes,
                'substrateTypes' => $substrateTypes
            ]);
        }

        if(!$model->load(Yii::$app->request->post())){
            return $this->render('update', [
                'model' => $model,
                'contestTypes' => $contestTypes,
                'generationTypes' => $generationTypes,
                'substrateTypes' => $substrateTypes
            ]);
        }

        $uploadedSubstrate = UploadedFile::getInstance($model, 'substrateFile');

        if (!empty($uploadedSubstrate)) {
            $extension     = pathinfo($uploadedSubstrate->name, PATHINFO_EXTENSION);
            $substrateFile = Yii::getAlias("@substrateBase/contest_{$model->id}." . $extension);
            $fullPath = Yii::getAlias("@app/$substrateFile");
            move_uploaded_file($uploadedSubstrate->tempName, $fullPath);

//            $image=new \Imagick($uploadedSubstrate->tempName);
//            $image->setImageFormat('png');
//            $image->writeImage($fullPath);
//
//            $substrateBackGroundFile = Yii::getAlias("@substrateBase/contest_{$model->id}_b.png");
//            $fullPathBackGround = Yii::getAlias("@app/$substrateBackGroundFile");
//
//            $image=new \Imagick($uploadedSubstrate->tempName);
//            $uploadedSubstrate->tempName;
//            $image->setImageOpacity(0.5);
//            $image->writeImage($fullPathBackGround);
//
//            $thumbnail = Yii::getAlias("@substrateBase/contest_{$model->id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            Image::frame($fullPath)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);

            $model->substrate_back = $substrateFile;
            $model->substrate_thumb = $substrateFile;
            $model->substrate = $substrateFile;
        }

        if(!empty($model->deleteSubstrate)){
            $model->substrate_back = null;
            $model->substrate_thumb = null;
            $model->substrate = null;
        }

        if (!$model->save()) {
            return $this->render('update', [
                'model' => $model,
                'contestTypes' => $contestTypes,
                'generationTypes' => $generationTypes,
                'substrateTypes' => $substrateTypes
            ]);
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Contest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionList($q = null, $id = null) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $out['results'] = [];

            $contests = Contest::findByString($q);

            foreach ($contests as $contest) {
                $out['results'][] = ['id' => $contest->id, 'text' => $contest->name];
            }
        }
        elseif ($id > 0) {
            $contest = $this->findModel($id);
            $out['results'] = ['id' => $id, 'text' => $contest->name];
        }

        return $out;
    }
}
