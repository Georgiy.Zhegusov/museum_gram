<?php
/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 03.03.17
 * Time: 22:53
 */

namespace app\controllers;


use app\models\DownloadFile;
use app\models\UploadFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\UploadedFile;

/**
 * Class ExportImportable
 *
 * @package app\controllers
 */

trait TraitExportImportable
{
    public function trySaveFile($type)
    {
        $model = new UploadFile();
        $model->type = $type;

        if($model->load(Yii::$app->request->post()) && $model->type == $type){

            $uploadedFile = UploadedFile::getInstance($model, 'uploadedFile');

            if(!empty($uploadedFile)){
                $extension     = pathinfo($uploadedFile->name, PATHINFO_EXTENSION);

                $fileName = time() . '_' . rand(10,10000);
                $newFileName = Yii::getAlias("@parsingBase/$fileName.$extension");
                $fullPath = Yii::getAlias("@app/$newFileName");

                move_uploaded_file($uploadedFile->tempName, $fullPath);

                chmod($fullPath, 0777);

                $model->path = $newFileName;
                $model->fileName = $uploadedFile->name;

                //needed id for generate path for substrate
                if($model->save()){
                    $uploadId = $model->id;
                    while (!file_exists($fullPath)){
                        sleep(1);
                    }
                    exec("/usr/local/bin/php " . \Yii::getAlias("@app/yii upload/parse-file $uploadId") . " > /dev/null &", $out);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => UploadFile::find()->andWhere(['type' => $type])->orderBy(['id' => SORT_DESC]),
        ]);

        $dataProvider->pagination->pageSize=5;
        $dataProvider->pagination->pageSizeParam='uploadPerPage';
        $dataProvider->pagination->pageParam='uploadPage';

        return $this->renderPartial('/upload/infoWidget', [
            'model' => $model,
            'type' => $type,
            'dataProvider' => $dataProvider
        ]);
    }

    public function tryStartGenerate($type)
    {
        $model = new DownloadFile();

        if($model->load(Yii::$app->request->post())){

            //needed id for generate path for substrate
            if($model->type == $type && $model->save()){
                $downloadId = $model->id;
                exec("/usr/local/bin/php " . \Yii::getAlias("@app/yii upload/generate-file $downloadId") . " > /dev/null &", $out);
            }
        }

        $model = new DownloadFile();
        $model->type = $type;

        $dataProvider = new ActiveDataProvider([
            'query' => DownloadFile::find()->andWhere(['type' => $type])->orderBy(['id' => SORT_DESC]),
        ]);

        $dataProvider->pagination->pageSize=5;
        $dataProvider->pagination->pageSizeParam='downloadPerPage';
        $dataProvider->pagination->pageParam='downloadPage';

        return $this->renderPartial('/download/infoWidget', [
            'model' => $model,
            'type' => $type,
            'dataProvider' => $dataProvider
        ]);
    }
}