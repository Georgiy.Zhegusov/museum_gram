<?php

namespace app\controllers;

use app\models\DownloadResult;
use Yii;
use app\models\DownloadFile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\DownloadedFile;

/**
 * DownloadController implements the CRUD actions for DownloadFile model.
 */
class DownloadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Finds the DownloadFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DownloadFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DownloadFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFileStatus($id){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $file = $this->findModel($id);
        $percent = $file->getPercent();
        //var_dump($file->processedCount);

        if($percent>100){
            if($file->status == DownloadFile::STATUS_IN_PROCESS){
                $percent = 99;
            }
            else{
                $percent = 100;
            }
        }

        if($percent==100){
            $text = 'Успех!';
        }
        else{
            $text = $percent . '%';
        }
        return [
            'status' => 'Success',
            'percent' => $percent,
            'text' => $text
        ];
    }

    public function actionDownload($id)
    {
        $file = $this->findModel($id);

        $fileName = Yii::getAlias('@app/' . $file->path);

        $date = new \DateTime();
        return Yii::$app->response->sendFile($fileName, $date->format('Y-m-d H:i:s') . '_' . $file->type . '.xlsx');
    }
}
