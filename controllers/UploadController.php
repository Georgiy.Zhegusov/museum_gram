<?php

namespace app\controllers;

use app\models\UploadResult;
use Yii;
use app\models\UploadFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * UploadController implements the CRUD actions for UploadFile model.
 */
class UploadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    private static $viewURL = [
        UploadFile::TYPE_CONTEST => '/contest/view'
    ];

    /**
     * Finds the UploadFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UploadFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UploadFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFileStatus($id){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $file = $this->findModel($id);
        $percent = $file->getPercent();
        //var_dump($file->processedCount);

        if($percent>100){
            $percent = 100;
        }

        if($percent==100){
            $text = 'Успех!';
        }
        else{
            $text = $percent . '%';
        }
        return [
            'status' => 'Success',
            'percent' => $percent,
            'text' => $text
        ];
    }

    public function actionDownload($id)
    {
        $file = $this->findModel($id);

        $fileName = Yii::getAlias('@app/' . $file->path);

        return Yii::$app->response->sendFile($fileName, $file->fileName);
    }

    public function actionErrors($id)
    {
        /** @var UploadResult[] $errors */
        $errors = UploadResult::find()
            ->andWhere(['upload_id' => $id, 'type' => UploadResult::TYPE_ERROR])->all();

        $data = [];

        foreach ($errors as $error) {
            $data[] = $error->message . (!empty($error->strings) ? ' в строках ' . $error->strings : '') . '.';
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => implode("\n<br>\n", $data)
            ];
    }

    public function actionNews($id)
    {
        $uploadFile = $this->findModel($id);

        /** @var UploadResult[] $errors */
        $errors = UploadResult::find()
            ->andWhere(['upload_id' => $id, 'type' => UploadResult::TYPE_NEW])->all();

        $data = [];

        foreach ($errors as $error) {
            $data[] = 'Добавлен ' . $this->getViewUrl($uploadFile->type, $error->message, $error->objectId) . ' из строки ' . $error->strings . '.';
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => implode("\n<br>\n", $data)
        ];
    }

    public function actionUpdates($id)
    {
        $uploadFile = $this->findModel($id);

        /** @var UploadResult[] $errors */
        $errors = UploadResult::find()
            ->andWhere(['upload_id' => $id, 'type' => UploadResult::TYPE_UPDATE])->all();

        $data = [];

        foreach ($errors as $error) {
            $data[] = 'Обновлен ' . $this->getViewUrl($uploadFile->type, $error->message, $error->objectId) . ' из строки ' . $error->strings . '.';
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'data' => implode("\n<br>\n", $data)
        ];
    }

    public function getViewUrl($type, $message, $id)
    {
        $url = '';

        if(isset(static::$viewURL[$type])){
            $url = static::$viewURL[$type];
        }

        return Html::a($message, Url::to([$url, 'id' => $id]));
    }
}
