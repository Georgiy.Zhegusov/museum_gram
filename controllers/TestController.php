<?php

namespace app\controllers;

use app\generator\HtmlGenerator;
use app\models\HonorsPeople;
use FPDI;
use Yii;
use app\models\TeamMosolimpId;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeamMosolimpIdController implements the CRUD actions for TeamMosolimpId model.
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new TeamMosolimpId model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPdf()
    {
        /** @var HonorsPeople $honorsPeople */
        $honorsPeople = HonorsPeople::find()->one();

        /** @var HonorsPeople[] $linkedHonorsPeoples */
        $linkedHonorsPeoples = $honorsPeople->linkedHonorsPeople;
        if(empty($linkedHonorsPeoples)){
            $linkedHonorsPeoples = [];
        }

        $linkedHonorsPeoples[] = $honorsPeople;

        try {
            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->setGenerationStart();
                $linkedHonorPeople->save();
            }

            //генерация TeX
            $generator = new HtmlGenerator();

            $pdf = $generator->generate($honorsPeople);

            $response = Yii::$app->getResponse();
            $response->sendContentAsFile($pdf, 'gram.pdf', [
                'mimeType' => 'application/pdf',
                'inline' => true,
            ]);
            Yii::$app->end();

            return $pdf;

            $pageCount = HtmlGenerator::getPageCount(\Yii::getAlias('@app/' . $path));

            if($pageCount != 1){
                throw new Exception('HonorsPeople pdf must contain only one page');
            }

            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->path = $path;
                $linkedHonorPeople->setGenerated();
                $linkedHonorPeople->save();
            }
        }
        catch (\Exception $e) {
            foreach ($linkedHonorsPeoples as $linkedHonorPeople) {
                $linkedHonorPeople->setGenerationError();
                $linkedHonorPeople->save();
            }
            var_dump($e->getMessage());

            echo 'Error' . "\n";
        }

        return 123;
    }
}
