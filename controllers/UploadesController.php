<?php

namespace app\controllers;

use app\models\ContestType;
use app\models\GenerationType;
use app\models\search\ContestSearch;
use app\models\UploadFile;
use Imagine\Image\Box;
use Yii;
use app\models\Contest;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ContestController implements the CRUD actions for Contest model.
 */
class UploadesController extends Controller
{

    use TraitExportImportable;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Lists all Contest models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'uploadEsr' => $this->trySaveFile(UploadFile::TYPE_MOSOLIMP),
            'downloadEsr' => $this->tryStartGenerate(UploadFile::TYPE_MOSOLIMP)
        ]);
    }
}
