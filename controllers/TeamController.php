<?php

namespace app\controllers;

use app\generator\HtmlGenerator;
use app\models\Contest;
use app\models\Honor;
use app\models\search\TeamSearch;
use app\models\UploadFile;
use Yii;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    use TraitExportImportable;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->user->can('gramPermission')) {
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Team models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider'                   => $dataProvider,
            'searchModel'                    => $searchModel,
            'teamBaseUploadForm'             => $this->trySaveFile(UploadFile::TYPE_TEAM_BASE),
            'teamBasePeopleUploadForm'       => $this->trySaveFile(UploadFile::TYPE_TEAM_BASE_PEOPLE),
            'teamBasePeople2StageUploadForm' => $this->trySaveFile(UploadFile::TYPE_TEAM_BASE_PEOPLE_2STAGE),
            'teamESRUploadForm'              => $this->trySaveFile(UploadFile::TYPE_MOSOLIMP),
            'teamResult'                     => $this->tryStartGenerate(UploadFile::TYPE_TEAM_RESULT),
            'teamStat'                       => $this->tryStartGenerate(UploadFile::TYPE_TEAM_STAT),
            'teamResultValidate'             => $this->trySaveFile(UploadFile::TYPE_TEAM_RESULT),
            'teamHonorImport'                => $this->trySaveFile(UploadFile::TYPE_HONOR),
            'mentionImport'                  => $this->trySaveFile(UploadFile::TYPE_MENTION)
        ]);
    }

    /**
     * Displays a single Team model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Team();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findContest($id)
    {
        if (($model = Contest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function honorTeam($teamId, $contestId)
    {
        $team = $this->findModel($teamId);

        $contest = $this->findContest($contestId);

        $teamInfo = $team->getTeamInfo($contest->stage);

        if (empty($teamInfo)) {
            throw new NotFoundHttpException('У команды отсутствует информацию по требуемому этапу.');
        }

        $existedHonor = Honor::findHonor($teamId, $contestId);

        if (!empty($existedHonor)) {
            return true;
        }

        $existedHonor             = new Honor();
        $existedHonor->contest_id = $contestId;
        $existedHonor->team_id    = $teamId;
        $existedHonor->save();

        return true;
    }

    /**
     * Deletes an existing Honor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDownload($id)
    {
        $team = $this->findModel($id);

        $generator = new HtmlGenerator();

        $lastClassNum = null;
        $schoolStart  = true;
        $generator->addTeam($team, $lastClassNum, $schoolStart, HtmlGenerator::WITHOUT_INFO, null, true, null, null);

        $pdf = $generator->render();

        $response = Yii::$app->getResponse();
        $response->sendContentAsFile($pdf, 'gram.pdf', [
            'mimeType' => 'application/pdf',
            'inline'   => true,
        ]);
        Yii::$app->end();
    }

    /**
     * @param null $q
     * @param null $id
     *
     * @return array
     */
    public function actionList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $out['results'] = [];

            $teams = Team::findByString($q);

            foreach ($teams as $team) {
                $out['results'][] = ['id' => $team->id, 'text' => $team->number];
            }
        } elseif ($id > 0) {
            $team           = $this->findModel($id);
            $out['results'] = ['id' => $id, 'text' => $team->number];
        }

        return $out;
    }
}
