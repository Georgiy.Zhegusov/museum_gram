<?php

namespace app\controllers\museumResult;

use app\controllers\TraitExportImportable;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\BaseMusObj;
use app\models\museumResult\MuseumAgeCategoryLink;
use app\models\museumResult\MuseumCategory;
use app\models\museumResult\MuseumCategoryLink;
use app\models\museumResult\MuseumCoefficient;
use app\models\UploadFile;
use Yii;
use app\models\museumResult\Museum;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MuseumsController implements the CRUD actions for Museum model.
 */
class MuseumsController extends Controller
{
    use TraitExportImportable;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Museum models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Museum::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'uploadForm'   => $this->trySaveFile(UploadFile::TYPE_MUSEUM),
            'coefUploadForm'   => $this->trySaveFile(UploadFile::TYPE_COEFFICIENT),
            'parkUploadForm'   => $this->trySaveFile(UploadFile::TYPE_PARK),
            'museumCorUploadForm'   => $this->trySaveFile(UploadFile::TYPE_MUSEUM_CORR),
            'museumInvUploadForm'   => $this->trySaveFile(UploadFile::TYPE_MUSEUM_INV),
            'museumPhotoUploadForm'   => $this->trySaveFile(UploadFile::TYPE_MUSEUM_PHOTO),
        ]);
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Displays a single Museum model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $museum = $this->findModel($id);

        return $this->render('view', [
            'model'            => $museum,
            'categoryForm'     => $this->trySaveCategory($museum),
            'ageCategoryForm'  => $this->trySaveAgeCategory($museum),
            'coefficientForm'  => $this->trySaveCoefficient($museum),
            'coefficientFocus' => false
        ]);
    }

    private function trySaveCategory(Museum $museum)
    {
        $museumCategoryLink = new MuseumCategoryLink();

        $idLoaded = $museumCategoryLink->load(Yii::$app->request->post());

        $museumCategoryLink->museums_id = $museum->id;

        if ($idLoaded && $museumCategoryLink->save()) {
            $museumCategoryLink             = new MuseumCategoryLink();
            $museumCategoryLink->museums_id = $museum->id;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $museum->getCategoriesLinks()
        ]);

        $dataProvider->pagination->pageSize      = 5;
        $dataProvider->pagination->pageSizeParam = 'categoriesPerPage';
        $dataProvider->pagination->pageParam     = 'categoriesPage';

        return $this->renderPartial('/museumResult/museum-category-link/_museumView', [
            'dataProvider'       => $dataProvider,
            'museumCategories'   => MuseumCategory::findAllCategories(),
            'museumCategoryLink' => $museumCategoryLink,
        ]);
    }

    private function trySaveAgeCategory(Museum $museum)
    {
        $museumAgeCategoryLink = new MuseumAgeCategoryLink();

        $idLoaded = $museumAgeCategoryLink->load(Yii::$app->request->post());

        $museumAgeCategoryLink->museum_id = $museum->id;

        if ($idLoaded && $museumAgeCategoryLink->save()) {
            $museumAgeCategoryLink            = new MuseumAgeCategoryLink();
            $museumAgeCategoryLink->museum_id = $museum->id;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $museum->getAgeCategoriesLinks()
        ]);

        $dataProvider->pagination->pageSize      = 5;
        $dataProvider->pagination->pageSizeParam = 'ageCategoriesPerPage';
        $dataProvider->pagination->pageParam     = 'ageCategoriesPage';

        return $this->renderPartial('/museumResult/museum-age-category-link/_museumView', [
            'dataProvider'          => $dataProvider,
            'ageCategories'         => AgeCategory::findAllCategories(),
            'museumAgeCategoryLink' => $museumAgeCategoryLink,
        ]);
    }

    private function trySaveCoefficient(Museum $museum, $coefficient_id = null)
    {
        if (empty($coefficient_id)) {
            $museumCoefficient = new MuseumCoefficient();
        } else {
            $museumCoefficient = MuseumCoefficient::findOne($coefficient_id);
        }

        $idLoaded = $museumCoefficient->load(Yii::$app->request->post());

        $museumCoefficient->museum_id = $museum->id;

        if ($idLoaded && $museumCoefficient->save()) {
            $this->redirect(Url::to(['view', 'id' => $museum->id]));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $museum->getCoefficients()
        ]);

        $dataProvider->pagination->pageSize      = 5;
        $dataProvider->pagination->pageSizeParam = 'coefficientPerPage';
        $dataProvider->pagination->pageParam     = 'coefficientPage';

        return $this->renderPartial('/museumResult/coefficient/_museumView', [
            'dataProvider' => $dataProvider,
            'coefficient'  => $museumCoefficient,
        ]);
    }

    /**
     * Creates a new Museum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Museum();

        $types = Museum::getTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'types' => $types
            ]);
        }
    }

    /**
     * Updates an existing Museum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $types = Museum::getTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'types' => $types
            ]);
        }
    }

    /**
     * Deletes an existing Museum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $museums_id
     * @param $museum_categories_id
     *
     * @return \yii\web\Response
     */
    public function actionCategoryDelete($museums_id, $museum_categories_id)
    {
        $model = MuseumCategoryLink::findBy($museums_id, $museum_categories_id);

        if (!empty($model)) {
            $model->delete();
        }

        return $this->redirect(['view', 'id' => $museums_id]);
    }

    /**
     * @param $museum_id
     * @param $age_category_id
     *
     * @return \yii\web\Response
     *
     */
    public function actionAgeCategoryDelete($museum_id, $age_category_id)
    {
        $model = MuseumAgeCategoryLink::findBy($museum_id, $age_category_id);

        if (!empty($model)) {
            $model->delete();
        }

        return $this->redirect(['view', 'id' => $museum_id]);
    }

    /**
     * @param $coefficient_id
     *
     * @return \yii\web\Response
     *
     */
    public function actionCoefficientDelete($coefficient_id)
    {
        $model = MuseumCoefficient::findOne($coefficient_id);

        if (!empty($model)) {
            $model->delete();
        }

        return $this->redirect(['view', 'id' => $model->museum_id]);
    }


    /**
     * @param $coefficient_id
     *
     * @return \yii\web\Response
     *
     */
    public function actionCoefficientUpdate($coefficient_id)
    {
        $coefficient = MuseumCoefficient::findOne($coefficient_id);

        $museum = $this->findModel($coefficient->museum_id);

        return $this->render('view', [
            'model'            => $museum,
            'categoryForm'     => $this->trySaveCategory($museum),
            'ageCategoryForm'  => $this->trySaveAgeCategory($museum),
            'coefficientForm'  => $this->trySaveCoefficient($museum, $coefficient_id),
            'coefficientFocus' => true
        ]);
    }


    /**
     * Finds the Museum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Museum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Museum::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param null $q
     * @param null $id
     *
     * @return array
     */
    public function actionList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $out['results'] = [];

            $museums = Museum::findByString($q);

            foreach ($museums as $museum) {
                $out['results'][] = ['id' => $museum->id, 'text' => $museum->number . ': ' . $museum->name];
            }
        } elseif ($id > 0) {
            $museum         = $this->findModel($id);
            $out['results'] = ['id' => $id, 'text' => $museum->number . ': ' . $museum->name];
        }

        return $out;
    }

    /**
     * @param null $q
     * @param null $id
     *
     * @return array
     */
    public function actionBaseList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $out['results'] = [];

            /** @var BaseMusObj[] $museums */
            $museums = BaseMusObj::findByString($q);

            foreach ($museums as $museum) {
                $out['results'][] = ['id' => $museum->id, 'text' => $museum->number . ': (' . $museum->baseTypeText . ') ' . $museum->name];
            }
        } elseif ($id > 0) {
            $museum         = $this->findModel($id);
            $out['results'] = ['id' => $id, 'text' => $museum->number . ': (' . $museum->baseTypeText . ') ' . $museum->name];
        }

        return $out;
    }
}
