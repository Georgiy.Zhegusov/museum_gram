<?php

namespace app\controllers\museumResult;

use app\models\museumResult\Museum;
use Imagine\Image\Box;
use Yii;
use app\models\museumResult\MuseumBall;
use yii\data\ActiveDataProvider;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MuseumBallsController implements the CRUD actions for MuseumBall model.
 */
class MuseumBallsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Lists all MuseumBall models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MuseumBall::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MuseumBall model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MuseumBall model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param      $teamId
     *
     * @return mixed
     */
    public function actionCreate($teamId=null)
    {
        $model = new MuseumBall();

        $model->team_id = $teamId;

        $statuses = MuseumBall::getStatuses();

        if(!$model->load(Yii::$app->request->post())){
            return $this->render('create', [
                'model' => $model,
                'statuses' => $statuses
            ]);
        }

        $uploadedImage = UploadedFile::getInstance($model, 'imageFile');

        //needed id for generate path for substrate
        if(!$model->save()){
            return $this->render('create', [
                'model' => $model,
                'statuses' => $statuses
            ]);
        }

        if(!empty($uploadedImage)){
            $extension     = pathinfo($uploadedImage->name, PATHINFO_EXTENSION);
            $imageFile = Yii::getAlias("@honorImage/museumBall_{$model->museum_id}." . $extension);
            $fullPath = Yii::getAlias("@app/$imageFile");
            move_uploaded_file($uploadedImage->tempName, $fullPath);

//            $image=new \Imagick($uploadedImage->tempName);
//            $image->setImageFormat('png');
//            $image->writeImage($fullPath);
//            chmod($fullPath, 0777);
//
//            $thumbnail = Yii::getAlias("@honorImage/museumBall_{$model->museum_id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            Image::frame($fullPath)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);
//            chmod($fullPathThumbnail, 0777);

            $model->image_path = $imageFile;
            $model->image_thumb = $imageFile;

            if(!$model->save()){
                return $this->render('create', [
                    'model' => $model,
                    'statuses' => $statuses
                ]);
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Updates an existing MuseumBall model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $statuses = MuseumBall::getStatuses();

        if(empty($model)){
            return $this->render('create', [
                'model' => $model,
                'statuses' => $statuses
            ]);
        }

        if(!$model->load(Yii::$app->request->post())){
            return $this->render('update', [
                'model' => $model,
                'statuses' => $statuses
            ]);
        }

        $uploadedImage = UploadedFile::getInstance($model, 'imageFile');

        if(!empty($uploadedImage)){
            $extension     = pathinfo($uploadedImage->name, PATHINFO_EXTENSION);
            $imageFile = Yii::getAlias("@honorImage/museumBall_{$model->museum_id}." . $extension);
            $fullPath = Yii::getAlias("@app/$imageFile");
            move_uploaded_file($uploadedImage->tempName, $fullPath);

//            $image=new \Imagick($uploadedImage->tempName);
//            $image->setImageFormat('png');
//            $image->writeImage($fullPath);
//            chmod($fullPath, 0777);
//
//            $thumbnail = Yii::getAlias("@honorImage/museumBall_{$model->museum_id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            Image::frame($fullPath)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);
//            chmod($fullPathThumbnail, 0777);

            $model->image_path = $imageFile;
            $model->image_thumb = $imageFile;
        }

        if(!empty($model->deleteImage)){
            $model->image_path = null;
            $model->image_thumb = null;
        }

        if(!$model->save()){
            return $this->render('create', [
                'model' => $model,
                'statuses' => $statuses
            ]);
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing MuseumBall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $balls = $this->findModel($id);

        $teamId = $balls->team_id;

        $balls->delete();

        return $this->redirect(['/team/view', 'id' => $teamId]);
    }

    /**
     * Finds the MuseumBall model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MuseumBall the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MuseumBall::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
