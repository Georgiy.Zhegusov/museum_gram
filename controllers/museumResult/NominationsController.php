<?php

namespace app\controllers\museumResult;

use app\controllers\TraitExportImportable;
use app\models\museumResult\AgeCategory;
use app\models\museumResult\MuseumCoefficient;
use app\models\museumResult\Nomination;
use app\models\museumResult\NominationAgeCategoryLink;
use app\models\museumResult\NominationBall;
use app\models\museumResult\NominationMuseumLink;
use app\models\UploadFile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MuseumsController implements the CRUD actions for Museum model.
 */
class NominationsController extends Controller
{
    use TraitExportImportable;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Lists all Museum models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Nomination::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'uploadForm' => $this->trySaveFile(UploadFile::TYPE_NOMINATIONS),
            'downloadResults' => $this->tryStartGenerate(UploadFile::TYPE_NOMINATIONS_BALLS),
            'downloadPercent' => $this->tryStartGenerate(UploadFile::TYPE_NOMINATIONS_PERCENT),
            'downloadStatus' => $this->tryStartGenerate(UploadFile::TYPE_NOMINATIONS_RESULT),
            'uploadCriteria' => $this->trySaveFile(UploadFile::TYPE_NOMINATIONS_CRITERIA),
            'importMaximum' => $this->tryStartGenerate(UploadFile::TYPE_NOMINATIONS_MAXIMUM)
        ]);
    }

    /**
     * Displays a single Museum model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $nomination = $this->findModel($id);

        return $this->render('view', [
            'model'           => $nomination,
            'ageCategoryForm' => $this->trySaveAgeCategory($nomination),
            'museumForm'      => $this->trySaveMuseum($nomination),
            'museumFocus'     => false,
            'ageCategoryFocus' => false
        ]);
    }

    private function trySaveAgeCategory(Nomination $nomination, $age_category_id=null)
    {
        if (empty($age_category_id)) {
            $museumAgeCategoryLink = new NominationAgeCategoryLink();
        } else {
            $museumAgeCategoryLink = NominationAgeCategoryLink::findBy($nomination->id, $age_category_id);
        }

        $idLoaded = $museumAgeCategoryLink->load(Yii::$app->request->post());

        $museumAgeCategoryLink->nomination_id = $nomination->id;

        if ($idLoaded && $museumAgeCategoryLink->save()) {
            $museumAgeCategoryLink                = new NominationAgeCategoryLink();
            $museumAgeCategoryLink->nomination_id = $nomination->id;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $nomination->getNominationsAgeCategories()
        ]);

        $dataProvider->pagination->pageSize      = 5;
        $dataProvider->pagination->pageSizeParam = 'ageCategoriesPerPage';
        $dataProvider->pagination->pageParam     = 'ageCategoriesPage';

        return $this->renderPartial('/museumResult/nomination-age-category-link/_museumView', [
            'dataProvider'              => $dataProvider,
            'ageCategories'             => AgeCategory::findAllCategories(),
            'nominationAgeCategoryLink' => $museumAgeCategoryLink,
        ]);
    }

    private function trySaveMuseum(Nomination $nomination, $museum_id = null)
    {
        if (empty($museum_id)) {
            $nominationMuseumLink = new NominationMuseumLink();
        } else {
            $nominationMuseumLink = NominationMuseumLink::findBy($nomination->id, $museum_id);
        }

        $idLoaded = $nominationMuseumLink->load(Yii::$app->request->post());

        $nominationMuseumLink->nomination_id = $nomination->id;

        if ($idLoaded && $nominationMuseumLink->save()) {
            $this->redirect(Url::to(['view', 'id' => $nomination->id]));
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $nomination->getNominationsMuseums()
        ]);

        $dataProvider->pagination->pageSize      = 5;
        $dataProvider->pagination->pageSizeParam = 'museumPerPage';
        $dataProvider->pagination->pageParam     = 'museumPage';

        return $this->renderPartial('/museumResult/nomination-museum-link/_museumView', [
            'nomination'           => $nomination,
            'dataProvider'         => $dataProvider,
            'nominationMuseumLink' => $nominationMuseumLink,
        ]);
    }

    /**
     * Creates a new Museum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nomination();

        $types = Nomination::getTypes();

        $operators = Nomination::getOperators();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'     => $model,
                'types'     => $types,
                'operators' => $operators
            ]);
        }
    }

    /**
     * Updates an existing Museum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $types = Nomination::getTypes();

        $operators = Nomination::getOperators();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!$model->save()){
                var_dump($model->errors);
                exit;
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'     => $model,
                'types'     => $types,
                'operators' => $operators
            ]);
        }
    }

    /**
     * Deletes an existing Museum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $nomination_id
     * @param $age_category_id
     *
     * @return \yii\web\Response
     *
     */
    public function actionAgeCategoryDelete($nomination_id, $age_category_id)
    {
        $model = NominationAgeCategoryLink::findBy($nomination_id, $age_category_id);

        if (!empty($model)) {
            $model->delete();
        }

        return $this->redirect(['view', 'id' => $nomination_id]);
    }

    /**
     * @param $nomination_id
     * @param $museum_id
     *
     * @return Response
     *
     */
    public function actionMuseumDelete($nomination_id, $museum_id)
    {
        $model = NominationMuseumLink::findBy($nomination_id, $museum_id);

        if (!empty($model)) {
            $model->delete();
        }

        return $this->redirect(['view', 'id' => $nomination_id]);
    }


    /**
     * @param $nomination_id
     * @param $museum_id
     *
     * @return Response
     *
     */
    public function actionMuseumUpdate($nomination_id, $museum_id)
    {
        $nomination = $this->findModel($nomination_id);

        return $this->render('view', [
            'model'           => $nomination,
            'ageCategoryForm' => $this->trySaveAgeCategory($nomination),
            'museumForm'      => $this->trySaveMuseum($nomination, $museum_id),
            'museumFocus'     => true,
            'ageCategoryFocus' => false
        ]);
    }

    /**
     * @param $nomination_id
     * @param $age_category_id
     *
     * @return Response
     *
     */
    public function actionAgeCategoryUpdate($nomination_id, $age_category_id)
    {
        $nomination = $this->findModel($nomination_id);

        return $this->render('view', [
            'model'           => $nomination,
            'ageCategoryForm' => $this->trySaveAgeCategory($nomination, $age_category_id),
            'museumForm'      => $this->trySaveMuseum($nomination),
            'museumFocus'     => false,
            'ageCategoryFocus' => true
        ]);
    }


    /**
     * Finds the Museum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Nomination the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nomination::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param null $q
     * @param null $id
     *
     * @return array
     */
    public function actionList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $out['results'] = [];

            /** @var Nomination[] $nominations */
            $nominations = Nomination::findByString($q);

            foreach ($nominations as $nomination) {
                $out['results'][] = ['id' => $nomination->id, 'text' => $nomination->number . ': ' . $nomination->name];
            }
        } elseif ($id > 0) {
            $nomination     = $this->findModel($id);
            $out['results'] = ['id' => $id, 'text' => $nomination->number . ': ' . $nomination->name];
        }

        return $out;
    }

    public function actionWinnerCounts($ageCategoryId, $winnerCount, $prizeCount, $save=false)
    {
    }
}
