<?php

namespace app\controllers;

use app\models\TeamType;
use Yii;
use app\models\TeamInfo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeamInfoController implements the CRUD actions for TeamInfo model.
 */
class TeamInfoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Creates a new TeamInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param $teamId
     *
     * @return mixed
     */
    public function actionCreate($teamId)
    {
        $model = new TeamInfo();
        $model->team_id = $teamId;
        $teamTypes = TeamType::getTeamTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/team/view', 'id' => $model->team_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'teamTypes' => $teamTypes
            ]);
        }
    }

    /**
     * Updates an existing TeamInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $teamTypes = TeamType::getTeamTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/team/view', 'id' => $model->team_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'teamTypes' => $teamTypes
            ]);
        }
    }

    /**
     * Deletes an existing TeamInfo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            $teamInfo = $this->findModel($id);

            $teamId = $teamInfo->team_id;

            $teamInfo->delete();

            return $this->redirect(['/team/view', 'id' => $teamId]);
    }

    /**
     * Finds the TeamInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeamInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeamInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
