<?php

namespace app\controllers;

use app\models\ContestCategory;
use app\models\ContestType;
use app\models\GeneratedFile;
use Yii;
use app\models\HonorGenerator;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * GeneratorController implements the CRUD actions for HonorGenerator model.
 */
class GeneratorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HonorGenerator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => HonorGenerator::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HonorGenerator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HonorGenerator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HonorGenerator();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            exec("/usr/local/bin/php " . \Yii::getAlias("@app/yii gen-stat/generate-generator " . $model->id) . " > /dev/null &", $out);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing HonorGenerator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HonorGenerator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HonorGenerator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HonorGenerator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Honor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $fileId
     *
     * @return mixed
     * @throws NotFoundHttpException
     *
     */
    public function actionDownload($fileId)
    {
        $file = GeneratedFile::findOne(['id' => $fileId]);

        if(empty($fileId)){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $response = Yii::$app->getResponse();
        $response->sendFile(Yii::getAlias('@app/' . $file->path),
            'gram_'
            . $file->honor_generator_id
            . '_'
            . $file->pageStart
            . '-'
            . $file->pageFinish
            . '.zip'
        );
        Yii::$app->end();
    }

    public function actionFileStatus($id){
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $file = $this->findModel($id);
        $percent = $file->getPercent();
        //var_dump($file->processedCount);

        if($percent>100){
            $percent = 100;
        }

        if($percent==100){
            $text = 'Успех!';
        }
        else{
            $text = $percent . '%';
        }
        return [
            'status' => 'Success',
            'percent' => $percent,
            'text' => $text
        ];
    }
}
