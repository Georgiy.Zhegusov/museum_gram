<?php

namespace app\controllers;

use app\generator\HtmlGenerator;
use app\models\search\HonorSearch;
//use Imagine\Image\Box;
use Yii;
use app\models\Honor;
use yii\data\ActiveDataProvider;
//use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HonorController implements the CRUD actions for Honor model.
 */
class HonorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if(!Yii::$app->user->can('gramPermission')){
            return $this->redirect(['site/login'], 302);
        }

        return parent::beforeAction($action);
    }


    /**
     * Lists all Honor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HonorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Honor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Honor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param null $teamId
     * @param null $contestId
     *
     * @return mixed
     */
    public function actionCreate($teamId, $contestId = null)
    {
        $model = new Honor();

        if(!$model->load(Yii::$app->request->post())){
            if(isset($teamId)){
                $model->team_id = $teamId;
            }

            if(isset($contestId)){
                $model->contest_id = $contestId;
            }

            return $this->render('create', [
                'model' => $model
            ]);
        }

        $uploadedImage = UploadedFile::getInstance($model, 'imageFile');

        //needed id for generate path for substrate
        if(!$model->save()){
            return $this->render('create', [
                'model' => $model
            ]);
        }

        if(!empty($uploadedImage)){
            $extension     = pathinfo($uploadedImage->name, PATHINFO_EXTENSION);
            $imageFile = Yii::getAlias("@honorImage/honor_{$model->id}." . $extension);
            $fullPathImageFile = Yii::getAlias("@app/$imageFile");
            move_uploaded_file($uploadedImage->tempName, $fullPathImageFile);

//            if(file_exists($fullPathImageFile)) {
//                unlink($fullPathImageFile);
//            }
//            Image::frame($uploadedImage->tempName)
//                ->thumbnail(new Box(1000, 1000))
//                ->save($fullPathImageFile, ['quality' => 100]);
//            chmod($fullPathImageFile, 0777);
//
//            $thumbnail = Yii::getAlias("@honorImage/honor_{$model->id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            if(file_exists($fullPathThumbnail)) {
//                unlink($fullPathThumbnail);
//            }
//            Image::frame($uploadedImage->tempName)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);
//            chmod($fullPathThumbnail, 0777);

            $model->image_thumb = $imageFile;
            $model->image_path = $imageFile;

            if(!$model->save()){
                return $this->render('create', [
                    'model' => $model
                ]);
            }
        }

        return $this->redirect(['/team/view', 'id' => $model->team_id]);
    }

    /**
     * Updates an existing Honor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(empty($model)){
            return $this->render('create', [
                'model' => $model
            ]);
        }

        if(!$model->load(Yii::$app->request->post())){
            return $this->render('update', [
                'model' => $model
            ]);
        }

        $uploadedImage = UploadedFile::getInstance($model, 'imageFile');

        if (!empty($uploadedImage)) {
            $extension     = pathinfo($uploadedImage->name, PATHINFO_EXTENSION);
            $imageFile = Yii::getAlias("@honorImage/honor_{$model->id}." . $extension);
            $fullPathImageFile = Yii::getAlias("@app/$imageFile");
            move_uploaded_file($uploadedImage->tempName, $fullPathImageFile);

//            if(file_exists($fullPathImageFile)) {
//                unlink($fullPathImageFile);
//            }
//            Image::frame($uploadedImage->tempName)
//                ->thumbnail(new Box(1000, 1000))
//                ->save($fullPathImageFile, ['quality' => 100]);
//            chmod($fullPathImageFile, 0777);
//
//            $thumbnail = Yii::getAlias("@honorImage/honor_{$model->id}_t." . $extension);
//            $fullPathThumbnail = Yii::getAlias("@app/$thumbnail");
//
//            if(file_exists($fullPathThumbnail)) {
//                unlink($fullPathThumbnail);
//            }
//            Image::frame($uploadedImage->tempName)
//                ->thumbnail(new Box(200, 200))
//                ->save($fullPathThumbnail, ['quality' => 80]);
//            chmod($fullPathThumbnail, 0777);

            $model->image_thumb = $imageFile;
            $model->image_path = $imageFile;
        }

        if(!empty($model->deleteImage)){
            $model->image_thumb = null;
            $model->image_path = null;
        }

        if (!$model->save()) {
            return $this->render('update', [
                'model' => $model
            ]);
        }

        return $this->redirect(['/team/view', 'id' => $model->team_id]);
    }

    /**
     * Deletes an existing Honor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $honor = $this->findModel($id);

        $teamId = $honor->team_id;

        $honor->delete();

        return $this->redirect(['/team/view', 'id' => $teamId]);
    }

    /**
     * Deletes an existing Honor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        $honor = $this->findModel($id);

        $peoples = $honor->honorsPeoples;

        $generator = new HtmlGenerator();

        $generator->addInfo($honor->team);

        foreach ($peoples as $people) {
            $generator->addGram($people);
        }

        $pdf = $generator->render();

        $response = Yii::$app->getResponse();
        $response->sendContentAsFile($pdf, 'gram.pdf', [
            'mimeType' => 'application/pdf',
            'inline' => true,
        ]);
        Yii::$app->end();
    }

    /**
     * Finds the Honor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Honor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Honor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
