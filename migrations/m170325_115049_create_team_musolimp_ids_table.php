<?php

use yii\db\Migration;

/**
 * Handles the creation of table `team_musolimp_ids`.
 * Has foreign keys to the tables:
 *
 * - `team`
 */
class m170325_115049_create_team_musolimp_ids_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team_musolimp_ids', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'number' => $this->integer()->unique(),
        ]);

        // creates index for column `team_id`
        $this->createIndex(
            'idx-team_musolimp_ids-team_id',
            'team_musolimp_ids',
            'team_id'
        );

        // add foreign key for table `team`
        $this->addForeignKey(
            'fk-team_musolimp_ids-team_id',
            'team_musolimp_ids',
            'team_id',
            'teams',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-team_musolimp_ids-team_id',
            'team_musolimp_ids'
        );

        // drops index for column `team_id`
        $this->dropIndex(
            'idx-team_musolimp_ids-team_id',
            'team_musolimp_ids'
        );

        $this->dropTable('team_musolimp_ids');
    }
}
