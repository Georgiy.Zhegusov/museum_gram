<?php

use yii\db\Migration;

/**
 * Handles adding substrate_back to table `contest_types`.
 */
class m170210_222648_add_substrate_back_column_to_contest_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_types', 'substrate_back', $this->string(255));
        $this->addColumn('contest_types', 'substrate_thumb', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contest_types', 'substrate_back');
        $this->dropColumn('contest_types', 'substrate_thumb');
    }
}
