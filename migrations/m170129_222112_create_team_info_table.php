<?php

use yii\db\Migration;

/**
 * Handles the creation for table `team_info`.
 * Has foreign keys to the tables:
 *
 * - `team`
 * - `team_type`
 */
class m170129_222112_create_team_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team_infos', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'team_type_id' => $this->integer(),
            'name' => $this->string(64),
            'stage' => $this->integer(),
        ]);

        // creates index for column `team_id`
        $this->createIndex(
            'idx-team_infos-team_id',
            'team_infos',
            'team_id'
        );

        // add foreign key for table `team`
        $this->addForeignKey(
            'fk-team_infos-team_id',
            'team_infos',
            'team_id',
            'teams',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `team_type_id`
        $this->createIndex(
            'idx-team_infos-team_type_id',
            'team_infos',
            'team_type_id'
        );

        // add foreign key for table `team_type`
        $this->addForeignKey(
            'fk-team_infos-team_type_id',
            'team_infos',
            'team_type_id',
            'team_types',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-team_infos-team_id',
            'team_infos'
        );

        // drops index for column `team_id`
        $this->dropIndex(
            'idx-team_infos-team_id',
            'team_infos'
        );

        // drops foreign key for table `team_type`
        $this->dropForeignKey(
            'fk-team_infos-team_type_id',
            'team_infos'
        );

        // drops index for column `team_type_id`
        $this->dropIndex(
            'idx-team_infos-team_type_id',
            'team_infos'
        );

        $this->dropTable('team_infos');
    }
}
