<?php

use yii\db\Migration;

class m170419_080816_add_column_status_to_nominations_balls_table extends Migration
{
    public function up()
    {
        $this->addColumn('nomination_balls', 'status', $this->boolean());

    }

    public function down()
    {
        $this->dropColumn('nomination_balls', 'status');
    }
}
