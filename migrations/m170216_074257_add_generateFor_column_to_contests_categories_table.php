<?php

use yii\db\Migration;

/**
 * Handles adding generateFor to table `contests_categories`.
 */
class m170216_074257_add_generateFor_column_to_contests_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_categories', 'generateFor', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contest_categories', 'generateFor');
    }
}
