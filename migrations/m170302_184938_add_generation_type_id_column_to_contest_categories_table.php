<?php

use yii\db\Migration;

/**
 * Handles adding generation_type_id to table `contest_categories`.
 * Has foreign keys to the tables:
 *
 * - `generation_type`
 */
class m170302_184938_add_generation_type_id_column_to_contest_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_categories', 'generation_type_id', $this->integer());

        // creates index for column `generation_type_id`
        $this->createIndex(
            'idx-contest_categories-generation_type_id',
            'contest_categories',
            'generation_type_id'
        );

        // add foreign key for table `generation_type`
        $this->addForeignKey(
            'fk-contest_categories-generation_type_id',
            'contest_categories',
            'generation_type_id',
            'generation_types',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `generation_type`
        $this->dropForeignKey(
            'fk-contest_categories-generation_type_id',
            'contest_categories'
        );

        // drops index for column `generation_type_id`
        $this->dropIndex(
            'idx-contest_categories-generation_type_id',
            'contest_categories'
        );

        $this->dropColumn('contest_categories', 'generation_type_id');
    }
}
