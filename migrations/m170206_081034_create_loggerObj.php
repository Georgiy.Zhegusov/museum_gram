<?php

use yii\db\Migration;

class m170206_081034_create_loggerObj extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('loggerObjects', [
            'id' => $this->primaryKey(),
            'objectClass' => $this->string(),
            'objectId' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('loggerObjects');
    }
}
