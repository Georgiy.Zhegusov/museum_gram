<?php

use yii\db\Migration;

class m170422_190649_drop_teamType_from_team_infos extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('team_infos', 'teamType');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('team_infos', 'teamType', $this->integer());
    }

}
