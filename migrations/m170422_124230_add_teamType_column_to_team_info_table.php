<?php

use yii\db\Migration;

/**
 * Handles adding teamType to table `team_info`.
 */
class m170422_124230_add_teamType_column_to_team_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('team_infos', 'teamType', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('team_infos', 'teamType');
    }
}
