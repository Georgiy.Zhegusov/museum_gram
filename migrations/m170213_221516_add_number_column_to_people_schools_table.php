<?php

use yii\db\Migration;

/**
 * Handles adding number to table `people_schools`.
 */
class m170213_221516_add_number_column_to_people_schools_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('people_schools', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('people_schools', 'number');
    }
}
