<?php

use yii\db\Migration;

class m170212_131551_alter_textBefore_column_from_contest_types_table extends Migration
{
    public function up()
    {
        $this->addColumn('contest_categories', 'oneForAll', $this->boolean());

        $this->dropColumn('contest_types', 'textBefore');
        $this->dropColumn('contest_types', 'textAfter');

        $this->addColumn('contest_types', 'textBefore1', $this->string(32));
        $this->addColumn('contest_types', 'textBefore2', $this->string(32));
        $this->addColumn('contest_types', 'textBefore3', $this->string(32));

        $this->addColumn('contest_types', 'textAfter1', $this->string(32));
        $this->addColumn('contest_types', 'textAfter2', $this->string(32));
        $this->addColumn('contest_types', 'textAfter3', $this->string(32));

        $this->dropColumn('contests', 'textBefore');
        $this->dropColumn('contests', 'textAfter');

        $this->addColumn('contests', 'textBefore1', $this->string(32));
        $this->addColumn('contests', 'textBefore2', $this->string(32));
        $this->addColumn('contests', 'textBefore3', $this->string(32));

        $this->addColumn('contests', 'textAfter1', $this->string(32));
        $this->addColumn('contests', 'textAfter2', $this->string(32));
        $this->addColumn('contests', 'textAfter3', $this->string(32));

        $this->addColumn('contest_types', 'oneForAll', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('contest_categories', 'oneForAll');

        $this->addColumn('contest_types', 'textBefore', $this->string(32));
        $this->addColumn('contest_types', 'textAfter', $this->string(32));

        $this->dropColumn('contest_types', 'textBefore1');
        $this->dropColumn('contest_types', 'textBefore2');
        $this->dropColumn('contest_types', 'textBefore3');

        $this->dropColumn('contest_types', 'textAfter1');
        $this->dropColumn('contest_types', 'textAfter2');
        $this->dropColumn('contest_types', 'textAfter3');

        $this->addColumn('contests', 'textBefore', $this->string(32));
        $this->addColumn('contests', 'textAfter', $this->string(32));

        $this->dropColumn('contests', 'textBefore1');
        $this->dropColumn('contests', 'textBefore2');
        $this->dropColumn('contests', 'textBefore3');

        $this->dropColumn('contests', 'textAfter1');
        $this->dropColumn('contests', 'textAfter2');
        $this->dropColumn('contests', 'textAfter3');

        $this->dropColumn('contest_types', 'oneForAll');
    }
}
