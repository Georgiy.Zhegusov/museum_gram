<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coefficients`.
 * Has foreign keys to the tables:
 *
 * - `museum`
 */
class m170317_171013_create_coefficients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coefficients', [
            'id' => $this->primaryKey(),
            'museum_id' => $this->integer(),
            'dateStart' => $this->date(),
            'dateFinish' => $this->date(),
            'value' => $this->float(),
            'museum_type' => $this->integer()
        ]);

        // creates index for column `museum_id`
        $this->createIndex(
            'idx-coefficients-museum_id',
            'coefficients',
            'museum_id'
        );

        // add foreign key for table `museum`
        $this->addForeignKey(
            'fk-coefficients-museum_id',
            'coefficients',
            'museum_id',
            'museums',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `museum`
        $this->dropForeignKey(
            'fk-coefficients-museum_id',
            'coefficients'
        );

        // drops index for column `museum_id`
        $this->dropIndex(
            'idx-coefficients-museum_id',
            'coefficients'
        );

        $this->dropTable('coefficients');
    }
}
