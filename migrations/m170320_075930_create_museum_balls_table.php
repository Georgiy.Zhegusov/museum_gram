<?php

use yii\db\Migration;

/**
 * Handles the creation of table `museum_balls`.
 * Has foreign keys to the tables:
 *
 * - `museum`
 * - `team`
 */
class m170320_075930_create_museum_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('museum_balls', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'museum_id' => $this->integer(),
            'team_id' => $this->integer(),
            'visitDate' => $this->date(),
            'value' => $this->float(),
        ]);

        // creates index for column `museum_id`
        $this->createIndex(
            'idx-museum_balls-museum_id',
            'museum_balls',
            'museum_id'
        );

        // add foreign key for table `museum`
        $this->addForeignKey(
            'fk-museum_balls-museum_id',
            'museum_balls',
            'museum_id',
            'museums',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `team_id`
        $this->createIndex(
            'idx-museum_balls-team_id',
            'museum_balls',
            'team_id'
        );

        // add foreign key for table `team`
        $this->addForeignKey(
            'fk-museum_balls-team_id',
            'museum_balls',
            'team_id',
            'teams',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `museum`
        $this->dropForeignKey(
            'fk-museum_balls-museum_id',
            'museum_balls'
        );

        // drops index for column `museum_id`
        $this->dropIndex(
            'idx-museum_balls-museum_id',
            'museum_balls'
        );

        // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-museum_balls-team_id',
            'museum_balls'
        );

        // drops index for column `team_id`
        $this->dropIndex(
            'idx-museum_balls-team_id',
            'museum_balls'
        );

        $this->dropTable('museum_balls');
    }
}
