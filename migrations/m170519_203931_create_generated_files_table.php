<?php

use yii\db\Migration;

/**
 * Handles the creation of table `generated_files`.
 * Has foreign keys to the tables:
 *
 * - `honor_generator`
 */
class m170519_203931_create_generated_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('generated_files', [
            'id' => $this->primaryKey(),
            'path' => $this->string(),
            'honor_generator_id' => $this->integer(),
        ]);

        // creates index for column `honor_generator_id`
        $this->createIndex(
            'idx-generated_files-honor_generator_id',
            'generated_files',
            'honor_generator_id'
        );

        // add foreign key for table `honor_generator`
        $this->addForeignKey(
            'fk-generated_files-honor_generator_id',
            'generated_files',
            'honor_generator_id',
            'honor_generators',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `honor_generator`
        $this->dropForeignKey(
            'fk-generated_files-honor_generator_id',
            'generated_files'
        );

        // drops index for column `honor_generator_id`
        $this->dropIndex(
            'idx-generated_files-honor_generator_id',
            'generated_files'
        );

        $this->dropTable('generated_files');
    }
}
