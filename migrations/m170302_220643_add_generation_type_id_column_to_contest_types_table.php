<?php

use yii\db\Migration;

/**
 * Handles adding generation_type_id to table `contest_types`.
 * Has foreign keys to the tables:
 *
 * - `generation_type`
 */
class m170302_220643_add_generation_type_id_column_to_contest_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_types', 'generation_type_id', $this->integer());

        // creates index for column `generation_type_id`
        $this->createIndex(
            'idx-contest_types-generation_type_id',
            'contest_types',
            'generation_type_id'
        );

        // add foreign key for table `generation_type`
        $this->addForeignKey(
            'fk-contest_types-generation_type_id',
            'contest_types',
            'generation_type_id',
            'generation_types',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `generation_type`
        $this->dropForeignKey(
            'fk-contest_types-generation_type_id',
            'contest_types'
        );

        // drops index for column `generation_type_id`
        $this->dropIndex(
            'idx-contest_types-generation_type_id',
            'contest_types'
        );

        $this->dropColumn('contest_types', 'generation_type_id');
    }
}
