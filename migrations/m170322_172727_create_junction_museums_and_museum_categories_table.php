<?php

use yii\db\Migration;

/**
 * Handles the creation of table `museums_museum_categories`.
 * Has foreign keys to the tables:
 *
 * - `museums`
 * - `museum_categories`
 */
class m170322_172727_create_junction_museums_and_museum_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('museums_museum_categories', [
            'museums_id' => $this->integer(),
            'museum_categories_id' => $this->integer(),
            'PRIMARY KEY(museums_id, museum_categories_id)',
        ]);

        // creates index for column `museums_id`
        $this->createIndex(
            'idx-museums_museum_categories-museums_id',
            'museums_museum_categories',
            'museums_id'
        );

        // add foreign key for table `museums`
        $this->addForeignKey(
            'fk-museums_museum_categories-museums_id',
            'museums_museum_categories',
            'museums_id',
            'museums',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `museum_categories_id`
        $this->createIndex(
            'idx-museums_museum_categories-museum_categories_id',
            'museums_museum_categories',
            'museum_categories_id'
        );

        // add foreign key for table `museum_categories`
        $this->addForeignKey(
            'fk-museums_museum_categories-museum_categories_id',
            'museums_museum_categories',
            'museum_categories_id',
            'museum_categories',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `museums`
        $this->dropForeignKey(
            'fk-museums_museum_categories-museums_id',
            'museums_museum_categories'
        );

        // drops index for column `museums_id`
        $this->dropIndex(
            'idx-museums_museum_categories-museums_id',
            'museums_museum_categories'
        );

        // drops foreign key for table `museum_categories`
        $this->dropForeignKey(
            'fk-museums_museum_categories-museum_categories_id',
            'museums_museum_categories'
        );

        // drops index for column `museum_categories_id`
        $this->dropIndex(
            'idx-museums_museum_categories-museum_categories_id',
            'museums_museum_categories'
        );

        $this->dropTable('museums_museum_categories');
    }
}
