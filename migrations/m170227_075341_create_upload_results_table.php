<?php

use yii\db\Migration;

/**
 * Handles the creation of table `upload_results`.
 * Has foreign keys to the tables:
 *
 * - `upload`
 */
class m170227_075341_create_upload_results_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('upload_results', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'message' => $this->string(),
            'upload_id' => $this->integer(),
            'objectId' => $this->integer(),
            'strings' => $this->string(),
        ]);

        // creates index for column `upload_id`
        $this->createIndex(
            'idx-upload_results-upload_id',
            'upload_results',
            'upload_id'
        );

        // add foreign key for table `upload`
        $this->addForeignKey(
            'fk-upload_results-upload_id',
            'upload_results',
            'upload_id',
            'uploads',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `upload`
        $this->dropForeignKey(
            'fk-upload_results-upload_id',
            'upload_results'
        );

        // drops index for column `upload_id`
        $this->dropIndex(
            'idx-upload_results-upload_id',
            'upload_results'
        );

        $this->dropTable('upload_results');
    }
}
