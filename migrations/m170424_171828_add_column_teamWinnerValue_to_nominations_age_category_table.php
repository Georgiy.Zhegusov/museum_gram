<?php

use yii\db\Migration;

class m170424_171828_add_column_teamWinnerValue_to_nominations_age_category_table extends Migration
{
    public function up()
    {
        $this->addColumn('nominations_age_category', 'teamWinnerValue', $this->float());
        $this->addColumn('nominations_age_category', 'teamPriseValue', $this->float());
    }

    public function down()
    {
        $this->dropColumn('nominations_age_category', 'teamWinnerValue');
        $this->dropColumn('nominations_age_category', 'teamPriseValue');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
