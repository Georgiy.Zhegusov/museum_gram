<?php

use yii\db\Migration;

class m170206_081026_create_logs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('loggerLogs', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(),
            'ip' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('loggerLogs');
    }
}
