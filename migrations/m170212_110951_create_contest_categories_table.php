<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contest_categories`.
 */
class m170212_110951_create_contest_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contest_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->unique(),
            'stage' => $this->integer(),
            'substrate' => $this->string(255),
            'substrate_back' => $this->string(255),
            'substrate_thumb' => $this->string(255),
        ]);

        $this->addColumn('contest_types', 'contest_category_id', $this->integer());

        // creates index for column `contest_type`
        $this->createIndex(
            'idx-contest_types-contest_category',
            'contest_types',
            'contest_category_id'
        );

        // add foreign key for table `contest_type`
        $this->addForeignKey(
            'fk-contest_types-contest_category',
            'contest_types',
            'contest_category_id',
            'contest_categories',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-contest_types-contest_category', 'contest_types');
        $this->dropIndex('idx-contest_types-contest_category', 'contest_types');

        $this->dropColumn('contest_types', 'contest_category_id');

        $this->dropTable('contest_categories');
    }
}
