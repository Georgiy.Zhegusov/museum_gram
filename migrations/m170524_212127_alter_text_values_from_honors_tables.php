<?php

use yii\db\Migration;

class m170524_212127_alter_text_values_from_honors_tables extends Migration
{
    public function up()
    {
        $this->alterColumn('honors', 'textBefore1', $this->string());
        $this->alterColumn('honors', 'textBefore2', $this->string());
        $this->alterColumn('honors', 'textBefore3', $this->string());
        $this->alterColumn('honors', 'textAfter1', $this->string());
        $this->alterColumn('honors', 'textAfter2', $this->string());
        $this->alterColumn('honors', 'textAfter3', $this->string());

        $this->alterColumn('contests', 'textBefore1', $this->string());
        $this->alterColumn('contests', 'textBefore2', $this->string());
        $this->alterColumn('contests', 'textBefore3', $this->string());
        $this->alterColumn('contests', 'textAfter1', $this->string());
        $this->alterColumn('contests', 'textAfter2', $this->string());
        $this->alterColumn('contests', 'textAfter3', $this->string());

        $this->alterColumn('contest_types', 'textBefore1', $this->string());
        $this->alterColumn('contest_types', 'textBefore2', $this->string());
        $this->alterColumn('contest_types', 'textBefore3', $this->string());
        $this->alterColumn('contest_types', 'textAfter1', $this->string());
        $this->alterColumn('contest_types', 'textAfter2', $this->string());
        $this->alterColumn('contest_types', 'textAfter3', $this->string());
    }

    public function down()
    {
        $this->alterColumn('honors', 'textBefore1', $this->string(32));
        $this->alterColumn('honors', 'textBefore2', $this->string(32));
        $this->alterColumn('honors', 'textBefore3', $this->string(32));
        $this->alterColumn('honors', 'textAfter1', $this->string(32));
        $this->alterColumn('honors', 'textAfter2', $this->string(32));
        $this->alterColumn('honors', 'textAfter3', $this->string(32));

        $this->alterColumn('contests', 'textBefore1', $this->string(32));
        $this->alterColumn('contests', 'textBefore2', $this->string(32));
        $this->alterColumn('contests', 'textBefore3', $this->string(32));
        $this->alterColumn('contests', 'textAfter1', $this->string(32));
        $this->alterColumn('contests', 'textAfter2', $this->string(32));
        $this->alterColumn('contests', 'textAfter3', $this->string(32));

        $this->alterColumn('contest_types', 'textBefore1', $this->string(32));
        $this->alterColumn('contest_types', 'textBefore2', $this->string(32));
        $this->alterColumn('contest_types', 'textBefore3', $this->string(32));
        $this->alterColumn('contest_types', 'textAfter1', $this->string(32));
        $this->alterColumn('contest_types', 'textAfter2', $this->string(32));
        $this->alterColumn('contest_types', 'textAfter3', $this->string(32));
    }
}
