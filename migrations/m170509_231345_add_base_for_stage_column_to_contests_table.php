<?php

use yii\db\Migration;

/**
 * Handles adding base_for_stage to table `contests`.
 */
class m170509_231345_add_base_for_stage_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'base_for_stage', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'base_for_stage');
    }
}
