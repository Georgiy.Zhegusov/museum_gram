<?php

use yii\db\Migration;

/**
 * Handles adding baseForTeam to table `team_types`.
 */
class m170422_125802_add_baseForTeam_column_to_team_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('team_types', 'baseForTeam', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('team_types', 'baseForTeam');
    }
}
