<?php

use yii\db\Migration;

/**
 * Handles the creation for table `team_types`.
 */
class m170129_221124_create_team_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team_types', [
            'id' => $this->primaryKey(),
            'text' => $this->string(64),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('team_types');
    }
}
