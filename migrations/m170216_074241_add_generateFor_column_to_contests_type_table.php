<?php

use yii\db\Migration;

/**
 * Handles adding generateFor to table `contests_type`.
 */
class m170216_074241_add_generateFor_column_to_contests_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_types', 'generateFor', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contest_types', 'generateFor');
    }
}
