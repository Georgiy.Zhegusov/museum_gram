<?php

use yii\db\Migration;

/**
 * Handles dropping stage from table `contest_types`.
 */
class m170310_174155_drop_stage_column_from_contest_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('contest_types', 'stage');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('contest_types', 'stage', $this->integer());
    }
}
