<?php

use yii\db\Migration;

/**
 * Handles the creation of table `museums`.
 */
class m170317_170527_create_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('museums', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer(),
            'name' => $this->string(64),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('museums');
    }
}
