<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contests`.
 * Has foreign keys to the tables:
 *
 * - `contest_type`
 */
class m170129_222752_create_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contests', [
            'id' => $this->primaryKey(),
            'contest_type_id' => $this->integer(),
            'substrate' => $this->string(128),
            'textBefore' => $this->string(),
            'textAfter' => $this->string(),
            'name' => $this->string(32),
        ]);

        // creates index for column `contest_type`
        $this->createIndex(
            'idx-contests-contest_type',
            'contests',
            'contest_type_id'
        );

        // add foreign key for table `contest_type`
        $this->addForeignKey(
            'fk-contests-contest_type',
            'contests',
            'contest_type_id',
            'contest_types',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `contest_type`
        $this->dropForeignKey(
            'fk-contests-contest_type',
            'contests'
        );

        // drops index for column `contest_type`
        $this->dropIndex(
            'idx-contests-contest_type',
            'contests'
        );

        $this->dropTable('contests');
    }
}
