<?php

use yii\db\Migration;

/**
 * Handles the creation for table `teams`.
 */
class m170129_221943_create_teams_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('teams', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('teams');
    }
}
