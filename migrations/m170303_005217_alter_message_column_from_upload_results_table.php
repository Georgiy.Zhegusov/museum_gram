<?php

use yii\db\Migration;

class m170303_005217_alter_message_column_from_upload_results_table extends Migration
{
    public function up()
    {
        $this->alterColumn('upload_results', 'message', $this->text() );
        $this->alterColumn('upload_results', 'strings', $this->text() );
    }

    public function down()
    {
        echo "m170303_005217_alter_message_column_from_upload_results_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
