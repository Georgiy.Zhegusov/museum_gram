<?php

use yii\db\Migration;

/**
 * Handles adding baseFor to table `generation_types`.
 */
class m170516_073658_add_baseFor_column_to_generation_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('generation_types', 'baseFor', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('generation_types', 'baseFor');
    }
}
