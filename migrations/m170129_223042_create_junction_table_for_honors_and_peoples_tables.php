<?php

use yii\db\Migration;

/**
 * Handles the creation for table `honors_peoples`.
 * Has foreign keys to the tables:
 *
 * - `honors`
 * - `peoples`
 */
class m170129_223042_create_junction_table_for_honors_and_peoples_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('honors_peoples', [
            'honors_id' => $this->integer(),
            'peoples_id' => $this->integer(),
            'PRIMARY KEY(honors_id, peoples_id)',
        ]);

        // creates index for column `honors_id`
        $this->createIndex(
            'idx-honors_peoples-honors_id',
            'honors_peoples',
            'honors_id'
        );

        // add foreign key for table `honors`
        $this->addForeignKey(
            'fk-honors_peoples-honors_id',
            'honors_peoples',
            'honors_id',
            'honors',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `peoples_id`
        $this->createIndex(
            'idx-honors_peoples-peoples_id',
            'honors_peoples',
            'peoples_id'
        );

        // add foreign key for table `peoples`
        $this->addForeignKey(
            'fk-honors_peoples-peoples_id',
            'honors_peoples',
            'peoples_id',
            'peoples',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `honors`
        $this->dropForeignKey(
            'fk-honors_peoples-honors_id',
            'honors_peoples'
        );

        // drops index for column `honors_id`
        $this->dropIndex(
            'idx-honors_peoples-honors_id',
            'honors_peoples'
        );

        // drops foreign key for table `peoples`
        $this->dropForeignKey(
            'fk-honors_peoples-peoples_id',
            'honors_peoples'
        );

        // drops index for column `peoples_id`
        $this->dropIndex(
            'idx-honors_peoples-peoples_id',
            'honors_peoples'
        );

        $this->dropTable('honors_peoples');
    }
}
