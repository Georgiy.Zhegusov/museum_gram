<?php

use yii\db\Migration;

class m170220_175159_add_unique_id_to_honors_poeples extends Migration
{
    public function up()
    {
        $this->dropPrimaryKey('PRIMARY', 'honors_peoples');
        $this->addColumn('honors_peoples', 'id', $this->primaryKey());

        $this->createIndex('uniqueIndex', 'honors_peoples', ['honors_id', 'peoples_id'], true);
    }

    public function down()
    {
        $this->dropColumn('honors_peoples', 'id');
        $this->addPrimaryKey('PRIMARY', 'honors_peoples', ['honors_id', 'peoples_id']);

        $this->dropIndex('uniqueIndex', 'honors_peoples');
    }
}
