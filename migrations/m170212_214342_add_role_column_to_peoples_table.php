<?php

use yii\db\Migration;

/**
 * Handles adding role to table `peoples`.
 */
class m170212_214342_add_role_column_to_peoples_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('peoples', 'role', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('peoples', 'role');
    }
}
