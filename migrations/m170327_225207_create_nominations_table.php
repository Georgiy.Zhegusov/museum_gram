<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nominations`.
 */
class m170327_225207_create_nominations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nominations', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'name' => $this->string(),
            'operator' => $this->integer(),
            'onlyMyAge' => $this->boolean(),
            'minCount' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nominations');
    }
}
