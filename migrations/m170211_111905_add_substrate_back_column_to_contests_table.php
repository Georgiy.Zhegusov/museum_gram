<?php

use yii\db\Migration;

/**
 * Handles adding substrate_back to table `contests`.
 */
class m170211_111905_add_substrate_back_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'substrate_back', $this->string(255));
        $this->addColumn('contests', 'substrate_thumb', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'substrate_back');
        $this->dropColumn('contests', 'substrate_thumb');
    }
}
