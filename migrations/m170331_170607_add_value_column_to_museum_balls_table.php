<?php

use yii\db\Migration;

/**
 * Handles adding value to table `museum_balls`.
 */
class m170331_170607_add_value_column_to_museum_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museum_balls', 'value', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museum_balls', 'value');
    }
}
