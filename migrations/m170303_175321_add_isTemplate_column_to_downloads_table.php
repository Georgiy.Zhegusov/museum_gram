<?php

use yii\db\Migration;

/**
 * Handles adding isTemplate to table `downloads`.
 */
class m170303_175321_add_isTemplate_column_to_downloads_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('downloads', 'isTemplate', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('downloads', 'isTemplate');
    }
}
