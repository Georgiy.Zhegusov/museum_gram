<?php

use yii\db\Migration;

/**
 * Handles adding base_for_status to table `contest_types`.
 */
class m170506_101137_add_base_for_status_column_to_contest_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contest_types', 'base_for_status', $this->integer());
        $this->addColumn('contest_types', 'base_for_type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contest_types', 'base_for_status');
        $this->dropColumn('contest_types', 'base_for_type');
    }
}
