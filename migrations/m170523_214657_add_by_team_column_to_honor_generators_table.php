<?php

use yii\db\Migration;

/**
 * Handles adding by_team to table `honor_generators`.
 */
class m170523_214657_add_by_team_column_to_honor_generators_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honor_generators', 'by_team', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honor_generators', 'by_team');
    }
}
