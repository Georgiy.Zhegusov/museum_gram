<?php

use yii\db\Migration;

/**
 * Handles adding age_category_id to table `team_infos`.
 * Has foreign keys to the tables:
 *
 * - `age_category`
 */
class m170331_073652_add_age_category_id_column_to_team_infos_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('team_infos', 'age_category_id', $this->integer());

        // creates index for column `age_category_id`
        $this->createIndex(
            'idx-team_infos-age_category_id',
            'team_infos',
            'age_category_id'
        );

        // add foreign key for table `age_category`
        $this->addForeignKey(
            'fk-team_infos-age_category_id',
            'team_infos',
            'age_category_id',
            'age_categories',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `age_category`
        $this->dropForeignKey(
            'fk-team_infos-age_category_id',
            'team_infos'
        );

        // drops index for column `age_category_id`
        $this->dropIndex(
            'idx-team_infos-age_category_id',
            'team_infos'
        );

        $this->dropColumn('team_infos', 'age_category_id');
    }
}
