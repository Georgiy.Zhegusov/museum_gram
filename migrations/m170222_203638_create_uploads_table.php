<?php

use yii\db\Migration;

/**
 * Handles the creation of table `uploads`.
 */
class m170222_203638_create_uploads_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('uploads', [
            'id' => $this->primaryKey(),
            'fileName' => $this->string(64),
            'path' => $this->string(128),
            'status' => $this->integer(),
            'type' => $this->string(5),
            'stringCount' => $this->integer(),
            'uploadedCount' => $this->integer(),
            'parsedCount' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('uploads');
    }
}
