<?php

use yii\db\Migration;

class m170411_174903_add_column_round_to_nominations_table extends Migration
{
    public function up()
    {
        $this->addColumn('nominations', 'round', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('nominations', 'round');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
