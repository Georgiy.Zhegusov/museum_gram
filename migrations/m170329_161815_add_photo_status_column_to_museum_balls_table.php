<?php

use yii\db\Migration;

/**
 * Handles adding photo_status to table `museum_balls`.
 */
class m170329_161815_add_photo_status_column_to_museum_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('museum_balls', 'photo_people_count');

        $this->addColumn('museum_balls', 'photo_status', $this->boolean());
        $this->addColumn('museum_balls', 'visit_people_count', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museum_balls', 'photo_status');
        $this->dropColumn('museum_balls', 'visit_people_count');

        $this->addColumn('museum_balls', 'photo_people_count', $this->integer());

    }
}
