<?php

use yii\db\Migration;

/**
 * Handles the creation of table `museum_categories`.
 */
class m170317_170426_create_museum_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('museum_categories', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'name' => $this->string(64),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('museum_categories');
    }
}
