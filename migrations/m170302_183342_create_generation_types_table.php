<?php

use yii\db\Migration;

/**
 * Handles the creation of table `generation_types`.
 */
class m170302_183342_create_generation_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('generation_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128),
            'generateFor' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('generation_types');
    }
}
