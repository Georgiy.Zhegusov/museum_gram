<?php

use yii\db\Migration;

/**
 * Handles the creation for table `honors`.
 * Has foreign keys to the tables:
 *
 * - `team`
 * - `contest`
 */
class m170129_222955_create_honors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('honors', [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(),
            'contest_id' => $this->integer(),
            'textBefore' => $this->string(),
            'textAfter' => $this->string(),
            'number' => $this->integer(),
        ]);

        // creates index for column `team_id`
        $this->createIndex(
            'idx-honors-team_id',
            'honors',
            'team_id'
        );

        // add foreign key for table `team`
        $this->addForeignKey(
            'fk-honors-team_id',
            'honors',
            'team_id',
            'teams',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `contest_id`
        $this->createIndex(
            'idx-honors-contest_id',
            'honors',
            'contest_id'
        );

        // add foreign key for table `contest`
        $this->addForeignKey(
            'fk-honors-contest_id',
            'honors',
            'contest_id',
            'contests',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-honors-team_id',
            'honors'
        );

        // drops index for column `team_id`
        $this->dropIndex(
            'idx-honors-team_id',
            'honors'
        );

        // drops foreign key for table `contest`
        $this->dropForeignKey(
            'fk-honors-contest_id',
            'honors'
        );

        // drops index for column `contest_id`
        $this->dropIndex(
            'idx-honors-contest_id',
            'honors'
        );

        $this->dropTable('honors');
    }
}
