<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nomination_balls`.
 * Has foreign keys to the tables:
 *
 * - `nomination`
 * - `team`
 */
class m170330_221321_create_nomination_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nomination_balls', [
            'id' => $this->primaryKey(),
            'nomination_id' => $this->integer(),
            'value' => $this->float(),
            'team_id' => $this->integer(),
        ]);

        // creates index for column `nomination_id`
        $this->createIndex(
            'idx-nomination_balls-nomination_id',
            'nomination_balls',
            'nomination_id'
        );

        // add foreign key for table `nomination`
        $this->addForeignKey(
            'fk-nomination_balls-nomination_id',
            'nomination_balls',
            'nomination_id',
            'nominations',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `team_id`
        $this->createIndex(
            'idx-nomination_balls-team_id',
            'nomination_balls',
            'team_id'
        );

        // add foreign key for table `team`
        $this->addForeignKey(
            'fk-nomination_balls-team_id',
            'nomination_balls',
            'team_id',
            'teams',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `nomination`
        $this->dropForeignKey(
            'fk-nomination_balls-nomination_id',
            'nomination_balls'
        );

        // drops index for column `nomination_id`
        $this->dropIndex(
            'idx-nomination_balls-nomination_id',
            'nomination_balls'
        );

        // drops foreign key for table `team`
        $this->dropForeignKey(
            'fk-nomination_balls-team_id',
            'nomination_balls'
        );

        // drops index for column `team_id`
        $this->dropIndex(
            'idx-nomination_balls-team_id',
            'nomination_balls'
        );

        $this->dropTable('nomination_balls');
    }
}
