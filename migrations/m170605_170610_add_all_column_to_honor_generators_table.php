<?php

use yii\db\Migration;

/**
 * Handles adding all to table `honor_generators`.
 */
class m170605_170610_add_all_column_to_honor_generators_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honor_generators', 'all', $this->integer());
        $this->addColumn('honor_generators', 'proceed', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honor_generators', 'all');
        $this->dropColumn('honor_generators', 'proceed');
    }
}
