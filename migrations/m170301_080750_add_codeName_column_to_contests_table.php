<?php

use yii\db\Migration;

/**
 * Handles adding codeName to table `contests`.
 */
class m170301_080750_add_codeName_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'codeName', $this->string(32));
        $this->addColumn('contest_types', 'codeName', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'codeName');
        $this->dropColumn('contest_types', 'codeName');
    }
}
