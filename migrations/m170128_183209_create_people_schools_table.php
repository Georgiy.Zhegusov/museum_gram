<?php

use yii\db\Migration;

/**
 * Handles the creation for table `people_schools`.
 */
class m170128_183209_create_people_schools_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('people_schools', [
            'id' => $this->primaryKey(),
            'text' => $this->string(128),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('people_schools');
    }
}
