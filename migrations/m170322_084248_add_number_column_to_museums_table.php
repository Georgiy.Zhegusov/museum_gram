<?php

use yii\db\Migration;

/**
 * Handles adding number to table `museums`.
 */
class m170322_084248_add_number_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museums', 'number');
    }
}
