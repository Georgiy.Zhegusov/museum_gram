<?php

use yii\db\Migration;

/**
 * Handles adding textOnly to table `generation_types`.
 */
class m170524_202831_add_textOnly_column_to_generation_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('generation_types', 'textsOnly', $this->boolean());
        $this->alterColumn('generation_types', 'teamGram', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('generation_types', 'textsOnly');
        $this->alterColumn('generation_types', 'teamGram', $this->boolean());
    }
}
