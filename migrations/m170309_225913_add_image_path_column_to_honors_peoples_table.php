<?php

use yii\db\Migration;

/**
 * Handles adding image_path to table `honors_peoples`.
 */
class m170309_225913_add_image_path_column_to_honors_peoples_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honors', 'image_path', $this->string(256));
        $this->addColumn('honors', 'image_thumb', $this->string(256));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honors', 'image_path');
        $this->dropColumn('honors', 'image_thumb');
    }
}
