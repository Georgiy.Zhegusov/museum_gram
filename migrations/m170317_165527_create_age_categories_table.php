<?php

use yii\db\Migration;

/**
 * Handles the creation of table `age_categories`.
 */
class m170317_165527_create_age_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('age_categories', [
            'id' => $this->primaryKey(),
            'maxClass' => $this->integer(),
            'minClass' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('age_categories');
    }
}
