<?php

use yii\db\Migration;

/**
 * Handles the creation for table `contest_types`.
 */
class m170129_222610_create_contest_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contest_types', [
            'id' => $this->primaryKey(),
            'substrate' => $this->string(),
            'stage' => $this->integer(),
            'textBefore' => $this->string(),
            'textAfter' => $this->string(),
            'name' => $this->string(32),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contest_types');
    }
}
