<?php

use yii\db\Migration;

/**
 * Handles adding esr_id to table `teams`.
 */
class m170325_113521_add_esr_id_column_to_teams_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('teams', 'temporary_number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('teams', 'temporary_number');
    }
}
