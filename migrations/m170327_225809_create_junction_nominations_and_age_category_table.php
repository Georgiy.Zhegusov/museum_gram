<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nominations_age_category`.
 * Has foreign keys to the tables:
 *
 * - `nominations`
 * - `age_category`
 */
class m170327_225809_create_junction_nominations_and_age_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nominations_age_category', [
            'nomination_id' => $this->integer(),
            'age_category_id' => $this->integer(),
            'PRIMARY KEY(nomination_id, age_category_id)',
        ]);

        // creates index for column `nominations_id`
        $this->createIndex(
            'idx-nominations_age_category-nominations_id',
            'nominations_age_category',
            'nomination_id'
        );

        // add foreign key for table `nominations`
        $this->addForeignKey(
            'fk-nominations_age_category-nominations_id',
            'nominations_age_category',
            'nomination_id',
            'nominations',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `age_category_id`
        $this->createIndex(
            'idx-nominations_age_category-age_category_id',
            'nominations_age_category',
            'age_category_id'
        );

        // add foreign key for table `age_category`
        $this->addForeignKey(
            'fk-nominations_age_category-age_category_id',
            'nominations_age_category',
            'age_category_id',
            'age_categories',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `nominations`
        $this->dropForeignKey(
            'fk-nominations_age_category-nominations_id',
            'nominations_age_category'
        );

        // drops index for column `nominations_id`
        $this->dropIndex(
            'idx-nominations_age_category-nominations_id',
            'nominations_age_category'
        );

        // drops foreign key for table `age_category`
        $this->dropForeignKey(
            'fk-nominations_age_category-age_category_id',
            'nominations_age_category'
        );

        // drops index for column `age_category_id`
        $this->dropIndex(
            'idx-nominations_age_category-age_category_id',
            'nominations_age_category'
        );

        $this->dropTable('nominations_age_category');
    }
}
