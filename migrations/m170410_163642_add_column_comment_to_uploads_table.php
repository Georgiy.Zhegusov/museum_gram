<?php

use yii\db\Migration;

class m170410_163642_add_column_comment_to_uploads_table extends Migration
{
    public function up()
    {
        $this->addColumn('uploads', 'comment', $this->string(128));
    }

    public function down()
    {
        $this->dropColumn('uploads', 'comment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
