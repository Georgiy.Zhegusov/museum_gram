<?php

use yii\db\Migration;

/**
 * Handles adding type to table `museums`.
 */
class m170327_074428_add_type_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museums', 'type');
    }
}
