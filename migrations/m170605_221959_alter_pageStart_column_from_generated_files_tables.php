<?php

use yii\db\Migration;

class m170605_221959_alter_pageStart_column_from_generated_files_tables extends Migration
{
    public function up()
    {
        $this->alterColumn('generated_files', 'pageStart', $this->string(64));
        $this->alterColumn('generated_files', 'pageFinish', $this->string(64));
    }

    public function down()
    {
        $this->alterColumn('generated_files', 'pageStart', $this->integer());
        $this->alterColumn('generated_files', 'pageFinish', $this->integer());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
