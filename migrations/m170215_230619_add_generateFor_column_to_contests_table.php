<?php

use yii\db\Migration;

/**
 * Handles adding generateFor to table `contests`.
 */
class m170215_230619_add_generateFor_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'generateFor', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'generateFor');
    }
}
