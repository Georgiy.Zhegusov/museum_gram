<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nominations_museums`.
 * Has foreign keys to the tables:
 *
 * - `nominations`
 * - `museums`
 */
class m170327_230028_create_junction_nominations_and_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nominations_museums', [
            'nomination_id' => $this->integer(),
            'museum_id' => $this->integer(),
            'useForMax' => $this->boolean(),
            'PRIMARY KEY(nomination_id, museum_id)',
        ]);

        // creates index for column `nominations_id`
        $this->createIndex(
            'idx-nominations_museums-nominations_id',
            'nominations_museums',
            'nomination_id'
        );

        // add foreign key for table `nominations`
        $this->addForeignKey(
            'fk-nominations_museums-nominations_id',
            'nominations_museums',
            'nomination_id',
            'nominations',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `museums_id`
        $this->createIndex(
            'idx-nominations_museums-museums_id',
            'nominations_museums',
            'museum_id'
        );

        // add foreign key for table `museums`
        $this->addForeignKey(
            'fk-nominations_museums-museums_id',
            'nominations_museums',
            'museum_id',
            'museums',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `nominations`
        $this->dropForeignKey(
            'fk-nominations_museums-nominations_id',
            'nominations_museums'
        );

        // drops index for column `nominations_id`
        $this->dropIndex(
            'idx-nominations_museums-nominations_id',
            'nominations_museums'
        );

        // drops foreign key for table `museums`
        $this->dropForeignKey(
            'fk-nominations_museums-museums_id',
            'nominations_museums'
        );

        // drops index for column `museums_id`
        $this->dropIndex(
            'idx-nominations_museums-museums_id',
            'nominations_museums'
        );

        $this->dropTable('nominations_museums');
    }
}
