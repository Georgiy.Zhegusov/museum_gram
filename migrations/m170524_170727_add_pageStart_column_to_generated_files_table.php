<?php

use yii\db\Migration;

/**
 * Handles adding pageStart to table `generated_files`.
 */
class m170524_170727_add_pageStart_column_to_generated_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('generated_files', 'pageStart', $this->integer());
        $this->addColumn('generated_files', 'pageFinish', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('generated_files', 'pageStart');
        $this->dropColumn('generated_files', 'pageFinish');
    }
}
