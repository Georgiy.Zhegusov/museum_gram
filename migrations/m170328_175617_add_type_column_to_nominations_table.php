<?php

use yii\db\Migration;

/**
 * Handles adding type to table `nominations`.
 */
class m170328_175617_add_type_column_to_nominations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('nominations', 'type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('nominations', 'type');
    }
}
