<?php

use yii\db\Migration;

/**
 * Handles adding substrate_type to table `contests`.
 */
class m170513_202115_add_substrate_type_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'substrate_type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'substrate_type');
    }
}
