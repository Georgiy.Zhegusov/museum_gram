<?php

use yii\db\Migration;

class m170517_083908_alter_name_columns_from_contests_table extends Migration
{
    public function up()
    {
        $this->alterColumn('contests', 'name', $this->string(256));
        $this->alterColumn('contests', 'codeName', $this->string(256));

        $this->alterColumn('contests', 'textBefore1', $this->string(256));
        $this->alterColumn('contests', 'textBefore2', $this->string(256));
        $this->alterColumn('contests', 'textBefore3', $this->string(256));

        $this->alterColumn('contests', 'textAfter1', $this->string(256));
        $this->alterColumn('contests', 'textAfter2', $this->string(256));
        $this->alterColumn('contests', 'textAfter3', $this->string(256));
    }

    public function down()
    {
        $this->alterColumn('contests', 'name', $this->string(128));
        $this->alterColumn('contests', 'codeName', $this->string(128));

        $this->alterColumn('contests', 'textBefore1', $this->string(32));
        $this->alterColumn('contests', 'textBefore2', $this->string(32));
        $this->alterColumn('contests', 'textBefore3', $this->string(32));

        $this->alterColumn('contests', 'textAfter1', $this->string(32));
        $this->alterColumn('contests', 'textAfter2', $this->string(32));
        $this->alterColumn('contests', 'textAfter3', $this->string(32));
    }
}
