<?php

use yii\db\Migration;

/**
 * Handles adding teamGram to table `generation_types`.
 */
class m170515_082702_add_teamGram_column_to_generation_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('generation_types', 'teamGram', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('generation_types', 'teamGram');
    }
}
