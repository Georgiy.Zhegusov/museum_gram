<?php

use yii\db\Migration;

/**
 * Handles adding type_id to table `museum_categories`.
 */
class m170322_081319_add_type_id_column_to_museum_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museum_categories', 'type_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museum_categories', 'type_id');
    }
}
