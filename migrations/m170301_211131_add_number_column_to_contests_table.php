<?php

use yii\db\Migration;

/**
 * Handles adding number to table `contests`.
 */
class m170301_211131_add_number_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contests', 'number');
    }
}
