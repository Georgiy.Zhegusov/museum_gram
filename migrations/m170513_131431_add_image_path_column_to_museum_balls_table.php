<?php

use yii\db\Migration;

/**
 * Handles adding image_path to table `museum_balls`.
 */
class m170513_131431_add_image_path_column_to_museum_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museum_balls', 'image_path', $this->string(255));
        $this->addColumn('museum_balls', 'image_thumb', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museum_balls', 'image_path');
        $this->dropColumn('museum_balls', 'image_thumb');
    }
}
