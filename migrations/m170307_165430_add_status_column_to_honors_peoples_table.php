<?php

use yii\db\Migration;

/**
 * Handles adding status to table `honors_peoples`.
 */
class m170307_165430_add_status_column_to_honors_peoples_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honors_peoples', 'status', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honors_peoples', 'status');
    }
}
