<?php

use yii\db\Migration;

/**
 * Handles adding textBefore1 to table `honors`.
 */
class m170217_080501_add_textBefore1_column_to_honors_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honors', 'textBefore1', $this->string(32));
        $this->addColumn('honors', 'textBefore2', $this->string(32));
        $this->addColumn('honors', 'textBefore3', $this->string(32));
        $this->addColumn('honors', 'textAfter1', $this->string(32));
        $this->addColumn('honors', 'textAfter2', $this->string(32));
        $this->addColumn('honors', 'textAfter3', $this->string(32));

        $this->dropColumn('honors', 'textAfter');
        $this->dropColumn('honors', 'textBefore');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honors', 'textBefore1');
        $this->dropColumn('honors', 'textBefore2');
        $this->dropColumn('honors', 'textBefore3');
        $this->dropColumn('honors', 'textAfter1');
        $this->dropColumn('honors', 'textAfter2');
        $this->dropColumn('honors', 'textAfter3');
        $this->dropColumn('honors', 'textAfter3');

        $this->addColumn('honors', 'textBefore', $this->string(32));
        $this->addColumn('honors', 'textAfter', $this->string(32));
    }
}
