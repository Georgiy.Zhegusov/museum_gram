<?php

use yii\db\Migration;

/**
 * Handles the creation for table `peoples`.
 * Has foreign keys to the tables:
 *
 * - `role`
 * - `school`
 * - `team_info`
 */
class m170129_222438_create_peoples_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('peoples', [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer(),
            'name' => $this->string(64),
            'surname' => $this->string(64),
            'patronomic' => $this->string(64),
            'school_id' => $this->integer(),
            'schoolText' => $this->string(128),
            'classNum' => $this->integer(),
            'classText' => $this->string(16),
            'team_info_id' => $this->integer(),
        ]);

        // creates index for column `role_id`
        $this->createIndex(
            'idx-peoples-role_id',
            'peoples',
            'role_id'
        );

        // add foreign key for table `role`
        $this->addForeignKey(
            'fk-peoples-role_id',
            'peoples',
            'role_id',
            'people_roles',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `school_id`
        $this->createIndex(
            'idx-peoples-school_id',
            'peoples',
            'school_id'
        );

        // add foreign key for table `school`
        $this->addForeignKey(
            'fk-peoples-school_id',
            'peoples',
            'school_id',
            'people_schools',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `team_info_id`
        $this->createIndex(
            'idx-peoples-team_info_id',
            'peoples',
            'team_info_id'
        );

        // add foreign key for table `team_info`
        $this->addForeignKey(
            'fk-peoples-team_info_id',
            'peoples',
            'team_info_id',
            'team_infos',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `role`
        $this->dropForeignKey(
            'fk-peoples-role_id',
            'peoples'
        );

        // drops index for column `role_id`
        $this->dropIndex(
            'idx-peoples-role_id',
            'peoples'
        );

        // drops foreign key for table `school`
        $this->dropForeignKey(
            'fk-peoples-school_id',
            'peoples'
        );

        // drops index for column `school_id`
        $this->dropIndex(
            'idx-peoples-school_id',
            'peoples'
        );

        // drops foreign key for table `team_info`
        $this->dropForeignKey(
            'fk-peoples-team_info_id',
            'peoples'
        );

        // drops index for column `team_info_id`
        $this->dropIndex(
            'idx-peoples-team_info_id',
            'peoples'
        );

        $this->dropTable('peoples');
    }
}
