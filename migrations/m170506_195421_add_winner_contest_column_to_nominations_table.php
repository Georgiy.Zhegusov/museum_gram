<?php

use yii\db\Migration;

/**
 * Handles adding winner_contest to table `nominations`.
 * Has foreign keys to the tables:
 *
 * - `winner_contest`
 * - `prise_contest`
 */
class m170506_195421_add_winner_contest_column_to_nominations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('nominations', 'winner_contest_id', $this->integer());
        $this->addColumn('nominations', 'prise_contest_id', $this->integer());

        // creates index for column `winner_contest_id`
        $this->createIndex(
            'idx-nominations-winner_contest_id',
            'nominations',
            'winner_contest_id'
        );

        // add foreign key for table `winner_contest`
        $this->addForeignKey(
            'fk-nominations-winner_contest_id',
            'nominations',
            'winner_contest_id',
            'contests',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        // creates index for column `prise_contest_id`
        $this->createIndex(
            'idx-nominations-prise_contest_id',
            'nominations',
            'prise_contest_id'
        );

        // add foreign key for table `prise_contest`
        $this->addForeignKey(
            'fk-nominations-prise_contest_id',
            'nominations',
            'prise_contest_id',
            'contests',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `winner_contest`
        $this->dropForeignKey(
            'fk-nominations-winner_contest_id',
            'nominations'
        );

        // drops index for column `winner_contest_id`
        $this->dropIndex(
            'idx-nominations-winner_contest_id',
            'nominations'
        );

        // drops foreign key for table `prise_contest`
        $this->dropForeignKey(
            'fk-nominations-prise_contest_id',
            'nominations'
        );

        // drops index for column `prise_contest_id`
        $this->dropIndex(
            'idx-nominations-prise_contest_id',
            'nominations'
        );

        $this->dropColumn('nominations', 'winner_contest_id');
        $this->dropColumn('nominations', 'prise_contest_id');
    }
}
