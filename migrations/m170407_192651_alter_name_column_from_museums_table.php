<?php

use yii\db\Migration;

class m170407_192651_alter_name_column_from_museums_table extends Migration
{
    public function up()
    {
        $this->alterColumn('museums', 'name', $this->string(128));
    }

    public function down()
    {
        $this->alterColumn('museums', 'name', $this->string(64));

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
