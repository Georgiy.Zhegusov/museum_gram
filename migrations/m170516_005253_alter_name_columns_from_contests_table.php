<?php

use yii\db\Migration;

class m170516_005253_alter_name_columns_from_contests_table extends Migration
{
    public function up()
    {
        $this->alterColumn('contests', 'name', $this->string(128));
        $this->alterColumn('contests', 'codeName', $this->string(128));
    }

    public function down()
    {
        $this->alterColumn('contests', 'name', $this->string(32));
        $this->alterColumn('contests', 'codeName', $this->string(32));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
