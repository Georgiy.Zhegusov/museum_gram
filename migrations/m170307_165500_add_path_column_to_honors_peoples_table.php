<?php

use yii\db\Migration;

/**
 * Handles adding path to table `honors_peoples`.
 */
class m170307_165500_add_path_column_to_honors_peoples_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honors_peoples', 'path', $this->string(256));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honors_peoples', 'path');
    }
}
