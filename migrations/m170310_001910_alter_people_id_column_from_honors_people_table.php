<?php

use yii\db\Migration;

class m170310_001910_alter_people_id_column_from_honors_people_table extends Migration
{
    public function up()
    {
        $this->alterColumn('honors_peoples', 'peoples_id', $this->integer()->null());
    }

    public function down()
    {
        $this->alterColumn('honors_peoples', 'peoples_id', $this->integer());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
