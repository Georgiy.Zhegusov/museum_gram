<?php

use yii\db\Migration;

/**
 * Handles adding maxValue to table `nominations_age_category`.
 */
class m170330_182840_add_maxValue_column_to_nominations_age_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('nominations_age_category', 'maxValue', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('nominations_age_category', 'maxValue');
    }
}
