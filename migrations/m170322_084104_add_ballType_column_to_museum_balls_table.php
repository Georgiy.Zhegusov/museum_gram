<?php

use yii\db\Migration;

/**
 * Handles adding ballType to table `museum_balls`.
 */
class m170322_084104_add_ballType_column_to_museum_balls_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museum_balls', 'ballType', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('museum_balls', 'ballType');
    }
}
