<?php

use yii\db\Migration;

/**
 * Handles the creation for table `people_roles`.
 */
class m170128_183123_create_people_roles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('people_roles', [
            'id' => $this->primaryKey(),
            'text' => $this->string(32),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('people_roles');
    }
}
