<?php

use yii\db\Migration;

/**
 * Handles adding by_school to table `honor_generators`.
 */
class m170524_081608_add_by_school_column_to_honor_generators_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('honor_generators', 'by_school', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('honor_generators', 'by_school');
    }
}
