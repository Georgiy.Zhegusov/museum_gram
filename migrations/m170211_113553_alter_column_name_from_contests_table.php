<?php

use yii\db\Migration;

class m170211_113553_alter_column_name_from_contests_table extends Migration
{
    public function up()
    {
        $this->alterColumn('contests', 'name', $this->string(32)->unique());
        $this->alterColumn('contest_types', 'name', $this->string(32)->unique());
    }

    public function down()
    {
        $this->alterColumn('contests', 'name', $this->string(32));
        $this->alterColumn('contest_types', 'name', $this->string(32));
    }
}
