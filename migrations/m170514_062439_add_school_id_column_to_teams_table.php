<?php

use yii\db\Migration;

/**
 * Handles adding school_id to table `teams`.
 * Has foreign keys to the tables:
 *
 * - `school`
 */
class m170514_062439_add_school_id_column_to_teams_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('teams', 'school_id', $this->integer());
        $this->addColumn('teams', 'classNum', $this->integer());

        // creates index for column `school_id`
        $this->createIndex(
            'idx-teams-school_id',
            'teams',
            'school_id'
        );

        // add foreign key for table `school`
        $this->addForeignKey(
            'fk-teams-school_id',
            'teams',
            'school_id',
            'people_schools',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `school`
        $this->dropForeignKey(
            'fk-teams-school_id',
            'teams'
        );

        // drops index for column `school_id`
        $this->dropIndex(
            'idx-teams-school_id',
            'teams'
        );

        $this->dropColumn('teams', 'school_id');
        $this->dropColumn('teams', 'classNum');
    }
}
