<?php

use yii\db\Migration;

/**
 * Handles adding winner_contest to table `museums`.
 * Has foreign keys to the tables:
 *
 * - `winner_contest`
 * - `prise_contest`
 */
class m170506_195436_add_winner_contest_column_to_museums_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('museums', 'winner_contest_id', $this->integer());
        $this->addColumn('museums', 'prise_contest_id', $this->integer());

        // creates index for column `winner_contest_id`
        $this->createIndex(
            'idx-museums-winner_contest_id',
            'museums',
            'winner_contest_id'
        );

        // add foreign key for table `winner_contest`
        $this->addForeignKey(
            'fk-museums-winner_contest_id',
            'museums',
            'winner_contest_id',
            'contests',
            'id',
            'CASCADE',
            'RESTRICT'
        );

        // creates index for column `prise_contest_id`
        $this->createIndex(
            'idx-museums-prise_contest_id',
            'museums',
            'prise_contest_id'
        );

        // add foreign key for table `prise_contest`
        $this->addForeignKey(
            'fk-museums-prise_contest_id',
            'museums',
            'prise_contest_id',
            'contests',
            'id',
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `winner_contest`
        $this->dropForeignKey(
            'fk-museums-winner_contest_id',
            'museums'
        );

        // drops index for column `winner_contest_id`
        $this->dropIndex(
            'idx-museums-winner_contest_id',
            'museums'
        );

        // drops foreign key for table `prise_contest`
        $this->dropForeignKey(
            'fk-museums-prise_contest_id',
            'museums'
        );

        // drops index for column `prise_contest_id`
        $this->dropIndex(
            'idx-museums-prise_contest_id',
            'museums'
        );

        $this->dropColumn('museums', 'winner_contest_id');
        $this->dropColumn('museums', 'prise_contest_id');
    }
}
