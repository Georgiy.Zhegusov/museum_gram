<?php

use yii\db\Migration;

/**
 * Handles adding generation_type_id to table `contests`.
 * Has foreign keys to the tables:
 *
 * - `generation_type`
 */
class m170302_221931_add_generation_type_id_column_to_contests_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contests', 'generation_type_id', $this->integer());

        // creates index for column `generation_type_id`
        $this->createIndex(
            'idx-contests-generation_type_id',
            'contests',
            'generation_type_id'
        );

        // add foreign key for table `generation_type`
        $this->addForeignKey(
            'fk-contests-generation_type_id',
            'contests',
            'generation_type_id',
            'generation_types',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `generation_type`
        $this->dropForeignKey(
            'fk-contests-generation_type_id',
            'contests'
        );

        // drops index for column `generation_type_id`
        $this->dropIndex(
            'idx-contests-generation_type_id',
            'contests'
        );

        $this->dropColumn('contests', 'generation_type_id');
    }
}
