<?php

use yii\db\Migration;

class m170324_175809_reformat_museum_balls_table extends Migration
{
    public function up()
    {
        $this->dropColumn('museum_balls', 'type');
        $this->dropColumn('museum_balls', 'value');

        $this->addColumn('museum_balls', 'correspondence_round', $this->integer());
        $this->addColumn('museum_balls', 'internal_round', $this->integer());
        $this->addColumn('museum_balls', 'photo_people_count', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('museum_balls', 'photo_people_count');
        $this->dropColumn('museum_balls', 'correspondence_round');
        $this->dropColumn('museum_balls', 'internal_round');

        $this->addColumn('museum_balls', 'type', $this->integer());
        $this->addColumn('museum_balls', 'value', $this->float());
    }
}
