<?php

use yii\db\Migration;

/**
 * Handles adding peopleCount to table `team_infos`.
 */
class m170406_080118_add_peopleCount_column_to_team_infos_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('team_infos', 'peopleCount', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('team_infos', 'peopleCount');
    }
}
