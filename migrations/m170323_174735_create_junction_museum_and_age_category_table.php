<?php

use yii\db\Migration;

/**
 * Handles the creation of table `museums_age_categories`.
 * Has foreign keys to the tables:
 *
 * - `museum`
 * - `age_category`
 */
class m170323_174735_create_junction_museum_and_age_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('museums_age_categories', [
            'museum_id' => $this->integer(),
            'age_category_id' => $this->integer(),
            'PRIMARY KEY(museum_id, age_category_id)',
        ]);

        // creates index for column `museum_id`
        $this->createIndex(
            'idx-museums_age_categories-museum_id',
            'museums_age_categories',
            'museum_id'
        );

        // add foreign key for table `museum`
        $this->addForeignKey(
            'fk-museums_age_categories-museum_id',
            'museums_age_categories',
            'museum_id',
            'museums',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        // creates index for column `age_category_id`
        $this->createIndex(
            'idx-museums_age_categories-age_category_id',
            'museums_age_categories',
            'age_category_id'
        );

        // add foreign key for table `age_category`
        $this->addForeignKey(
            'fk-museums_age_categories-age_category_id',
            'museums_age_categories',
            'age_category_id',
            'age_categories',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `museum`
        $this->dropForeignKey(
            'fk-museums_age_categories-museum_id',
            'museums_age_categories'
        );

        // drops index for column `museum_id`
        $this->dropIndex(
            'idx-museums_age_categories-museum_id',
            'museums_age_categories'
        );

        // drops foreign key for table `age_category`
        $this->dropForeignKey(
            'fk-museums_age_categories-age_category_id',
            'museums_age_categories'
        );

        // drops index for column `age_category_id`
        $this->dropIndex(
            'idx-museums_age_categories-age_category_id',
            'museums_age_categories'
        );

        $this->dropTable('museums_age_categories');
    }
}
