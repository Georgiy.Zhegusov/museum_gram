<?php

use yii\db\Migration;

/**
 * Handles adding unique to table `generation_types`.
 */
class m170524_211142_add_unique_column_to_generation_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('generation_types', 'unique', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('generation_types', 'unique');
    }
}
