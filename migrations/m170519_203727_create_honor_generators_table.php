<?php

use yii\db\Migration;

/**
 * Handles the creation of table `honor_generators`.
 */
class m170519_203727_create_honor_generators_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('honor_generators', [
            'id' => $this->primaryKey(),
            'JContestTypes' => $this->string(),
            'JContestCategories' => $this->string(),
            'JContests' => $this->string(),
            'JTeams' => $this->string(),
            'stages' => $this->integer(),
            'with_gram' => $this->boolean(),
            'wiht_info' => $this->boolean(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('honor_generators');
    }
}
