<?php

use yii\db\Migration;

/**
 * Handles dropping museum_type from table `coefficients`.
 */
class m170324_175447_drop_museum_type_column_from_coefficients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('coefficients', 'museum_type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('coefficients', 'museum_type', $this->integer());
    }
}
