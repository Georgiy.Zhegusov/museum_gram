<?php

use yii\db\Migration;

/**
 * Handles the creation of table `downloads`.
 */
class m170303_081752_create_downloads_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('downloads', [
            'id' => $this->primaryKey(),
            'path' => $this->string(128),
            'status' => $this->integer(),
            'stringCount' => $this->integer(),
            'generatedCount' => $this->integer(),
            'type' => $this->string(5),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('downloads');
    }
}
