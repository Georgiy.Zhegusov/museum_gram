<?php

use yii\db\Migration;

class m170419_080438_add_column_winnerValue_to_nominations_table extends Migration
{
    public function up()
    {
        $this->addColumn('nominations_age_category', 'winnerValue', $this->float());
        $this->addColumn('nominations_age_category', 'priseValue', $this->float());
    }

    public function down()
    {
        $this->dropColumn('nominations_age_category', 'winnerValue');
        $this->dropColumn('nominations_age_category', 'priseValue');
    }
}
