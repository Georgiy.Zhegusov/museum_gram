<?php

/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 06.03.17
 * Time: 20:54
 */

namespace app\generator;

use app\models\HonorsPeople;
use Imagick;
use Yii;
use yii\base\View;
use yii\helpers\ArrayHelper;

class TexGenerator
{
    /**
     * generate pdf file for honorsPeople
     *
     * @param HonorsPeople $honorsPeople
     *
     * @return string
     * @throws \Exception
     */
    public function generate(HonorsPeople $honorsPeople)
    {
        $schoolText = null;
        $classText  = null;
        $personText = null;
        $headText   = null;

        if (empty($honorsPeople->people)) {
            $headText = 'награждается команда ' . $honorsPeople->honor->teamInfo->name;
        } else {
            if ($honorsPeople->people->isShowRoleAlways) {
                $headText = 'награждается ' . $honorsPeople->people->roleName . ' команды' . $honorsPeople->honor->teamInfo->name;
            } else {
                $headText = 'награждается';
            }

            $schoolText = empty($honorsPeople->people->schoolGramText)
                ? $honorsPeople->honor->teamInfo->schoolText
                : $honorsPeople->people->schoolGramText;

            $classText = empty($honorsPeople->people->classNum)
                ? $honorsPeople->people->classText
                : $honorsPeople->people->classNum;

            $personText = $honorsPeople->people->surname . ' ' . $honorsPeople->people->name;
        }

        $linked = $honorsPeople->linkedHonorsPeople;

        $imagePath = $honorsPeople->honor->image_path;

        $texFile = (new View)->render('@app/generator/views/gram', [
            'textBefore1' => $this->texify($this->buildByLinked('nestedTextBefore1', $honorsPeople, $linked)),
            'textBefore2' => $this->texify($this->buildByLinked('nestedTextBefore2', $honorsPeople, $linked)),
            'textBefore3' => $this->texify($this->buildByLinked('nestedTextBefore3', $honorsPeople, $linked)),
            'textAfter1' => $this->texify($this->buildByLinked('nestedTextAfter1', $honorsPeople, $linked)),
            'textAfter2' => $this->texify($this->buildByLinked('nestedTextAfter2', $honorsPeople, $linked)),
            'textAfter3' => $this->texify($this->buildByLinked('nestedTextAfter3', $honorsPeople, $linked)),
            'personText' => $this->texify($personText),
            'headText'   => $this->texify($headText),
            'schoolText' => $this->texify($schoolText),
            'classText'  => $this->texify($classText),
            'gramNumber' => $this->texify($honorsPeople->number),
            'imageName' => (empty($linked) && !empty($imagePath)) ? Yii::getAlias('@app/' . $imagePath) : null,
            /**
             * При генерации объединенной грамоты, список забирается на основе объединяющего элемента,
             * т.е. если объединено, для категории, то на печать попадут роли, указанные в категории
             */
            'teamPeopleTexts' => $this->texify($honorsPeople->teamPeoplesText),
            'captainText' => $this->texify($honorsPeople->captainText),
            'convoyText' => $this->texify($honorsPeople->convoyText)
        ]);

        $date     = (new \DateTime())->format('Y-m-d_H-i-s');
        $fileName = $date . '_' . $honorsPeople->id;
        $filePath = Yii::getAlias("@tex/$fileName.tex");

        file_put_contents($filePath, $texFile);

        $pdfPartialFolder = Yii::getAlias('@texPdf');
        $pdfFolder        = Yii::getAlias('@app/' . $pdfPartialFolder);

        $result = $this->startRenderTeX($filePath, $pdfFolder);

        $this->unlinkSystemFiles($fileName, Yii::getAlias("@tex"), $pdfFolder);

        $resultFile = "$pdfFolder/$fileName.pdf";

        if (!file_exists($resultFile)) {
            throw new \Exception(json_encode($result), 800);
        }

        return "$pdfPartialFolder/$fileName.pdf";
    }

    private function startRenderTeX($path, $destinationPath)
    {
        exec("/usr/local/bin/xelatex -interaction=nonstopmode -output-directory=$destinationPath $path", $out);

        return $out;
    }

    private function unlinkSystemFiles($fileName, $path, $destinationPath)
    {
        if(file_exists("$path/$fileName.tex")) {
            unlink("$path/$fileName.tex");
        }

        if(file_exists("$destinationPath/$fileName.log")) {
            unlink("$destinationPath/$fileName.log");
        }

        if(file_exists("$destinationPath/$fileName.aux")) {
            unlink("$destinationPath/$fileName.aux");
        }
    }

    /**
     * get page count for pdf file
     *
     * @param $pdfPath
     *
     * @return int
     */
    public static function getPageCount($pdfPath)
    {
        $document = new Imagick($pdfPath);

        return $document->getNumberImages();
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    private function texify($string)
    {
        return str_replace('_', '\\_', $string);
    }

    private function buildByLinked($fieldName, $mainObject, $linkedObjects)
    {
        if(empty($linkedObjects)){
            return $mainObject->$fieldName;
        }

        $neededFields = array_unique(
            ArrayHelper::getColumn(
                $linkedObjects,
                $fieldName
            )
        );

        return implode(', ', $neededFields);
    }
}