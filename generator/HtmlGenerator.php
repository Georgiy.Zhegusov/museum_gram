<?php

/**
 * Created by PhpStorm.
 * User: monkey
 * Date: 06.03.17
 * Time: 20:54
 */

namespace app\generator;

use app\models\HonorsPeople;
use app\models\PeopleSchool;
use app\models\Team;
use Imagick;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\Exception;
use yii\base\View;
use yii\helpers\ArrayHelper;
use ZipArchive;

class HtmlGenerator
{
    const WITH_INFO = true;
    const WITHOUT_INFO = false;
    private $_content = '';

    private $_pageCountFrom;
    private $_pageCountTo;
    private $_filename;
    private $_bySchool;
    private $_byTeam;

    private $_baseFolder;

    private $_allCount = 0;
    private $_activeCount = 0;
    private $_partCount = 0;

    private $_firstFile = null;
    private $_lastFile = null;

    private $_activeFiles = [];

    private $_used = [];

    public function __construct($maxPageCountFrom = null, $maxPageCountTo = null, $fileName = null, $bySchool = false, $byTeam = false)
    {
        $this->_pageCountFrom = $maxPageCountFrom;
        $this->_pageCountTo = $maxPageCountTo;
        $this->_filename = $fileName;

        if(!empty($fileName)) {
            $this->_baseFolder = $fileName;
            mkdir($this->_baseFolder);
            chmod($this->_baseFolder, 0777);
        }

        $this->_bySchool = $bySchool;
        $this->_byTeam = $byTeam;
    }

    public function __destruct()
    {
        if($this->_byTeam || $this->_bySchool) {
            $path = Yii::getAlias('@app/' . $this->_baseFolder);
            $this->removeFolder($path);
        }
    }

    public function removeFolder($folder){
        $files = glob($folder . '/*');

        foreach ($files as $file) {
            if(is_dir($file)){
                $this->removeFolder($file);
            }
            else {
                unlink($file);
            }
        }

        rmdir($folder);
    }

    public function addSchool(
        PeopleSchool $peopleSchool=null,
        $withInfo=self::WITH_INFO,
        $stage,
        $withGram,
        $contests,
        $contestTypes,
        $teamsFilter,
        $newFileCallBack = null,
        $addProceedCallBack = null)
    {
        $count = 0;

        /** @var Team[] $teams */
        $teams = Team::find()
            ->andWhere(['school_id' => empty($peopleSchool) ? null : $peopleSchool->id])
            ->orderBy(['classNum' => SORT_ASC])
            ->all();

        echo "\r" . empty($peopleSchool) ? null : $peopleSchool->esr . ': ' . count($teams) . " teams\n";

        $schoolStart = true;
        $lastClassNum = -1;

        foreach ($teams as $id => $team){
            $count++;

            if($count >= 20 && is_callable($addProceedCallBack)){
                $addProceedCallBack($count);
                $count = 0;
            }

            if(!empty($teamsFilter) && !in_array($team->number, $teamsFilter)){
                continue;
            }

            echo "\r" . $id;

            $this->addTeam($team, $lastClassNum, $schoolStart, $withInfo, $stage, $withGram, $contests, $contestTypes);

            if($this->_byTeam){
                //Если требуется разбиаение по командам, то генерим файл для каждой команды
                if($this->_bySchool){
                    //Если требуется разбиение по командам и школам, то для каждой школы создаем отдельную папку куда и кладем файлы команды
                    $baseFolder = $this->_baseFolder . '/' . 'School_' . (empty($peopleSchool) ? null : $peopleSchool->esr);
                    $baseFile = $baseFolder . '/' . $team->number . '.pdf';
                    $relativeFile =  'School_' . (empty($peopleSchool) ? null : $peopleSchool->esr) . '/' . $team->number . '.pdf';

                    if(!file_exists($baseFolder)){
                        mkdir($baseFolder);
                        chmod($baseFolder, 0777);
                    }

                    if($this->_activeCount == 0){
                        $this->_firstFile = (empty($peopleSchool) ? null : $peopleSchool->esr);
                    }
                    $this->_lastFile = (empty($peopleSchool) ? null : $peopleSchool->esr);
                }
                else{
                    //Если требуется разбиение только по командам, то для каждой команды просто создается оотдельный файл
                    $baseFile = $this->_baseFolder . '/' . $team->number . '.pdf';
                    $relativeFile =  $team->number . '.pdf';
                    if($this->_activeCount == 0){
                        $this->_firstFile =  $team->number;
                    }
                    $this->_lastFile = $team->number;
                }

                $this->render(null, $baseFile, true);

                if(file_exists($baseFile)) {
                    $fileSize = filesize($baseFile) / (1024 * 1024);

                    $this->_allCount += $fileSize;
                    $this->_activeCount += $fileSize;

                    $this->_activeFiles[$relativeFile] = $baseFile;
                }

                if(!$this->_bySchool && $this->_activeCount >= $this->_pageCountFrom){
                    $this->renderZip($newFileCallBack);
                    $this->_activeCount = 0;
                    $this->_activeFiles = [];
                    $this->_partCount++;
                }
            }
            elseif(!$this->_bySchool) {
                //Если пришла пора и не требуется разбиение по школам, то генерим файл
                if ($this->_activeCount >= $this->_pageCountFrom && !$this->_bySchool) {
                    $this->render($newFileCallBack);
                    $this->_activeCount = 0;
                    $this->_partCount++;
                }
            }
        }

        if($count > 0 && is_callable($addProceedCallBack)){
            $addProceedCallBack($count);
        }

        if($this->_bySchool && !$this->_byTeam){
            $baseFile = $this->_baseFolder . '/' . 'School_' . $peopleSchool->esr . '.pdf';
            $relativeFile =  'School_' . $peopleSchool->esr . '.pdf';

            if($this->_activeCount == 0){
                $this->_firstFile = $peopleSchool->number;
            }

            $this->render(null, $baseFile, true);

            if(file_exists($baseFile)) {
                $fileSize = filesize($baseFile) / (1024 * 1024);

                $this->_allCount += $fileSize;
                $this->_activeCount += $fileSize;

                $this->_activeFiles[$relativeFile] = $baseFile;
            }
        }

        if($this->_bySchool && $this->_activeCount >= $this->_pageCountFrom){
            $this->renderZip($newFileCallBack);
            $this->_activeCount = 0;
            $this->_partCount++;
            $this->_activeFiles = [];
        }
    }

    public function addTeam(
        Team $team,
        &$lastClassNum,
        &$schoolStart,
        $withInfo=self::WITH_INFO,
        $stage,
        $withGram,
        $contests,
        $contestTypes)
    {
        $firstHonor = true;

        foreach ($team->honors as $honor) {
            if((empty($stage) || $honor->contest->stage==$stage)
                && (empty($contests) || in_array($honor->contest_id, $contests))
                && (empty($contestTypes) || in_array($honor->contest->contest_type_id, $contestTypes))){
                foreach ($honor->honorsPeoples as $honorsPeople) {
                    if ($firstHonor && $withInfo) {
                        $this->addInfo($team, $lastClassNum != $team->classNum, $schoolStart);
                        $lastClassNum = $team->classNum;
                        $schoolStart  = false;
                        $firstHonor   = false;
                        $this->_activeCount++;
                        $this->_allCount++;
                    }

                    if($withGram) {
                        if ($this->addGram($honorsPeople)) {
                            $this->_activeCount++;
                            $this->_allCount++;
                        }
                    }
                }
            }
        }
    }

    public function addGram(HonorsPeople $honorsPeople)
    {
        if(isset($this->_used[$honorsPeople->id])){
            return false;
        }

        $this->_content .= $this->renderOne($honorsPeople);

        return true;
    }

    public function addInfo(Team $team, $withClass=true, $withSchool=true)
    {
        $schoolText = null;
        $classText  = null;
        $captainText = null;
        $schoolESR = null;
        $tommyText = [];

       // Для каждой команды школа, класс, капитан и список грамот
        $teamNumber = $team->number;

        $teamInfo = $team->getTeamInfo(1);

        if(empty($teamInfo)){
            $teamInfo = $team->getTeamInfo(2);
        }

        if(empty($teamInfo)){
            return;
        }

        $teamName = $teamInfo->name;

        $classText = $team->classNum;

        $school = $team->school;

        if(!empty($school)){
            $schoolESR = $school->esr;
            $schoolText = $school->text;
        }

        $captain = $teamInfo->captain;

        if(!empty($captain)) {
            $captainText = $captain->name . ' ' . $captain->surname;
        }

        foreach ($teamInfo->tommies as $tommy) {
            $tommyText[] = $tommy->name . ' ' . $tommy->surname;
        }

        $byType = [];

        $gramCount = 0;

        $usedHonors = [];

        foreach ($team->honors as $honor) {
            if(isset($usedHonors[$honor->id])){
                continue;
            }

            $type = $honor->contest->generationType->name;

            if(!isset($byType[$type])){
                $byType[$type] = [
                    'contests' => [],
                    'count' => 0
                ];
            }

            $linkedHonors = $honor->linkedHonors;

            $names = [];

            foreach ($linkedHonors as $linkedHonor) {
                $usedHonors[$linkedHonor->id] = true;
                $names[] = $linkedHonor->contest->name;
                $byType[$type]['count'] ++;
            }

            $names = implode(', ', $names);

            $byType[$type]['contests'][$names] = count($honor->honorsPeoples);

            $gramCount += $byType[$type]['contests'][$names];
        }

        $this->_content .= Yii::$app->view->render('@app/generator/views/infoHtml', [
            'mainSchoolText' => $schoolText,
            'mainSchoolEsr' => $schoolESR,
            'withSchool' => $withSchool,
            'withClass' => $withClass,
            'mainClassText' =>  $classText,
            'teamNumber' => $teamNumber,
            'teamName' => $teamName,
            'schoolText' => $schoolText,
            'schoolEsr' => $schoolESR,
            'classText' => $classText,
            'captainText' => $captainText,
            'tommiesText' => $tommyText,
            'contestsByType' => $byType,
            'gramCount' => $gramCount
        ]);
    }

    /**
     *
     * @param null $newFileCallBack
     * @param null $fileName
     *
     * @return bool|mixed
     */
    public function render($newFileCallBack = null, $fileName = null, $justFile = false)
    {
        if(!$justFile && ($this->_bySchool || $this->_byTeam)){
            $this->renderZip($newFileCallBack);
            $this->_activeCount = 0;
            $this->_partCount++;
            $this->_activeFiles = [];
            return true;
        }

        $content = $this->_content;
        if(empty($content)){
            return null;
        }

        $pdf = new Pdf([
            // set to use core fonts only
            'mode'        => Pdf::MODE_UTF8,
            // A4 paper format
            'format'      => Pdf::FORMAT_FOLIO,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_STRING,
            // your html content input
            'content'     => $this->_content,
            'cssFile' => __DIR__ . '/views/css/gramCss.css',
            'marginTop' => 6,
            'marginBottom' => 6,
            'marginLeft' => 6,
            'marginRight' => 6
        ]);

        $this->_content = '';
        $this->_used = [];

        $filePath = $this->_filename;

        if(empty($filePath)){
            return $pdf->render();
        }

        if(empty($fileName)) {
            $filePath = $filePath . '_' . $this->_partCount . '.pdf';
        }
        else{
            $filePath = $fileName;
        }

        $path = Yii::getAlias('@app/' . $filePath);

        file_put_contents($path, $pdf->render());
        chmod($path, 0777);

        if(!$justFile && is_callable($newFileCallBack)){
            $pageStart  = $this->_allCount - $this->_activeCount;
            $pageFinish = $this->_allCount;
            $newFileCallBack($filePath, $pageStart, $pageFinish);
        }

        return true;
    }

    public function renderZip($newFileCallBack)
    {
        $activeFiles = $this->_activeFiles;
        if(empty($activeFiles)){
            return true;
        }

        $filePath = $this->_filename;

        if(empty($filePath)){
            throw new Exception('Cant create zip without filename');
        }
        $zipName = $filePath . '_' . $this->_partCount . '.zip';

        if( file_exists( $zipName ) ){
            unlink( $zipName );
        }

        $zip = new ZipArchive();

        if ($zip->open( $zipName, ZipArchive::CREATE)!==TRUE) {
            echo "Невозможно открыть <$zipName>\n";
            exit("Невозможно открыть <$zipName>\n");
        }

        foreach ($this->_activeFiles as $relativePath => $activeFile) {
            $zip->addFile( $activeFile, $relativePath );
        }

        $zip->close();

        chmod($zipName, 0777);

        if (is_callable($newFileCallBack)) {
            $newFileCallBack($zipName, $this->_firstFile, $this->_lastFile);
        }

        return true;
    }

    private function renderOne(HonorsPeople $honorsPeople)
    {
        $schoolText = null;
        $classText  = null;
        $personText = null;
        $headText   = null;

        if(!$honorsPeople->honor->contest->generationType->textsOnly) {
            if (empty($honorsPeople->people)) {
                $headText = 'награждается команда ' . $honorsPeople->honor->teamInfo->name;
            } else {
                if ($honorsPeople->people->isShowRoleAlways) {
                    $headText = 'награждается ' . $honorsPeople->people->roleName . ' команды' . $honorsPeople->honor->teamInfo->name;
                } else {
                    $headText = 'награждается';
                }

                $savedSchoolGramText = $honorsPeople->people->schoolGramText;

                $schoolText = empty($savedSchoolGramText)
                    ? $honorsPeople->honor->teamInfo->schoolText
                    : $honorsPeople->people->schoolGramText;

                $savedClassNum = $honorsPeople->people->classNum;

                $classText = (empty($savedClassNum)
                    || $honorsPeople->people->classText == 'дошкольник'
                    || $honorsPeople->people->classText == 'Дошкольник')
                    ? $honorsPeople->people->classText
                    : $honorsPeople->people->classNum . ' класс';

                $personText = $honorsPeople->people->surname . ' ' . $honorsPeople->people->name;
            }
        }

        $linked = $honorsPeople->linkedHonorsPeople;

        $this->_used[$honorsPeople->id] = true;

        if(!empty($linked)) {
            foreach ($linked as $item) {
                $this->_used[$item->id] = true;
            }
        }

        $imagePath = Yii::getAlias('@app/' . $honorsPeople->honor->image_path);

        $localPath = $honorsPeople->honor->image_path;

        if(file_exists($imagePath) && !empty($localPath)){
            list($width, $height, $type, $attr) = getimagesize($imagePath);
            $ratio = $width > 0 ? $height / $width : 0;
        }
        else{
            $imagePath = null;
            $ratio = 0;
        }

        $esr = $honorsPeople->honor->teamInfo->team->number;
        $school = $honorsPeople->honor->teamInfo->team->school;

        $schoolESR = null;

        if(!empty($school)){
            $schoolESR = $school->number;
        }

        $substrate = Yii::getAlias('@app/' . $honorsPeople->honor->contest->nestedSubstrate);

        $teamPeoplesText = $honorsPeople->teamPeoplesText;

        $captainText = empty($teamPeoplesText) ? null : $honorsPeople->captainText;

        return Yii::$app->view->render('@app/generator/views/gramHtml', [
            'textBefore1' => $this->buildByLinked('nestedTextBefore1', $honorsPeople, $linked),
            'textBefore2' => $this->buildByLinked('nestedTextBefore2', $honorsPeople, $linked),
            'textBefore3' => $this->buildByLinked('nestedTextBefore3', $honorsPeople, $linked),
            'textAfter1' => $this->buildByLinked('nestedTextAfter1', $honorsPeople, $linked),
            'textAfter2' => $this->buildByLinked('nestedTextAfter2', $honorsPeople, $linked),
            'textAfter3' => $this->buildByLinked('nestedTextAfter3', $honorsPeople, $linked),
            'personText' => $personText,
            'headText'   => $headText,
            'schoolText' => $schoolText,
            'classText'  => $classText,
            'gramNumber' => $honorsPeople->number,
            'imageName' => !empty($imagePath) ? $imagePath : null,
            //'imageName' => (empty($linked) && !empty($imagePath)) ? Yii::getAlias('@app/' . $imagePath) : null,
            'ratio' => $ratio,
            /**
             * При генерации объединенной грамоты, список забирается на основе объединяющего элемента,
             * т.е. если объединено, для категории, то на печать попадут роли, указанные в категории
             */
            'teamPeopleTexts' => $teamPeoplesText,
            'captainText' => $captainText,
            'convoyText' => $honorsPeople->convoyText,
            'substrate' => $substrate,
            'type' => intval($honorsPeople->honor->contest->substrate_type),
            'schoolEsr' => $schoolESR,
            'esr' => $esr
        ]);
    }

    /**
     * generate pdf file for honorsPeople
     *
     * @param HonorsPeople $honorsPeople
     *
     * @return string
     * @throws \Exception
     */
    public function generate(HonorsPeople $honorsPeople)
    {
        $schoolText = null;
        $classText  = null;
        $personText = null;
        $headText   = null;

        if(!$honorsPeople->honor->contest->generationType->textsOnly) {
            if (empty($honorsPeople->people)) {
                $headText = 'награждается команда ' . $honorsPeople->honor->teamInfo->name;
            } else {
                if ($honorsPeople->people->isShowRoleAlways) {
                    $headText = 'награждается ' . $honorsPeople->people->roleName . ' команды' . $honorsPeople->honor->teamInfo->name;
                } else {
                    $headText = 'награждается';
                }

                $schoolGramText = $honorsPeople->people->schoolGramText;
                $schoolText     = empty($schoolGramText)
                    ? $honorsPeople->honor->teamInfo->schoolText
                    : $honorsPeople->people->schoolGramText;

                $classNum  = $honorsPeople->people->classNum;
                $classText = empty($classNum)
                    ? $honorsPeople->people->classText
                    : $honorsPeople->people->classNum . 'класс';

                $personText = $honorsPeople->people->surname . ' ' . $honorsPeople->people->name;
            }

            $teamPeoplesText = $honorsPeople->teamPeoplesText;
        }

        $imagePath = Yii::getAlias('@app/' . $honorsPeople->honor->image_path);

        $localPath = $honorsPeople->honor->image_path;

        if(file_exists($imagePath) && !empty($localPath)){
            list($width, $height, $type, $attr) = getimagesize($imagePath);
            $ratio = $width > 0 ? $height / $width : 0;
        }
        else{
            $imagePath = null;
            $ratio = 0;
        }

        $linked = $honorsPeople->linkedHonorsPeople;

        $substrate = Yii::getAlias('@app/' . $honorsPeople->honor->contest->nestedSubstrate);

        $content = Yii::$app->view->render('@app/generator/views/gramHtml', [
            'textBefore1' => $this->buildByLinked('nestedTextBefore1', $honorsPeople, $linked),
            'textBefore2' => $this->buildByLinked('nestedTextBefore2', $honorsPeople, $linked),
            'textBefore3' => $this->buildByLinked('nestedTextBefore3', $honorsPeople, $linked),
            'textAfter1' => $this->buildByLinked('nestedTextAfter1', $honorsPeople, $linked),
            'textAfter2' => $this->buildByLinked('nestedTextAfter2', $honorsPeople, $linked),
            'textAfter3' => $this->buildByLinked('nestedTextAfter3', $honorsPeople, $linked),
            'personText' => $personText,
            'headText'   => $headText,
            'schoolText' => $schoolText,
            'classText'  => $classText,
            'gramNumber' => $honorsPeople->number,
            'imageName' => !empty($imagePath) ? $imagePath : null,
            //'imageName' => (empty($linked) && !empty($imagePath)) ? Yii::getAlias('@app/' . $imagePath) : null,
            'ratio' => $ratio,
            /**
             * При генерации объединенной грамоты, список забирается на основе объединяющего элемента,
             * т.е. если объединено, для категории, то на печать попадут роли, указанные в категории
             */
            'teamPeopleTexts' => $teamPeoplesText,
            'captainText' => empty($teamPeoplesText) ? null : $honorsPeople->captainText,
            'convoyText' => $honorsPeople->convoyText,
            'substrate' => $substrate,
            'type' => 1
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode'        => Pdf::MODE_UTF8,
            // A4 paper format
            'format'      => Pdf::FORMAT_FOLIO,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_STRING,
            // your html content input
            'content'     => $content,
            'cssFile' => __DIR__ . '/views/css/gramCss.css',
            'marginTop' => 6,
            'marginBottom' => 6,
            'marginLeft' => 6,
            'marginRight' => 6
        ]);

        return $pdf->render();
    }

    /**
     * get page count for pdf file
     *
     * @param $pdfPath
     *
     * @return int
     */
    public static function getPageCount($pdfPath)
    {
        $document = new Imagick($pdfPath);

        return $document->getNumberImages();
    }

    private function buildByLinked($fieldName, $mainObject, $linkedObjects)
    {
        if(empty($linkedObjects)){
            return $mainObject->$fieldName;
        }

        $neededFields = [];

        foreach ($linkedObjects as $object) {
            $neededValue = $object->$fieldName;
            if(!empty($neededValue) && !in_array($neededValue ,$neededFields)){
                $neededFields[] = $neededValue;
            }
        }

        return implode(', ', $neededFields);
    }
}