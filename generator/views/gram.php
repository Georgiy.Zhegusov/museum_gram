<?php
/**
 * @var  string $textBefore1
 * @var  string $textBefore2
 * @var  string $textBefore3
 * @var  string $textAfter1
 * @var  string $textAfter2
 * @var  string $textAfter3
 * @var  string $headText
 * @var  string $personText
 * @var  string $schoolText
 * @var  string $classText
 * @var  string $gramNumber
 * @var  string $imageName
 * @var  array  $teamPeopleTexts
 * @var  string $captainText
 * @var  string $convoyText
 * @var  string $schoolEsr
 * @var  string $esr
 */

if(mb_strlen($textBefore2)<=500 ){
    $textBefore2Size = '\Large ';
}
elseif( mb_strlen($textBefore2)<=850 ){
    $textBefore2Size = '\large ';
}
else{
    $textBefore2Size = '\normalsize ';
}

if(is_array($teamPeopleTexts)) {
    $teamPeopleTexts = array_map(
        function ($value) {
            return str_replace(' ', '~', $value);
        },
        $teamPeopleTexts
    );
}
else{
    if(empty($teamPeopleTexts)){
        $teamPeopleTexts = [];
    }
    else{
        $teamPeopleTexts = [$teamPeopleTexts];
    }
}

if(is_array($teamPeopleTexts)) {
    if (count($teamPeopleTexts) >= 8) {
        $teamSize = '\large';
    } else {
        $teamSize = '';
    }
}
else{
    $teamSize = null;
}
?>

\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}
\usepackage{amssymb,amsmath}

\ifx\pdfoutput\undefined
    \usepackage{graphicx}
\else
    \usepackage[pdftex]{graphicx}
\fi

\usepackage{grffile}
\usepackage{wrapfig}
\usepackage{eso-pic}
\usepackage{geometry} \geometry{verbose,a4paper,tmargin=1cm,bmargin=0.5cm,lmargin=2cm,rmargin=2cm}
\usepackage{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}

\setmainfont[
  Ligatures=TeX,
  Extension=.otf,
  BoldFont=cmunbx,
  ItalicFont=cmunti, % cmunbmo%si
  BoldItalicFont=cmunbi,
]{cmunrm}

\usepackage{xltxtra}
\usepackage{polyglossia}
\setdefaultlanguage{russian}
\setotherlanguage{english}

\pagestyle{empty}
\begin{document}
\footskip0pt
\noindent

\vspace*{20mm}
\begin{center}

<?php if(!empty($textBefore1)):?>
    <?=$textBefore1?> \\ \medskip
<?php endif;?>

<?php if(!empty($textBefore2)):?>
    {<?=$textBefore2Size?> <?=$textBefore2?> } \\ \medskip
<?php endif;?>

\Large

<?php if(!empty($textBefore3)):?>
    <?=$textBefore3?> \\ \medskip
<?php endif;?>

<?php if(!empty($headText)):?>
    <?=$headText?> \\ \medskip
<?php endif;?>

<?php if(!empty($personText)):?>
    {\bf <?=$personText?>}\\ \medskip
<?php endif;?>

<?php if(!empty($schoolText)):?>
    <?=$schoolText?> \\ \medskip
<?php endif;?>

<?php if(!empty($classText)):?>
    <?=$classText?> \\ \medskip
<?php endif;?>

<?php if(!empty($textAfter1)):?>
    <?=$textAfter1?> \\ \medskip
<?php endif;?>

<?php if(!empty($textAfter2)):?>
    <?=$textAfter2?> \\ \medskip
<?php endif;?>

<?php if(!empty($textAfter3)):?>
    <?=$textAfter3?> \\ \medskip
<?php endif;?>

<?php if(!empty($captainText)):?>
    Капитан команды: <?= $captainText?> \\ \medskip
<?php endif;?>

<?php if(!empty($teamPeopleTexts)):?>
    {<?=$teamSize?> состав команды: <?=implode(', ',$teamPeopleTexts)?> } \\ \medskip
<?php endif;?>

<?php if(!empty($convoyText)):?>
    Cопровождающий: <?=$convoyText?> \\ \medskip
<?php endif;?>

\end{center}

\vspace*{\fill}
{\normalsize
    \hspace{5mm}
    \vbox{Зам. председателя оргкомитета\\\\
        \hbox to 145mm{Директор ГАОУ ДПО ЦПМ
            % \begin{minipage}[b][0cm][l]{0cm}
            %\vspace{-2.5cm}
            %\includegraphics[width=5cm,height=5cm,keepaspectratio]{<?=Yii::getAlias('@texImg/P1')?>}
            % \end{minipage}
            \hfill
            И.\,В.\,Ященко
        }
    }
} \\ \vskip-2em

\begin{center}
\hskip20mm
\includegraphics[height=7cm,width=9cm,keepaspectratio]{<?=empty($imageName) ? Yii::getAlias('@texImg/blank') : $imageName ?>}
\end{center}

\vskip40mm
\rightline{ Грамота №\,<?=$schoolEsr?>_<?=$esr?>_<?=$gramNumber?>\quad}

\end{document}