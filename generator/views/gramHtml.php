<?php
/**
 * @var  string $substrate
 * @var  string $textBefore1
 * @var  string $textBefore2
 * @var  string $textBefore3
 * @var  string $textAfter1
 * @var  string $textAfter2
 * @var  string $textAfter3
 * @var  string $headText
 * @var  string $personText
 * @var  string $schoolText
 * @var  string $classText
 * @var  string $gramNumber
 * @var  string $imageName
 * @var  array  $teamPeopleTexts
 * @var  string $captainText
 * @var  string $convoyText
 * @var  string $ratio
 * @var  string $type
 * @var  string $schoolEsr
 * @var  string $esr
 */

if(mb_strlen($textBefore2)<=500 ){
    $textBefore2Size = 'huge';
}
elseif( mb_strlen($textBefore2)<=850 ){
    $textBefore2Size = 'large';
}
else{
    $textBefore2Size = 'normal';
}

if(is_array($teamPeopleTexts)) {
    if (count($teamPeopleTexts) >= 9) {
        $teamSize = 'large';
    } else {
        $teamSize = 'huge';
    }

    foreach ($teamPeopleTexts as &$teamPeopleText) {
        $teamPeopleText = str_replace(' ', '&nbsp;', $teamPeopleText);
    }
    unset($teamPeopleText);
}
else{
    $teamSize = null;
}

$textBefore1 = trim($textBefore1);
$textBefore2 = trim($textBefore2);
$textBefore3 = trim($textBefore3);
$textAfter1 = trim($textAfter1);
$textAfter2 = trim($textAfter2);
$textAfter3 = trim($textAfter3);
?>

<div class="body type<?=$type?>" style="background-image: url(<?=$substrate?>);">
    <div class="header"></div>

    <div class="wrapper">
        <?=!empty($textBefore1) ? $textBefore1 . '<br>' : ''?>
        <span class="<?=$textBefore2Size?>">
            <b><?=!empty($textBefore2) ? $textBefore2 . '<br>' : ''?></b>
        </span>

        <span class="huge">
            <?=!empty($textBefore3) ? $textBefore3 . '<br>' : ''?>
            <?=$headText ? $headText . '<br>' : ''?>
            <b>
                <?=$personText ? $personText . '<br>' : ''?>
            </b>
            <?=$schoolText ? $schoolText . '<br>' : ''?>
            <?=$classText ? $classText . '<br>' : ''?>
            <?=!empty($textAfter1) ? $textAfter1 . '<br>' : ''?>
            <?=!empty($textAfter2) ? $textAfter2 . '<br>' : ''?>
            <?=!empty($textAfter3) ? $textAfter3 . '<br>' : ''?>
            <?=$captainText ? 'Капитан команды: ' . $captainText . '<br>' : ''?>

            <span class="<?=$teamSize?>">
                <?=$teamPeopleTexts ? 'состав команды: ' . implode(', ',$teamPeopleTexts) . '<br>' : ''?>
            </span>

            <?=$convoyText ? 'Cопровождающий: ' . $convoyText . '<br>' : ''?>
        </span>
    </div>

    <div class="footer">

        <table class="normal">
            <col width="20%">
            <col width="20%">
            <col width="auto">
            <col width="20%">
            <tr>
                <td>

                </td>
                <td rowspan=3>
                    <img class="sign" src="<?=__DIR__?>/images/P1.png">
                </td>
                <td rowspan=3>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                </td>
                <td rowspan=2>
                </td>
            </tr>
            <tr>
                <td>
                    Зам. председателя оргкомитета
                </td>
            </tr>
            <tr>
                <td>
                    Директор ГАОУ ДПО ЦПМ
                </td>
                <td>
                    И.В. Ященко
                </td>
            </tr>
        </table>
    </div>

    <div class="imageBlock">
        <div class="imageWrapper <?=$ratio > 1 ? 'tall' : 'fat' ?>" style="background-image: url(<?=$imageName?>);">
        </div>
    </div>

    <div class="gramNumberFill">
    </div>

    <div class="gramNumber">
        Грамота №<?=$schoolEsr?>_<?=$esr?>_<?=$gramNumber?>
    </div>
</div>