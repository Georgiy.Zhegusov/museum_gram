<?php
/**
 * @var  string $mainSchoolText
 * @var  string $mainSchoolEsr
 * @var  string $mainClassText
 * @var  string $teamNumber
 * @var  string $teamName
 * @var  string $schoolText
 * @var  string $schoolEsr
 * @var  string $classText
 * @var  string $captainText
 * @var  string $tommiesText
 * @var  array  $contestsByType
 * @var  string  $gramCount
 * @var  string  $withSchool
 * @var  string  $withClass
 */
use yii\helpers\ArrayHelper;

?>

<div class="body large" >
    <?php if($withSchool):?>
        <b><span class="veryhuge"> <?=$mainSchoolEsr?>: </span> <span class="hugehuge"> <?=$mainSchoolText?> </span></b> <br>
        <hr/>
    <?php endif;?>
    <?php if($withClass):?>
        <b><span class="hugehuge"> <?=$mainClassText?> класс </span></b> <br>
        <hr/>
    <?php endif;?>
        <b><span class="veryhuge"> <?=$teamNumber?> - <?=$gramCount?> грамот. </span></b> <br>
    <br>
    <br>
    <br>
    <span>
        Команда -  <b><?=$teamName?></b>. <br>
        Школа -  <b><?=$schoolEsr?>: <?=$schoolText?></b>. <br>
        Класс -  <b><?=$classText?></b>. <br>
        Капитан -  <b><?=$captainText?></b>. <br>
        Состав команды - <?=implode('; ', $tommiesText)?> <br>
        Список полученных грамот - <br>
        <?php foreach ($contestsByType as $type => $info):?>
            &nbsp;&nbsp;&nbsp;&nbsp;<?=$type?> : <?=$info['count']?> наград (<?=array_sum($info['contests'])?> грамот) <br>
            <?php foreach ($info['contests'] as $contestName=>$contestCount):?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$contestName?>: <?=$contestCount?> грамот
            <?php endforeach?>
        <?php endforeach?>

    </span>
</div>
<div class="page-break"></div>